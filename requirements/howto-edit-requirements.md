To add requirement add requirement (preferably without version spec to proper ``in`` file (eg. ``base.in``) tj
    
    export LC_ALL=C.UTF-8
    export LANG=C.UTF-8

    pip-compile requirements/base.in
    pip-compile requirements/local.in
    pip-compile requirements/production.in

To upgrade requirements just do: 
    
    pip-compile --upgrade requirements/base.in
    pip-compile --upgrade requirements/local.in
    pip-compile --upgrade requirements/production.in

In both cases order is important. 

When you add new requirements here please consider copying them to https://bitbucket.org/olcms/www-base (after you merge)
as this will speed up future builds.


