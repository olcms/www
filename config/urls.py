# -*- coding: utf-8 -*-
"""Global routings."""
import pathlib

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve
from rest_framework import routers

from www.filestorage.views import StartFileUpload
from www.landing_page.views import LandingPage
from www.tags.views import TagViewset
from www.translated_page.views import PageCatchallView

from discourse_django_sso.views import SSOProviderView


API_ROUTER = routers.DefaultRouter()

API_ROUTER.register('tag/(?P<content_type_id>\d+)', TagViewset)


api_urls = [
    url(r'fileupload/?$', StartFileUpload.as_view(), name="start-file-upload")
]

api_urls += API_ROUTER.urls

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='landing-discipline'), name='home'),
    url(r'^discipline/?$', LandingPage.as_view()),
    url(r'^discipline(?:/(?P<discipline>[\w+.\-_]+))?/?$', LandingPage.as_view(), name='landing-discipline'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='javascript-catalog'),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    # User management
    url(r'^users/', include('www.users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    url('^api/', include(api_urls, namespace="api")),

    url(r'^resource/simulation', include('www.simulation.urls', namespace='simulation')),
    url(r'^resource/', include('www.resource.urls', namespace='resource')),
    url(r'^content_item/', include('www.content_item.urls', namespace='content_item')),
    url(r'^course/', include('www.course.urls', namespace='course')),
    url(
        r'^discourse/sso/?',
        SSOProviderView.as_view(
            sso_redirect=settings.DISCOURSE_SSO_URL,
            sso_secret=settings.DISCOURSE_SSO_SECRET,
        ),
        name='sso'
    ),
    url(
        r'^silf/sso/?',
        SSOProviderView.as_view(
            sso_redirect=settings.SILF_SSO_URL,
            sso_secret=settings.SILF_SSO_SECRET,
        ),
        name='silfsso'
    ),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]

    # This only in debug config as in production config we use js reverse rendered to a static file.
    from django_js_reverse.views import urls_js

    favicon_path = pathlib.Path('compose') / 'nginx' / 'www-data' / 'favicon.ico'
    root = pathlib.Path(__file__).parents[1]
    urlpatterns += [
        url(r'^django_js_reverse/?$', urls_js, name='js_reverse'),
        url(r'^favicon.ico/?$', serve, kwargs={"path": str(favicon_path), 'document_root': root})
    ]


urlpatterns += [
    url('(?P<slug>.*)/?', PageCatchallView.as_view())
]

if settings.ENABLE_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
