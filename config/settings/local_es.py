# -*- coding: utf-8 -*

"""
Local docker cluster using elasticsearch.
"""

from .local import *  # noqa

# Your production stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://elasticsearch:9200/',
        # 'URL': 'http://localhost:9200/',
        'INDEX_NAME': 'haystack',
    },
}
