# -*- coding: utf-8 -*-
"""
Local settings.

- Run in Debug mode

- Use mailhog for emails

- Add Django Debug Toolbar
- Add django-extensions as app
"""

import os
import socket

from .common import *  # noqa


ENABLE_TOOLBAR = True

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)  # noqa
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # noqa

SHOW_MISSING_PAGE_BLOCKS = True

STATIC_ROOT = '/tmp/olcms-staticfiles'

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY', default='1%_x81a_6r4dwm_j90_8a8oh3jbn^d0r7yhm2m%q=(suomlgb-')  # noqa

# Mail settings
# ------------------------------------------------------------------------------

EMAIL_PORT = 1025

EMAIL_HOST = env("EMAIL_HOST", default='mailhog')  # noqa


# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# django-debug-toolbar
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)  # noqa
INSTALLED_APPS += ('debug_toolbar', )  # noqa

INTERNAL_IPS = ['*']

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
    'SHOW_TOOLBAR_CALLBACK': lambda request: not request.is_ajax()
}


# django-extensions
# ------------------------------------------------------------------------------
INSTALLED_APPS += ('django_extensions', )

# TESTING
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# CELERY
# In development, all tasks will be executed locally by blocking until the task returns
# CELERY_ALWAYS_EAGER = True
# END CELERY

# Your local stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

ENABLE_XVBF = env.bool('ENABLE_XVBF', True)  # noqa

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': env.str('LOCAL_WHOOSH_DATA', '/whoosh-data/data')   # noqa
    },
}

ACCOUNT_EMAIL_VERIFICATION = 'none'

SELENIUM_LOGIN_START_PAGE = "/"

ALLOWED_HOSTS = ["*"]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
        'swiftclient': {
            'level': 'WARNING',
            'propagate': True
        },
        'requests.packages': {
            'level': 'WARNING',
            'propagate': True
        },
        'selenium': {
            'level': 'WARNING',
            'propagate': True
        },
        'easyprocess': {
            'level': 'ERROR',
            'propagate': True
        },
        'factory': {
            'level': "WARNING",
            'propagate': True
        },
        'pyvirtualdisplay': {
            'level': "INFO",
            'propagate': True
        },
    }
}
