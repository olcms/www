# -*- coding: utf-8 -*-
"""
Django settings for www project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

import pathlib

import environ


def ugettext_noop(message):
    return message


ENABLE_TOOLBAR = False

ROOT_DIR = environ.Path(__file__) - 3  # (www/config/settings/common.py - 3 = www/)
APPS_DIR = ROOT_DIR.path('www')

env = environ.Env()

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # NOTE: due to some weird errors in [Django][0] contenttypes must before auth
    # [0]: http://stackoverflow.com/questions/18281137/selenium-django-gives-foreign-key-error
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'bootstrap3',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin
    'django.contrib.admin',
)

THIRD_PARTY_APPS = (
    'crispy_forms',  # Form layouts
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'allauth.socialaccount.providers.google',
    # 'allauth.socialaccount.providers.facebook',
    'rest_framework',
    'rest_framework.authtoken',  # Token auth
    # Reverse urls in JS
    'django_js_reverse',
    # Indexing
    'haystack',
    # models ordering
    'ordered_model',
    # formsets
    'jquery',
    'djangoformsetjs',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'www.users.apps.UsersConfig',
    'www.resource.apps.ResourceConfig',
    'www.content_item.apps.ContentItemConfig',
    'www.tags.apps.TagsConfig',
    'www.filestorage.apps.FilestorageConfig',
    'www.page_block.apps.PageBlockConfig',
    'www.landing_page.apps.LandingPageConfig',
    'www.course',
    'www.simulation.apps.SimulationConfig',

    # cms related apps
    'feincms',
    'mptt',
    'feincms.module.medialibrary',

    'www.translated_page.apps.TranslatedPageConfig',
)

LOCALE_PATHS = [
    str(pathlib.Path(str(ROOT_DIR)) / "www" / "templates" / "locale"),
]


# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'www.contrib.sites.migrations',
    'page': 'www.contrib.page.migrations',
    'medialibrary': 'www.contrib.medialibrary.migrations',
}


# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)
USE_LOCAL_ASSETS = env.bool('DJANGO_USE_LOCAL_ASSETS', DEBUG)

SHOW_MISSING_PAGE_BLOCKS = DEBUG

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ("""Mikołaj Olszewski""", 'mikus156@gmail.com'),
    ("""Jacek Bzdak""", 'jacek@askesis.pl'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///www'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'CET'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'www.utils.context_processors.use_local_assets',
                'www.utils.context_processors.add_discourse_url',
                'www.utils.context_processors.add_google_analytics_code',
                'www.utils.context_processors.add_disciplines',
            ],
        },
    },
]

# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = 'username'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'

ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_ADAPTER = 'www.users.adapters.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'www.users.adapters.SocialAccountAdapter'
SOCIALACCOUNT_PROVIDERS = {
    'google': {
        'SCOPE': [
            'profile',
            'email',
        ],
        'AUTH_PARAMS': {
            'access_type': 'online',
        }
    }
}


# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'users:redirect'
LOGIN_URL = 'account_login'

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'

# CELERY
INSTALLED_APPS += ('www.taskapp.celery.CeleryConfig',)
BROKER_URL = env('CELERY_BROKER_URL', default='django://')
if BROKER_URL == 'django://':
    CELERY_RESULT_BACKEND = 'redis://'
else:
    CELERY_RESULT_BACKEND = BROKER_URL
# END CELERY

# django-compressor
# ------------------------------------------------------------------------------
# INSTALLED_APPS += ("compressor", )
# STATICFILES_FINDERS += ("compressor.finders.CompressorFinder", )

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# languages
# ------------------------------------------------------------------------------

LANGUAGES = (
    ('en', ugettext_noop('English')),
    ('pl', ugettext_noop('Polish')),
    ('el', ugettext_noop('Greek')),
    ('cs', ugettext_noop('Czech')),
    ('ca', ugettext_noop('Catalan')),
    ('es', ugettext_noop('Spanish')),
    ('sl', ugettext_noop('Slovene')),
    ('it', ugettext_noop('Italian'))
)

FEINCMS_FRONTEND_LANGUAGES = LANGUAGES

# Your common stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

FEINCMS_RICHTEXT_INIT_CONTEXT = {
    'TINYMCE_JS_URL': '//cdn.tinymce.com/4.1/tinymce.min.js',
}

DEFAULT_FILE_STORAGE = env.str('DEFAULT_FILE_STORAGE', 'swift.storage.SwiftStorage')

if DEFAULT_FILE_STORAGE == 'swift.storage.SwiftStorage':
    SWIFT_LAZY_CONNECT = True
    SWIFT_AUTH_VERSION = env.int('SWIFT_AUTH_VERSION', 1)
    SWIFT_AUTH_URL = env.str('SWIFT_AUTH_URL')
    SWIFT_USERNAME = env.str('SWIFT_USERNAME')
    SWIFT_KEY = env.str('SWIFT_KEY')
    # This is required by django-swift-storage, but unused for our provider,
    # so empty string should suffice, I hope.
    SWIFT_TENANT_ID = env.str('SWIFT_TENANT_ID', '')
    SWIFT_CONTAINER_NAME = env.str('SWIFT_CONTAINER_NAME')
    SWIFT_AUTO_CREATE_CONTAINER = env.bool('SWIFT_AUTO_CREATE_CONTAINER', False)
    SWIFT_USE_TEMP_URLS = env.bool('SWIFT_USE_TEMP_URLS', True)
    SWIFT_CONTENT_TYPE_FROM_FD = env.str('SWIFT_CONTENT_TYPE_FROM_FD', False)

    SWIFT_TEMP_URL_KEY = env.str('SWIFT_TEMP_URL_KEY')
    # NOTE: This in minutes
    SWIFT_TEMP_URL_DURATION = env.str('SWIFT_TEMP_URL_DURATION')


SEARCH_RESULT_PAGE_SIZE = 25


JS_REVERSE_OUTPUT_PATH = "/app/www/static/js/"


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': SEARCH_RESULT_PAGE_SIZE,
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',)
}

BOOTSTRAP3 = {
    # Class to indicate error (better to set this in your Django form)
    'error_css_class': 'has-danger',
    'field_renderers': {
        'default': 'www.utils.bootstrap.renderer.FieldRendererWithErrorFormatting'
    }
}


ENABLE_DISCOURSE_FORUM_TRIGGERS = env.str('ENABLE_DISCOURSE_FORUM_TRIGGERS', False)
ENABLE_YOUTUBE_VIDEOS = env.str('ENABLE_YOUTUBE_VIDEOS', False)


DISCOURSE_SSO_SECRET = env.str('COMPOSE_DISCOURSE_SSO_SECRET')
DISCOURSE_HOST = env.str('DISCOURSE_HOSTNAME')
DISCOURSE_SSO_URL = env.str('DJANGO_DISCOURSE_SSO_URL')
DISCOURSE_ROOT_URL = env.str('DJANGO_DISCOURSE_ROOT_URL', None)

SILF_SSO_URL = env.str('COMPOSE_SILF_SSO_URL')
SILF_SSO_SECRET = env.str('COMPOSE_SILF_SSO_SECRET')

YOUTUBE = {
    'playlist_id': env.str('YT_PLAYLIST_ID')
}

GOOGLE_ANALYTICS_TRACKING_CODE = env.str("GOOGLE_ANALYTICS_TRACKING_CODE", None)

SWIFT_SIMULATION_CONTAINER = env.str('SWIFT_SIMULATION_CONTAINER')

SIMULATION_SERVICE = 'www.simulation.services.swift_service.SwiftSimulationService'

COMPETITION_SETTINGS = {
    "TERMS_AND_CONDITIONS_URL": '/voting/terms/',
    "VOTE_REGISTERED_URL": '/voting/registered/',
    "VOTE_SUCCESS_URL": '/voting/done/',
}
