# -*- coding: utf-8 -*-
"""
Test settings.

- Used to run tests fast on the continuous integration server and locally
"""


import tempfile

from .local import *  # noqa

# DEBUG
# ------------------------------------------------------------------------------
# Turn debug off so tests run faster
DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = False  # noqa

DJANGO_USE_LOCAL_ASSETS = True

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY', default='CHANGEME!!!')  # noqa

# Mail settings
# ------------------------------------------------------------------------------
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# In-memory email backend stores messages in django.core.mail.outbox
# for unit testing purposes
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

# CACHING
# ------------------------------------------------------------------------------
# Speed advantages of in-memory caching without having to run Memcached
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# TESTING
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

INSTALLED_APPS = [
    app
    for app in INSTALLED_APPS  # noqa
    if app != 'debug_toolbar'
]

# Disable toolbar in tests altogether
MIDDLEWARE_CLASSES = [
    middleware
    for middleware in MIDDLEWARE_CLASSES  # noqa
    if middleware != 'debug_toolbar.middleware.DebugToolbarMiddleware'
]

# PASSWORD HASHING
# ------------------------------------------------------------------------------
# Use fast password hasher so tests run faster
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

# TEMPLATE LOADERS
# ------------------------------------------------------------------------------
# Keep templates in memory so tests run faster
TEMPLATES[0]['OPTIONS']['loaders'] = [  # noqa
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    ]),
]

ENABLE_XVBF = env.bool('ENABLE_XVBF', True)  # noqa

MIGRATION_MODULES = {
    app.split(".")[0]: None
    for app in INSTALLED_APPS  # noqa
}

# Disable django toolbar
INTERNAL_IPS = []

WHOOSH_INDEX_DIR = tempfile.mkdtemp()

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': env.str('LOCAL_WHOOSH_DATA', WHOOSH_INDEX_DIR)   # noqa
    },
}
