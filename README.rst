Open Learning Content Management System
=======================================



.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django

.. image:: https://img.shields.io/docker/stars/olcms/www.svg
     :target: https://hub.docker.com/r/olcms/www/
     :alt: Docker Stars

.. image:: https://img.shields.io/docker/pulls/olcms/www.svg
     :target: https://hub.docker.com/r/olcms/www/
     :alt: Docker Pulls


+---------+--------------------------------------------------------------------------------+------------------------------------------------------------------------------+
| Branch  | Build                                                                          | Size                                                                         |
+=========+================================================================================+==============================================================================+
| master  | .. image:: https://semaphoreci.com/api/v1/olcms/www/branches/master/badge.svg  | .. image:: https://images.microbadger.com/badges/image/olcms/www.svg         |
|         |      :target: https://semaphoreci.com/olcms/www                                |      :target: https://microbadger.com/images/olcms/www                       |
|         |      :alt: Build Status                                                        |      :alt: Docker image size                                                 |
+---------+--------------------------------------------------------------------------------+------------------------------------------------------------------------------+
| develop | .. image:: https://semaphoreci.com/api/v1/olcms/www/branches/develop/badge.svg | .. image:: https://images.microbadger.com/badges/image/olcms/www:preprod.svg |
|         |      :target: https://semaphoreci.com/olcms/www                                |      :target: https://microbadger.com/images/olcms/www:preprod               |
|         |      :alt: Build Status                                                        |      :alt: Docker image size                                                 |
+---------+--------------------------------------------------------------------------------+------------------------------------------------------------------------------+


:License: Apache Software License 2.0


Open Learning Content Management system is a open source repository for
educational materials.

This project has received funding from the European Union Horizon 2020 Research
and Innovation programme `under grant agreement no. 710577.
<https://cordis.europa.eu/project/rcn/203170_en.html>`__.

Documentation
-------------

Documentation is hosted on `readthedocs.org <https://olcms.readthedocs.io/en/latest/index.html>`__.




