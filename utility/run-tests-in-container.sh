#!/usr/bin/env bash

set -e

export DJANGO_SETTINGS_MODULE=config.settings.test
echo coverage run manage.py test -v2 "$@"
coverage run manage.py test -v2 "$@"

# Don't generate html report on coverage

# Fails on invalid coverage
coverage html || true

if coverage report -m
then
   echo "Coverage OK"
   exit 0
else
   echo "Missing coverage"
   exit 1
fi
