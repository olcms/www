#!/usr/bin/env bash

set -e
cmd="$@"

# Following sets mapping from olcms.dev.local to ip of host machine.
# See this: http://stackoverflow.com/a/24716645/7918
# We need swift container to be resolvable under the same name
# both in container (so django can interact with it) and from developer machine
# (so developer can interact with it)
echo "$(/sbin/ip route|awk '/default/ { print $3 }')    ${OLCMS_DOMAIN}" >> /etc/hosts

swift_command="swift -A ${SWIFT_AUTH_URL} -U ${SWIFT_USERNAME} -K ${SWIFT_KEY}"

until ${swift_command} stat; do
  >&2 echo ${swift_command}
  >&2 echo "Swift is unavailable - sleeping"
  sleep 1
done

>&2 echo "Swift is up -- configuring it"

# Create swift container
${swift_command} post ${SWIFT_CONTAINER_NAME}

python /scripts/configure-swift-container.py --auth-url ${SWIFT_AUTH_URL} --username ${SWIFT_USERNAME} \
       --password ${SWIFT_KEY} --container-name ${SWIFT_CONTAINER_NAME} \
       --temp-url-key ${SWIFT_TEMP_URL_KEY}

/scripts/configure-simulaiton-container.sh

>&2 echo "Swift is up -- done; continuing"

cd /app

export DATABASE_URL=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres:5432/$POSTGRES_USER

/scripts/collectstatic.sh

mkdir -p /home/django /videotmp/conf /whoosh-data
chown -R django /home/django /app /videotmp /whoosh-data

gosu django /scripts/entrypoint.sh $cmd
#exec /scripts/entrypoint.sh $cmd
