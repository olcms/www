#!/usr/bin/env bash
set -e

swift_command="swift -A ${SWIFT_AUTH_URL} -U ${SWIFT_USERNAME} -K ${SWIFT_KEY}"

${swift_command} post ${SWIFT_SIMULATION_CONTAINER}
${swift_command} post -r '.r:*,.rlistings' ${SWIFT_SIMULATION_CONTAINER}
