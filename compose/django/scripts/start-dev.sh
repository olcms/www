#!/bin/sh
echo "Migrating"
python manage.py migrate
echo "Migrated. Running npm"
npm install --force
echo "ran npm, now compile messages"
./manage.py compilemessages
echo "starting server"
./manage.py runserver_plus 0.0.0.0:8000
