#!/bin/sh

set -e

/scripts/collectstatic.sh
./manage.py compilemessages

/usr/local/bin/gunicorn config.wsgi --access-logfile - --error-logfile - -w 4 -b 0.0.0.0:5000 --chdir=/app
