#!/usr/bin/env bash

set -e

currdir=$(pwd)

cd /app

# Tsc uses configuration from tsconfig.json, so no params needed.

tsc

# This has to be before collectstatic as collectstatic does some post-processing
# on collected files. We control where this file is stored by JS_REVERSE_OUTPUT_PATH
# setting.
# NOTE: There is no --noinput switch in this command.
./manage.py collectstatic_js_reverse

./manage.py collectstatic --noinput

cd ${currdir}
