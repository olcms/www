#!/usr/bin/env bash

./manage.py update_index --age 24

while :
do
    # Every 6 minutes reindex entries from last hour, not efficient but good enough

    ./manage.py update_index --age 1
	sleep 6m;
done
