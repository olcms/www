#!/bin/bash
set -e
cmd="$@"

# This entrypoint is used to play nicely with the current cookiecutter configuration.
# Since docker-compose relies heavily on environment variables itself for configuration, we'd have to define multiple
# environment variables just to support cookiecutter out of the box. That makes no sense, so this little entrypoint
# does all this for us.
export REDIS_URL=redis://redis:6379

# the official postgres image uses 'postgres' as default user if not set explictly.
if [ -z "$POSTGRES_USER" ]; then
    export POSTGRES_USER=postgres
fi

# the official postgres image uses 'postgres' as default user if not set explictly.
if [ -z "$POSTGRES_HOST" ]; then
    export POSTGRES_HOST=postgres
fi


if [ -z "$POSTGRES_DB" ]; then
    export POSTGRES_DB=$POSTGRES_USER
fi

if [ "$DJANGO_SETTINGS_MODULE" = "config.settings.production" ]; then

    if [ -z "$YOUTUBE_SECRET_JSON" ]; then
        echo "youtube secret is missing"
        exit 1
    fi
    if [ -z "$YOUTUBE_OAUTH2_JSON" ]; then
        echo "youtube OAUTH2 data is missing"
        exit 1
    fi
fi

mkdir -p /videotmp/conf
echo $YOUTUBE_SECRET_JSON > /videotmp/conf/client_secret.json
echo $YOUTUBE_OAUTH2_JSON > /videotmp/conf/upload_oauth2.json

export DATABASE_URL=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@${POSTGRES_HOST}:5432/$POSTGRES_DB

export CELERY_BROKER_URL=$REDIS_URL/0


function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTGRES_DB", user="$POSTGRES_USER", password="$POSTGRES_PASSWORD", host="${POSTGRES_HOST}")
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."

rm -f celerybeat.pid || /bin/true

exec $cmd
