# -*- coding: utf-8 -*-

"""
Script that configures swift container to be compatible with OLCMS.

It updates temp url key, and updates cors headers.
"""

import argparse
import subprocess
import sys


# flake8: noqa


def sh(command):

    print(command)

    kwargs = {'shell': True, 'stderr': subprocess.STDOUT, 'stdout': subprocess.PIPE}
    p = subprocess.Popen(command, **kwargs)
    p_stdout = p.communicate()[0]
    if p_stdout is not None:
        p_stdout = p_stdout.decode(sys.getdefaultencoding(), 'ignore')
    if p.returncode:
        print(p_stdout)
        raise ValueError("Subprocess return code: %d" % p.returncode)

    return p_stdout


parser = argparse.ArgumentParser()

parser.add_argument("--auth-url", action="store", required=True)
parser.add_argument("--username", action="store", required=True)
parser.add_argument("--password", action="store", required=True)
parser.add_argument("--container-name", action="store", required=True)
parser.add_argument("--temp-url-key", action="store", required=True)
parser.add_argument("--temp-url-key-2", action="store", default=None, required=False)

SWIFT_CMD_TEMPLATE = """
swift -A {auth_url} -U {username} -K {password}
""".strip('\n')

UPDATE_TEMP_URL = """
post {container_name} -H "{temp_url_key_name}:{temp_url_key}"
""".strip('\n')

UPDATE_CORS = "post {container_name} " \
  "-H 'X-Container-Meta-Access-Control-Allow-Origin:*' " \
  "-H 'X-Container-Meta-Access-Control-Expose-Headers:*' " \
  "-H 'X-Container-Meta-Access-Control-Allow-Headers: *' "

MAKE_CONTAINER_READABLE = "post {container_name} -r '.r:*'"

def render_command(**kwargs):
    return SWIFT_CMD_TEMPLATE.format(**kwargs) + " "


def update_temp_url(temp_url, temp_url_2=False, **kwargs):
    if temp_url_2:
        temp_url_name = "X-Container-Meta-Temp-URL-Key-2"
    else:
        temp_url_name = "X-Container-Meta-Temp-URL-Key"
    kwargs = dict(**kwargs)
    kwargs.update({
        "temp_url_key_name": temp_url_name,
        "temp_url_key": temp_url
    })
    sh(render_command(**kwargs) + UPDATE_TEMP_URL.format(**kwargs))


def update_cors(**kwargs):
    sh(render_command(**kwargs) + UPDATE_CORS.format(**kwargs))


def make_container_readable(**kwargs):
    sh(render_command(**kwargs) + MAKE_CONTAINER_READABLE.format(**kwargs))


def main():
    namespace = parser.parse_args()
    data = dict(**namespace.__dict__)
    update_temp_url(namespace.temp_url_key, False, **data)
    if namespace.temp_url_key_2:
        update_temp_url(namespace.temp_url_key_2, True, **data)
    update_cors(**data)
    make_container_readable(**data)


if __name__ == "__main__":
    main()
