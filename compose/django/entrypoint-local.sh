# Entrypoint for running OLCMS locally

export REDIS_URL=redis://redis:6379

export POSTGRES_USER='www'
export DATABASE_URL=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@localhost:15432/$POSTGRES_USER

export OLCMS_DOMAIN=olcms.dev.local

export DJANGO_SETTINGS_MODULE=config.settings.local

export CELERY_BROKER_URL=$REDIS_URL/0

export SWIFT_AUTH_URL=http://${OLCMS_DOMAIN}:8105/auth/v1.0
export SWIFT_USERNAME=test:tester
export SWIFT_KEY=testing
export SWIFT_CONTAINER_NAME=olcms-media
export SWIFT_TEMP_URL_KEY=changemeplease
export SWIFT_TEMP_URL_DURATION=3600

export ENABLE_XVBF=false

export LOCAL_WHOOSH_DATA=/tmp/whoosh
