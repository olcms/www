#!/usr/bin/env sh
set -e
cmd="$@"

echo ${NGINX_KEY} | base64 -d > /etc/nginx/cert.key
echo ${NGINX_CERT} | base64 -d > /etc/nginx/cert.cert

# Put your domain name into the nginx reverse proxy config.
sed -i "s/OLCMS_DOMAIN/${OLCMS_DOMAIN}/g" /etc/nginx/nginx.conf
sed -i "s/DISCOURSE_HOSTNAME/${DISCOURSE_HOSTNAME}/g" /etc/nginx/nginx.conf


# Add the system's nameserver (the docker network dns) so we can resolve container names in nginx
NAMESERVER=`cat /etc/resolv.conf | grep "nameserver" | awk '{print $2}' | tr '\n' ' '`
echo replacing ___NAMESERVER___/${NAMESERVER}

sed -i "s/___NAMESERVER___/${NAMESERVER}/g" /etc/nginx/nginx.conf

echo "Entrypoint done running: ${cmd}"

exec ${cmd}
