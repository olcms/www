# -*- coding: utf-8 -*-
"""Selenium content item tests."""

import time
import typing

from django.urls.base import resolve
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from www.content_item import models as content_item_models
from www.content_item.models import ContentItem, Domain, DomainDescription
from www.content_item.test.utils import ContentItemFactory, UserFactory
from www.resource import models as resource_models
from www.resource.tests.utils import LinkResourceFactory, DocumentResourceFactory, FileResourceFactory
from www.utils.selenium_utils import LiveServerBrowserTestCase
from www.utils.test_utils import model_rich_compare


# Pylint is not aware of pk attribute.
# pylint: disable=no-member,invalid-sequence-index,no-self-use


class ContentItemBaseTest(LiveServerBrowserTestCase):
    """
    Some utilities for testing content item.
    """

    def fill_form_from_model(
            self, model: content_item_models.ContentItem, *,
            is_edit: bool,
            submit=True
    ) -> typing.Optional[int]:
        """
        This assumes browser has already been navigated to a proper page.

        If submit is true, it returns pk of created ContentItem instance
        """

        if not is_edit:
            self.select_bootsrap_dropdown("id_type", model.type)

        if is_edit:
            self.driver.find_element_by_id("id_title").clear()
            self.driver.find_element_by_id("id_description").clear()

        self.driver.find_element_by_id("id_title").send_keys(model.title)
        self.driver.find_element_by_id("id_description").send_keys(model.description)
        self.select_bootsrap_dropdown("id_license", model.license)
        self.select_bootsrap_dropdown("id_language", model.language)
        self.select_bootsrap_dropdown("id_main_discipline", model.main_discipline.pk)

        if model.authors:
            if is_edit:
                self.clean_tokenfield(self.driver.find_element_by_id("id_authors"))
            self.fill_tokenfield(self.driver.find_element_by_id("id_authors-tokenfield"), model.authors)

        if model.tags:
            if is_edit:
                self.clean_tokenfield(self.driver.find_element_by_id("id_tags"))
            self.fill_tokenfield(
                self.driver.find_element_by_id("id_tags-tokenfield"),
                model.tags.all().values_list("name", flat=True)
            )

        if submit:
            return self.submit_contentitem_form()
        return None

    def go_to_content_item(self, content_item):
        """Directs browser to detail view for this content item. """
        self.driver.get(self.reverse_url("content_item:detail", kwargs={"pk": content_item.pk}))

    def create_and_go_to_example_content_item(self, owners=None) -> 'content_item_models.ContentItem':
        """Creates content item and opens it in browser."""
        if owners is None:
            owners = [self.user]
        self.login()
        original_item = ContentItemFactory(owners=owners)
        self.assertEqual(0, original_item.resources.all().count())
        self.go_to_content_item(original_item)
        return original_item

    MODAL_HIDDEN_CONDITION = EC.invisibility_of_element_located((By.ID, "generic-modal"))
    MODAL_VISIBLE_CONDITION = EC.visibility_of_element_located((By.CSS_SELECTOR, "#generic-modal form"))
    UPLOAD_FORM_VISIBLE_CONDITION = EC.visibility_of_element_located((By.ID, "upload-button"))

    def submit_resource_form(self, form_submitted_condition=MODAL_HIDDEN_CONDITION):
        """Submits form displayed in modal."""
        self.save_screenshot("filled-resource-form")
        self.driver.find_element_by_css_selector("#generic-modal button.modal-submit").click()
        WebDriverWait(self.driver, self.PAGE_LOAD_TIMEOUT_SEC).until(form_submitted_condition)

    def get_all_resources(self) -> typing.Sequence[WebElement]:
        """Gets all resource links for content item."""
        return self.driver.find_elements_by_css_selector("#resourceList div.resource-tile")

    def fill_resource_from_model(self, instance: resource_models.Resource, submit=True):
        """Fills resource form according to data in a already created model template."""
        self.assertIsNotNone(instance.title)
        self.assertIsNotNone(instance.role)
        self.assertIsNotNone(instance.role.pk)
        self.driver.find_element_by_id("id_title").send_keys(instance.title)
        if instance.description:
            self.driver.find_element_by_id("id_description").send_keys(instance.description)

        self.select_bootsrap_dropdown("id_role", str(instance.role.pk))

        if submit:
            self.submit_resource_form()

    def fill_link_resource_from_model(self, instance: resource_models.LinkResource, submit=True):
        """ Fills link resource form according to data in a already created model template. """
        self.fill_resource_from_model(instance, False)
        self.driver.find_element_by_id("id_url").send_keys(instance.url)

        if submit:
            self.submit_resource_form()

    def submit_contentitem_form(self) -> int:
        """Submits ContentItem form."""
        self.save_screenshot("pre-submit")
        with self.wait_for_page_load():
            self.driver.find_element_by_id("submit").click()
        match = resolve(self.get_current_path())

        return match.kwargs['pk']

    def click_add_resource_button_and_wait_for_form(self, resource_type):
        """Helper. Clicks add_resource button and waits for modal form to appear."""
        selector = """#add-resource a[data-type="{}"]""".format(resource_type)
        self.driver.find_element_by_css_selector("#add-resource button").click()
        self.driver.find_element_by_css_selector(selector).click()
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_id("generic-modal"))
        )


class ContentItemTest(ContentItemBaseTest):
    """
    Test for adding Content item and Resources.
    """

    USER_PERMS = ["content_item.is_autoverified"]

    def test_add_content_item(self):
        """Simple case when content item is added. """
        self.login()
        self.driver.get(self.reverse_url("content_item:add"))
        item = ContentItemFactory(knowledge_gain='', technical_requirements='')
        created_pk = self.fill_form_from_model(item, is_edit=False)
        # NOTE: ignore language group in comparison (it will be different because factory creates new group by default)
        model_rich_compare(
            self,
            item,
            ContentItem.objects.get(
                pk=created_pk
            ),
            exclude_pk=True,
            ignored_attrs=["group"]
        )

    def test_create_content_item_invalid(self):
        """Checks HTML5 form errors in form."""
        self.login()
        self.driver.get(self.reverse_url("content_item:add"))
        item = ContentItemFactory()
        self.fill_form_from_model(item, is_edit=False, submit=False)
        self.driver.find_element_by_id("id_title").clear()

        self.driver.find_element_by_id("submit").click()

        # After clearing the title form submission should be invalid,
        # however this is handled by HTML5 form.
        element = self.driver.find_element_by_css_selector("input:invalid")
        self.assertEqual(element.get_attribute("id"), "id_title")

    def test_add_content_item_with_default_author(self):
        """Checks logic that sets initial value for author field."""
        self.login()
        self.user.first_name = "Luke"
        self.user.last_name = "Skywaker"
        self.user.save()
        item = ContentItemFactory(
            authors=[],
            knowledge_gain='',
            technical_requirements=''
        )
        self.driver.get(self.reverse_url("content_item:add"))
        created_pk = self.fill_form_from_model(item, is_edit=False)
        item.authors = ["Luke Skywaker"]
        model_rich_compare(
            self,
            item,
            ContentItem.objects.get(
                pk=created_pk
            ),
            exclude_pk=True,
            ignored_attrs=["group"]
        )

    def test_add_tags(self):
        """
        Checks that we handle tag input somehow.
        """
        self.login()
        item = ContentItemFactory(
            tags=[]
        )

        self.driver.get(self.reverse_url("content_item:add"))
        self.fill_form_from_model(item, is_edit=False, submit=False)
        self.driver.find_element_by_id("id_tags-tokenfield").send_keys(
            "foo, bar, baz"
        )
        pk = self.submit_contentitem_form()
        item = ContentItem.objects.get(pk=pk)

        self.assertEqual(
            {'foo', 'bar', 'baz'},
            {tag.name for tag in item.tags.all()}
        )

    def test_content_item_edition(self):
        """Test for content item edition."""
        self.login()
        original_item = ContentItemFactory(owners=self.user)
        original_group = original_item.group
        new_item = ContentItemFactory(
            owners=self.user,
            authors=["Darth Vader"]
        )
        self.driver.get(self.reverse_url("content_item:edit", kwargs={"pk": original_item.pk}))
        WebDriverWait(self.driver, 60).until(
            EC.presence_of_element_located((By.ID, "submit"))
        )
        self.save_screenshot("before-edit")
        self.fill_form_from_model(new_item, is_edit=True, submit=True)
        # NOTE: skip language group in comparison, new_item created through factory has different group
        model_rich_compare(
            self,
            new_item,
            ContentItem.objects.get(
                pk=original_item.pk
            ),
            exclude_pk=True,
            ignored_attrs=["group"]
        )
        # verify that group remained unchanged
        self.assertEqual(original_item.group, original_group)

    def test_content_item_add_resource(self):
        """Test adding resource to content item."""
        original_item = self.create_and_go_to_example_content_item()
        resource = LinkResourceFactory()
        self.click_add_resource_button_and_wait_for_form(
            resource.resource_type
        )
        self.fill_link_resource_from_model(resource, True)
        content_item_res = original_item.resources.all()
        self.assertEqual(1, content_item_res.count())
        model_rich_compare(
            self,
            resource,
            content_item_res[0].linkresource,
            exclude_pk=True,
            ignored_attrs=["content_item", "resource_ptr"]
        )

    def test_content_item_delete(self):
        """Test adding resource to content item."""
        original_item = self.create_and_go_to_example_content_item()
        self.assertFalse(original_item.hidden)
        self.driver.find_element_by_id("content-item-delete").click()
        self.wait().until(self.MODAL_VISIBLE_CONDITION)
        with self.wait_for_page_load():
            self.submit_resource_form()
        original_item.refresh_from_db()
        self.assertTrue(original_item.hidden)

    def test_content_item_add_resource_invalid_form(self):
        """
        Tests that invalid form is marked as such.
        """
        original_item = self.create_and_go_to_example_content_item()
        resource = LinkResourceFactory()
        self.click_add_resource_button_and_wait_for_form("link")
        resource.title = ""  # Set empty title for invalid form
        self.fill_link_resource_from_model(resource, submit=False)
        self.save_screenshot("filled-resource-form")
        self.driver.find_element_by_css_selector("#generic-modal button.modal-submit").click()
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "error_1_id_title")
            )
        )
        self.assertEqual(
            self.driver.find_element_by_id("error_1_id_title").text,
            "This field is required."
        )
        self.assertEqual(0, original_item.resources.all().count())

    def test_content_item_language_groups(self):
        """
        Tests that two content items created independently have different group id.
        """
        self.login()
        self.driver.get(self.reverse_url("content_item:add"))
        item = ContentItemFactory(knowledge_gain='', technical_requirements='')
        created_pk = self.fill_form_from_model(item, is_edit=False)

        self.driver.get(self.reverse_url("content_item:add"))
        created_pk2 = self.fill_form_from_model(item, is_edit=False)

        content_item1 = ContentItem.objects.get(pk=created_pk)
        content_item2 = ContentItem.objects.get(pk=created_pk2)

        # NOTE: ignore language group in comparison, compare it later manually
        model_rich_compare(
            self,
            item,
            content_item1,
            exclude_pk=True,
            ignored_attrs=["group"]
        )

        model_rich_compare(
            self,
            item,
            content_item2,
            exclude_pk=True,
            ignored_attrs=["group"]
        )

        self.assertNotEqual(item.group.pk, content_item1.group.pk)
        self.assertNotEqual(item.group.pk, content_item2.group.pk)
        self.assertNotEqual(content_item1.group.pk, content_item2.group.pk)

    def test_content_item_another_language_version(self):
        """
        Tests that content item added through add another language version link has the same language group.
        """
        self.login()
        item = ContentItemFactory(knowledge_gain='', technical_requirements='', language='en')
        item.language = 'pl'
        self.driver.get(self.reverse_url("content_item:add-lang", kwargs={"group_id": item.group.pk}))
        created_pk = self.fill_form_from_model(item, is_edit=False)

        # NOTE: ignore language in comparison, compare it later manually
        model_rich_compare(
            self,
            item,
            ContentItem.objects.get(pk=created_pk),
            exclude_pk=True,
            ignored_attrs=["language"]
        )

        self.assertEqual(item.language, ContentItem.objects.get(pk=created_pk).language)

    def test_content_item_same_language_version(self):
        """
        Tests that content item added through add another language version link
        cannot add multiple items with same language.
        """
        self.login()
        item = ContentItemFactory(knowledge_gain='', technical_requirements='')
        self.driver.get(self.reverse_url("content_item:add-lang", kwargs={"group_id": item.group.pk}))

        # Submit form by hand (pk return value will not be available)
        self.fill_form_from_model(item, is_edit=False, submit=False)

        with self.wait_for_page_load():
            self.driver.find_element_by_id("submit").click()

        # We should be stuck on form page after submission (validation error on form level)
        # No additional content items are created
        self.assertEqual(ContentItem.objects.all().count(), 1)


class TestNotCompleteResourcesAreHidden(ContentItemBaseTest):
    """Test that resources are hidden from non-owners if incomplete."""

    def setUp(self):
        super().setUp()
        self.content_item_owner = UserFactory()
        self.content_item = ContentItemFactory(
            owners=[self.content_item_owner]
        )

    def __assert_resource_count(self, expected_count=1):
        elements = self.driver.find_elements_by_css_selector("#resourceList div.resource-tile")
        self.assertEqual(len(elements), expected_count)

    def test_completed_document_visible(self):
        """Tests that document resource with markdown is visible to non-owner"""
        original_item = self.create_and_go_to_example_content_item(
            owners=[self.content_item_owner]
        )
        DocumentResourceFactory(content_item=original_item)
        self.go_to_content_item(original_item)
        self.__assert_resource_count(1)

    def test_not_completed_document_hidden(self):
        """Tests that document resource without markdown is invisible to non-owner"""
        original_item = self.create_and_go_to_example_content_item(
            owners=[self.content_item_owner]
        )
        DocumentResourceFactory(content_item=original_item, markdown=None)
        self.go_to_content_item(original_item)
        self.__assert_resource_count(0)

    def test_completed_file_visible(self):
        """Tests that file resource with uploaded is visible to non-owner"""
        original_item = self.create_and_go_to_example_content_item(
            owners=[self.content_item_owner]
        )
        FileResourceFactory(content_item=original_item)
        self.go_to_content_item(original_item)
        self.__assert_resource_count(1)

    def test_not_completed_file_hidden(self):
        """Tests that file resource without file is invisible to non-owner"""

        original_item = self.create_and_go_to_example_content_item(
            owners=[self.content_item_owner]
        )
        FileResourceFactory(content_item=original_item, path=None)
        self.go_to_content_item(original_item)
        self.__assert_resource_count(0)


class TestRelatedRedirects(ContentItemBaseTest):
    """Test related link."""

    def setUp(self):
        super().setUp()
        self.domain = Domain.objects.get(slug="physics")
        self.content_item = ContentItemFactory(main_discipline=self.domain)

    def test_related(self):
        """Test related link."""
        self.go_to_content_item(self.content_item)
        expected_url = self.reverse_url(
            "content_item:search", kwargs={"domain": self.domain.slug}
        )
        for __ in range(10):
            try:
                href_elements = [
                    e.get_attribute('href') for e in self.driver.find_elements_by_css_selector("a")
                ]
                self.assertIn(expected_url, href_elements)
                break
            except (StaleElementReferenceException, AssertionError):
                # It means that we are in the middle of page reload via AJAX.
                time.sleep(0.1)


class TestRRIDescriptions(ContentItemBaseTest):
    """Test adding and editing RRI descriptions"""

    def setUp(self):
        super().setUp()
        self.login()
        self.domain = Domain.objects.get(slug="physics")
        self.content_item = ContentItemFactory(main_discipline=self.domain, owners=self.user)
        self.rri = Domain.objects.get(slug="learning")
        self.content_item.domains.add(self.rri)

    def test_add_rri_description(self):
        """Test rri description can be added."""
        TEST_DESCRIPTION = "testowy opis"

        self.go_to_content_item(self.content_item)
        self.driver.find_element_by_id("rri-link").click()

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_class_name("rri-add-desc-link"))
        )

        self.driver.find_element_by_class_name("rri-add-desc-link").click()

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_id("id_description"))
        )

        self.driver.find_element_by_id("id_description").send_keys(TEST_DESCRIPTION)

        with self.wait_for_page_load():
            self.driver.find_element_by_id("submit").click()

        # Content item should have RRI description set by now
        self.content_item.refresh_from_db()
        dwd = self.content_item.domain_descriptions.all()

        self.assertEqual(len(dwd), 1)
        self.assertEqual(dwd[0].description, TEST_DESCRIPTION)

    def test_edit_rri_description(self):
        """Test RRI description can be edited."""
        TEST_DESCRIPTION1 = "testowy opis1"
        TEST_DESCRIPTION2 = "zedytowany opis"

        desc = DomainDescription()
        desc.content_item = self.content_item
        desc.domain = self.rri
        desc.description = TEST_DESCRIPTION1
        desc.save()

        self.go_to_content_item(self.content_item)
        self.driver.find_element_by_id("rri-link").click()

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_class_name("rri-edit-desc-link"))
        )

        self.driver.find_element_by_class_name("rri-edit-desc-link").click()

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_id("id_description"))
        )

        self.driver.find_element_by_id("id_description").clear()
        self.driver.find_element_by_id("id_description").send_keys(TEST_DESCRIPTION2)

        with self.wait_for_page_load():
            self.driver.find_element_by_id("submit").click()

        # Content item should have RRI description set by now
        self.content_item.refresh_from_db()
        dwd = self.content_item.domain_descriptions.all()

        self.assertEqual(len(dwd), 1)
        self.assertEqual(dwd[0].description, TEST_DESCRIPTION2)

        # check if domains_with_descriptions returns correct results (mainly to get full coverage)
        dwd = self.content_item.domains_with_descriptions()

        self.assertEqual(dwd[self.rri].description, TEST_DESCRIPTION2)
