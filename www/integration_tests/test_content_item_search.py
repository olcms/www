# -*- coding: utf-8 -*-

# pylint: disable=no-member,no-value-for-parameter,not-callable

"""
Selenium tests for search the form.
"""

import typing
from urllib.parse import urlparse

from django.urls.base import resolve

import www.content_item.models
from www.content_item.const import License
from www.content_item.models import Domain
from www.content_item.test.utils import ContentItemFactory
from www.utils.search_utils import RealtimeSearchTest
from www.utils.selenium_utils import LiveServerBrowserTestCase

ContentItemSearchData = typing.NamedTuple(
    "ContentItemSearchData",
    (
        ("tags", typing.Optional[typing.Sequence[str]]),
        ("term", typing.Optional[str]),
        ("license", typing.Optional[str]),
        ("domain", typing.Optional[Domain]),
        ("methodology", typing.Optional[Domain]),
    )
)


def create_search_data(
        tags: typing.Optional[typing.Sequence[str]]=None,
        term: typing.Optional[str]=None,
        license: typing.Optional[str]=None,  # pylint: disable=redefined-builtin
        domain: typing.Optional[Domain]=None,
        methodology: typing.Optional[Domain] = None
):
    """Create ContentItemSearchData passing None for unfilled parameters."""
    return ContentItemSearchData(
        tags=tags, term=term, license=license, domain=domain, methodology=methodology
    )


# Tests will fail if there is missing member --- this is triggered by mixins.
# pylint: disable=no-member


class BaseContentItemSearchTest(RealtimeSearchTest):
    """
    Base test for searching content items
    """

    def go_to_search_page(self):
        """Opens the search page in browser."""
        self.driver.get(self.reverse_url("content_item:search"))

    def submit_search_form(self):
        """Submits the search form, and waits for page reload."""
        with self.wait_for_page_load():
            self.driver.find_element_by_id("search-submit").click()

    def fill_search_form(self, data: ContentItemSearchData, submit=True):
        """Fills search form."""
        if data.term is not None:
            self.driver.find_element_by_id("id_q").send_keys(data.term)
        if data.tags is not None:
            self.fill_tokenfield("id_tags-tokenfield", data.tags)
        if data.license is not None:
            self.select_bootsrap_dropdown("id_license", data.license)
        if data.domain is not None:
            self.select_bootsrap_dropdown("id_domain", data.domain.pk)
        if data.methodology is not None:
            self.select_bootsrap_dropdown("id_methodology", data.methodology.pk)

        self.save_screenshot("pre-submit")

        if submit:
            self.submit_search_form()

    def get_content_item_if_from_href(self, url: str) -> int:
        """Returns a id of content item from detail url."""
        parsed = urlparse(url)
        resolved = resolve(parsed.path)
        self.assertEqual(resolved.url_name, "detail")
        self.assertEqual(resolved.namespace, "content_item")
        return int(resolved.kwargs['pk'])

    def get_content_items_ids_from_results(self) -> typing.Container[int]:
        """Returns list of ids of ContentItems returned in by displayed query."""

        return [
            self.get_content_item_if_from_href(elem.get_attribute("href"))
            for elem in self.driver.find_elements_by_css_selector("a.search-result")
        ]

    def get_content_items_from_results(
            self
    ) -> typing.Container['www.content_item.models.ContentItem']:
        """Returns list of ContentItems returned in by displayed query."""
        return www.content_item.models.ContentItem.objects.filter(
            pk__in=self.get_content_items_ids_from_results()
        )


class DefaultContentItemSearchTest(BaseContentItemSearchTest):
    """
    Base test case.
    """

    CLEAR_INDEX = False

    def search_data(self) -> ContentItemSearchData:
        """Return data to be submitted to search form."""
        raise NotImplementedError

    def expected_results(self) -> typing.Sequence['www.content_item.models.ContentItem']:
        """Return expected results."""
        raise NotImplementedError

    def test_search(self):
        """
        Go to search page, fill the form, check results are as expected.
        """
        self.go_to_search_page()
        self.fill_search_form(self.search_data())

        expected = self.expected_results()
        actual = self.get_content_items_from_results()

        if not isinstance(expected, set):
            expected = set(expected)
        self.assertEqual(expected, set(actual))


class NoSearchTest(DefaultContentItemSearchTest, LiveServerBrowserTestCase):
    """Test where there is no search parameters."""

    def setUp(self):
        super().setUp()
        self.valid_items = [
            ContentItemFactory(language="en") for __ in range(10)
        ]

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return set(self.valid_items)

    def search_data(self) -> ContentItemSearchData:
        return create_search_data()


class TermSearchSimpleSearch(DefaultContentItemSearchTest, LiveServerBrowserTestCase):
    """Check full-text search."""

    def setUp(self):
        super().setUp()
        self.valid_items = [
            ContentItemFactory(
                title="Compton Palimpsest",
                description="A description",
                authors=["sir Arthur Compton"],
                tags=[],
                language="en"
            ),
            ContentItemFactory(
                title="Rutherford Palimpsest",
                description="A description",
                authors=["Ernst Rutherford"],
                tags=[],
                language="en"
            ),
        ]
        self.invalid_items = [
            ContentItemFactory(
                tags=[]
            ),
            ContentItemFactory(
                tags=[]
            )
        ]

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return set(self.valid_items)

    def search_data(self) -> ContentItemSearchData:
        return create_search_data(term="palimpsest")


class TermSearchSimpleSearch2(DefaultContentItemSearchTest, LiveServerBrowserTestCase):
    """Check full-text search."""
    def setUp(self):
        super().setUp()
        self.compton = ContentItemFactory(
            title="Compton Experiment",
            description="A description",
            authors=["sir Arthur Compton"],
            tags=[],
            language="en",
        )
        self.rutherford = ContentItemFactory(
            title="Rutherford Experiment",
            description="A description",
            authors=["Ernst Rutherford"],
            tags=[],
            language="en",
        )

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return {self.compton}

    def search_data(self) -> ContentItemSearchData:
        return create_search_data(term="compton")


class TestLicenseSearch(LiveServerBrowserTestCase, DefaultContentItemSearchTest):
    """License search."""

    def setUp(self):
        super().setUp()
        self.valid_items = [
            ContentItemFactory(
                license=License.cc0,
                language="en",
            )
        ]
        self.invalid_items = [
            ContentItemFactory(
                license=License.cc_by_nc,
                language="en",
            ),
            ContentItemFactory(
                license=License.cc_by_nc_nd,
                language="en",
            )
        ]

    def search_data(self) -> ContentItemSearchData:
        return create_search_data(license=License.cc0)

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return set(self.valid_items)


class DisciplineSearch(DefaultContentItemSearchTest, LiveServerBrowserTestCase):
    """Test filtering by discipline."""
    def setUp(self):
        super().setUp()
        self.physics = Domain.objects.get(slug='physics')
        self.chemistry = Domain.objects.get(slug='chemistry')
        self.valid_items = [
            ContentItemFactory(domains=self.physics, language="en")
            for __ in range(3)
        ]
        self.invalid_items = [
            ContentItemFactory(domains=self.chemistry, language="en")
            for __ in range(3)
        ]

    def search_data(self) -> ContentItemSearchData:
        return create_search_data(domain=self.physics)

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return set(self.valid_items)


class MethodologySearch(DefaultContentItemSearchTest, LiveServerBrowserTestCase):
    """Test filtering by discipline."""
    def setUp(self):
        super().setUp()
        self.multimedia = Domain.objects.get(slug='multimedia')
        self.hands_on = Domain.objects.get(slug='hands-on')
        self.valid_items = [
            ContentItemFactory(domains=self.multimedia, language="en")
            for __ in range(3)
        ]
        self.invalid_items = [
            ContentItemFactory(domains=self.hands_on, language="en")
            for __ in range(3)
        ]

    def search_data(self) -> ContentItemSearchData:
        return create_search_data(methodology=self.multimedia)

    def expected_results(self) -> typing.Container['www.content_item.models.ContentItem']:
        return set(self.valid_items)


del DefaultContentItemSearchTest
