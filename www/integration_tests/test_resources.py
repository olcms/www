# coding=utf-8
"""Generic tests for all resources."""
from www.integration_tests.resource_utils import TestLinkResourceUtils
from www.resource.tests.utils import LinkResourceFactory


# Pylint is not aware of pk attribute.
# pylint: disable=no-member,invalid-sequence-index,no-self-use


class TestResource(TestLinkResourceUtils):
    """ Tests for resources """

    def test_add_resource_twice(self):
        """Test you can add resource twice (regression)."""
        original_item = self.create_and_go_to_example_content_item()
        self.assertEqual(0, original_item.resources.count())
        self.click_add_resource_button_and_wait_for_form("link")

        link_res = LinkResourceFactory()
        self.fill_link_resource_from_model(link_res, True)

        self.wait_unit_link_visible(link_res.url)

        self.click_add_resource_button_and_wait_for_form("link")

        link_res = LinkResourceFactory()
        self.fill_link_resource_from_model(link_res, True)

        self.wait_unit_link_visible(link_res.url)

        res_list = list(original_item.resources.all())
        self.assertEqual(2, len(res_list))

        resulting_link = res_list[0]
        self.assertEqual(link_res.title, resulting_link.title)
        self.assertEqual(link_res.url, resulting_link.linkresource.url)
