# coding=utf-8
"""
Tests for document resource.
"""

import typing

from unittest.mock import (
    patch,
    call
)

import requests

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from django.test import override_settings

from www.integration_tests.resource_utils import TestResourceIsEditable
from www.integration_tests.test_content_item import ContentItemBaseTest

from www.resource.tests.utils import DocumentResourceFactory

if typing.TYPE_CHECKING:
    from www.content_item.models import ContentItem  # pylint: disable=unused-import
    from www.resource.models import Resource, DocumentResource  # pylint: disable=unused-import

# pylint: disable=no-member


class TestDocumentResourceUtils(ContentItemBaseTest):
    """ Utilities for link resource tests """

    # This is the id of small status-bar under the editor, this status bar
    # happens to be initialized after markdown is loaded to the editor
    # so when it is shown we *KNOW* that editor is initialized
    WAIT_FOR_DOCUMENT_EDITOR = EC.visibility_of_element_located((By.CSS_SELECTOR, "div.editor-statusbar"))

    def send_keys_to_simple_mde(self, contents):
        """Write contents to the only SimpleMDE editor on the page."""

        code_element = self.driver.find_element_by_css_selector("div.CodeMirror-code")
        if not code_element.is_displayed():
            # Sometimes editor area does not load, however if you click on it it loads
            # just fine,
            self.driver.find_element_by_css_selector("div.CodeMirror").click()
            WebDriverWait(self.driver, 4).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, "div.CodeMirror-code"))
            )

        self.driver.execute_script("window.simplemde.editor.value('{}')".format(contents))

    def get_link_from_only_resource_in_content_item(self, css_selector: str) -> WebElement:
        """
        Assumes we are on content item detail page.
        """
        resources = self.get_all_resources()
        self.assertEqual(len(resources), 1)
        resource = resources[0]
        download_link = resource.find_element_by_css_selector(css_selector)
        return download_link

    def download_document_and_return_contents(self, href: str) -> str:
        """Downloads document and returns contents. """
        response = requests.get(href)
        self.assertEqual(response.status_code, 200)
        return response.text

    def create_and_go_to_content_item_with_document(self) -> 'typing.Tuple[ContentItem, DocumentResource]':
        """
        Creates content item and document resource. Opens Content Item page.
        """
        content_item = self.create_and_go_to_example_content_item()
        document_resource = DocumentResourceFactory(content_item=content_item)
        self.go_to_content_item(content_item)
        return content_item, document_resource


class TestDocumentResourceEditable(TestResourceIsEditable):
    """Tests Document resource hides edit links for persons who shouldn't see them."""
    def create_resource(self, content_item) -> 'Resource':
        return DocumentResourceFactory(content_item=content_item)

    def test_resource_editable_for_logged_in(self):
        self.login()
        self._test_resources(2, owner=self.user)


class TestDocumentResourceCreation(TestDocumentResourceUtils):
    """Tests document resource."""

    @staticmethod
    def verify_document_conversions_triggered(task, args):
        """Checks if task has been called with desired arguments"""
        task.assert_has_calls([call(*arg) for arg in args])

    @override_settings(CELERY_ALWAYS_EAGER=True)
    @patch('www.resource.models.convert_document_resource.run')
    def test_create_document_resource(self, task):
        """Tests creation of document resource."""
        self.create_and_go_to_example_content_item()
        document_resource = DocumentResourceFactory()
        self.click_add_resource_button_and_wait_for_form('document')
        self.fill_resource_from_model(document_resource, submit=False)
        self.submit_resource_form(self.WAIT_FOR_DOCUMENT_EDITOR)
        canary = "Test Canary"
        self.send_keys_to_simple_mde(canary)
        self.submit_resource_form()
        self.verify_document_conversions_triggered(task, [
            (document_resource.id, 'Printout_for_students'),
            (document_resource.id + 1, 'Printout_for_students')
        ])

    @override_settings(CELERY_ALWAYS_EAGER=True)
    @patch('www.resource.models.convert_document_resource.run')
    def test_edit_document_resource(self, task):
        """Test edition of document resource."""
        __, document_resource = self.create_and_go_to_content_item_with_document()
        edit_link = self.get_link_from_only_resource_in_content_item(".edit-link.data")
        edit_link.click()
        WebDriverWait(self.driver, 10).until(self.WAIT_FOR_DOCUMENT_EDITOR)
        canary = "Little yellow bird"
        self.send_keys_to_simple_mde(canary)
        self.submit_resource_form()
        self.verify_document_conversions_triggered(task, [
            (document_resource.id, 'Printout_for_students'),
            (document_resource.id, 'Printout_for_students')
        ])

    def test_edit_document_resource_metadata(self):
        """Test edition of document resource."""
        __, resource = self.create_and_go_to_content_item_with_document()
        edit_link = self.get_link_from_only_resource_in_content_item(".edit-link.metadata")
        edit_link.click()
        WebDriverWait(self.driver, 10).until(self.MODAL_VISIBLE_CONDITION)
        canary = "Little yellow bird"
        self.driver.find_element_by_id("id_title").send_keys(canary)
        self.submit_resource_form()
        resource.refresh_from_db()
        self.assertIn(canary, resource.title)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    @patch('www.resource.models.convert_document_resource.run')
    def test_edit_document_in_distraction_free_mode(self, task):
        """Tests distraction-free mode."""
        __, document_resource = self.create_and_go_to_content_item_with_document()
        self.driver.get(
            self.reverse_url("resource:document-upload", kwargs={'pk': document_resource.pk})
        )
        WebDriverWait(self.driver, 10).until(self.WAIT_FOR_DOCUMENT_EDITOR)
        canary = "Little yellow bird"
        self.send_keys_to_simple_mde(canary)
        with self.wait_for_page_load():
            self.driver.find_element_by_css_selector("button.btn-primary").click()
        self.verify_document_conversions_triggered(task, [
            (document_resource.id, 'Printout_for_students'),
            (document_resource.id, 'Printout_for_students')
        ])
