# coding=utf-8

"""
Utilities for many resource types.
"""
from unittest.case import SkipTest

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from www.content_item.test.utils import ContentItemFactory
from www.resource.models import Resource
from .test_content_item import ContentItemBaseTest


class TestResourceIsEditable(ContentItemBaseTest):
    """Utils for all resources"""

    def create_resource(self, content_item) -> Resource:  # pylint: disable=unused-argument,no-self-use
        """This needs to be overridden."""
        raise SkipTest

    def _test_resources(self, number_of_edit_links, owner=None):

        content_item = ContentItemFactory(owners=owner)
        self.create_resource(content_item)
        self.go_to_content_item(content_item)
        resources = self.get_all_resources()
        self.assertGreaterEqual(len(resources), 1)
        for resource in resources:
            edit_link = resource.find_elements_by_css_selector(".edit-link")
            # Edit links are hidden
            self.assertEqual(len(edit_link), number_of_edit_links)

    def test_resource_not_editable_for_non_owner(self):
        """Check if resource is not editable for a logged-in user who is not a document owner."""
        self.login()
        self._test_resources(0)

    def test_resource_editable_for_unlogged(self):
        """Check if resource is not editable and unlogged user."""
        self._test_resources(0)

    def test_resource_editable_for_logged_in(self):
        """Check if resource is editable for an logged-in owner."""
        self.login()
        self._test_resources(1, owner=self.user)


class TestLinkResourceUtils(ContentItemBaseTest):
    """ Utils for link resource tests """

    def wait_unit_link_visible(self, link_href: str):
        """
        :param linkStr: text of the link element
        :return: Wait until link with text is visible
        """
        link_xpath = "//a[contains(@class, 'resource-tile-linkresource-url') and @href='{}']".format(link_href)

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, link_xpath))
        )

    def click_delete_link_and_wait_for_form(self):
        """ Find element of 'delete-link' class and click it, then wait for form """
        self.driver.find_element_by_class_name('delete-link').click()
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(self.driver.find_element_by_id("generic-modal"))
        )
