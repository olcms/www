# -*- coding: utf-8 -*-
"""
Tests for file resources
"""
import hashlib
import os
import time

import typing

import requests

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from www.content_item import models as content_item_models
from www.integration_tests.resource_utils import TestResourceIsEditable
from www.resource import models as resource_models
from www.resource.models import Resource
from www.resource.tests.utils import FileResourceFactory
from www.utils.test_utils import model_rich_compare
from www.content_item.test.utils import ContentItemFactory
from .test_content_item import ContentItemBaseTest


# Pylint is not aware of pk attribute.
# pylint: disable=no-member,invalid-sequence-index,no-self-use

class TestFileResourceUtils(ContentItemBaseTest):

    """
    Class that contains useful methods for testing upload functionality.
    """
    USER_PERMS = [
        'filestorage.add_fileupload'
    ]

    def go_to_upload_dialog(
            self, file_size_mb=1.0
    ) -> typing.Tuple[content_item_models.ContentItem, resource_models.FileResource]:
        """Creates a content item, and fills first phase of file upload form."""
        original_item = self.create_and_go_to_example_content_item()
        self.assertEqual(0, original_item.resources.count())
        file = FileResourceFactory()
        FileResourceFactory.create_example_file(file, file_size_mb=file_size_mb, upload=False)
        self.click_add_resource_button_and_wait_for_form("file")
        self.fill_resource_from_model(file, submit=False)
        self.submit_resource_form(self.UPLOAD_FORM_VISIBLE_CONDITION)
        return original_item, file

    def set_file(self, file_resource: resource_models.FileResource):
        """Fills the file input."""
        file_element = self.driver.find_element_by_id("id_file")
        file_element.send_keys(file_resource.local_file_path)
        # NOTE: Workaround for a a known selenium bug. Send keys doesn't send onChange event.
        # Here we send synthetic change event
        self.driver.execute_script("document.getElementById('id_file').dispatchEvent(new Event('change'));")

    def upload_file(self):
        """Clicks upload button."""
        upload = self.driver.find_element_by_id("upload-button")
        self.wait_until_upload_button_clickable()
        upload.click()

    def wait_until_upload_success(self):
        """Waits until upload is successful."""
        # One of this uploads is 1 GB, so it can take some time especially on CI.
        wait = 20
        if os.environ.get("CI", False):
            wait = 5 * 60
        WebDriverWait(self.driver, wait).until(self.MODAL_HIDDEN_CONDITION)

    def wait_until_upload_error(self):
        """Waits until upload is is failed."""
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, "div.alert.alert-danger"))
        )

    def wait_until_upload_button_clickable(self):
        """Waits until upload button is clickable."""
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(
                (By.ID, "upload-button")
            )
        )

    def download_and_verify_file_locally(self, expected_file, db_file):
        """Downloads file from Swift and verifies contents are the same."""
        # Requests are used on purpose: we want to check file is downloadable without any auth
        self.assertIsNotNone(db_file.path)
        self.wait_until_file_downloadable(db_file.download_url)
        response = requests.get(db_file.download_url)
        self.assertEqual(response.status_code, 200)
        with open(expected_file.local_file_path, 'rb') as f:
            expected_bytes = f.read()
        self.assertEqual(response.content, expected_bytes)

    def verify_file_was_uploaded(self, expected_file, db_file):
        """Compares two file resources."""
        model_rich_compare(
            self, expected_file, db_file, exclude_pk=True, ignored_attrs=['path', 'content_item']
        )

    def wait_until_file_downloadable(self, download_url):
        """Waits until Swift processes the file, and file is downloadable."""
        for __ in range(100):
            response = requests.head(download_url)
            if response.status_code == 200:
                return
            time.sleep(.2)
        raise TimeoutError(
            "This test may fail if you are running tests and create new containers at the same time. "
            "For example during ./dev.sh test you run ./dev.sh lint. "
            "Failing url: {}".format(download_url)
        )

    def compute_file_checksum(self, file_path):
        """Computes file md5 checksum."""
        md5 = hashlib.md5()
        chunk_size = 1024 * 1024
        with open(file_path, 'rb') as f:
            chunk = f.read(chunk_size)
            while len(chunk) > 0:
                md5.update(chunk)
                chunk = f.read(chunk_size)
        return md5.hexdigest()

    def verify_file_checksum(self, expected_file, db_file):
        """
        Like download_and_verify_file_locally but doesn't download a file, compares locally computed md5
        with Swift's Etag.
        """
        local_checksum = self.compute_file_checksum(expected_file.local_file_path)
        self.assertIsNotNone(db_file.path)
        self.wait_until_file_downloadable(db_file.download_url)
        response = requests.head(db_file.download_url)
        self.assertEqual(response.status_code, 200)
        remote_checksum = response.headers['Etag']
        self.assertEqual(local_checksum, remote_checksum)


class TestFileResource(TestFileResourceUtils):
    """Tests for file resource"""

    def test_small_successful_file_upload(self):
        """
        Test small successful file upload.
        """
        with self.subTest("First upload"):
            original_item, file = self.go_to_upload_dialog()
            self.set_file(file)
            self.upload_file()
            self.wait_until_upload_success()
            self.assertEqual(1, original_item.resources.count())
            db_resource = original_item.resources.all()[0].fileresource
            self.verify_file_was_uploaded(file, db_resource)
            self.download_and_verify_file_locally(file, db_resource)
        with self.subTest("Second Upload"):
            new_file = FileResourceFactory()
            FileResourceFactory.create_example_file(new_file, file_size_mb=1, upload=False)
            self.wait().until(EC.visibility_of_element_located((By.CSS_SELECTOR, "a.upload-link")))
            self.driver.find_element_by_css_selector("a.upload-link").click()
            self.wait().until(self.UPLOAD_FORM_VISIBLE_CONDITION)
            self.set_file(new_file)
            self.upload_file()
            self.wait_until_upload_success()
            self.assertEqual(1, original_item.resources.count())
            db_resource = original_item.resources.all()[0].fileresource
            self.verify_file_was_uploaded(new_file, db_resource)
            self.download_and_verify_file_locally(new_file, db_resource)

    def test_large_file_upload(self):
        """
        Test successful upload for 128mb GB file.
        """
        original_item, file = self.go_to_upload_dialog(file_size_mb=128)
        self.set_file(file)
        self.upload_file()
        self.wait_until_upload_success()
        self.assertEqual(1, original_item.resources.count())
        db_resource = original_item.resources.all()[0].fileresource
        self.verify_file_was_uploaded(file, db_resource)
        self.verify_file_checksum(file, db_resource)

    def test_too_large_file_upload(self):
        """
        Test failed upload for 5 GB file (should raise file too big error)
        """
        __, file = self.go_to_upload_dialog(file_size_mb=5 * 1024)
        self.set_file(file)
        self.wait_until_upload_error()
        error_message = self.driver.find_element(By.CSS_SELECTOR, "div.alert.alert-danger")
        self.assertIn('File is too big', error_message.text)


class TestFileResourceNoPerms(TestFileResourceUtils):
    """
    Test where user has no permissions.
    """

    USER_PERMS = []

    def test_no_perms(self):
        """Test where user has no permissions."""
        original_item = self.create_and_go_to_example_content_item()
        self.assertEqual(0, original_item.resources.count())
        file = FileResourceFactory()
        FileResourceFactory.create_example_file(file, file_size_mb=1024, upload=False)
        self.click_add_resource_button_and_wait_for_form("file")
        self.fill_resource_from_model(file, submit=False)
        self.submit_resource_form(self.UPLOAD_FORM_VISIBLE_CONDITION)
        self.set_file(file)
        self.upload_file()
        self.wait_until_upload_error()
        error_message = self.driver.find_element(By.CSS_SELECTOR, "div.alert.alert-danger")
        self.assertIn("You don't appear to have rights to upload", error_message.text)


class TestFileResourceNotEditable(TestResourceIsEditable):
    """Checks edit links are hidden for non-owner users."""
    def create_resource(self, content_item) -> Resource:
        return FileResourceFactory(content_item=content_item)


class TestResourceIncomplete(ContentItemBaseTest):
    """Test if incomplete resources are visible only for owner"""

    def create_resource(self, content_item) -> Resource:
        """Create resource attached to content_item"""
        return FileResourceFactory(content_item=content_item)

    def __test_resources(self, number_of_resources, number_of_upload_links, path=None, owner=None):
        content_item = ContentItemFactory(owners=owner)
        FileResourceFactory(content_item=content_item, path=path)
        self.go_to_content_item(content_item)
        resources = self.get_all_resources()
        self.assertGreaterEqual(len(resources), number_of_resources)
        for resource in resources:
            upload_link = resource.find_elements_by_css_selector(".upload-link")
            # file upload links are hidden
            self.assertEqual(len(upload_link), number_of_upload_links)

    def test_incomplete_resource_not_visible_for_non_owner(self):
        """Check if incomplete resource is not visible for a logged-in user who is not a document owner."""
        self.login()
        self.__test_resources(0, 0)

    def test_incomplete_resource_upload_for_owner(self):
        """Check if has two upload links --- one normal and one from warning icon."""
        self.login()
        self.__test_resources(1, 2, owner=self.user)

    def test_complete_resource_no_upload_for_others(self):
        """Check if other users can see upload link for complete resource."""
        self.login()
        self.__test_resources(1, 0, path='/dummy/path')

    def test_complete_resource_no_upload_for_unlogged(self):
        """Check if unlogged users can see upload link for complete resource."""
        self.__test_resources(1, 0, path='/dummy/path')
