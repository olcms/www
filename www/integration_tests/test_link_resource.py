# -*- coding: utf-8 -*-
"""
Tests for link resources
"""

from www.content_item.test.utils import ContentItemFactory
from www.integration_tests.resource_utils import TestResourceIsEditable, TestLinkResourceUtils
from www.resource.models import Resource
from www.resource.tests.utils import LinkResourceFactory


# Pylint is not aware of pk attribute.
# pylint: disable=no-member,invalid-sequence-index,no-self-use


class TestLinkResource(TestLinkResourceUtils):
    """ Tests for link resources """

    def test_add_link_resource(self):
        """ Add link resource and check that it is present on list on GUI in content item, and also check DB """
        original_item = self.create_and_go_to_example_content_item()
        self.assertEqual(0, original_item.resources.count())
        self.click_add_resource_button_and_wait_for_form("link")

        link_res = LinkResourceFactory(content_item=original_item)
        self.fill_link_resource_from_model(link_res, True)

        self.wait_unit_link_visible(link_res.url)

        res_list = list(original_item.resources.all())
        self.assertEqual(2, len(res_list))  # There are two resources: link_res and this one created by Selenium
        resulting_link = res_list[0]
        self.assertEqual(link_res.title, resulting_link.title)
        self.assertEqual(link_res.url, resulting_link.linkresource.url)

    def test_delete_link_resource(self):
        """ Add link resource and then delete it, verify that resource is in fact gone"""
        self.login()
        original_item = ContentItemFactory(owners=self.user)
        self.assertEqual(0, original_item.resources.count())
        LinkResourceFactory(content_item=original_item)
        self.go_to_content_item(original_item)

        self.click_delete_link_and_wait_for_form()
        self.submit_resource_form()

        self.assertEqual(0, original_item.resources.all().count())


class TestLinkResourceNotEditable(TestResourceIsEditable):
    """Checks edit links are hidden for non-owner users."""

    def create_resource(self, content_item) -> Resource:
        return LinkResourceFactory(content_item=content_item)
