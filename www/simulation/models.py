# -*- coding: utf-8 -*-
"""Models module."""

from django.db import models
from django.utils.translation import ugettext_lazy

from www.resource import models as resource_models


class SimulationResource(resource_models.FileResource):
    """
    Simulation resource model.

    Inherits from FileResource.
    """

    @property
    def is_complete(self):
        """
        This resource is complete when:

        1) Zip file is uploaded
        2) Unpacked version is uploaded
        """
        return super().is_complete and self.bucket_prefix is not None

    @property
    def simulation_url(self):
        """
        :return: Returns full url to simulation.
        """
        if not self.is_complete:
            return None
        from .services import SIMULATION_SERVICE
        return SIMULATION_SERVICE.get_index_url(self.pk)

    index_file = models.CharField(
        verbose_name=ugettext_lazy("Main html file in this simulation"),
        default="index.html",
        max_length=1024
    )
    """
    Name of index file.    
    """

    bucket_prefix = models.TextField(
        null=True,
        blank=True,
        editable=False,
        verbose_name=ugettext_lazy(
            "Swift prefix under which this simulation is unpacked"
        )
    )
    """
    Bucket prefix or 'folder' where bucket_prefix is uploaded.
    """

    upload_error = models.TextField(
        editable=False,
        null=True,
        blank=True,
        verbose_name=ugettext_lazy(
            "Any error encountered while uploading this resource."
        )
    )
    """Upload error message."""
