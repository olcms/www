# -*- coding: utf-8 -*-
"""Apps module for simulation."""
from django.apps import AppConfig


class SimulationConfig(AppConfig):
    """Simulation app"""
    name = 'www.simulation'
