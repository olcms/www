"""Module with celery tasks."""
# -*- coding: utf-8 -*-

from celery import Task, shared_task
from celery.utils.log import get_task_logger


from www.simulation.services import SIMULATION_SERVICE


logger = get_task_logger(__name__)


class SimulationTask(Task):  # pylint: disable=abstract-method,no-self-use

    """
    Base class that uploads simulation.

    Used by concrete task: start_simulation_upload below.

    """

    abstract = True
    max_retries = 5

    def upload_simulation(self, simulation_pk: int):  # pylint: disable=no-self-use
        """Actually upload simulation."""
        SIMULATION_SERVICE.upload_to_storage(simulation_pk)  # pragma: no cover


@shared_task(
    bind=True,
    name='simulation.upload',
    base=SimulationTask,
    throws=(Exception,),
    ignore_result=True
)
def start_simulation_upload(self, simulation_pk):
    """Task to call to upload the simulation."""
    try:
        self.upload_simulation(simulation_pk)
    except Exception as exc:
        logger.exception('There was exception while simulation for resource: %s', simulation_pk)
        raise self.retry(exc=exc, countdown=60 * 10)
