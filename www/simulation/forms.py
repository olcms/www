# -*- coding: utf-8 -*-
"""Forms module"""

from django import forms

from . import models


class SimulationResourceCreateForm(forms.ModelForm):
    """Form that creates simulation resource."""

    class Meta:
        """Meta Model."""
        model = models.SimulationResource
        fields = [
            'title',
            'index_file',
            'resource_license',
            'role',
            'description',
            'default',
        ]
        widgets = {
            'description': forms.Textarea(attrs={'rows': 5}),
        }


class SimulationFileUploadForm(forms.ModelForm):
    """Form for uploading simulations."""
    class Meta:
        model = models.SimulationResource
        fields = ['path']
        widgets = {"path": forms.HiddenInput}
