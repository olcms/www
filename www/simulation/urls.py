# -*- coding: utf-8 -*-

"""Urls for simulation app."""

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^create/(?P<content_item_id>\d+)/?$',
        view=views.SimulationCreateView.as_view(),
        name='create'
    ),
    url(
        regex=r'^create/(?P<content_item_id>\d+)/?$',
        view=views.QuizCreateView.as_view(),
        name='createQuiz'
    ),
    url(
        regex=r'^upload/(?P<pk>\d+)/?$',
        view=views.SimulationUploadView.as_view(),
        name='upload'
    ),
]
