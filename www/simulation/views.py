# -*- coding: utf-8 -*-
"""Views module."""

from django.forms import ModelForm
from django.http import HttpResponseRedirect
from django.urls import reverse

from www.resource import models as resource_models, views as resource_views
from www.resource.views import BaseResourceCreateView
from www.simulation.tasks import start_simulation_upload
from . import models, forms


# This is standard Django view, and probably we need to rework resource
# upload to make it more user friendly.
class SimulationCreateView(BaseResourceCreateView):  # pragma: no cover
    """
    This works exactly like FileResource except it unpacks simulation
    to container.
    """

    model = models.SimulationResource
    form_class = forms.SimulationResourceCreateForm
    template_name = "resource/resource_form.html"
    resource_type = resource_models.ResourceType.simulation

    def get_success_http_response(self):
        return HttpResponseRedirect(
            reverse("simulation:upload", kwargs={"pk": self.object.pk})
        )


# This is standard Django view, and probably we need to rework resource
# upload to make it more user friendly.
class SimulationUploadView(resource_views.FileResourceUploadView):  # pragma: no cover
    """
    Upload view.
    """

    model = models.SimulationResource
    form_class = forms.SimulationFileUploadForm
    template_name = "resource/file_resource_upload_form.html"
    resource_type = resource_models.ResourceType.simulation

    def form_valid(self, form: ModelForm):
        response = super().form_valid(form)
        start_simulation_upload.delay(simulation_pk=self.object.pk)
        return response


# This is standard Django view, and probably we need to rework resource
# upload to make it more user friendly.
class QuizCreateView(BaseResourceCreateView):  # pragma: no cover
    """
    This works exactly like FileResource except it unpacks simulation
    to container.
    """

    model = models.SimulationResource
    form_class = forms.SimulationResourceCreateForm
    template_name = "resource/resource_form.html"
    resource_type = resource_models.ResourceType.simulation

    def get_success_http_response(self):
        return HttpResponseRedirect(
            reverse("simulation:upload", kwargs={"pk": self.object.pk})
        )
