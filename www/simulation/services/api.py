# -*- coding: utf-8 -*-

"""API for simulation service."""

import abc

import typing

UploadResponse = typing.NamedTuple(
    "UploadResponse",
    (
        ('success', bool),
        ('fail_reason', typing.Union[str]),
    )
)


class SimulationService(object, metaclass=abc.ABCMeta):

    """Simulation service API."""

    def upload_to_storage(self, simulation_resource_pk: int) -> None:
        """
        Upload resource to storage. Resource is passed via
        primary key so we can easily launch this from tasks.

        You can and should edit resource during the upload, especially
        you should set:

        * upload_error --- if upload was failed
        * any other field you need to successfully recover
          index url from get_index_url.
        """
        raise NotImplementedError

    def get_index_url(self, simulation_resource_pk: int) -> str:
        """
        Return index url for the resource in the storage.
        """
        raise NotImplementedError

    def delete_simulation(self, simulation_resource_pk: int) -> None:
        """Deletes simulation from storage."""
        raise NotImplementedError
