# coding=utf-8

"""
Only implementation of simulation service.

This implementation uploads simulations to swift container.
"""

import os
import shutil
import tempfile
import typing
import zipfile
from urllib import parse

from django.conf import settings
from django.utils.crypto import get_random_string
from slugify import slugify
from swiftclient.service import SwiftService, SwiftUploadObject, get_conn

from config.settings.common import SWIFT_SIMULATION_CONTAINER
from .api import SimulationService
from .utils import (
    download_file,
    InvalidZipContents,
    is_simulation_valid,
    get_file_list
)
from .. import models


class SwiftException(Exception):

    """Raised on Swift exception."""

    def __init__(self, errors: typing.Sequence[typing.Mapping], message: str=None):
        errors = list(errors)
        super().__init__(errors, message)
        self.errors = errors
        self.message = message


class SafeSwiftService(object):

    """
    Thin wrapper around SwiftService that checks for errors.
    """

    def __init__(self, container, options):
        self.options = options
        self.__container = container
        self.__service = SwiftService(options=options)
        self.swift_url = None
        self.check_container()
        self.base_url = self.__obtain_base_url()

    def __obtain_base_url(self):
        # We need to read options preprocessed by swift service.
        conn = get_conn(self.__service._options)  # pylint: disable=protected-access
        try:
            url, __ = conn.get_auth()
            return url
        finally:
            conn.close()

    @classmethod
    def __filter_error_responses(
            cls, responses: typing.Union[typing.Mapping, typing.Iterable[typing.Mapping]]
    ) -> typing.Iterable[typing.Mapping]:

        if isinstance(responses, typing.Mapping):
            if responses['success']:
                return []
            return [responses]

        return (
            response for response in responses
            if not response['success']
        )

    @classmethod
    def __verify_response(cls, response) -> None:
        errors = list(cls.__filter_error_responses(response))
        if errors:
            raise SwiftException(errors)

    def check_container(self):
        """Stats container --- should fail if we don't have access."""
        try:
            self.__verify_response(self.__service.stat(self.__container))
        except SwiftException as e:
            raise SwiftException(e.errors, "Most probably this is auth error.")

    def delete_prefix(self, prefix: str):
        """Delete "folder" on swift."""
        self.__verify_response(self.__service.delete(
            self.__container,
            options={'prefix': prefix}
        ))

    def upload_container(self, sources: typing.Iterable[SwiftUploadObject]):
        """Uploads objects to container."""
        self.__verify_response(self.__service.upload(
            self.__container, sources
        ))


class _SwiftSimulationImpl(object):

    """Stateful object that performs operations on behalf of SwiftSimulationService."""

    def __init__(self, simulation_pk: int) -> None:
        super().__init__()
        self.service = SafeSwiftService(
            container=settings.SWIFT_SIMULATION_CONTAINER,
            options={
                "auth_version": settings.SWIFT_AUTH_VERSION,
                "auth": settings.SWIFT_AUTH_URL,
                "user": settings.SWIFT_USERNAME,
                "key": settings.SWIFT_KEY,
                "debug": True,
            }
        )
        self.simulation = models.SimulationResource.objects.get(pk=simulation_pk)
        self.tempdir = tempfile.mkdtemp()

    @classmethod
    def __download_zip(cls, simulation: models.SimulationResource) -> str:
        return download_file(simulation.download_url)

    def validate_zip_file(self, zip_file: zipfile.ZipFile) -> str:
        """
        Validates zip file and returns common prefix.

        See is_simulation_valid for documentation what is common prefix.

        Will raise on invalid file.
        """
        validation_result = is_simulation_valid(
            self.simulation.index_file,
            get_file_list(zip_file)
        )
        if not validation_result.is_valid:
            self.simulation.upload_error = validation_result.invalid_reason
            self.simulation.save(update_fields=['upload_error'])
            raise InvalidZipContents("Invalid zip file")
        return validation_result.common_prefix

    def unpack_and_upload(
            self,
            local_zip_file_name: str
    ):
        """
        Unpack, validate and upload zip file.
        :param local_zip_file_name:
        :return:
        """
        def to_swift_upload(
                zip_info: zipfile.ZipInfo,
                zip_file: zipfile.ZipFile,
                common_prefix: str
        ) -> SwiftUploadObject:
            """Converts ZipExtFile to SwiftUploadObject."""

            entry_name = zip_info.filename
            entry = zip_file.open(entry_name)

            if common_prefix:
                entry_name = entry_name[len(common_prefix) + 1:]

            temp = tempfile.NamedTemporaryFile(delete=False, dir=self.tempdir)

            with open(temp.name, 'wb') as file:
                file.write(entry.read())

            swift_name = self.simulation.bucket_prefix + "/" + entry_name
            return SwiftUploadObject(
                source=temp.name,
                object_name=swift_name
            )

        zip_file = zipfile.ZipFile(local_zip_file_name)
        try:
            common_prefix = self.validate_zip_file(zip_file)
            self.service.upload_container((
                to_swift_upload(zip_info, zip_file, common_prefix)
                for zip_info in zip_file.filelist
                if not common_prefix or len(common_prefix) < len(zip_info.filename)
            ))
        finally:
            zip_file.close()

    def ensure_random_prefix(self):
        """Update simulation object with unique prefix (or 'folder')."""
        if not self.simulation.bucket_prefix:
            self.simulation.bucket_prefix = "{}/{}".format(
                get_random_string(12),
                slugify(self.simulation.title)
            )
            self.simulation.save(update_fields=['bucket_prefix'])

    def upload_simulation(self):
        """Upload simulation to swift."""

        self.ensure_random_prefix()

        zip_file_name = self.__download_zip(self.simulation)
        try:
            self.unpack_and_upload(zip_file_name)
        finally:
            os.remove(zip_file_name)
            shutil.rmtree(self.tempdir)

    def delete_simulation(self):
        """Delete simulation from storage"""
        self.service.delete_prefix(prefix=self.simulation.bucket_prefix)

    def get_index_url(self):
        """Return index url for simulation."""
        prefix = self.simulation.bucket_prefix
        assert isinstance(prefix, str)
        prefix = prefix.lstrip('/').rstrip('/')

        url = '/' + SWIFT_SIMULATION_CONTAINER + "/" + prefix + "/" + self.simulation.index_file

        return self.service.base_url + parse.quote(url)


class SwiftSimulationService(SimulationService):
    """Implementation of Simulation service that uses Swift object store service."""

    def upload_to_storage(self, simulation_resource_pk: int) -> None:
        _SwiftSimulationImpl(simulation_resource_pk).upload_simulation()

    def get_index_url(self, simulation_resource_pk: int) -> str:
        return _SwiftSimulationImpl(simulation_resource_pk).get_index_url()

    def delete_simulation(self, simulation_resource_pk: int) -> None:
        _SwiftSimulationImpl(simulation_resource_pk).delete_simulation()
