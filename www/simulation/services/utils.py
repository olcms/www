# coding=utf-8
"""Services module"""

import re
import shutil
import typing
import zipfile
from tempfile import NamedTemporaryFile

import requests
from django.utils.translation import ugettext_lazy


# pylint: disable=not-callable

def download_file(url: str) -> str:
    """Downloads file via requests."""
    r = requests.get(url, stream=True)

    assert r.status_code == 200, "Couldn't download simulation ZIP"

    with NamedTemporaryFile(delete=False) as f:
        shutil.copyfileobj(r.raw, f)
        return f.name


class InvalidZipContents(Exception):
    """Raised when ZIP file doesn't meet our requirements."""
    pass


def get_file_list(zip_file: zipfile.ZipFile) -> typing.Set[str]:
    """Get list of files in zip archive."""
    return {
        zip_info.filename.strip()
        for zip_info in zip_file.filelist
    }


SimulationValidResponse = typing.NamedTuple(
    "SimulationValidResponse",
    (
        ("is_valid", bool),
        ("common_prefix", typing.Optional[str]),
        ("invalid_reason", typing.Optional["str"])
    )
)


def strip_common_prefix(
        common_prefix, zip_file: typing.Set[str]
) -> typing.Set[str]:
    """
    Strips common prefix from a list of files.
    """
    # Strip prefix + "/"
    strip_len = len(common_prefix) + 1
    return {
        name[strip_len:]
        for name in zip_file
        if len(name) > strip_len
    }


def _has_up_entries(zip_file: typing.Set[str]):
    """Checks if zip files contain entries that go 'up'."""
    regex = re.compile(r"^\.+$")

    def __has_up_entry(name: str) -> bool:
        return any(regex.match(part) for part in name.split("/"))

    return any(__has_up_entry(name) for name in zip_file)


def is_simulation_valid(
        index_name, zip_file: typing.Set[str]
) -> SimulationValidResponse:
    """
    Performs basic sanity checks on zip_file.

    File is considered valid if and only if:

    1. It has no absolute paths
    2. Has no "." or ".+" entries.
    2. It contains file named "index_name" OR all entries are in a single folder
       and after stripping it we can find index file.

       Second condition is done as a convenience for users that create a zip
       file by assembling all files in a directory and then compressing this
       directory.

       Example (in all examples "index_name" is "index.html"

       1. ["index.html", "image.png"] --- is valid
       2. ["index.html", "images/image.png"] --- is valid
       3. ["foo/index.html", "foo/image.png"] --- is valid and we need to strip "foo".
       4. ["image.png"] --- is not valid (no index file)
       5. ["foo/index.html", "bar/image.png"] --- is not valid as there is no common prefix;
       6. ["foo/bar/index.html", "foo/bar/image.png"] ---
          is not valid as we only strip a single folder;
    """
    has_absolute_paths = any(
        name.strip().startswith("/")
        for name in zip_file
    )
    if has_absolute_paths:
        return SimulationValidResponse(False, None, "Upload contains absolute paths")
    if _has_up_entries(zip_file):
        return SimulationValidResponse(False, None, "Upload contains '..' entries")
    if index_name in zip_file:
        return SimulationValidResponse(True, None, None)
    folder_prefixes = {
        name.split("/")[0]
        for name in zip_file
    }
    if len(list(folder_prefixes)) == 1:
        common_prefix = next(iter(folder_prefixes))
        stripped_file = strip_common_prefix(common_prefix, zip_file)
        if index_name in stripped_file:
            return SimulationValidResponse(
                True, common_prefix, None
            )
    return SimulationValidResponse(
        False, None, ugettext_lazy("Couldn't find {}".format(index_name))
    )
