# -*- coding: utf-8 -*-

"""Main service import point."""
import importlib

from django.conf import settings
from django.utils.functional import LazyObject


class LazySimulationService(LazyObject):
    """Lazy object representing simulation service."""

    def _setup(self):
        package, service_type = settings.SIMULATION_SERVICE.rsplit(".", 1)
        imported = importlib.import_module(package)
        service_type = getattr(imported, service_type)
        self._wrapped = service_type()


SIMULATION_SERVICE = LazySimulationService()
"""
Use this. It will be auto-populated with proper service.
"""
