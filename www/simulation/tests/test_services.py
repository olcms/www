# coding=utf-8
from django.test import TestCase

from www.simulation.services.swift_service import _SwiftSimulationImpl, \
    SwiftException, SwiftSimulationService
from www.simulation.services.utils import InvalidZipContents
from . import factories


class TestServiceErrorHandling(TestCase):

    def test_container(self):
        resource = factories.SimulationResourceFactory()
        with self.assertRaises(SwiftException):
            with self.settings(SWIFT_KEY="invalid key"):
                self.service = _SwiftSimulationImpl(resource.pk)


class TestUploadError(TestCase):

    def setUp(self):
        self.resource = factories.SimulationResourceFactory()
        factories.SimulationResourceFactory.create_invalid_resource(self.resource)
        self.service = SwiftSimulationService()

    def test_invalid_validation(self):
        with self.assertRaises(InvalidZipContents):
            self.service.upload_to_storage(self.resource.pk)
