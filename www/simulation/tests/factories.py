# -*- coding: utf-8 -*-

import pathlib

from django.core.files.storage import default_storage

from www.resource.tests.utils import FileResourceFactory
from .. import models

EXAMPLE_SIMULATION = pathlib.Path(__file__).parent / 'data' / 'simulation-example.zip'

INVALID_SIMULATION = pathlib.Path(__file__).parent / 'data' / 'invalid-simulation.zip'


class SimulationResourceFactory(FileResourceFactory):

    index_file = "index.html"

    bucket_prefix = None

    @staticmethod
    def create_example_resource(res):
        res.path = SimulationResourceFactory.__upload_to_swift(
            EXAMPLE_SIMULATION.name,
            str(EXAMPLE_SIMULATION)
        )
        res.save()

    @staticmethod
    def create_invalid_resource(res):
        res.path = SimulationResourceFactory.__upload_to_swift(
            INVALID_SIMULATION.name,
            str(INVALID_SIMULATION)
        )
        res.save()

    @staticmethod
    def __upload_to_swift(file_name, contents) -> str:
        with open(contents, 'rb') as f:
            return default_storage.save(file_name, f)

    class Meta:
        model = models.SimulationResource

