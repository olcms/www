# -*- coding: utf-8 -*-

import requests
from django.test import TestCase

from . import factories
from .. import services


class TestModels(TestCase):

    def setUp(self):
        self.resource = factories.SimulationResourceFactory()
        factories.SimulationResourceFactory.create_example_resource(self.resource)

    def test_manual_upload(self):
        services.SIMULATION_SERVICE.upload_to_storage(self.resource.pk)

        self.resource.refresh_from_db()

        simulation_url = self.resource.simulation_url
        assert requests.get(simulation_url).status_code == 200

    def test_delete(self):
        services.SIMULATION_SERVICE.upload_to_storage(self.resource.pk)

        self.resource.refresh_from_db()
        simulation_url = self.resource.simulation_url

        services.SIMULATION_SERVICE.delete_simulation(self.resource.pk)
        assert requests.get(simulation_url).status_code == 404


    def test_not_completed_url(self):
        """When bucket_prefix is None, is_complete is false and simulation url is None"""
        self.resource.bucket_prefix = None

        self.assertFalse(self.resource.is_complete)
        self.assertIsNone(self.resource.simulation_url)






