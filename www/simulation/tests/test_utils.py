# -*- coding: utf-8 -*-

import pathlib
from zipfile import ZipFile

from django.core.files.storage import default_storage
from django.test import SimpleTestCase

from ..services import utils

EXAMPLE_SIMULATION = pathlib.Path(__file__).parent / 'data' / 'simulation-example.zip'


class TestTasks(SimpleTestCase):

    def test_is_valid_positive(self):
        examples = (
            ({"index.html", "image.png"}, "index.html", None),
            ({"index.html", "imgs/image.png"}, "index.html", None),
            ({"foo/index.html", "foo/image.png"}, "index.html", "foo"),
            (
                {'simulation-example/', 'simulation-example/index.html', 'simulation-example/logo.png'},
                "index.html",
                "simulation-example"
            )
        )
        for example in examples:
            with self.subTest(example):
                files, index, prefix = example
                validation_response = utils.is_simulation_valid(index, files)
                self.assertTrue(validation_response.is_valid)
                self.assertEquals(prefix, validation_response.common_prefix)
                self.assertIsNone(validation_response.invalid_reason)

    def test_is_valid_negative(self):
        examples = (
            ({"image.png"}, "index.html", "Couldn't find index.html"),
            ({"foo/index.html", "bar/image.png"}, "index.html", "Couldn't find index.html"),
            ({"foo/bar/index.html", "foo/bar/image.png"}, "index.html", "Couldn't find index.html"),
            (
                {"index.html", "image.png", "../image.png"},
                "index.html",
                "Upload contains '..' entries"
            ),
            (
                {"index.html", "image.png", "./image.png"},
                "index.html",
                "Upload contains '..' entries"
            ),
            (
                {"index.html", "image.png", ".../image.png"},
                "index.html",
                "Upload contains '..' entries"
            ),
            (
                {"/index.html", "image.png", ".../image.png"},
                "index.html",
                "Upload contains absolute paths"
            ),

        )

        for example in examples:
            with self.subTest(examples):
                files, index,  error_msg = example
                validation_response = utils.is_simulation_valid(index, files)
                self.assertFalse(validation_response.is_valid)
                self.assertEquals(error_msg, validation_response.invalid_reason)
                self.assertIsNone(validation_response.common_prefix)

    def test_get_filelist(self):
        self.assertEquals(
            utils.get_file_list(ZipFile(EXAMPLE_SIMULATION.open("rb"))),
            {'simulation-example/', 'simulation-example/index.html', 'simulation-example/logo.png'}
        )

    def test_download_file_invalid(self):
        with self.assertRaises(AssertionError):
            utils.download_file(default_storage.url("no/such/file"))
