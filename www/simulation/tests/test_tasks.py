"""Tests tasks.py module."""
import requests
import time

from django.test import TestCase

from www.simulation.services.utils import InvalidZipContents
from . import factories
from .. import tasks


class TestTasks(TestCase):

    """Tests tasks.py module."""

    def test_upload(self):
        """Test that simulation upload task works."""
        resource = factories.SimulationResourceFactory()
        factories.SimulationResourceFactory.create_example_resource(resource)
        tasks.start_simulation_upload(resource.pk)
        for __ in range(30):
            resource.refresh_from_db()
            if requests.get(resource.simulation_url).status_code == 200:
                return
            else:
                time.sleep(.1)
        assert False, "Couldn't download simulation for 3 sec"

    def test_invalid_upload(self):
        """Test that simulation upload task works."""
        resource = factories.SimulationResourceFactory()
        factories.SimulationResourceFactory.create_invalid_resource(resource)
        with self.assertRaises(InvalidZipContents):
            tasks.start_simulation_upload(resource.pk)
        resource.refresh_from_db()
        assert requests.get(resource.simulation_url).status_code == 404
