# coding=utf-8
# pylint: disable=missing-docstring,no-member,no-self-use,protected-access
from django.test import TestCase

from www.content_item.models import Domain
from www.content_item.test.utils import ContentItemFactory
from www.landing_page.utils import DomainUtils


class TestUtils(TestCase):

    def make_test_data(
            self, discipline="physics", count=11, featured=False, language="en"
    ):
        for __ in range(count):
            cif = ContentItemFactory(
                main_discipline=Domain.objects.get(slug=discipline),
                featured=featured,
                language=language
            )
            cif.domains.add(Domain.objects.get(slug=discipline))

    def test_language_codes(self):
        self.make_test_data(count=1, language="en")
        self.make_test_data(count=1, language="pl")
        self.make_test_data(count=1, language="el")
        utils = DomainUtils(Domain.objects.get(slug="physics"))
        self.assertEqual(utils.get_content_item_language_codes(), ('el', 'en', 'pl'))

    def test_language_codes_2(self):
        self.make_test_data(count=1, language="en", discipline="physics")
        self.make_test_data(count=1, language="pl", discipline="chemistry")
        self.make_test_data(count=1, language="el", discipline="mathematics")
        utils = DomainUtils(Domain.objects.get(slug="physics"))
        self.assertEqual(utils.get_content_item_language_codes(), ('en',))

    def test_language_codes_3(self):
        self.make_test_data(count=1, language="en", discipline="physics")
        self.make_test_data(count=1, language="pl", discipline="chemistry")
        self.make_test_data(count=1, language="el", discipline="mathematics")
        utils = DomainUtils(None)
        self.assertEqual(utils.get_content_item_language_codes(), ('el', 'en', 'pl'))
        utils = DomainUtils(Domain(slug="none"))
        self.assertEqual(utils.get_content_item_language_codes(), ('el', 'en', 'pl'))

    def test_get_language_for_user(self):
        self.make_test_data(count=1, language="en")
        self.make_test_data(count=1, language="pl")
        self.make_test_data(count=1, language="el")
        utils = DomainUtils(Domain.objects.get(slug="physics"))
        self.assertEqual(
            utils.get_language_for_user("en"),
            "en"
        )
        self.assertEqual(
            utils.get_language_for_user("el"),
            "el"
        )
        self.assertEqual(
            utils.get_language_for_user("it"),
            "en"
        )

    def test_get_language_for_user_2(self):
        self.make_test_data(count=1, language="pl")
        self.make_test_data(count=1, language="el")
        utils = DomainUtils(Domain.objects.get(slug="physics"))
        self.assertEqual(
            utils.get_language_for_user("en"),
            "el"
        )
        self.assertEqual(
            utils.get_language_for_user("el"),
            "el"
        )
        self.assertEqual(
            utils.get_language_for_user("it"),
            "el"
        )

    def test_get_language_for_user_3(self):
        utils = DomainUtils(Domain.objects.get(slug="physics"))
        self.assertEqual(
            utils.get_language_for_user("en"),
            None
        )
