# coding=utf-8
# pylint: disable=missing-docstring,no-member,no-self-use,protected-access

from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls.base import reverse

from www.content_item.models import Domain, ContentItem
from www.content_item.test.utils import ContentItemFactory
from www.landing_page.utils import DomainUtils
from www.landing_page.views import LandingPage


class TestLandingPageUnit(TestCase):

    def make_test_data(
            self, discipline="physics", count=11, featured=False, language="en"
    ):
        for __ in range(count):
            ContentItemFactory(
                main_discipline=Domain.objects.get(slug=discipline),
                featured=featured,
                language=language
            )

    def test_no_discipline(self):
        response = self.client.get(reverse("landing-discipline"))
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(
            response.context_data['current_discipline'].slug,
            'none'
        )
        self.assertEqual(
            response.context_data['discipline_description_block'],
            'discipline.description.none',
        )
        self.assertEqual(
            response.context_data['discipline_block'],
            'discipline.none',
        )
        self.assertEqual(
            response.context_data['discipline_cover'],
            'img/disciplines/none.jpg',
        )
        self.assertEqual(
            response.context_data['toolbar_styling_discipline'],
            'medicine',
        )

    def test_discipline(self):
        response = self.client.get(
            reverse("landing-discipline", kwargs={'discipline': 'physics'})
        )

        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(
            response.context_data['current_discipline'].slug,
            'physics'
        )
        self.assertEqual(
            response.context_data['discipline_block'],
            'discipline.physics',
        )
        self.assertEqual(
            response.context_data['discipline_description_block'],
            'discipline.description.physics',
        )
        self.assertEqual(
            response.context_data['discipline_cover'],
            'img/disciplines/physics.jpg',
        )
        self.assertEqual(
            response.context_data['toolbar_styling_discipline'],
            'physics',
        )

    def test_sort_qs(self):
        self.make_test_data(count=5)
        qs = ContentItem.objects.all()
        self.assertEqual(
            LandingPage._limit_and_sort_qs(qs),
            list(qs)
        )

    def test_sort_qs_over_limit(self):
        self.make_test_data(count=20)
        self.assertEqual(
            LandingPage._limit_and_sort_qs(ContentItem.objects.all()),
            list(ContentItem.objects.all()[:10])
        )

    def test_get_queryset_no_featured(self):
        self.make_test_data(featured=False, discipline="physics", count=10)
        self.make_test_data(featured=True, discipline="chemistry")
        self.make_test_data(featured=False, discipline="chemistry")
        physics = Domain.objects.get(slug='physics')
        self.assertQuerysetEqual(
            LandingPage._get_query_set(DomainUtils(physics)),
            ContentItem.objects.filter(main_discipline=physics),
            ordered=True,
            transform=lambda x: x,
        )

    def test_get_queryset_no_featured_too_many(self):
        self.make_test_data(featured=False, discipline="physics", count=12)
        self.make_test_data(featured=True, discipline="chemistry")
        self.make_test_data(featured=False, discipline="chemistry")
        physics = Domain.objects.get(slug='physics')
        self.assertQuerysetEqual(
            LandingPage._get_query_set(DomainUtils(physics)),
            ContentItem.objects.filter(main_discipline=physics),
            ordered=False,
            transform=lambda x: x,
        )

    def test_get_queryset_featured(self):
        self.make_test_data(featured=True, discipline="physics", count=5)
        self.make_test_data(featured=True, discipline="physics", count=123)
        self.make_test_data(featured=True, discipline="chemistry")
        self.make_test_data(featured=False, discipline="chemistry")
        physics = Domain.objects.get(slug='physics')
        self.assertQuerysetEqual(
            LandingPage._get_query_set(DomainUtils(physics)),
            ContentItem.objects.filter(main_discipline=physics, featured=True),
            ordered=False,
            transform=lambda x: x
        )

    def test_get_queryset_featured_all_domains(self):
        self.make_test_data(featured=True, discipline="physics", count=5)
        self.make_test_data(featured=True, discipline="chemistry", count=5)
        self.make_test_data(featured=False, discipline="physics", count=123)
        physics = Domain(slug='none', name="No discipline")
        self.assertQuerysetEqual(
            LandingPage._get_query_set(DomainUtils(physics)),
            ContentItem.objects.filter(featured=True),
            ordered=False,
            transform=lambda x: x,
        )

    def test_switch_language_redirect(self):
        """
        This is regression test. Set language does url translation (despite
        this url is not translatable). This would replace "correct" /discipline
        to (incorrect) /discipline/None. U have added extra url mapping to fix
        this and this is regression test for this issue.

        """
        response = self.client.post(reverse("set_language"), data={
            "next": "/discipline",
            "language": "pl"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/discipline")
