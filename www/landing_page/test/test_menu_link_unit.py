# coding=utf-8
# pylint: disable=missing-docstring,no-member,no-self-use,protected-access
from django.test import TestCase
from django.urls import reverse

from .. import models


class TestMenuLinks(TestCase):

    def test_menu_link_with_no_labels(self):

        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/bar",
        )
        assert link.label is None

    def test_menu_link_not_active(self):

        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/bar",
        )
        assert not link.active

    def test_menu_link_active(self):

        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/foo",
        )
        assert link.active

    def test_menu_link_active_2(self):

        link = models.MenuLink.objects.create(url="/foo/")
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/foo",
        )
        assert link.active

    def test_menu_link_translation(self):
        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLinkLabel.objects.create(
            link=link,
            language="pl",
            label="Strona główna"
        )
        models.MenuLinkLabel.objects.create(
            link=link,
            language="en",
            label="Main page"
        )
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/foo",
            "pl"
        )
        assert link.label == "Strona główna"

    def test_menu_link_translation_2(self):
        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLinkLabel.objects.create(
            link=link,
            language="pl",
            label="Strona główna"
        )
        models.MenuLinkLabel.objects.create(
            link=link,
            language="en",
            label="Main page"
        )
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/foo",
            "en"
        )
        assert link.label == "Main page"

    def test_menu_link_translation_default(self):
        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLinkLabel.objects.create(
            link=link,
            language="pl",
            label="Strona główna"
        )
        models.MenuLinkLabel.objects.create(
            link=link,
            language="en",
            label="Main page",
            default_translation=True
        )
        models.MenuLink.objects.update_menu_link_for_display(
            link,
            "/foo",
            "el"
        )
        assert link.label == "Main page"

    def test_page_html(self):
        link = models.MenuLink.objects.create(url="/foo")
        models.MenuLinkLabel.objects.create(
            link=link,
            language="en",
            label="AAA CANARY AAA 5noFMirdq6",
            default_translation=True
        )
        response = self.client.get(reverse("landing-discipline"))
        assert b"AAA CANARY AAA 5noFMirdq6" in response.content
