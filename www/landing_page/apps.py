# coding=utf-8

"""Apps module."""

from django.apps import AppConfig


class LandingPageConfig(AppConfig):
    """Landing page app."""

    name = 'www.landing_page'
