# coding=utf-8
"""Models module for landing_page app"""
import typing

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy
from ordered_model.models import OrderedModel


class MenuLinkManager(models.Manager):
    """Manager for MenuLink model"""

    def get_language_version(  # pylint: disable=no-self-use
            self,
            menu_link: 'MenuLink',
            desired_language: typing.Optional['str']
    ) -> typing.Optional['MenuLink']:
        """
        Return valid language version for MenuLinK label.

        Will find menu link with desired language, if none exist will return
        default one.
        """

        translations = MenuLinkLabel.objects.filter(link=menu_link)
        if desired_language is not None:
            try:
                return translations.get(language=desired_language)
            except ObjectDoesNotExist:
                pass
        return translations.filter(default_translation=True).first()

    def update_menu_link_for_display(
            self,
            link: 'MenuLink',
            request_path: 'str',
            desired_language: typing.Optional['str']=None
    ):
        """Updates menu link for display in template."""
        translated_label = self.get_language_version(link, desired_language)
        link.active = request_path == link.url.rstrip('/')
        if translated_label is not None:
            link.label = translated_label.label
        else:
            link.label = None
        return link


class MenuLink(OrderedModel):

    """Represents a single link in menu."""

    url = models.CharField(
        verbose_name=ugettext_lazy("URL this item points to."),
        blank=True,
        null=False,
        max_length=1024,
        unique=True
    )

    objects = MenuLinkManager()


class MenuLinkLabel(models.Model):

    """Represents a label for language for menu item."""

    link = models.ForeignKey(MenuLink, related_name="labels")

    language = models.CharField(max_length=2, choices=settings.LANGUAGES)
    default_translation = models.BooleanField(default=False)

    label = models.CharField(
        verbose_name=ugettext_lazy("Link label"),
        blank=False,
        null=False,
        max_length=1024
    )

    class Meta:
        unique_together = [
            ['link', 'language']
        ]


@receiver(pre_save, sender=MenuLinkLabel)
def update_default_translation(instance, **kwargs):  # pylint: disable=unused-argument
    """If this is default translation make all other translations non-default."""

    if instance.default_translation:
        MenuLinkLabel.objects.filter(link=instance.link).exclude(pk=instance.pk).update(default_translation=False)
