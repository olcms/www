# coding=utf-8

"""Views."""


from django.shortcuts import get_object_or_404
from django.utils.translation import get_language
from django.views.generic.base import TemplateView

from www.content_item import models
from www.content_item.models import Domain
from www.landing_page.utils import DomainUtils


class LandingPage(TemplateView):
    """
    Landing page view.
    """

    max_content_items_to_return = 10

    template_name = "landing_page/landing_html.html"

    def __get_discipline_from_kwargs(self):
        if "discipline" in self.kwargs and self.kwargs['discipline']:
            return get_object_or_404(Domain, slug=self.kwargs['discipline'])
        return Domain(slug="none", name="No discipline selected")

    @classmethod
    def _get_query_set(cls, utils: DomainUtils):
        """
        Load queryset for discipline.
        """
        qs = models.ContentItem.objects.filter(hidden=False, verified=True)

        user_lang = utils.get_language_for_user(get_language())

        if user_lang:
            qs = qs.filter(language=user_lang)

        if utils.discipline.slug != 'none':
            qs = qs.filter(main_discipline=utils.discipline)

        if qs.filter(featured=True).exists():
            qs = qs.filter(featured=True)

        if qs.count() > cls.max_content_items_to_return:
            qs = qs.order_by('?')

        return qs

    @classmethod
    def _limit_and_sort_qs(cls, qs):
        """
        Limit queryset.
        """
        return sorted(qs[:cls.max_content_items_to_return], key=lambda x: x.pk)

    def get_context_data(self, **kwargs):

        discipline = self.__get_discipline_from_kwargs()
        utils = DomainUtils(discipline)
        kwargs.update(
            {
                "workforce_block": "workforce.{}".format(discipline.slug if discipline.slug else "none"),
                "rri_block": "rri.{}".format(discipline.slug if discipline.slug else "none"),
                "innovative_block": "innovative.{}".format(discipline.slug if discipline.slug else "none"),
                "toolbar_styling_discipline": discipline.slug if discipline.slug != 'none' else 'medicine',
                "current_discipline": discipline,
                "discipline_block": "discipline.{}".format(discipline.slug),
                "discipline_description_block": "discipline.description.{}".format(discipline.slug),
                "discipline_cover": "img/disciplines/{}.jpg".format(discipline.slug),
                "extra_languages": utils.get_available_language_names(),
                "featured_content_items":
                    self._limit_and_sort_qs(self._get_query_set(utils))
            }
        )
        return super().get_context_data(**kwargs)
