# coding=utf-8

"""Admin module."""

from django.contrib import admin
from ordered_model.admin import OrderedModelAdmin

from . import models


class MenuLinkLabel(admin.StackedInline):
    """Inline that displays MenuLinkLabel"""
    model = models.MenuLinkLabel


@admin.register(models.MenuLink)
class MenuLinkAdmin(OrderedModelAdmin):
    """Menu Link admin."""
    list_display = ('url', 'default_label', 'move_up_down_links')
    inlines = [MenuLinkLabel]

    @classmethod
    def default_label(cls, obj):  # pragma: no cover
        """Returns default label to show something when link has no url."""
        label = models.MenuLink.objects.get_language_version(obj, None)
        if label:
            return label.label
        return ""
