# coding=utf-8
"""Template tags library."""

import django_tag_parser
from django import template

from .. import models

register = template.Library()


@register.tag("menu_links")
def do_menu_links(parser, token):
    """
    Creates a node that will render menu links node.

    This node just loads menu links from database and outputs it to context
    as "as" parameter.
    """
    parser_parse = django_tag_parser.TagParser(
        args=["as"],
        opt_kwargs=["language"]
    )

    return LinksNode(parser_parse.parse(parser, token))


class LinksNode(template.Node):
    """Links node."""

    def __init__(self, parsed_args: django_tag_parser.ParsedArguments) -> None:
        super().__init__()
        self.parsed_args = parsed_args

    def render(self, context):

        resolved = self.parsed_args.resolve(context)
        language = resolved.get('language', None)
        save_as = resolved['as']
        request = context['request']

        links = []

        path = request.path.rstrip('/')

        for link in models.MenuLink.objects.all():
            models.MenuLink.objects.update_menu_link_for_display(link, path, language)
            links.append(link)

        context[save_as] = links

        return ""
