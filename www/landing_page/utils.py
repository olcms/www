# coding=utf-8
"""
Utilities for the landing_page module.
"""
import typing

from django.conf import settings
from django.utils.translation import ugettext

from www.content_item.models import ContentItem, Domain


class DomainUtils(object):
    """Helper class to filter by domain in the database, and select proper language"""

    DEFAULT_LANG = 'en'

    def __init__(self, discipline: Domain):
        self.discipline = discipline

    def get_content_item_language_codes(self) -> typing.Sequence[str]:
        """Get language codes we have content items for in this discipline"""

        qs = ContentItem.objects.filter(hidden=False)

        if self.discipline and self.discipline.slug != "none":
            qs = qs.filter(domains=self.discipline)

        qs = qs.values_list('language').distinct('language').order_by('language')

        return tuple((entry[0] for entry in qs))  # pylint: disable=invalid-sequence-index

    def get_available_language_names(  # pylint: disable=invalid-sequence-index
            self
    ) -> typing.Sequence[typing.Tuple[str, str]]:
        """Get tuples (code, language) for all languages this discipline has content items for. """
        language_dict = dict(settings.LANGUAGES)
        return tuple((
            (code, ugettext(language_dict[code]))
            for code in self.get_content_item_language_codes()
        ))

    def get_language_for_user(self, user_preferred_language) -> typing.Optional[str]:
        """Returns "best" language for user (for current discipline)."""
        supported_langs = set(self.get_content_item_language_codes())
        if user_preferred_language in supported_langs:
            return user_preferred_language
        if self.DEFAULT_LANG in supported_langs:
            return self.DEFAULT_LANG
        if supported_langs:
            return sorted(supported_langs)[0]
        return None
