 django.jQuery(function ($) {
     var label = $("label[for='id_contents_md']");
     var areas = $("#id_contents_md");
     areas.detach();
     var areasDiv = $("<div/>");
     areasDiv.addClass('field-row');
     areasDiv.append(areas);
     $(".form-row.field-contents_md").after(areasDiv);
     var simplemde = new SimpleMDE({ element: document.getElementById("id_contents_md") });
 });
