# coding=utf-8
"""Apps module"""

from django.apps import AppConfig


class PageBlockConfig(AppConfig):
    """App object for page_block"""
    name = 'www.page_block'
