Simple tagging utility
======================

Has following features:

1. You can tag many different models (tag contains a
   Foreign Key to the ContentType model).
2. Tags contain numbers of uses, this number is not exact,
   and should only serve to sort the tags (not to do any
   qualitative analysis).

Python API
----------

To add a tag to content item use following call:

.. code-block::

    from www.tags import models
    from django.contrib.contenttypes.models import ContentType

    content_type = ContentType.objects.get_for_model(ContentItem)

    tag = models.Tag.objects.get_or_create_tag('tag-text', content_type.pk)

WWW API
-------

There is a single usable endpoint named: ``api:tag-list``,
to obtain url for this endpoint use following code:

.. code-block::

    from www.tags import models
    from django.contrib.contenttypes.models import ContentType

    ct = ContentType.objects.get_for_model(ContentItem)
    url = reverse("api:tag-list", kwargs={
        "content_type_id": ct.pk
    })

Listing tags
************

To list tags perform ``GET`` request on ``api:tag-list``,
following parameters are available:

* ``name`` filters tags using tag contents, this filter requires
  exact match.
* ``term`` filters tags using tag contents, this filter uses
  startswith match.
* ``limit`` and ``offset``, see:
  http://www.django-rest-framework.org/api-guide/pagination/#limitoffsetpagination

Adding a tag
************

To attach a tag to content item, perform a ``POST`` request to
``api:tag-list``, this endpoint will expect single parameter
``name`` --- full name of tag to be added. This will return
an object containing a ``pk`` attribute that can be used
to attach this tag to the Content Item.

Calling ``POST`` to ``api:tag-list`` will also increment
usage counter on tag.
