# -*- coding: utf-8 -*-

"""Serializers for the tag app."""
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model as DjangoModel
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from rest_framework.relations import SlugRelatedField

from . import models


class TagRelatedField(SlugRelatedField):
    """Related field for tag model."""

    queryset = models.Tag.objects.all()

    def __init__(self, model: DjangoModel, **kwargs):
        """
        :param model: Model this related field is attached to.
        :param kwargs: Passed to superclass
        """
        slug_field = "name"
        self.__model = model
        super().__init__(slug_field, **kwargs)

    def to_internal_value(self, data):
        assert isinstance(data, str), "If not true it will lead to nasty errors."
        content_item = ContentType.objects.get_for_model(self.__model)
        try:
            return self.get_queryset().get(
                tagged_model=content_item,
                name=data
            )
        except ObjectDoesNotExist:
            return models.Tag.objects.get_or_create_tag(
                name=data,
                tagged_model_id=content_item.pk
            )
        except (TypeError, ValueError):  # pragma: no cover
            self.fail('invalid')  # pragma: no cover


class TagSerializer(serializers.ModelSerializer):
    """Serializer for Tag model."""

    class Meta:
        """Options for the serializer."""

        model = models.Tag
        fields = [
            "pk",
            "name",
            "tagged_items"
        ]
