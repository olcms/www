# -*- coding: utf-8 -*-

"""Models module for tag app."""

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction

from django.db.models.expressions import F


class TagManager(models.Manager):
    """Manager for Tab model."""

    def __try_update_tag(self, name: str, tagged_model_id: int, increment: bool):
        qs = self
        if increment:
            qs = qs.select_for_update()
        tag = qs.get(
            name=name,
            tagged_model=tagged_model_id
        )
        if increment:
            tag.tagged_items = F('tagged_items') + 1
            tag.save(update_fields=['tagged_items'])
            tag.refresh_from_db(fields=['tagged_items'])
        return tag

    def get_or_create_tag(self, name: str, tagged_model_id: int, increment=True):
        """
        Retrieves or creates a tag.

        Retrieves or creates a tag, increasing tagged_items count.
        Ideally this should be done using native postgresql upsert,
        it is however available from postgresql 9.5, while we
        need to support 9.3 (our cloud provider uses that).

        This solution might result in in error on transaction commit
        from time to time.
        """
        with transaction.atomic():
            try:
                return self.__try_update_tag(name, tagged_model_id, increment)
            except Tag.DoesNotExist:
                return Tag.objects.create(
                    name=name,
                    tagged_model_id=tagged_model_id,
                    tagged_items=1
                )

    def recount_tags(self):
        """We will need recounting someday."""
        raise NotImplementedError


def validate_tag_name(value: str):
    """Checks that tag doesn't contain whitespace around values."""
    if value != value.strip():
        raise ValidationError("Tags can't have whitespace on the sides.")


class Tag(models.Model):
    """Tag Model."""

    name = models.CharField(max_length=128, null=False, validators=[validate_tag_name])
    tagged_model = models.ForeignKey(ContentType, null=False)
    tagged_items = models.PositiveIntegerField(null=False, default=0)

    objects = TagManager()

    class Meta:
        """Options for Tag model."""

        ordering = ['tagged_model', '-tagged_items', 'name']
        unique_together = [
            ['tagged_model', 'name']
        ]
        index_together = [
            ['tagged_model', 'name'],
            ['tagged_model', 'tagged_items', 'name']

        ]

    def __str__(self):
        return self.name
