# -*- coding: utf-8 -*-

"""Apps module for the Tag app."""

from django.apps import AppConfig


class TagsConfig(AppConfig):
    """App configuration for tag class."""

    name = 'www.tags'
