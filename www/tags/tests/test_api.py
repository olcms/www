# -*- coding: utf-8 -*-
import json

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.test.testcases import TestCase
from django.urls import reverse
from rest_framework.exceptions import ParseError

from www.content_item.models import ContentItem
from www.tags.views import TagViewset
from . import utils
from .. import models


class TestList(TestCase):

    def setUp(self):

        self.ct = ContentType.objects.get_for_model(ContentItem)
        self.url = reverse("api:tag-list", kwargs={
            "content_type_id": self.ct.pk
        })

    def test_single_item(self):
        """Tests a list with a single tag."""
        tag = utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=0
        )
        response = self.client.get(self.url)
        self.assertEqual(
            {'results': [{'name': 'test-tag', 'tagged_items': 0, 'pk': tag.pk}], 'count': 1,
             'previous': None, 'next': None},
            response.json()
        )

    def test_sorting(self):
        """Tests that list with two tags shows tag with bigger tagged_items first."""
        tag1 = utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=1
        )
        tag2 = utils.TagFactory(
            name="test-tag-2",
            tagged_model=self.ct,
            tagged_items=0
        )
        response = self.client.get(self.url)
        self.assertEqual(
            {
                'previous': None, 'results': [
                {'pk': tag1.pk, 'tagged_items': 1, 'name': 'test-tag'},
                {'pk': tag2.pk, 'tagged_items': 0, 'name': 'test-tag-2'}
            ],
                'count': 2, 'next': None
            },
            response.json()
        )

    def test_content_type_filtering(self):
        """Tests that tags from different content type are hidden."""
        utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=1
        )
        utils.TagFactory(
            name="test-tag-2",
            tagged_model=self.ct,
            tagged_items=0
        )
        response = self.client.get(reverse("api:tag-list", kwargs={
            "content_type_id": ContentType.objects.get_for_model(Group).pk
        }))
        self.assertEqual(
            {'previous': None, 'count': 0, 'next': None, 'results': []},
            response.json()
        )

    def test_exact_filtering(self):
        """Tests that tags from different content type are hidden."""
        tag1=utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=1
        )
        utils.TagFactory(
            name="test-tag-2",
            tagged_model=self.ct,
            tagged_items=0
        )
        response = self.client.get(self.url, data={'name': 'test-tag'})
        self.assertEqual(
            {
                'previous': None, 'results': [
                    {'pk': tag1.pk, 'tagged_items': 1, 'name': 'test-tag'}
                ],
                'count': 1, 'next': None
            },
            response.json()
        )

    def test_filtering_not_exact(self):
        """Tests that list with two tags shows tag with bigger tagged_items first."""
        tag1 = utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=1
        )
        tag2 = utils.TagFactory(
            name="test-tag-2",
            tagged_model=self.ct,
            tagged_items=0
        )
        response = self.client.get(self.url, data={'term': 'test-tag'})
        self.assertEqual(
            {
                'previous': None, 'results': [
                {'pk': tag1.pk, 'tagged_items': 1, 'name': 'test-tag'},
                {'pk': tag2.pk, 'tagged_items': 0, 'name': 'test-tag-2'}
            ],
                'count': 2, 'next': None
            },
            response.json()
        )


class TestAddTag(TestCase):

    def setUp(self):
        user = get_user_model().objects.create(
            username="testuser",
        )
        user.set_password("test")
        user.save()
        self.user = user
        self.ct = ContentType.objects.get_for_model(ContentItem)
        self.url = reverse("api:tag-list", kwargs={
            "content_type_id": self.ct.pk
        })
        self.data = {
            "name": "test-tag"
        }

    def login(self):
        self.assertTrue(
            self.client.login(username='testuser', password='test')
        )

    def test_not_logged_user_cant_add_tag(self):
        self.assertEqual(
            0, models.Tag.objects.count()
        )
        response = self.client.post(self.url, self.data, 'application/json')
        self.assertEqual(
            response.status_code,
            403
        )

    def test_add_tag(self):
        self.login()
        self.assertEqual(
            0, models.Tag.objects.count()
        )
        response = self.client.post(self.url, json.dumps(self.data), 'application/json')
        self.assertEqual(
            response.status_code,
            201
        )
        self.assertEqual(
            1, models.Tag.objects.count()
        )
        db_tag = models.Tag.objects.all()[0]
        self.assertEqual(
            db_tag.name,
            'test-tag'
        )
        self.assertEqual(
            db_tag.tagged_model,
            self.ct
        )
        self.assertEqual(
            db_tag.tagged_items,
            1
        )

    def test_existing_tag(self):
        self.login()
        self.assertEqual(
            0, models.Tag.objects.count()
        )

        utils.TagFactory(
            name="test-tag",
            tagged_model=self.ct,
            tagged_items=1
        )
        response = self.client.post(self.url, json.dumps(self.data), 'application/json')
        self.assertEqual(
            response.status_code,
            201
        )
        self.assertEqual(
            1, models.Tag.objects.count()
        )
        db_tag = models.Tag.objects.all()[0]
        self.assertEqual(
            db_tag.name,
            'test-tag'
        )
        self.assertEqual(
            db_tag.tagged_model,
            self.ct
        )
        self.assertEqual(
            db_tag.tagged_items,
            2
        )

    def test_invalid_request(self):
        self.login()
        response = self.client.post(self.url, json.dumps({}), 'application/json')
        self.assertEqual(
            response.status_code,
            400
        )

    def test_content_type_exception(self):
        tv = TagViewset()
        tv.kwargs = {}
        with self.assertRaises(ParseError):
            tv.get_content_type()

    def test_content(self):
        tv = TagViewset()
        tv.kwargs = {}
        tv.kwargs['content_type_id'] = str(self.ct.pk)
        self.assertEqual(
            self.ct,
            tv.get_content_type()
        )

