# -*- coding: utf-8 -*-

import factory
from django.contrib.contenttypes.models import ContentType

from .. import models


class TagFactory(factory.DjangoModelFactory):

    name = "a tag"
    tagged_model = factory.Iterator(ContentType.objects.all())
    tagged_items = 0

    class Meta:
        model = models.Tag
