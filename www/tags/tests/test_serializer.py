# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.test.testcases import TestCase

from www.tags import serializers, models
from www.tags.tests.tag_generator import GeneratorFactory


class TestSerializer(TestCase):

    def test_serialization_format(self):
        example_tag = models.Tag(
            pk=123, name="A tag value", tagged_items=321
        )
        serializer = serializers.TagSerializer(instance=example_tag)
        self.assertEqual(
            {'name': 'A tag value', 'tagged_items': 321, 'pk': 123},
            serializer.data
        )


class TestRelatedField(TestCase):

    def setUp(self):
        self.tag = models.Tag(
            pk=123, name="A tag value", tagged_items=321,
            tagged_model=ContentType.objects.get_for_model(get_user_model())
        )
        self.tags = GeneratorFactory(get_user_model()).generate_tags()
        self.tag_names = [tag.name for tag in self.tags]

    def test_related_field_read(self):
        related = serializers.TagRelatedField(get_user_model())
        self.assertEqual(
            self.tag.name,
            related.to_representation(self.tag)
        )

    def test_related_field_read_many(self):
        related = serializers.TagRelatedField(get_user_model(), many=True)
        self.assertEqual(
            self.tag_names,
            related.to_representation(self.tags)
        )

    def test_related_field_write(self):
        related = serializers.TagRelatedField(get_user_model())
        updated_tag = related.to_internal_value("A tag value")
        self.assertIsInstance(updated_tag, models.Tag)
        self.assertEqual(
            updated_tag.name,
            self.tag.name
        )

    def test_related_field_write_new_tag(self):
        related = serializers.TagRelatedField(get_user_model())
        updated_tag = related.to_internal_value("Another tag that is not in the db")
        self.assertIsInstance(updated_tag, models.Tag)
        self.assertEqual(
            updated_tag.name,
            "Another tag that is not in the db"
        )



