import random
import string
import typing

from django.contrib.contenttypes.models import ContentType
from django.db import models

from www.tags.models import Tag

import itertools
import pathlib
from factory.declarations import OrderedDeclaration

TagItem = typing.NamedTuple(
    "TagItem",
    [
        ("weight", int),
        ("tag_sequence", typing.Iterable[str])
    ]
)


class BaseTagGenerator(object):

    @classmethod
    def tag_generators(cls) -> typing.Sequence[TagItem]:
        raise NotImplemented

    def __init__(self):
        """
        >>> gen = TagGenerator()
        >>> gen.weights
        [0.6, 0.3, 0.1]
        """
        super().__init__()
        weights = []
        self.tag_sequences = []
        self.random = random.Random()
        for item in self.tag_generators():
            weights.append(item.weight)
            self.tag_sequences.append(iter(item.tag_sequence))

        total_weight = sum(weights)
        self.weights = [
            w / total_weight
            for w in weights
            ]

    def get_bin_for_key(self, key):
        """
        >>> gen = TagGenerator()
        >>> gen.get_bin_for_key(0.5)
        0
        >>> gen.get_bin_for_key(0.8)
        1
        >>> gen.get_bin_for_key(0.95)
        2
        >>> gen.get_bin_for_key(1.1)
        -1

        :param key:
        :return:
        """
        curr_sum = 0
        for ii, w in enumerate(self.weights):
            curr_sum += w
            if curr_sum >= key:
                return ii
        return -1

    def __call__(self) -> str:
        """
        >>> gen = TagGenerator()
        >>> examples = [gen() for __ in range(100)]
        >>> all([isinstance(x, str) for x in examples])
        True
        """
        key = random.random()
        return next(self.tag_sequences[self.get_bin_for_key(key)])


class TagGenerator(BaseTagGenerator):

    """
    A thing that generates tags. 60% of generated tags are from among 7 most popular ones,
    30% are taken from list of 1bout 1500 most common english words 10% are randomly generated.
    """

    @classmethod
    def get_common_words(cls) -> typing.Iterable[str]:
        word_file = pathlib.Path(__file__).parent / "words"
        words = list(word_file.open())
        while True:
            choice = random.choice(words).strip()
            if choice:
                yield choice

    @classmethod
    def get_random_gibberish(cls) -> typing.Iterable[str]:
        charset = string.ascii_letters + string.digits
        while True:
            yield "".join([random.choice(charset) for __ in range(10)])

    @classmethod
    def tag_generators(cls) -> typing.Sequence[TagItem]:
        return [
            TagItem(3, itertools.cycle([
                "book", "movie", "quiz", "math", "interactive",
                "interesting", "boring", "super", "physics",
                "metaphysics", "ontology"
            ])),
            TagItem(3, cls.get_common_words()),
            TagItem(1, cls.get_random_gibberish())
        ]


class GeneratorFactory(OrderedDeclaration):
    def __init__(
        self, model: models.Model, *,
        generator: BaseTagGenerator=TagGenerator,
        num_generated: int=10
    ):
        super().__init__()
        self.model = model
        self.generator = generator()
        self.num_generated = num_generated
        self.content_type_id = ContentType.objects.get_for_model(model).pk

    def evaluate(self, sequence, obj, create, extra=None, containers=()):
       return self.generate_tags()

    def generate_tags(self):
        return [
            Tag.objects.get_or_create_tag(
                self.generator(),
                tagged_model_id=self.content_type_id
            )
            for __ in range(self.num_generated)
        ]
