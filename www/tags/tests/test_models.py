# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.test import TestCase
from .utils import TagFactory


class TestTagModel(TestCase):

    def test_invalid_tag_name(self):
        tag = TagFactory(name="  invalid name  ")
        with self.assertRaises(ValidationError):
            tag.full_clean()

    def test_tag_str(self):
        tag = TagFactory(name="super")
        self.assertEqual(
            str(tag),
            "super"
        )
