# -*- coding: utf-8 -*-

"""Admin for the Tag app."""

from django.contrib import admin

from . import models


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    """Admin page for Tag model."""

    list_display = [
        'name', 'tagged_model', 'tagged_items'
    ]

    list_filter = [
        'tagged_model'
    ]
