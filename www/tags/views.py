# -*- coding: utf-8 -*-

"""Views for the tag app."""


from django import forms
from django.contrib.contenttypes.models import ContentType

import django_filters.rest_framework

from rest_framework import status
from rest_framework.exceptions import ParseError
from rest_framework.mixins import CreateModelMixin
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from . import models, serializers


class TagFilter(django_filters.rest_framework.FilterSet):
    """Filter for the Tag list."""

    term = django_filters.CharFilter(
        name="name",
        lookup_expr='istartswith'
    )

    class Meta:
        """Meta for the filter."""

        model = models.Tag
        fields = ['name', 'term']


class AddTagForm(forms.Form):
    """Form that parses tag filters."""

    name = forms.CharField(required=True)


class TagViewset(CreateModelMixin, ReadOnlyModelViewSet):
    """A viewset for the tag app."""

    queryset = models.Tag.objects
    serializer_class = serializers.TagSerializer
    filter_class = TagFilter

    def get_content_type(self) -> ContentType:
        """Gets content type from url."""
        if 'content_type_id' in self.kwargs:
            return ContentType.objects.get(pk=self.kwargs['content_type_id'])
        raise ParseError()

    def filter_queryset(self, queryset):
        """Shows only tags for current content type."""
        content_type = self.get_content_type()
        queryset = queryset.filter(tagged_model=content_type)
        queryset = super().filter_queryset(queryset)
        return queryset

    def create(self, request: Request, *args, **kwargs):
        """
        Gets the tag or creates it.

        Also increments the usage count.
        """
        content_type = self.get_content_type()

        form = AddTagForm(data=request.data)

        if not form.is_valid():
            raise ParseError(form.errors)

        tag = models.Tag.objects.get_or_create_tag(
            name=form.cleaned_data['name'],
            tagged_model_id=content_type.pk
        )
        serializer = serializers.TagSerializer(instance=tag)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )
