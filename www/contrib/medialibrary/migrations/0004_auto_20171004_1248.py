# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-10-04 10:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medialibrary', '0003_auto_20170520_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mediafiletranslation',
            name='language_code',
            field=models.CharField(choices=[('en', 'English'), ('pl', 'Polish'), ('el', 'Greek'), ('cs', 'Czech'), ('ca', 'Catalan'), ('es', 'Spanish'), ('sl', 'Slovene')], default='en', max_length=10, verbose_name='language'),
        ),
    ]
