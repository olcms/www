# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-09 14:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0003_auto_20170520_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template_key',
            field=models.CharField(choices=[('feincms.html', 'Standard template')], default='feincms.html', max_length=255, verbose_name='template'),
        ),
    ]
