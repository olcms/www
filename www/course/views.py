"""Views for course app."""
# -*- coding: utf-8 -*-

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Prefetch
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, DeleteView, CreateView, \
    UpdateView
from django.views.generic.detail import SingleObjectMixin

from www.course import models
from www.course.models import CourseItem
from . import forms
from .models import Course
from ..content_item.models import ContentItem
from ..content_item.views import get_context_for_resources


class CourseListView(ListView):
    """List view for Courses"""

    model = Course
    context_object_name = 'courses'


class MyCoursesListView(LoginRequiredMixin, ListView):
    """List view for Courses owned by current user"""

    context_object_name = 'courses'

    def get_queryset(self):
        return Course.objects.filter(owners=self.request.user)


class CourseDetailMixin(object):
    """Course detail mixin"""

    model = Course
    context_object_name = 'course'
    queryset = Course.objects.prefetch_related(
        Prefetch(
            'items',
            ContentItem.objects.order_by('courseitem__order')
        )
    )


class CourseDetailView(CourseDetailMixin, DetailView):
    """Course detail view"""

    def get_context_data(self, **kwargs):
        user = self.request.user
        course = self.get_object()
        kwargs['is_owner'] = user.is_authenticated and course.owners.filter(id=user.pk).exists()
        return super().get_context_data(**kwargs)


class CourseLessonView(CourseDetailMixin, DetailView):
    """Course lesson view"""

    template_name = 'course/course_lesson.html'

    def get_context_data(self, **kwargs):
        try:
            number = int(self.kwargs['order'])
            order = number - 1
            course = kwargs['object']
            content = course.items.get(courseitem__order=order)

            kwargs.update(get_context_for_resources(self.request, content))
            kwargs.update({
                'lesson': {
                    'content': content,
                    'number': number,
                    'has_prev': number > 1,
                    'has_next': number < course.items.count()
                }
            })
            return super().get_context_data(**kwargs)
        except ContentItem.DoesNotExist as ex:
            raise Http404('No %s matches the given query.' % ex)


class CourseCreateView(LoginRequiredMixin, CreateView):
    """Create Course view"""
    model = Course
    form_class = forms.CourseForm

    def get_success_url(self):
        return self.object.get_absolute_url()

    def form_valid(self, form):
        result = super().form_valid(form)
        self.object.save()
        self.object.owners.add(self.request.user)
        return result


class CourseOwnerMixin(UserPassesTestMixin):  # pylint: disable=no-member
    """View visible only to owner of course."""
    raise_exception = True
    model = Course

    def test_func(self) -> bool:
        user = self.request.user
        course = self.get_object()
        return user.is_authenticated and course.owners.filter(id=user.pk).exists()


class CourseItemOwnerMixin(UserPassesTestMixin):  # pylint: disable=no-member
    """View visible only to owner of course."""
    raise_exception = True
    model = CourseItem

    def test_func(self) -> bool:
        user = self.request.user
        course = self.get_object().course
        return user.is_authenticated and course.owners.filter(id=user.pk).exists()


class CourseUpdateView(CourseOwnerMixin, UpdateView):
    """Update Course View"""
    form_class = forms.CourseForm

    def get_items_queryset(self):
        """Returns (filtered) queryset of content items that can be added."""
        qs = ContentItem.objects.filter(hidden=False)
        filter_form = forms.CourseItemFilterForm(data=self.request.GET)
        if filter_form.is_valid():
            if filter_form.cleaned_data['language']:
                qs = qs.filter(language=filter_form.cleaned_data['language'])
            if filter_form.cleaned_data['discipline']:
                qs = qs.filter(domains=filter_form.cleaned_data['discipline'])
        return qs

    def get_item_add_from(self):
        """Create to add new items to this course."""
        form = forms.CourseItemAddForm()
        form.fields['content_item'].queryset = self.get_items_queryset()
        return form

    def get_context_data(self, **kwargs):
        kwargs.update({
            "filter_form": forms.CourseItemFilterForm(data=self.request.GET),
            "item_add_form": self.get_item_add_from(),
            "course_items": models.CourseItem.objects.filter(course=self.object).prefetch_related('content_item')
        })
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        return self.object.get_absolute_url()


class CourseItemViewBase(CourseItemOwnerMixin, SingleObjectMixin, View):
    """Base class for views that operate on course items."""

    def perform_action(self, item: CourseItem):
        """Extension point, to perform specific action"""

    def post(self, request, **kwargs):  # pylint: disable=unused-argument
        """POST handler (only handler here)"""
        course_item = self.get_object()
        self.perform_action(course_item)
        if self.request.META.get("HTTP_REFERER"):
            # Preserve filters set via get parameters
            return redirect(self.request.META.get("HTTP_REFERER"))
        return redirect('course:edit', pk=course_item.course_id)


class CourseItemUpView(CourseItemViewBase):
    """Moves course item UP."""

    def perform_action(self, item: CourseItem):
        item.up()


class CourseItemDownView(CourseItemViewBase):
    """Moves course item DOWN."""
    def perform_action(self, item: CourseItem):
        item.down()


class CourseItemDeleteView(CourseItemViewBase):
    """Deletes course item"""
    def perform_action(self, item: CourseItem):
        item.delete()


class CourseItemAddView(UserPassesTestMixin, CreateView):
    """Adds course item."""
    model = CourseItem
    raise_exception = True
    form_class = forms.CourseItemAddForm

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.course = None

    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=self.kwargs['course_id'])
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        user = self.request.user
        course = self.course
        return user.is_authenticated and course.owners.filter(id=user.pk).exists()

    def form_invalid(self, form):  # pragma: no cover
        return HttpResponse(status=400)

    def form_valid(self, form):
        item = form.save(commit=False)
        item.course = self.course
        item.save()
        if self.request.META.get("HTTP_REFERER"):
            # Preserve filters set via get parameters
            return redirect(self.request.META.get("HTTP_REFERER"))
        return redirect('course:edit', pk=item.pk)


class CourseDeleteView(CourseOwnerMixin, DeleteView):
    """Delete Course View"""
    success_url = reverse_lazy('course:list')
