# coding=utf-8
"""Constants for course module."""


class ImageProperties(object):
    """
    Image sizes for Content Items.
    """
    cover_image_size = (1080, 160)
    miniature_size = (154, 130)
