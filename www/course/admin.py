# -*- coding: utf-8 -*-
"""Django admin config for course app."""

from django.contrib import admin
from ordered_model.admin import OrderedTabularInline

from .models import (
    Course,
    CourseItem
)


class CourseItemInline(OrderedTabularInline):
    """Inline admin form for CourseItem"""
    model = CourseItem
    fields = ('content_item', 'order', 'move_up_down_links',)
    readonly_fields = ('order', 'move_up_down_links',)
    extra = 1
    ordering = ('order',)


class CourseAdmin(admin.ModelAdmin):
    """Admin form for Course"""
    list_display = ('name', 'description', 'cover_image')
    inlines = (CourseItemInline, )

    def get_urls(self):
        urls = super(CourseAdmin, self).get_urls()
        for inline in self.inlines:
            if hasattr(inline, 'get_urls'):
                urls = inline.get_urls(self) + urls
        return urls


admin.site.register(Course, CourseAdmin)
