# -*- coding: utf-8 -*-
"""Forms for course app."""

from django import forms
from django.conf import settings

from www.content_item.models import Domain
from www.course import models, tasks


class CourseForm(forms.ModelForm):
    """Common Course form"""

    def save(self, commit=True):
        instance = super().save(commit)
        tasks.resize_course_images.delay(course_pk=instance.pk)
        return instance

    class Meta:
        """Meta class"""
        model = models.Course
        exclude = ('items', 'owners')


class CourseItemFilterForm(forms.Form):
    """Form used to filter content items to be added to a course."""

    language = forms.ChoiceField(required=False)
    discipline = forms.ModelChoiceField(queryset=Domain.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['language'].choices = (('', '-' * 7), ) + tuple(settings.LANGUAGES)
        self.fields['discipline'].queryset = Domain.objects.filter(type__slug="discipline")


class CourseItemAddForm(forms.ModelForm):
    """Form for adding Course Items to a course"""

    class Meta:
        """Metaclass"""
        model = models.CourseItem
        fields = [
            'content_item'
        ]
