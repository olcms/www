"""Models for course app."""
# -*- coding: utf-8 -*-

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy
from ordered_model.models import OrderedModel

from ..content_item.models import ContentItem
from ..utils.validators import image_name_validator


class Course(models.Model):
    """Course model"""
    name = models.CharField(
        verbose_name='Course name',
        max_length=128,
        blank=False,
        null=False
    )

    description = models.TextField(
        verbose_name='Course description',
        blank=False,
        null=False
    )

    cover_image = models.ImageField(
        verbose_name='Course cover image',
        blank=True,
        null=True,
        validators=[image_name_validator],
        help_text=ugettext_lazy('Will be cropped to aspect ratio 6.75:1 (or 1080x160px)')
    )

    cover_image_resized = models.ImageField(
        editable=False, blank=True, null=True
    )

    items = models.ManyToManyField(
        ContentItem,
        related_name='courses',
        through='CourseItem',
        verbose_name='Course content'
    )

    owners = models.ManyToManyField(
        to=settings.AUTH_USER_MODEL,
        verbose_name=ugettext_lazy("Owners"),
        help_text=ugettext_lazy("List of users that can edit this course")
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        """Return absolute url to this course"""
        from django.urls import reverse
        return reverse('course:detail', args=[str(self.id)])

    class Meta:
        ordering = ['-created_at']


class CourseItem(OrderedModel):
    """M2M through model for Course and ContentItem"""
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    content_item = models.ForeignKey(ContentItem, on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)
    order_with_respect_to = 'course'

    class Meta(OrderedModel.Meta):
        ordering = ('course', 'order')

    def __str__(self):
        return '{} - {}'.format(
            self.course.__str__(),
            self.content_item.__str__()
        )

    def save(self, **kwargs):  # pylint: disable=arguments-differ
        self.course.updated_at = self.updated_at
        self.course.save()
        super().save(**kwargs)
