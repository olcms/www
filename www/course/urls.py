"""Urls for course app."""
# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'(?P<pk>\d+)/edit$',
        view=views.CourseUpdateView.as_view(),
        name='edit'
    ),
    url(
        regex=r'(?P<pk>\d+)/delete$',
        view=views.CourseDeleteView.as_view(),
        name='delete'
    ),
    url(
        regex=r'^(?P<pk>\d+)$',
        view=views.CourseDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^(?P<pk>\d+)/(?P<order>\d+)$',
        view=views.CourseLessonView.as_view(),
        name='lesson'
    ),
    url(
        regex=r'new$',
        view=views.CourseCreateView.as_view(),
        name='new'
    ),
    url(
        regex=r'my$',
        view=views.MyCoursesListView.as_view(),
        name='my-list'
    ),
    url(
        regex=r'item/(?P<pk>\d+)/up',
        view=views.CourseItemUpView.as_view(),
        name="up"
    ),
    url(
        regex=r'item/(?P<pk>\d+)/down',
        view=views.CourseItemDownView.as_view(),
        name="down"
    ),
    url(
        regex=r'item/(?P<pk>\d+)/delete',
        view=views.CourseItemDeleteView.as_view(),
        name="delete"
    ),
    url(
        regex=r'item/(?P<course_id>\d+)/add-item',
        view=views.CourseItemAddView.as_view(),
        name="add_item"
    ),
    url(
        regex=r'$',
        view=views.CourseListView.as_view(),
        name='list'
    ),
]
