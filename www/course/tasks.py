# coding=utf-8
"""Tasks for the course module"""

from PIL import Image
from celery import task

from www.course import const, models
from www.utils.image_utils import image_resize_and_crop_to_aspect, \
    save_pil_image


@task(bind=True)
def resize_course_images(self, course_pk):
    """
    Crop and resize cover image and miniature image to correct aspect ratios
    If there is no miniature image use cover image for it.
    :param content_item:
    :return:
    """
    try:
        course = models.Course.objects.get(pk=course_pk)
        if course.cover_image:
            image = Image.open(course.cover_image)
            resized_image = image_resize_and_crop_to_aspect(
                image, *const.ImageProperties.cover_image_size
            )
            save_pil_image(
                course,
                course.cover_image_resized,
                resized_image,
                course.cover_image.name,
                image.format
            )

    except Exception as e:
        raise self.retry(exc=e)
