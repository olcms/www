# -*- coding: utf-8 -*-
"""Unittests for views."""
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from test_plus import TestCase

from www.content_item.models import Domain, DomainType, \
    ContentItemLanguageGroup, ContentItem
from www.content_item.test.utils import ContentItemFactory
from www.course.models import Course, CourseItem
from www.course.views import CourseUpdateView, CourseItemUpView, \
    CourseItemDownView, CourseItemDeleteView, CourseItemAddView
from www.users.models import User
from www.utils.test_utils import LoginTestMixin


class TestMyCoursesListView(LoginTestMixin):

    def test_unlogged_user_list(self):
        response = self.client.get(
            reverse("course:my-list")
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/accounts/login/?'))

    def test_logged_user_list(self):
        self.login()
        rocket_science = Course.objects.create(name="Rocket Science")
        rocket_science.owners.add(self.user)
        Course.objects.create(name="Funky Science")

        response = self.client.get(
            reverse("course:my-list")
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Rocket Science", response.content)
        self.assertNotIn(b"Funky Science", response.content)


class TestCourseDetailView(LoginTestMixin):

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name="Rocket Science")

    def test_not_editable(self):
        response = self.client.get(
            reverse("course:detail", args=[self.course.pk])
        )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(b"Edit", response.content)

    def test_editable_by_owner(self):
        self.login()
        self.course.owners.add(self.user)
        response = self.client.get(
            reverse("course:detail", args=[self.course.pk])
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Edit", response.content)


class CourseEditViewTestMixin:

    course_data = {
        "name": "Rocket Science",
        "description": "Rocket science is a primary branch of aerospace engineering.",
        "courseitem_set-TOTAL_FORMS": 0,
        "courseitem_set-INITIAL_FORMS": 0,
    }

    url = None
    url_args = None
    raise_exception = False

    def test_unlogged_user_edit_form_get(self):
        response = self.client.get(
            reverse(self.url, args=self.url_args)
        )
        if self.raise_exception:
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 302)
            self.assertTrue(response.url.startswith('/accounts/login/?'))

    def test_unlogged_user_edit_form_post(self):
        count = Course.objects.count()
        response = self.client.post(
            reverse(self.url, args=self.url_args),
            data=self.course_data
        )
        if self.raise_exception:
            self.assertEqual(response.status_code, 403)
        else:
            self.assertEqual(response.status_code, 302)
            self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(count, Course.objects.count())

    def logged_user_edit_form_post(self, new_items_count=0):
        count = Course.objects.count()
        self.login()
        response = self.client.post(
            reverse(self.url, args=self.url_args),
            data=self.course_data
        )
        self.assertEqual(response.status_code, 302, response.content)
        self.assertEqual(count + new_items_count, Course.objects.count())
        course = Course.objects.all()[count + new_items_count - 1]
        self.assertTrue(response.url.startswith('/course/{}'.format(course.pk)))
        self.assertEqual(self.course_data['name'], course.name)
        self.assertEqual(self.course_data['description'], course.description)
        self.assertTrue(course.owners.filter(pk=self.user.pk).exists())


class TestCourseCreateView(CourseEditViewTestMixin, LoginTestMixin):
    url = "course:new"

    def test_logged_user_create_form_get(self):
        self.login()
        response = self.client.get(
            reverse(self.url)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Course.objects.count())

    def test_logged_user_create_form_post(self):
        self.logged_user_edit_form_post(1)


class TestCourseUpdateView(CourseEditViewTestMixin, LoginTestMixin):
    url = "course:edit"
    raise_exception = True

    def setUp(self):
        super().setUp()
        self.course = Course.objects.create(name="Rocket Science")
        self.url_args = [self.course.pk]

    def test_logged_user_not_owner_update_form_get(self):
        self.login()
        response = self.client.get(
            reverse(self.url, args=self.url_args)
        )
        self.assertEqual(response.status_code, 403)

    def test_logged_user_owner_update_form_get(self):
        self.course.owners.add(self.user)
        self.login()
        response = self.client.get(
            reverse(self.url, args=self.url_args)
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.course.name.encode('ascii'), response.content)

    def test_logged_user_not_owner_update_form_post(self):
        self.login()
        response = self.client.post(
            reverse(self.url, args=self.url_args),
            data=self.course_data
        )
        self.assertEqual(response.status_code, 403)

    def test_logged_user_owner_update_form_post(self):
        self.course.owners.add(self.user)
        self.logged_user_edit_form_post()


class TestItemAddForms(TestCase):

    maxDiff = None

    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.course = Course.objects.create(
            name="A course", description="Description"
        )
        self.course.owners.add(self.user)
        self.medicine_pl = ContentItemFactory(
            language='pl', domains=[Domain.objects.get(slug='medicine')]
        )
        self.medicine_en = ContentItemFactory(
            language='en', domains=[Domain.objects.get(slug='medicine')]
        )
        self.physics_pl = ContentItemFactory(
            language='pl', domains=[Domain.objects.get(slug='physics')]
        )
        self.physics_en = ContentItemFactory(
            language='en', domains=[Domain.objects.get(slug='physics')]
        )

    def get_item_field(self, query):
        rf = RequestFactory()
        request = rf.get(query)
        request.user=self.user
        response = CourseUpdateView.as_view()(request, pk=self.course.pk)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response, TemplateResponse)
        item_add_form = response.context_data['item_add_form']
        return item_add_form.fields['content_item']

    def compare_qs(self, actual, expected):
        self.assertEqual(
            {i.pk for i in actual}, {i.pk for i in expected},
            "{} != {}".format(list(actual), list(expected))
        )

    def test_filter_form(self):
        item_form = self.get_item_field("/url")
        self.compare_qs(
            item_form.queryset,
            ContentItem.objects.all(),
        )

    def test_filter_form_pl(self):
        item_form = self.get_item_field("/url?language=pl")
        self.compare_qs(
            item_form.queryset,
            ContentItem.objects.filter(language='pl')
        )

    def test_filter_form_medicine(self):
        medicine_pk = Domain.objects.get(slug='medicine').pk
        item_form = self.get_item_field("/url?discipline={}".format(medicine_pk))
        self.compare_qs(
            item_form.queryset,
            ContentItem.objects.filter(domains=Domain.objects.get(slug='medicine')),
        )

    def test_filter_form_pl_medicine(self):
        medicine_pk = Domain.objects.get(slug='medicine').pk
        item_form = self.get_item_field("/url?discipline={}&language=pl".format(medicine_pk))
        self.compare_qs(
            item_form.queryset,
            ContentItem.objects.filter(
                language='pl',
                domains=Domain.objects.get(slug='medicine')),
        )


class TestCourseLessonView(TestCase):

    def setUp(self):
        super().setUp()
        domain = Domain.objects.create(type=DomainType.objects.create())
        self.content_items = [
            ContentItem.objects.create(title="Engine", authors=["Wernher von Braun"],
                                       main_discipline=domain,
                                       group=ContentItemLanguageGroup.objects.create()),
            ContentItem.objects.create(title="Aviation", authors=["Charles Lindbergh"],
                                       main_discipline=domain,
                                       group=ContentItemLanguageGroup.objects.create()),
            ContentItem.objects.create(title="Sound Barrier", authors=["Sergei Korolev"],
                                       main_discipline=domain,
                                       group=ContentItemLanguageGroup.objects.create()),
        ]
        self.course = Course.objects.create(name="Rocket Science")
        self.course_items = [
            CourseItem.objects.create(course=self.course, content_item=content_item)
            for content_item in self.content_items
        ]

    def test_no_such_course(self):
        response = self.client.get(
            reverse("course:lesson", args=[12345, 12345])
        )
        self.assertEqual(response.status_code, 404)

    def test_no_such_lesson(self):
        response = self.client.get(
            reverse("course:lesson", args=[self.course.pk, 12345])
        )
        self.assertEqual(response.status_code, 404)

    def test_only_next_lesson(self):
        response = self.client.get(
            reverse("course:lesson", args=[self.course.pk, 1])
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(reverse("course:lesson", args=[self.course.pk, 2]).encode('ascii'), response.content)
        self.assertNotIn(reverse("course:lesson", args=[self.course.pk, 0]).encode('ascii'), response.content)

    def test_only_prev_lesson(self):
        response = self.client.get(
            reverse("course:lesson", args=[self.course.pk, 3])
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(reverse("course:lesson", args=[self.course.pk, 2]).encode('ascii'), response.content)
        self.assertNotIn(reverse("course:lesson", args=[self.course.pk, 4]).encode('ascii'), response.content)

    def test_prev_next_lesson(self):
        response = self.client.get(
            reverse("course:lesson", args=[self.course.pk, 2])
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(reverse("course:lesson", args=[self.course.pk, 1]).encode('ascii'), response.content)
        self.assertIn(reverse("course:lesson", args=[self.course.pk, 3]).encode('ascii'), response.content)


class ComplexCourseMixin(TestCase):

    def setUp(self):
        self.medicine_pl = ContentItemFactory(
            title="Medicine PL",
            language='pl', domains=[Domain.objects.get(slug='medicine')]
        )
        self.medicine_en = ContentItemFactory(
            title="Medicine EN",
            language='en', domains=[Domain.objects.get(slug='medicine')]
        )
        self.physics_pl = ContentItemFactory(
            title="Physics PL",
            language='pl', domains=[Domain.objects.get(slug='physics')]
        )
        self.physics_en = ContentItemFactory(
            title="Physics EN",
            language='en', domains=[Domain.objects.get(slug='physics')]
        )

        self.owner = User.objects.create(username="foo")
        self.course = Course.objects.create(name="foo", description="bar")
        self.course.owners.add(self.owner)

        CourseItem.objects.create(content_item=self.medicine_pl, course=self.course)
        CourseItem.objects.create(content_item=self.medicine_en, course=self.course)
        CourseItem.objects.create(content_item=self.physics_pl, course=self.course)
        CourseItem.objects.create(content_item=self.physics_en, course=self.course)


class TestCourseOperationsAccess(ComplexCourseMixin):

    def setUp(self):
        super().setUp()
        self.item = CourseItem.objects.get(
            content_item=self.medicine_pl,
            course=self.course
        )

    def assert_no_access_for_non_owner(self, view):
        rf = RequestFactory()
        request = rf.get("/stuff")
        request.user = User.objects.create(username="foobar")
        with self.assertRaises(PermissionDenied):
            view(request, pk=self.item.pk)

    def test_up(self):
        self.assert_no_access_for_non_owner(
            CourseItemUpView.as_view()
        )

    def test_down(self):
        self.assert_no_access_for_non_owner(
            CourseItemDownView.as_view()
        )

    def test_delete(self):
        self.assert_no_access_for_non_owner(
            CourseItemDeleteView.as_view()
        )


class TestCourseOperation(ComplexCourseMixin):

    def make_rf(self):
        rf = RequestFactory()
        request = rf.post("/stuff")
        request.user = self.owner
        return request

    def call_view_on_ci(self, view, ci, referer=None):
        request = self.make_rf()
        if referer:
            request.META['HTTP_REFERER'] = referer
        course_item = CourseItem.objects.get(
            content_item=ci.pk, course=self.course)
        response = view.as_view()(request, pk=course_item.pk)
        self.assertEqual(response.status_code, 302)
        return response

    def test_sanity(self):
        self.assertEqual(
            list(self.course.items.all()),
            [self.medicine_pl, self.medicine_en, self.physics_pl, self.physics_en]
        )

    def verify_content_item_order(self, expected):
        self.assertEqual(
            [ci.content_item for ci in CourseItem.objects.filter(course=self.course)],
            expected
        )

    def test_up(self):
        self.call_view_on_ci(CourseItemUpView, self.medicine_en)
        self.course.refresh_from_db()
        self.verify_content_item_order(
            [self.medicine_en, self.medicine_pl, self.physics_pl, self.physics_en]
        )

    def test_down(self):
        self.call_view_on_ci(CourseItemDownView, self.medicine_en)
        self.course.refresh_from_db()
        self.verify_content_item_order(
            [self.medicine_pl, self.physics_pl, self.medicine_en, self.physics_en]
        )

    def test_delete(self):
        self.call_view_on_ci(CourseItemDeleteView, self.medicine_en)
        self.course.refresh_from_db()
        self.verify_content_item_order(
            [self.medicine_pl, self.physics_pl, self.physics_en]
        )

    def test_referer(self):
        referer_value = "http://localhost?language=pl"
        response = self.call_view_on_ci(CourseItemUpView, self.medicine_en, referer_value)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referer_value)
        self.course.refresh_from_db()
        self.verify_content_item_order(
            [self.medicine_en, self.medicine_pl, self.physics_pl,
             self.physics_en]
        )


class TestCourseAddView(ComplexCourseMixin):

    def setUp(self):
        super().setUp()
        self.physics_el = ContentItemFactory(
            title="Physics EN",
            language='en', domains=[Domain.objects.get(slug='physics')]
        )

    def test_no_access(self):
        rf = RequestFactory()
        request = rf.post("/stuff", data={'content_item': self.physics_el.pk})
        request.user = User.objects.create(username="bar")
        with self.assertRaises(PermissionDenied):
            CourseItemAddView.as_view()(request, course_id=self.course.pk)

    def test_with_access(self):
        rf = RequestFactory()
        request = rf.post("/stuff", data={'content_item': self.physics_el.pk})
        request.user = self.owner
        response = CourseItemAddView.as_view()(request, course_id=self.course.pk)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(self.course.items.filter(pk=self.physics_el.pk).exists())

    def test_referer(self):
        referer_value = "http://localhost?language=pl"
        rf = RequestFactory()
        request = rf.post("/stuff", data={'content_item': self.physics_el.pk})
        request.user = self.owner
        request.META['HTTP_REFERER'] = referer_value
        response = CourseItemAddView.as_view()(request, course_id=self.course.pk)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, referer_value)
        self.course.refresh_from_db()

