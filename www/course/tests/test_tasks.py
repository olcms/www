from unittest import mock

from PIL import Image
from django.test import TestCase

from www.course import const
from www.course.models import Course
from www.course.tasks import resize_course_images
from www.utils.image_utils import save_pil_image


class TestTasks(TestCase):

    def test_resize_task(self):
        image = Image.new("RGB", (512, 512), "white")
        course = Course.objects.create(
          name="Course", description="ImageDescription"
        )
        save_pil_image(course, course.cover_image, image, "testimage", "jpeg")
        self.assertFalse(course.cover_image_resized)
        resize_course_images(course.pk)
        course.refresh_from_db()
        self.assertTrue(course.cover_image_resized)
        self.assertEqual((
                course.cover_image_resized.width,
                course.cover_image_resized.height
            ),
            const.ImageProperties.cover_image_size
        )

    @mock.patch('www.course.tasks.resize_course_images.retry')
    @mock.patch('www.course.tasks.image_resize_and_crop_to_aspect')
    def test_failed_resize_retry(self, mock_crop, mock_retry):
        """ Test if failed resizing task will be retried. """
        image = Image.new("RGB", (512, 512), "white")
        course = Course.objects.create(
          name="Course", description="ImageDescription"
        )
        save_pil_image(course, course.cover_image, image, "testimage", "jpeg")

        mock_crop.side_effect = error = Exception()

        with self.assertRaises(Exception):
            resize_course_images(course_pk=course.pk)  # pylint: disable=no-value-for-parameter
        mock_retry.assert_called_with(exc=error)


