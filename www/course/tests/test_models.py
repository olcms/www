# -*- coding: utf-8 -*-
"""Model tests for course app."""

from test_plus import TestCase

from www.content_item.models import ContentItem, ContentItemLanguageGroup, \
    Domain, DomainType
from www.course.models import Course, CourseItem


class TestCourse(TestCase):

    def setUp(self):
        self.course = Course.objects.create(name="Rocket Science")

    def test_str(self):
        self.assertEqual(str(self.course), "Rocket Science")

    def test_absolute_url(self):
        self.assertEqual(self.course.get_absolute_url(), "/course/1")


class TestCourseItem(TestCase):

    def setUp(self):
        domain = Domain.objects.create(type=DomainType.objects.create())
        language_group = ContentItemLanguageGroup.objects.create()
        self.content_item = ContentItem.objects.create(title="Engine", authors=["Wernher von Braun"],
                                                       main_discipline=domain, group=language_group)
        self.course = Course.objects.create(name="Rocket Science")
        self.course_item = CourseItem.objects.create(course=self.course, content_item=self.content_item)

    def test_str(self):
        self.assertEqual(str(self.course_item), "Rocket Science - Engine")

    def test_save(self):
        self.course_item.save()
        self.assertTrue((self.course_item.updated_at - self.course.updated_at).seconds < 1)
