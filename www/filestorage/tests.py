# -*- coding: utf-8 -*-
"""Tests module."""

import io
import os
import unittest.mock
import urllib.parse
from collections import namedtuple

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.core.files.storage import default_storage
from django.db.utils import IntegrityError
from django.test import TestCase
from django.urls.base import reverse

import requests

from www.users.models import User

from . import api, models

# NamedTuples are marked as not callable by mistake.
# pylint: disable=not-callable


class TestNormalDjangoStorage(TestCase):
    """Test that default_storage configured in this env just works."""

    TMP_FILE_SIZE = 1024

    def setUp(self):
        """Set up method."""
        self.file_contents = os.urandom(self.TMP_FILE_SIZE)
        self.file_name = "some-random-data.txt"

    def test_normal_storage(self):
        """Test that default_storage configured in this env just works."""
        available_name = default_storage.get_available_name(self.file_name)
        self.assertIsNotNone(available_name)
        target_name = default_storage.generate_filename(available_name)
        self.assertIsNotNone(target_name)
        try:
            default_storage.save(target_name, io.BytesIO(self.file_contents))
            self.assertEqual(
                self.TMP_FILE_SIZE,
                default_storage.size(target_name)
            )
            opened_remote_file = default_storage.open(target_name)
            remote_data = opened_remote_file.read()
            self.assertEqual(
                remote_data,
                self.file_contents
            )
            default_storage.delete(target_name)
        except Exception:  # pragma: no cover pylint: disable=broad-except
            try:  # pragma: no cover
                default_storage.delete(target_name)  # pragma: no cover
            except Exception:  # pragma: no cover pylint: disable=broad-except
                pass  # Don't want to shadow current exception pragma: no cover


class TestApiMethods(TestCase):
    """Unit-tests for various API methods."""

    def test_get_signed_url(self):
        """Tests signed URL. Expected URL was created by running swift sample code by hand."""
        expected = 'https://example.com/v1/AUTH_test/test/foo.pdf' \
                   '?temp_url_sig=69d1ab4879e41fb98bda7d2bb7c64d09b48b398b' \
                   '&temp_url_expires=1234'
        actual = api.get_signed_url(
            method='PUT',
            path='/v1/AUTH_test/test/foo.pdf',
            key='a-key',
            netloc='example.com',
            scheme='https',
            expires=1234
        )
        self.assertEqual(actual, expected)

    def test_get_signed_url_assert(self):
        """Tests that get_singed_URL will choke on invalid method."""
        with self.assertRaises(AssertionError):
            api.get_signed_url(
                method='NoSuchMethod',
                path='/v1/AUTH_test/test/foo.pdf',
                key='a-key',
                netloc='example.com',
                scheme='https',
                expires=1234
            )

    def test_get_swift_endpoint_data(self):
        """
        Test for get_swift_endpoint_data.

        This method gets swift host, and path from django swift storage that performs swift auth.
        """
        expected = api.SwiftEndpointData(
            host='olcms.dev.local:8105', scheme='http', base_url='/v1/AUTH_test/container/'
        )
        MockStorage = namedtuple("MockStorage", "base_url")
        mock_storage = MockStorage(
            base_url="http://olcms.dev.local:8105/v1/AUTH_test/container/"
        )
        with unittest.mock.patch(
            "django.core.files.storage.default_storage._wrapped", mock_storage
        ):
            actual = api.get_swift_endpoint_data()
        self.assertEqual(expected, actual)

    def test_get_unique_file(self):
        """Tests that get_unique_file_name creates names according to spec."""
        file = api.get_unique_file_name("excercise.png", "happy-math")
        self.assertTrue(
            file.startswith("happy-math/")
        )
        self.assertTrue(
            file.endswith("/excercise.png")
        )

    def test_get_unique_file_default_namespace(self):
        """Tests that get_unique_file_name creates names according to spec."""
        file = api.get_unique_file_name("excercise.png")
        self.assertTrue(
            file.startswith(api.DEFAULT_NAMESPACE)
        )

    def test_get_unique_file_default_namespace_empty_namespace(self):
        """Tests that get_unique_file_name creates names according to spec."""
        file = api.get_unique_file_name("excercise.png", namespace="")
        self.assertTrue(
            file.startswith(api.DEFAULT_NAMESPACE)
        )

    def test_file_is_unique(self):
        """Tests that you'll get different paths in different get_unique_file_name invocations."""
        file = api.get_unique_file_name("excercise.png", "happy-math")
        file2 = api.get_unique_file_name("excercise.png", "happy-math")
        self.assertNotEqual(file, file2)

    def test_file_is_registered(self):
        """Tests that auditing model stores all data."""
        self.assertEqual(0, models.FileUpload.objects.count())
        user = get_user_model().objects.create(username="foo")
        response = api.register_and_prepare_upload(file_name="excercise.png", user=user)
        self.assertEqual(1, models.FileUpload.objects.count())
        upload = models.FileUpload.objects.all()[0]
        self.assertEqual(
            upload.file_path,
            response.path
        )
        self.assertEqual(
            upload.file_name,
            response.file_name
        )
        self.assertEqual(
            upload.file_owner,
            user
        )

    def test_unique_file_raises_an_error(self):
        """
        Test we get an error if uploads are not unique.

        If VM /dev/urandom state is cloned (which **might** happen
        in a cloud) randomly generated files might not be unique,
        Right now I believe this this scenario is not very probable,
        but data integrity bugs could are bad enough that we should
        at least raise error.
        """
        user = get_user_model().objects.create(username="foo")

        with unittest.mock.patch(
            "www.filestorage.api.get_unique_file_name", return_value="/path/file"
        ):
            api.register_and_prepare_upload(file_name="test.png", user=user)
            with self.assertRaises(IntegrityError):
                api.register_and_prepare_upload(file_name="test.png", user=user)

    def test_get_file_path(self):
        """Tests that tempURL links are generated properly."""
        expected = "http://olcms.dev.local:8105/v1/AUTH_test/container/something.png?" \
                   "temp_url_sig=10e046e0f527871d3be9a718ae5b2022651a3f51&temp_url_expires=1234"

        MockStorage = namedtuple("MockStorage", "base_url")
        mock_storage = MockStorage(
            base_url="http://olcms.dev.local:8105/v1/AUTH_test/container/"
        )
        with unittest.mock.patch(
            "django.core.files.storage.default_storage._wrapped", mock_storage
        ), self.settings(SWIFT_TEMP_URL_KEY="a-key"):
            actual = api.prepare_tempurl_link('PUT', 'something.png', 1234)
            self.assertEqual(expected, actual)


def perform_upload_and_verify_it(self: TestCase, upload: api.PrepareFileUploadResponse):
    """Helper method, prepare upload URL, does upload and verifies it."""
    upload_data = os.urandom(1024)
    response = requests.put(upload.url, upload_data)
    self.assertEqual(response.status_code, 201)
    opened_remote_file = default_storage.open(upload.path)
    remote_data = opened_remote_file.read()
    self.assertEqual(
        remote_data,
        upload_data
    )


class TestFileUpload(TestCase):
    """Tests actually uploading a file."""

    def test_prepare_file_for_upload(self):
        """Tests that prepared upload URL is valid."""
        file_name = "excercies.pdf"
        upload = api.prepare_file_upload(file_name)
        parsed = urllib.parse.urlparse(upload.url)
        self.assertTrue(parsed.path.endswith(file_name))
        self.assertIn("temp_url_sig", parsed.query)

    def test_file_upload(self):
        """Tests that prepared upload URL is valid."""
        file_name = "excercies.pdf"
        upload = api.prepare_file_upload(file_name)
        perform_upload_and_verify_it(self, upload)


class TestStartFileUploadView(TestCase):
    """Test for file upload view."""

    def prepare_client(self):
        """Helper that prepares user with enough permissions to upload a file."""
        user = User.objects.create(
            username="testuser",
        )
        user.set_password("test")
        user.user_permissions.add((
            Permission.objects.get(
                codename='add_fileupload'
            )
        ))
        user.save()
        self.assertTrue(self.client.login(username="testuser", password="test"))

    def start_upload(self, file_name="some-file.gif", namespace='fluffy-cats'):
        """Starts file upload."""
        return self.client.post(
            reverse("api:start-file-upload"),
            data={
                "file_name": file_name,
                "namespace": namespace
            }
        )

    def test_simple_upload(self):
        """Test happy path."""
        file_name = "cat.png"
        self.prepare_client()
        response = self.start_upload(file_name)
        self.assertEqual(response.status_code, 201)
        perform_upload_and_verify_it(self, api.PrepareFileUploadResponse(
            **response.json()
        ))

    def test_cors(self):
        """Test that swift responds with proper CORS headers, so JS will be able to upload."""
        self.prepare_client()
        response = self.start_upload("cat.png")
        upload = api.PrepareFileUploadResponse(
            **response.json()
        )
        response = requests.options(
            upload.url,
            headers={
                "Access-Control-Request-Method": "PUT",
                "Origin": "http://unimportant"
            }
        )
        self.assertEqual(
            "*",
            response.headers["access-control-allow-origin"]
        )
        self.assertIn(
            "PUT",
            response.headers["access-control-allow-methods"]
        )
        self.assertIn(
            "GET",
            response.headers["access-control-allow-methods"]
        )

    def test_upload_not_logged_in(self):
        """Test that user that is not logged in will get 403."""
        response = self.start_upload()
        self.assertEqual(response.status_code, 403)

    def test_upload_no_perms(self):
        """Test that user that is logged but has no perms will get 403."""
        user = User.objects.create(username="testuser")
        user.set_password("test")
        user.save()
        self.assertTrue(self.client.login(username="testuser", password="test"))
        response = self.start_upload()
        self.assertEqual(response.status_code, 403)

    def test_invalid_form(self):
        """When view gets invalid data it should return 400 response."""
        self.prepare_client()
        response = self.client.post(
            reverse("api:start-file-upload"),
            data={}
        )
        self.assertEqual(400, response.status_code)

    def test_simple_upload_file_is_downloadable_from_django_storage(self):
        """Test happy path."""
        file_name = "cat.png"
        self.prepare_client()
        response = self.start_upload(file_name)
        self.assertEqual(response.status_code, 201)
        upload_response = api.PrepareFileUploadResponse(**response.json())
        perform_upload_and_verify_it(self, upload_response)
        self.assertTrue(default_storage.exists(upload_response.path))
