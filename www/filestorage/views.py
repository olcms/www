# -*- coding: utf-8 -*-

"""Views module."""

from django import forms
from django.core.validators import RegexValidator

import rest_framework.request
from rest_framework import response, views
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated

from . import api


class FileUploadForm(forms.Form):
    """Form used to parse StartFileUpload request."""

    file_name = forms.CharField(required=True)
    namespace = forms.CharField(
        required=False, validators=[RegexValidator(r"[\d\w\-_]+")])


class StartFileUpload(views.APIView):
    """A view that starts file upload."""

    permission_classes = (
        IsAuthenticated,
    )

    def check_permissions(self, request: rest_framework.request.Request):
        """Check user has ``"filestorage.add_fileupload`` perm."""
        super().check_permissions(request)
        if not request.user.has_perm("filestorage.add_fileupload"):
            self.permission_denied(request)

    def post(self, request: rest_framework.request.Request):  # pylint: disable=no-self-use
        """Prepare signed URL for upload."""
        form = FileUploadForm(data=request.data)
        if not form.is_valid():
            raise ParseError()

        upload = api.register_and_prepare_upload(
            user=request.user,
            file_name=form.cleaned_data['file_name'],
            namespace=form.cleaned_data['namespace']
        )
        return response.Response(
            upload._asdict(),
            status=201
        )
