# -*- coding: utf-8 -*-
"""Module that contains public Python API of this app."""
import hashlib
import hmac
import os
import time
import typing
import urllib.parse
from base64 import b64encode

from django.conf import settings
from django.core.files.storage import default_storage

from . import models

DEFAULT_NAMESPACE = "upload"
"""
See: :func:`get_unique_file_name`.
"""


UNIQUE_FOLDER_LENGTH = 9
"""
See: :func:`get_unique_file_name`.
"""

# NamedTuples are marked as not callable by mistake.
# pylint: disable=not-callable


def get_unique_file_name(
        file_name: str,
        namespace: typing.Optional[str]=None
) -> str:
    """
    This method creates a unique file name.

    This file has following format, in format
    ``/namespace/random-folder/file-name.ext``, for example:
    ``/fun-with-math/9AW8EQ4CGtE6/exercise.pdf``, normally Django will
    make path unique by adding random postfix to file name which works
    but when you download the file it has mangled to something like:
    ``exercise_9AW8EQ4CGtE6.pdf``, which is ugly.

    Length of unique folder is set to 9 bytes, in which case we should expect
    a collision every 10^12 uploads.

    :param file_name: A file name, without any folder.
    :param namespace: An optional namespace, e.g. course or content-item name.
                      It is purely for aesthetic purposes.
    """
    if namespace is None or not namespace.strip():
        namespace = DEFAULT_NAMESPACE

    unique_pseudo_folder = b64encode(os.urandom(UNIQUE_FOLDER_LENGTH)).decode()

    # File will be stored in swift pseudo-folder, so we don't need to use
    # system independent path operations
    return "/".join([namespace, unique_pseudo_folder, file_name])


HTTP_METHODS = {"GET", "PUT"}


def get_signed_url(
        method: str, *,
        path: str,
        key: str,
        netloc: str,
        scheme: str,
        expires: int
):
    """
    Generates signed URLs for Swift.

    :param method: Http method to use. Right now either a 'GET' or 'PUT' string.
    :param path: full path of the resource (without host). Something like:
    ``/v1/AUTH_test/test/file.pdf``
    :param key: Encryption key.
    :param netloc: Hostname of your Swift provider
    :param scheme: Either http or https.
    :param expires: When the link should expire. It uses the same
                    unit as ``time.time()`` from python stdlib (seconds after epoch)
                    If you want link to be valid for 300 seconds just do:
                    int(time.time() + 300).
    """
    if not (method in HTTP_METHODS and path.startswith('/')):
        raise AssertionError()
    hmac_body = '\n'.join(map(str, (method, expires, path)))
    sig = hmac.new(key.encode(), hmac_body.encode(), hashlib.sha1).hexdigest()
    query = "temp_url_sig={sig}&temp_url_expires={expires}".format(
        sig=sig, expires=expires
    )
    url = urllib.parse.ParseResult(
        scheme=scheme, netloc=netloc, path=path,
        query=query, params=None, fragment=None
    )
    return url.geturl()


SwiftEndpointData = typing.NamedTuple(
    "SwiftEndpointData",
    (
        ('host', str),
        ('scheme', str),
        ('base_url', str)
    )
)


def get_swift_endpoint_data() -> SwiftEndpointData:
    """Utility method that parses swift endpoint data obtained from SwiftStorage."""
    url = urllib.parse.urlparse(default_storage.base_url)
    return SwiftEndpointData(
        host=url.netloc,
        scheme=url.scheme,
        base_url=url.path
    )


def prepare_tempurl_link(method: str, file_name: str, expires: int):
    """
    This returns HTTP link for file upload.

    :param method: either GET or PUT.
    :param file_name: File (with pseudo-folder) under which you can store the URL.
    :param expires: When the link should expire. It uses the same
                unit as ``time.time()`` from python stdlib (seconds after epoch)
                If you want link to be valid for 300 seconds just do:
                int(time.time() + 300).
    :return:
    """
    endpoint_data = get_swift_endpoint_data()
    file_name = file_name.lstrip('/')
    full_path = endpoint_data.base_url + file_name
    return get_signed_url(
        method,
        path=full_path,
        netloc=endpoint_data.host,
        scheme=endpoint_data.scheme,
        expires=expires,
        key=settings.SWIFT_TEMP_URL_KEY
    )


PrepareFileUploadResponse = typing.NamedTuple(
    "PrepareFileUploadResponse",
    (
        ("url", str),
        ("file_name", str),
        ("path", str),
    )
)
"""
Properties mean the following:

* ``file_name`` Name of the uploaded file
* ``path`` Path relative to swift main storage location
* ``url`` temporary upload url
"""


def prepare_file_upload(
        file_name: str, namespace: str=None, timeout: int=(15 * 60)
) -> PrepareFileUploadResponse:
    """
    Prepares file for upload, and returns object containing temporary URL.

    :param file_name: Desired file name
    :param namespace: Desired namespace
    :param timeout: Timeout in seconds. Upload URL will be invalid after this time
    """
    mangled_file_name = get_unique_file_name(file_name, namespace)
    return PrepareFileUploadResponse(
        url=prepare_tempurl_link('PUT', mangled_file_name, int(time.time() + timeout)),
        file_name=file_name,
        path=mangled_file_name
    )


def register_and_prepare_upload(
        *, file_name: str,
        namespace: str=None,
        timeout: int=(15 * 60),
        user
) -> PrepareFileUploadResponse:
    """
    Stores information about the upload in database.

    :param file_name: Desired file name
    :param namespace: Desired namespace
    :param timeout: Timeout in seconds. Upload URL will be invalid after this time
    :param user: User performing the operation
    :return:
    """
    upload = prepare_file_upload(
        file_name=file_name, namespace=namespace, timeout=timeout
    )

    models.FileUpload.objects.create(
        file_owner=user,
        file_path=upload.path,
        file_name=upload.file_name
    )

    return upload
