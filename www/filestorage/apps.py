# -*- coding: utf-8 -*-
"""Apps module."""
from django.apps import AppConfig


class FilestorageConfig(AppConfig):
    """App."""

    name = 'www.filestorage'
