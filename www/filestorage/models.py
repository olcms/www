# -*- coding: utf-8 -*-

"""Models."""

from django.conf import settings
from django.db import models


class FileUpload(models.Model):
    """
    Model that tracks file uploads it's here for auditory purposes only.

    Moreover unique index on file_path will raise error if
    there is path clash for files. Such a clash should not happen.
    """

    file_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=False, blank=False
    )
    file_name = models.CharField(
        max_length=1000, unique=False, null=False
    )
    file_path = models.CharField(
        max_length=1000, unique=True, null=False
    )
    upload_datetime = models.DateTimeField(
        null=False,
        auto_now_add=True
    )
