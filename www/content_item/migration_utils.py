# coding=utf-8

"""
Utils used for migrations.

This methods are used for migrations --- so please don't change the logic unless you know what to do.
"""


def create_domain_type(apps, slug: str, domain_type_name: str, order=None):
    """Creates a domain type."""
    DomainType = apps.get_model("content_item", "DomainType")
    domain_type, created = DomainType.objects.get_or_create(  # pylint: disable=unused-variable
        slug=slug
    )
    domain_type.name = domain_type_name
    if order is not None:
        domain_type.order = order
    domain_type.save()


def create_domain(apps, type_slug: str, slug: str, name: str, order=None):
    """Creates a domain."""
    DomainType = apps.get_model("content_item", "DomainType")
    Domain = apps.get_model("content_item", "Domain")
    domain_type = DomainType.objects.get(slug=type_slug)
    domain, created = Domain.objects.get_or_create(  # pylint: disable=unused-variable
        slug=slug,
        type=domain_type
    )
    if order is not None:
        domain.order = order
    domain.name = name
    domain.save()
