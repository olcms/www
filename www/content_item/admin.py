# -*- coding: utf-8 -*-
"""Django admin config for resource app."""

from django.contrib import admin
from django.utils.translation import ugettext_lazy

from . import models


@admin.register(models.DomainType)
class DomainTypeAdmin(admin.ModelAdmin):
    """Admin for Domain types"""

    list_display = [
        "slug",
        "name",
        "order",
    ]

    fields = [
        "slug",
        "name",
        "order",
    ]


@admin.register(models.Domain)
class Domain(admin.ModelAdmin):
    """Admin for domains"""

    list_display = [
        "type",
        "slug",
        "name",
        "order",
    ]


@admin.register(models.ContentItem)
class ContentItemAdmin(admin.ModelAdmin):
    """Admin for content items"""

    list_display = [
        "title",
        "main_discipline",
        "get_owners",
        "created_at",
        "last_updated_at",
        "featured",
        "verified"
    ]

    search_fields = [
        "title",
        "description",
        "owners__username",
        "owners__email",
        "main_discipline__name"
    ]

    list_filter = [
        "featured",
        "verified",
        "main_discipline",
        "domains"
    ]

    filter_horizontal = [
        "owners",
    ]

    def get_owners(self, obj) -> str:  # pylint: disable=no-self-use
        """Format owners for list display"""
        return "; ".join(owner.username for owner in obj.owners.all())  # pragma: no cover

    get_owners.short_description = ugettext_lazy("Owner usernames")


@admin.register(models.ContentItemVerifier)
class ContentItemVerifierAdmin(ContentItemAdmin):

    """Admin that allows only verification of content items."""

    fields = [
        "title",
        "description",
        "verified"
    ]

    readonly_fields = [
        "title",
        "description",
    ]
