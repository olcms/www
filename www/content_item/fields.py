# -*- coding: utf-8 -*-
"""Additional form fields for content items"""

import typing

from django import forms
from django.contrib.postgres.forms import SimpleArrayField

from www.content_item import models
from www.tags.serializers import TagRelatedField

SuggestionUrlType = typing.Union[str, typing.Callable[[], str], None]


class TagField(SimpleArrayField):

    """
    A simple field that allows user to input tags.

    Right now it does not support initial data.
    """

    def __init__(
            self, *args, suggestion_url: SuggestionUrlType=None, **kwargs
    ):
        self.suggestion_url = suggestion_url
        kwargs['base_field'] = forms.CharField()
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        if value is None:
            return []
        tag_names = super().to_python(value)
        tag_serializer = TagRelatedField(model=models.ContentItem)
        return [tag_serializer.to_internal_value(tag) for tag in tag_names]
