# -*- coding: utf-8 -*

"""
Contains search index instances that configure Haystack.
"""

from haystack import indexes
from haystack.query import SearchQuerySet

from . import models


class ContentIndex(indexes.SearchIndex, indexes.Indexable):

    """
    How to add new field to index:

    1. Decide if this field will be a part of indexed document
       (it should be searchable using main "Search" box it needs to),
       or an extra field (searchable using dedicated field in search view. ).
    2. If it will be part of indexed document you'll need to add it to
       ``indexes/content_item/contentitem_text.txt`` (this is template
       used to generate indexes)
    3. If it needs to be indexed separately add it to this model index
       and then add it to ``forms.ContentItemSearchForm``.
    4. Rebuild the index (``./manage.py rebuild_index``).
    """

    text = indexes.CharField(document=True, use_template=True)

    license = indexes.CharField(model_attr="license")
    title = indexes.CharField(boost=2, model_attr="title")
    description = indexes.CharField(boost=1.25, model_attr="description")

    language = indexes.CharField(model_attr="language")

    hidden = indexes.BooleanField(model_attr="hidden")

    domains = indexes.MultiValueField(boost=1.5, model_attr="domains__slug")
    tags = indexes.MultiValueField(boost=1.5, model_attr="tags__name")
    verified = indexes.BooleanField(model_attr='verified')

    def prepare(self, obj: 'models.ContentItem'):
        data = super().prepare(obj)
        if obj.featured:
            data['boost'] = 3.0
        return data

    @staticmethod
    def get_search_queryset() -> SearchQuerySet:
        """Search Queryset filtered to hide deleted ContentItems."""
        return SearchQuerySet().filter(hidden='false', verified='true')

    def get_updated_field(self):
        return "last_updated_at"

    def get_model(self):
        return models.ContentItem
