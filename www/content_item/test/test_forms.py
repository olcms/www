# -*- coding: utf-8 -*-
"""Test for forms."""
from allauth.tests import TestCase

from www.content_item import forms, models
from . import utils


class BaseFormTest(TestCase):
    """Base test class."""

    BASE_FORM_DATA = {
        "title": "A content item title",
        "description": "Some description",
        "authors": "Julius Ceasear, Marcus Aurelius",
        "tags": "some, tag, list",
        "type": "basic",
        "license": "CC0",
        "language": "en"
    }

    def setUp(self):
        self.base_form_data = dict(**self.BASE_FORM_DATA)
        self.base_form_data["main_discipline"] = models.Domain.objects.get(slug="medicine").pk

    def verify_model_with_basic_data(self, model):
        """Checks model created from BASE_FORM_DATA for having appropriate values."""
        with self.subTest("type"):
            self.assertEqual(model.type, "basic")
        with self.subTest("license"):
            self.assertEqual(model.license, "CC0")
        with self.subTest("tags"):
            self.assertEqual(
                {tag.name for tag in model.tags.all()},
                {'some', 'tag', 'list'}
            )
        with self.subTest("authors"):
            self.assertEqual(
                model.authors,
                ["Julius Ceasear", "Marcus Aurelius"]
            )
        with self.subTest("title"):
            self.assertEqual(
                model.title,
                "A content item title"
            )
        with self.subTest("description"):
            self.assertEqual(
                model.description,
                "Some description"
            )
        with self.subTest("language"):
            self.assertEqual(model.language, "en")

    def test_save_happy_path(self):
        """Test create object."""

        form = forms.ContentItemCreateForm(
            data=self.base_form_data
        )
        form.full_clean()

        model = form.save(commit=False)
        group = utils.ContentItemLanguageGroupFactory()
        group.save()
        model.group = group
        model.save()
        form.save_m2m()

        self.verify_model_with_basic_data(model)

    def test_disciplines_contain_main_discipline(self):
        """Test create object."""

        form = forms.ContentItemCreateForm(
            data=self.base_form_data
        )
        form.full_clean()

        model = form.save(commit=False)
        group = utils.ContentItemLanguageGroupFactory()
        group.save()
        model.group = group
        model.save()
        form.save_m2m()

        self.assertEqual(
            set(model.domains.all()),
            {model.main_discipline}
        )

    def test_override_whole_content_item(self):
        """Test update object."""
        initial = utils.ContentItemFactory()
        form = forms.ContentItemCreateForm(
            instance=initial,
            data=self.base_form_data)
        form.full_clean()
        self.assertTrue(form.is_valid(), form._errors)  # pylint: disable=protected-access
        model = form.save()
        self.verify_model_with_basic_data(model)
