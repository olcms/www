# coding=utf-8
"""Tests for Google analytics."""
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse

from www.content_item.test.utils import ContentItemFactory

GOOGLE_ANALYTICS_CANARY = "UA-CANARY-1"


class TestAnalytics(TestCase):
    """Tests for Google analytics."""

    def assert_ga_in_response(self, response):
        """
        Check if google analytics is rendered into response.

        Won't check if suddenly Google Analytics will change their API,
        but this shouldn't happen.
        """
        content = response.rendered_content
        self.assertIn("'" + GOOGLE_ANALYTICS_CANARY + "'", content)
        self.assertIn("GoogleAnalyticsObject", content)
        self.assertIn("https://www.google-analytics.com/analytics.js", content)

    def assert_ga_not_in_response(self, response):
        """Check if analytics is missing from response."""
        with self.assertRaises(AssertionError):
            self.assert_ga_in_response(response)

    @override_settings(GOOGLE_ANALYTICS_TRACKING_CODE=GOOGLE_ANALYTICS_CANARY)
    def test_analytics_present_in_search_page(self):
        """Test analytics in search page."""
        self.assert_ga_in_response(self.client.get(reverse("content_item:search")))

    @override_settings(GOOGLE_ANALYTICS_TRACKING_CODE=GOOGLE_ANALYTICS_CANARY)
    def test_analytics_in_content_item(self):
        """Test analytics in search content item page."""
        item = ContentItemFactory()
        self.assert_ga_in_response(
            self.client.get(reverse("content_item:detail", kwargs={'pk': item.pk}))
        )

    @override_settings(GOOGLE_ANALYTICS_TRACKING_CODE=None)
    def test_analytics_missing_if_no_tracking_code(self):
        """Test analytics is missing if there is no tracking code setting."""
        self.assert_ga_not_in_response(self.client.get(reverse("content_item:search")))
