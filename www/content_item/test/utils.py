# -*- coding: utf-8 -*-
"""Factories for tests."""
import collections
import random

import factory
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.conf import settings

from www.content_item.const import License
from www.tags.models import Tag
from www.tags.tests import tag_generator
from .. import models


# This is used in tests so it will fail if there is an error, moreover
# factory_boy uses some Python magic that confuses pylint.
# pylint: disable=no-member, unused-argument


class DomainTypeFactory(factory.django.DjangoModelFactory):
    """DomainType factory"""

    slug = factory.Sequence(lambda n: "age_group%03d" % n)
    name = "Age Group"

    class Meta:
        """Metaclass"""
        model = models.DomainType


class DomainFactory(factory.django.DjangoModelFactory):
    """Domain factory"""

    type = factory.SubFactory(DomainTypeFactory)  # noqa: B001
    slug = factory.Sequence(lambda n: "middle_school%03d" % n)
    name = "Middle School"

    class Meta:
        """Metaclass"""
        model = models.Domain


class UserFactory(factory.django.DjangoModelFactory):
    """User Factory --- used by factories for models that reference users."""
    username = factory.Sequence(lambda n: "user%03d" % n)
    first_name = factory.Sequence(lambda n: "User 1 %03d" % n)
    last_name = "Lastname"
    email = factory.Sequence(lambda n: "user%03d@gmail.com" % n)

    class Meta:
        """Metaclass"""
        model = get_user_model()


class ContentItemLanguageGroupFactory(factory.django.DjangoModelFactory):
    """Content item language group factory."""
    class Meta:
        """Metaclass"""
        model = models.ContentItemLanguageGroup


class ContentItemFactory(factory.django.DjangoModelFactory):
    """Content item factory."""
    title = factory.Sequence(lambda n: "Content Item {}".format(n))
    description = "A description"
    type = "basic"  # noqa: B001
    authors = ["Jacek Bzdak"]
    license = factory.Sequence(lambda x: random.choice(License.choices)[0])  # noqa: B001
    technical_requirements = 'Technical requirements'
    knowledge_gain = 'Knowledge gain'
    hidden = False
    language = factory.Sequence(lambda x: random.choice(settings.LANGUAGES)[0])
    featured = False
    group = factory.SubFactory(ContentItemLanguageGroupFactory)
    main_discipline = factory.LazyAttribute(
        lambda x: random.choice(models.Domain.objects.filter(type__slug="discipline"))
    )
    workforce_description = ''
    workforce_title = ''
    verified = True

    created_at = timezone.datetime(1985, 9, 19, 21, 30)
    last_updated_at = timezone.now()

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        """Generates tags. """
        gen = tag_generator.GeneratorFactory(
            models.ContentItem,
            generator=tag_generator.TagGenerator,
            num_generated=10
        )
        if not create:
            return

        if extracted is None:
            extracted = gen.generate_tags()

        for tag in extracted:
            if isinstance(tag, str):
                tag = Tag.objects.get_or_create_tag(
                    tag,
                    ContentType.objects.get_for_model(models.ContentItem).pk
                )
            self.tags.add(tag)

    @factory.post_generation
    def domains(self, create, extracted, **kwargs):
        """Generates domains."""
        if not create:
            pass
        if extracted:
            if not isinstance(extracted, models.Domain):
                for domain in extracted:
                    self.domains.add(domain)
            else:
                self.domains.add(extracted)
        else:
            try:
                discipline = models.DomainType.objects.get(slug="discipline")
                domains = models.Domain.objects.exclude(type=discipline).all()
                for __ in range(10):
                    self.domains.add(random.choice(domains))
            except models.DomainType.DoesNotExist:
                pass

    @factory.post_generation
    def owners(self, create, extracted, **kwargs):
        """Generates owners."""
        if not create:
            return
        if extracted:
            if not isinstance(extracted, collections.Sequence):
                extracted = [extracted]
            self.owners.add(*extracted)
        else:
            self.owners.add(UserFactory())

    class Meta:
        """Metaclass"""
        model = models.ContentItem
