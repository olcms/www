# -*- coding: utf-8 -*-
"""Test the management commands in this app."""
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test.testcases import TestCase

from www.content_item import models


class TestMakeTestData(TestCase):
    """Test for make_test_data command."""

    def test_make_test_data_command(self):
        """We just check that appropriate number of ContentItems is created."""
        self.assertEqual(0, models.ContentItem.objects.all().count())
        call_command('make_test_data', count=10, batch=7)
        self.assertEqual(10, models.ContentItem.objects.all().count())

    def test_make_test_data_command_with_owner(self):
        """We just check that appropriate number of ContentItems is created."""
        user = get_user_model().objects.create(username="test")
        self.assertEqual(0, models.ContentItem.objects.all().count())
        call_command('make_test_data', count=10, batch=7)
        self.assertEqual(10, models.ContentItem.objects.all().count())
        self.assertIn(
            user,
            models.ContentItem.objects.all()[0].owners.all()
        )
