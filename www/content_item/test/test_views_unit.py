# -*- coding: utf-8 -*-
"""Unit-tests for views."""

from unittest import mock
from django.contrib.auth import get_user_model
from django.urls import reverse

from www.content_item.const import License
from www.content_item.models import ContentItem, Domain
from www.page_block.models import PageBlock, PageBlockTranslation
from www.resource.tests.utils import LinkResourceFactory
from www.utils.test_utils import LoginTestMixin, model_rich_compare
from . import utils


# Pylint doesn't know about pk property
# pylint: disable=no-member


class BaseTestView(LoginTestMixin):
    """Base test case."""

    def content_item_form_data(self):  # pylint: disable=no-self-use
        """
        Form data for example ContentItem.

        Corresponds to content_item_json_data()
        """
        medicine_pk = Domain.objects.get(slug="medicine").pk
        return {
            "title": "A content item title",
            "description": "",
            "authors": "Jacek, Placek",
            "type": "basic",
            "tags": "foo, bar, baz",
            # Domains would be here, but form doesn't handle them right
            # now
            "license": License.cc_by,
            "cover_image": None,
            "language": "en",
            "main_discipline": medicine_pk
        }

    def verify_content_item_with_form(self, model, form_data):
        """
            Checks that model and form representation match.
        """

        def _verify_simple_prop(property_name: str):
            with self.subTest(property_name):
                self.assertEqual(getattr(model, property_name), form_data[property_name])

        def _split_comma_separated(property_name: str):
            return list(map(str.strip, form_data[property_name].split(",")))

        _verify_simple_prop("license")
        _verify_simple_prop("title")
        _verify_simple_prop("description")
        _verify_simple_prop("type")
        with self.subTest("tags"):
            self.assertEqual(
                set(_split_comma_separated("tags")),
                {tag.name for tag in model.tags.all()}
            )
        with self.subTest("authors"):
            self.assertEqual(
                _split_comma_separated('authors'),
                model.authors
            )


class TestContentItemView(BaseTestView):
    """
    Tests for normal views.
    """

    def test_unloged_user_add_get(self):
        """
        Anonymous user should be redirected to login when he tries to add content item.

        This one tests GET method (user entering the page)
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        response = self.client.get(
            reverse("content_item:add")
        )

        self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(0, ContentItem.objects.all().count())

    def test_unloged_user_add(self):
        """
        Anonymous user should be redirected to login when he tries to add content item.

        This one tests POST method (user trying to submit the form).
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        response = self.client.post(
            reverse("content_item:add"),
            data=self.content_item_form_data()
        )

        self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(0, ContentItem.objects.all().count())

    def test_unloged_user_edit_get(self):
        """
        Anonymous user should be redirected to login when he tries to edit content item.

        This one tests GET method (user entering the page)
        """
        content_item = utils.ContentItemFactory()
        response = self.client.get(
            reverse("content_item:edit", kwargs={'pk': content_item.pk})
        )

        self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(response.status_code, 302)
        model_rich_compare(self, content_item, ContentItem.objects.get(pk=content_item.pk))

    def test_unloged_user_edit(self):
        """
        Anonymous user should be redirected to login when he tries to edit content item.

        This one tests POST method (user trying to submit the form).
        """
        content_item = utils.ContentItemFactory()
        response = self.client.post(
            reverse("content_item:edit", kwargs={'pk': content_item.pk}),
            data=self.content_item_form_data()
        )

        self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(response.status_code, 302)
        model_rich_compare(self, content_item, ContentItem.objects.get(pk=content_item.pk))

    def test_logged_in_add(self):
        """
        Test that logged in user can add ContentItem.
        :return:
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        self.login()

        form_data = self.content_item_form_data()
        response = self.client.post(
            reverse("content_item:add"),
            data=form_data,
            follow=False
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/content_item/detail'))
        self.assertEqual(1, ContentItem.objects.all().count())
        instance = ContentItem.objects.all()[0]
        self.verify_content_item_with_form(instance, form_data)

    def test_resize_images_on_add(self):
        """
        Test task which resizes images is generated on content item add
        :return:
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        self.login()

        # replace async_apply method in resize images task and check if it was called
        with mock.patch("www.content_item.tasks.resize_content_item_images.apply_async") as apply_async:
            form_data = self.content_item_form_data()

            response = self.client.post(
                reverse("content_item:add"),
                data=form_data,
                follow=False
            )
            self.assertEqual(response.status_code, 302)
            self.assertTrue(response.url.startswith('/content_item/detail'))
            self.assertEqual(1, ContentItem.objects.all().count())
            content_item = ContentItem.objects.first()
            apply_async.assert_called_with(args=[content_item.pk], countdown=10)

    def test_logged_in_add_invalid_form(self):
        """
        Test that logged in user can add ContentItem.
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        self.login()

        form_data = self.content_item_form_data()
        del form_data['title']
        response = self.client.post(
            reverse("content_item:add"),
            data=form_data,
            follow=False
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"This field is required.", response.content)
        self.assertEqual(0, ContentItem.objects.all().count())

    def test_logged_in_edit_all_fields(self):
        """
        Test edition for logged in user.
        """
        initial_instance = utils.ContentItemFactory(
            owners=self.user.pk
        )
        self.login()

        form_data = self.content_item_form_data()
        response = self.client.post(
            reverse("content_item:edit", kwargs={'pk': initial_instance.pk}),
            data=form_data,
            follow=False
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith('/content_item/detail'))
        self.assertEqual(1, ContentItem.objects.all().count())
        instance = ContentItem.objects.all()[0]
        self.verify_content_item_with_form(instance, form_data)

    def test_cant_edit_content_item_when_not_owner(self):
        """
        Test that logged in user can't edit ContentItem he doesn't own.
        """
        other_user = get_user_model().objects.create(
            email="else@example.com",
            username="someone-else"
        )
        content_item = utils.ContentItemFactory(
            owners=other_user.pk
        )
        self.login()
        response = self.client.post(
            reverse("content_item:edit", kwargs={'pk': content_item.pk}),
            data=self.content_item_form_data()
        )

        self.assertTrue(response.url.startswith('/accounts/login/?'))
        self.assertEqual(response.status_code, 302)
        model_rich_compare(self, content_item, ContentItem.objects.get(pk=content_item.pk))

    def test_owner_can_see_deleted_content_item(self):
        """Checks that owner can see deleted content item."""
        content_item = utils.ContentItemFactory(
            owners=self.user,
            hidden=True
        )
        self.login()
        resposnse = self.client.get(reverse("content_item:detail", kwargs={'pk': content_item.pk}))
        self.assertEqual(resposnse.status_code, 200)

    def test_unlogged_user_cant_see_deleted_content_item(self):
        """Checks that unlogged user can't see deleted content item."""
        content_item = utils.ContentItemFactory(
            owners=self.user,
            hidden=True
        )
        resposnse = self.client.get(reverse("content_item:detail", kwargs={'pk': content_item.pk}))
        self.assertEqual(resposnse.status_code, 404)

    def test_cc_icons(self):
        """
        Test we don't get 500 errors on missing icons. This is an actual bug.
        """
        for tested_license in License.values:
            with self.subTest(tested_license):
                item = utils.ContentItemFactory(license=tested_license)
                LinkResourceFactory(content_item=item, resource_license=tested_license)
                resposnse = self.client.get(reverse("content_item:detail", kwargs={'pk': item.pk}))
                self.assertEqual(resposnse.status_code, 200)

    def test_no_description(self):
        """
        Test that without local description in content item PageBlock is installed.
        :return:
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        form_data = self.content_item_form_data()

        self.login()
        block = PageBlock.objects.create(block_type="workforce.medicine")
        PageBlockTranslation.objects.create(
            page_block=block,
            default_translation=True,
            language=form_data['language'],
            title="AAAAA",
            contents_md="BBBBB canary BBBBB"
        )

        response = self.client.post(
            reverse("content_item:add"),
            data=form_data,
            follow=True
        )

        assert response.status_code == 200
        assert b"BBBBB canary BBBBB" in response.content

    def test_description(self):
        """
        Test that when workforce description is present it replaces page block.
        """
        self.assertEqual(0, ContentItem.objects.all().count())
        form_data = self.content_item_form_data()
        form_data['workforce_description'] = "CCCCC canary CCCCC"

        self.login()
        block = PageBlock.objects.create(block_type="workforce.medicine")
        PageBlockTranslation.objects.create(
            page_block=block,
            default_translation=True,
            language=form_data['language'],
            title="AAAAA",
            contents_md="BBBBB canary BBBBB"
        )

        response = self.client.post(
            reverse("content_item:add"),
            data=form_data,
            follow=True
        )

        assert response.status_code == 200
        assert b"BBBBB canary BBBBB" not in response.content
        assert b"CCCCC canary CCCCC" in response.content


class TestContentItemVerifiedView(BaseTestView):
    """
    Test verified views.
    """

    USER_PERMS = []

    def test_default_content_item_is_unverified(self):
        """
        When user has no "is_autoverified" permission content item is not verified initially.
        """
        self.login()
        self.assertEqual(0, ContentItem.objects.all().count())
        self.client.post(
            reverse("content_item:add"),
            data=self.content_item_form_data()
        )
        self.assertEqual(1, ContentItem.objects.all().count())
        content_item = ContentItem.objects.all()[0]
        assert not content_item.verified


class TestContentItemVerifiedViewAutoverify(BaseTestView):
    """
    Test verified views.
    """

    USER_PERMS = ["content_item.is_autoverified"]

    def test_ci_is_verified_when_created_by_autoverify_user(self):
        """
        When user has "is_autoverified" permission content item is verified.
        """
        self.login()
        self.assertEqual(0, ContentItem.objects.all().count())
        self.client.post(
            reverse("content_item:add"),
            data=self.content_item_form_data()
        )
        self.assertEqual(1, ContentItem.objects.all().count())
        content_item = ContentItem.objects.all()[0]
        assert content_item.verified
