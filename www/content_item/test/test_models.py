# -*- coding: utf-8 -*-
"""Tests foe models."""
from unittest import mock

from PIL import Image
from allauth.tests import TestCase
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy

import www.utils.validators
from www.content_item import const
from www.content_item import models
from www.content_item.tasks import save_pil_image, resize_content_item_images
from www.content_item.test.utils import ContentItemFactory
from www.resource.tests.utils import VideoResourceFactory
from . import utils


# pylint: disable=no-member,no-self-use

class TestLanguageValidator(TestCase):
    """Test language codes validator."""

    def test_validator_fail(self):
        """Test that incorrect language code throws validation error."""
        with self.assertRaises(ValidationError):
            www.utils.validators.validate_language_code('Qe')

    def test_validator_pass(self):
        """Test that correct language code passes."""
        for lang in (l[0] for l in const.LANGUAGES):
            www.utils.validators.validate_language_code(lang)


class TestStringRepresentation(TestCase):
    """Test string representation used in various combo-boxes"""

    def test_content_item(self):
        """Tests content item str function"""
        model = ContentItemFactory(title="Super Movie")
        self.assertEqual(str(model), "Super Movie")

    def test_content_item_repr(self):
        """Tests content item str function"""
        model = ContentItemFactory(title="Super Movie")
        self.assertRegex(  # pylint: disable=deprecated-method
            repr(model), r"<Content Item pk=\d+ title=Super Movie>"
        )

    def test_domain_str(self):
        """Tests str it is used in combo-boxes so should be readable."""
        domain_type = models.Domain(
            slug="a-domain",
            name="domain name"
        )
        self.assertEqual(str(domain_type), "domain name")

    def test_domain_type_str(self):
        """Tests str it is used in combo-boxes so should be readable."""
        domain_type = models.DomainType(
            slug="a-domain",
            name="domain name"
        )
        self.assertEqual(str(domain_type), "domain name")


class TestContentItemTypes(TestCase):
    """Test types are as expected."""

    def test_movie_content_item_resource_types(self):
        """Test resource types for video resource."""
        content_item = utils.ContentItemFactory()
        content_item.type = 'movie'
        self.assertIsInstance(content_item.resource_types, dict)  # pylint: disable=no-member

        expected = [
            ['file', 'file', ugettext_lazy('File')],
            ['video', 'video', ugettext_lazy('Video')],
            ['subtitle', 'subtitle', ugettext_lazy('Subtitle')],
        ]
        self.assertEqual(
            [
                [key, value.value, value.label]
                for key, value in content_item.resource_types.items()  # pylint: disable=no-member
            ],
            expected
        )

    def test_movie_content_item_resource_types_with_video_res(self):
        """Test resource types for video resource."""
        video_res = VideoResourceFactory()
        video_res.content_item.type = 'movie'
        self.assertIsInstance(video_res.content_item.resource_types, dict)  # pylint: disable=no-member

        expected = [
            ['file', 'file', ugettext_lazy('File')],
            ['subtitle', 'subtitle', ugettext_lazy('Subtitle')],
        ]
        self.assertEqual(
            [
                [key, value.value, value.label]
                for key, value in video_res.content_item.resource_types.items()  # pylint: disable=no-member
            ],
            expected
        )


class TestContentItemGroupSize(TestCase):
    """ Test group size property of content_item """

    def test_content_item_group_size(self):
        """
        Check that for valid values of group size validation passes
        :return:
        """
        content_item = utils.ContentItemFactory()
        content_item.min_group_size = 3
        content_item.max_group_size = 5
        content_item.clean()

    def test_content_item_wrong_group_size(self):
        """
        Check content item validation of group size field
        :return:
        """
        content_item = utils.ContentItemFactory()
        content_item.min_group_size = 3
        content_item.max_group_size = 1

        with self.assertRaises(ValidationError) as cm:
            content_item.clean()

        self.assertIn('max_group_size', cm.exception.message_dict)


class TestContentItemImageResize(TestCase):
    """ Test if resizing cover images and miniature images in content items works properly. """

    def test_resize_cover_image(self):
        """ Test if all image fields are populated after uploading only cover_image, check image sizes. """

        # run celery tasks synchronous from local thread
        from django.conf import settings
        settings.CELERY_ALWAYS_EAGER = True

        content_item = utils.ContentItemFactory()

        image = Image.new("RGB", (512, 512), "white")
        save_pil_image(content_item, content_item.cover_image, image, "testimage", "jpeg")

        self.assertTrue(content_item.cover_image)
        self.assertFalse(content_item.miniature_image)
        self.assertFalse(content_item.cover_image_resized)
        self.assertFalse(content_item.miniature_image_resized)

        # call Celery task, and wait for result (task should execute right away)
        # with CELERY_ALWAYS_EAGER tasks should execute from local thread, but this seems
        # to be false - I need to refresh object from db to get updated fields
        res = resize_content_item_images.apply(args=[content_item.pk])
        res.wait(timeout=None, propagate=True, interval=0.5)

        content_item.refresh_from_db()

        # we should have all other image fields populated by now
        self.assertTrue(content_item.cover_image)
        self.assertTrue(content_item.miniature_image)
        self.assertTrue(content_item.cover_image_resized)
        self.assertTrue(content_item.miniature_image_resized)

        # check if images have correct sizes
        self.assertEqual((content_item.cover_image.width, content_item.cover_image.height),
                         (512, 512))

        self.assertEqual((content_item.cover_image.width, content_item.cover_image.height),
                         (content_item.miniature_image.width, content_item.miniature_image.height))

        self.assertEqual((content_item.cover_image_resized.width, content_item.cover_image_resized.height),
                         const.ImageProperties.cover_image_size)

        self.assertEqual((content_item.miniature_image_resized.width, content_item.miniature_image_resized.height),
                         const.ImageProperties.miniature_size)

        content_item.clean()

    def test_resize_cover_and_miniature_images(self):
        """ Test if all image fields are populated after uploading cover_image and miniature, check image sizes. """

        # run celery tasks synchronous from local thread
        from django.conf import settings
        settings.CELERY_ALWAYS_EAGER = True

        content_item = utils.ContentItemFactory()

        image = Image.new("RGB", (1280, 150), "white")
        image2 = Image.new("RGB", (128, 128), "white")

        save_pil_image(content_item, content_item.cover_image, image, "testimage", "jpeg")
        save_pil_image(content_item, content_item.miniature_image, image2, "testimage2", "jpeg")

        self.assertTrue(content_item.cover_image)
        self.assertTrue(content_item.miniature_image)
        self.assertFalse(content_item.cover_image_resized)
        self.assertFalse(content_item.miniature_image_resized)

        # see previous test for explanation (test_resize_cover_image)
        res = resize_content_item_images.apply(args=[content_item.pk])
        res.wait(timeout=None, propagate=True, interval=0.5)

        content_item.refresh_from_db()

        # we should have all other image fields populated by now

        self.assertTrue(content_item.cover_image)
        self.assertTrue(content_item.miniature_image)
        self.assertTrue(content_item.cover_image_resized)
        self.assertTrue(content_item.miniature_image_resized)

        # check if images have correct sizes

        self.assertEqual((content_item.cover_image.width, content_item.cover_image.height),
                         (1280, 150))

        self.assertEqual((content_item.miniature_image.width, content_item.miniature_image.height),
                         (128, 128))

        self.assertEqual((content_item.cover_image_resized.width, content_item.cover_image_resized.height),
                         const.ImageProperties.cover_image_size)

        self.assertEqual((content_item.miniature_image_resized.width, content_item.miniature_image_resized.height),
                         const.ImageProperties.miniature_size)

        content_item.clean()

    @mock.patch('www.content_item.tasks.resize_content_item_images.retry')
    @mock.patch('www.content_item.tasks.save_pil_image')
    def test_failed_resize_retry(self, mock_save, mock_retry):
        """ Test if failed resizing task will be retried. """

        # run celery tasks synchronous from local thread
        from django.conf import settings
        settings.CELERY_ALWAYS_EAGER = True

        content_item = utils.ContentItemFactory()

        image = Image.new("RGB", (512, 512), "white")
        save_pil_image(content_item, content_item.cover_image, image, "testimage", "jpeg")

        self.assertTrue(content_item.cover_image)
        self.assertFalse(content_item.miniature_image)
        self.assertFalse(content_item.cover_image_resized)
        self.assertFalse(content_item.miniature_image_resized)

        mock_save.side_effect = error = Exception()

        with self.assertRaises(Exception):
            resize_content_item_images(content_item_pk=content_item.pk)  # pylint: disable=no-value-for-parameter
        mock_retry.assert_called_with(exc=error)

        content_item.clean()
