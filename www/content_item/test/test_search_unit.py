# -*- coding: utf-8 -*-

"""
Misc tests for content item index.
"""
import typing
import urllib.parse
from unittest import skip
from unittest.mock import Mock

from django.http.response import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.test import TestCase, RequestFactory
from django.urls.base import reverse

from www.content_item import forms
from www.content_item.const import License
from www.content_item.models import ContentItem, DomainType, Domain
from www.content_item.test.utils import ContentItemFactory
from www.utils.search_utils import RealtimeSearchTest
from www.utils.test_utils import auto_repr, LoginTestMixin


# Tests will fail if there is missing member --- this is triggered by mixins.
# pylint: disable=no-member,no-value-for-parameter,not-callable,missing-docstring


class TestRanking(RealtimeSearchTest, TestCase):

    """
    This tests whether we have working ranking in our search.

    I have added boosts to title, description and tags. We create content item each containing
    searched term ("canary") in title, description tags and authors, and check if they come out in
    right order.
    """

    def setUp(self):
        super().setUp()
        # Whoosh returned entries in creation order when they got the same rank,
        # so create items in different order, to check if ordering takes place.
        self.content_items = [
            ContentItemFactory(tags=["canary", "bar", "baz"]),
            ContentItemFactory(description="This does canary", tags=[]),
            ContentItemFactory(authors=["canary", "bar", "baz"], tags=[]),
            ContentItemFactory(title="A super canary", tags=[]),
            ContentItemFactory(description="This does canary", tags=[], featured=True),
        ]
        self.expected_order = [
            self.content_items[4],
            self.content_items[3],
            self.content_items[1],
            self.content_items[0],
            self.content_items[2],
        ]

    @skip
    def test_ranking_query(self):  # pragma: no cover
        """Test whether returned items are ranked properly."""
        form = forms.ContentItemSearchForm(data={"q": "canary"})
        sqs = form.search()
        self.assertEqual(4, sqs.count())
        items = [s.object.pk for s in sqs]
        ids = [ci.pk for ci in self.expected_order]

        self.assertEqual(items, ids, [auto_repr(s.object) for s in sqs])


class TestSearchForm(RealtimeSearchTest, TestCase):

    """Misc tests for search form."""

    def test_form_no_query_found(self):
        """Checks that no_query_found returns SearchQuerySet.all()."""
        form = forms.ContentItemSearchForm()
        form.searchqueryset = Mock()
        form.searchqueryset.all = lambda: "canary"
        self.assertFalse(form.is_valid())
        self.assertEqual(form.no_query_found(), "canary")

    def test_everything_returned_on_invalid_form(self):
        """Checks that if form is not valid, all items are returned from search."""
        form = forms.ContentItemSearchForm(
            data={'license': "NoSuchLicense"}
        )
        form.searchqueryset = Mock()
        form.searchqueryset.all = lambda: "canary"
        self.assertFalse(form.is_valid())
        self.assertEqual(form.no_query_found(), "canary")
        self.assertEqual(form.search(), "canary")


class TestSearchView(LoginTestMixin, TestCase):
    """
    Test search view.
    """

    def setUp(self):
        super().setUp()
        self.login()
        self.request_factory = RequestFactory()

    def test_post(self):
        """
        Tests POST requests are redirected to nicely rendered GET links.
        """
        discipline = Domain.objects.filter(type__slug="discipline").first()
        data = {
            "license": License.cc0,
            "tags": ",".join(['foo', 'bar', 'baz']),
            "page": 10,
            "q": "A term",
            "domain": discipline.pk,
            "language": ["en"]
        }
        response = self.client.post(reverse("content_item:search"), data)
        self.assertIsInstance(response, HttpResponseRedirect)

        parsed = urllib.parse.urlparse(response['Location'])

        expected_kwargs = {
            "page": 10,
            "term": "A term",
            "domain": discipline.slug,
        }

        expected_query = {
            "license": [License.cc0],
            "tags": [",".join(['foo', 'bar', 'baz'])],
            "language": ["en"]
        }

        self.assertEqual(parsed.path, reverse("content_item:search", kwargs=expected_kwargs))

        self.assertEqual(urllib.parse.parse_qs(urllib.parse.unquote(parsed.query)), expected_query)

    def test_invalid_get(self):
        """
        Test that on invalid form parameters you'll get nice 400 error.
        """
        discipline = Domain.objects.filter(type__slug="discipline").first()
        data = {
            "license": License.cc0,
            "tags": ",".join(['foo', 'bar', 'baz']),
            "page": 10,
            "q": "A term",
            "domain": discipline.pk,
            "language": ["Johnny ); DROP TABLE users; ---"]
        }
        response = self.client.get(reverse("content_item:search"), data)
        self.assertEqual(response.status_code, 400)


class TestTestDomainSearch(RealtimeSearchTest, TestCase):
    """Tests search by domain works."""

    def assert_content_item_in_response(
            self,
            content_items: typing.Iterable[ContentItem],
            response: TemplateResponse
    ):
        """
        Asserts that response contains content items.
        """
        object_list = response.context_data['object_list']

        self.assertEqual(set(content_items), {item.object for item in object_list})

    def setUp(self):
        super().setUp()
        self.physics = Domain.objects.get(slug="physics")
        self.chemistry = Domain.objects.get(slug="chemistry")
        self.ci1 = ContentItemFactory(domains=self.physics, language="en")
        self.ci2 = ContentItemFactory(domains=self.chemistry, language="en")
        self.ci3 = ContentItemFactory(domains=self.chemistry, language="en")

    def test_all_domains_in_default_page(self):
        """When no filter all content items are visible."""
        response = self.client.get(reverse("content_item:search"))
        assert response.status_code == 200
        assert isinstance(response, TemplateResponse)
        self.assert_content_item_in_response(
            {self.ci1, self.ci2, self.ci3},
            response
        )

    def test_search_physics(self):
        """Test view filtered by domain."""
        url = reverse("content_item:search", kwargs={'domain': self.physics.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response)
        self.assertIsInstance(response, TemplateResponse)
        self.assert_content_item_in_response(
            {self.ci1},
            response
        )

    def test_search_chemistry(self):
        """Test view filtered by domain."""
        url = reverse("content_item:search", kwargs={'domain': self.chemistry.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response)
        self.assertIsInstance(response, TemplateResponse)
        self.assert_content_item_in_response(
            {self.ci2, self.ci3},
            response
        )

    def test_all_disciplines_in_search_view(self):
        """Checks that are disciplines are in search view."""
        discipline = DomainType.objects.get(slug="discipline")
        domains = discipline.domains.all()

        response = self.client.get(reverse("content_item:search"))
        self.assertEqual(response.status_code, 200, response)
        self.assertIsInstance(response, TemplateResponse)

        self.assertEqual(set(domains), set(response.context_data.get('disciplines')))


class TestLanguages(RealtimeSearchTest, TestCase):
    """Test search form language handling."""

    @classmethod
    def make_test_data(cls, discipline="physics", language="en", count=3):
        """Create test disciplines."""

        result = []
        for __ in range(count):
            cif = ContentItemFactory(
                main_discipline=Domain.objects.get(slug=discipline), language=language
            )
            cif.domains.add(Domain.objects.get(slug=discipline))
            result.append(cif)
        return result

    def setUp(self):
        super().setUp()
        self.phys_en = self.make_test_data(discipline="physics", language="en")
        self.phys_pl = self.make_test_data(discipline="physics", language="pl")
        self.chem_pl = self.make_test_data(discipline="chemistry", language="pl")

    def test_all_languages(self):
        response = self.client.get(reverse("content_item:search") + '?language=all')
        self.assertIsInstance(response, TemplateResponse)
        form = response.context_data['form']
        self.assertEqual(form.cleaned_data['language'], None)

    def test_english(self):
        response = self.client.get(reverse("content_item:search") + '?language=en')
        self.assertIsInstance(response, TemplateResponse)
        form = response.context_data['form']
        self.assertEqual(form.cleaned_data['language'], "en")

    def test_missing_language(self):
        response = self.client.get(reverse("content_item:search") + '?language=pl')
        self.assertIsInstance(response, TemplateResponse)
        form = response.context_data['form']
        self.assertEqual(form.cleaned_data['language'], "pl")

    def test_search_results_pl(self):
        response = self.client.get(reverse("content_item:search") + '?language=pl')
        self.assertIsInstance(response, TemplateResponse)
        results = response.context_data['object_list']
        self.assertEqual(len(results), 6)

    def test_search_results_en(self):
        response = self.client.get(reverse("content_item:search") + '?language=en')
        self.assertIsInstance(response, TemplateResponse)
        results = response.context_data['object_list']
        self.assertEqual(len(results), 3)

    def test_search_results_all(self):
        response = self.client.get(reverse("content_item:search") + '?language=all')
        self.assertIsInstance(response, TemplateResponse)
        results = response.context_data['object_list']
        self.assertEqual(len(results), 9)


class TestSearchForAllDomains(TestCase):

    def test_search_for_all_domains(self):

        for domain in Domain.objects.all():
            with self.subTest(domain.slug):
                url = reverse("content_item:search", kwargs={'domain': domain.slug})
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)


class TestAdvancedSearch(RealtimeSearchTest, TestCase):

    def setUp(self):
        super().setUp()
        self.classroom = ContentItemFactory(
            title="Classroom",
            domains=Domain.objects.get(slug='classroom')
        )
        self.parent = ContentItemFactory(
            title="Parent",
            domains=Domain.objects.get(slug='parent')
        )
        self.teacher = ContentItemFactory(
            title="Teacher",
            domains=Domain.objects.get(slug='teacher')
        )
        self.multi_domains = ContentItemFactory(
            title="Multi",
            domains=[
                Domain.objects.get(slug='classroom'),
                Domain.objects.get(slug='teacher')
            ]
        )

    def test_single_domain(self):
        response = self.client.post(reverse("content_item:search-advanced"), data={
            'supervision': Domain.objects.get(slug='teacher').pk
        })
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(
            {res.object for res in response.context_data['object_list']},
            {self.teacher, self.multi_domains}
        )

    def test_or_inside_domain_type(self):
        response = self.client.post(reverse("content_item:search-advanced"), data={
            'supervision': [
                Domain.objects.get(slug='teacher').pk,
                Domain.objects.get(slug='parent').pk,
            ]
        })
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(
            {res.object for res in response.context_data['object_list']},
            {self.teacher, self.parent, self.multi_domains}
        )

    def test_multi_domain(self):
        response = self.client.post(reverse("content_item:search-advanced"), data={
            'supervision': Domain.objects.get(slug='teacher').pk,
            'setting': Domain.objects.get(slug='classroom').pk
        })
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(
            {res.object for res in response.context_data['object_list']},
            {self.multi_domains}
        )
