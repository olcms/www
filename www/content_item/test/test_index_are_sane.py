# -*- coding: utf-8 -*-
"""
Test indexes are properly configured and are searchable.
"""
import datetime
import typing

from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from haystack.query import SearchQuerySet

from www.content_item import const, forms
from www.content_item.models import ContentItem
from www.content_item.search_indexes import ContentIndex
from www.content_item.test.utils import ContentItemFactory, DomainFactory
from www.resource.models import Resource
from www.utils.search_utils import (
    RealtimeSearchTest,
    TestSearchBase
)
from www.utils.test_utils import auto_repr

# I added docstrings where applicable
# pylint: disable=missing-docstring

# Tests will fail if there is missing member --- this is triggered by mixins.
# pylint: disable=no-member,attribute-defined-outside-init,no-value-for-parameter

FilterExpression = typing.Mapping[str, typing.Any]
FormData = typing.Mapping[str, typing.Any]


#                                     There are a lot of tests here, but it makes sense.
class BaseSearchTestMixin(object):  # pylint: disable=too-many-public-methods
    """
    Base class for a lot of tests. Each subclass checks single indexed property.

    This basically tests whether our index configuration is sane.

    Index is tested in following settings:

    1. As SearchQuerySet filter.
    3. API calls.

    It works as follows:

    create_test_item --- creates a single ContentItem, then positive tests should find it, and
    negative shouldn't.

    Hence for each field following tests are executed

    * ``test_positive_auto_queries`` and ``test_negative_auto_queries`` ---
      test cases for "auto queries" --- that is for using filter.auto_query.`

    * ``test_positive_form`` and ``test_negative_form`` check that form works properly.

    * ``test_positive_filters`` and ``test_positive_filters`` check raw filters on SearchQuerySet.

    Tests use data from:

    * ``positive_auto_queries`` and ``negative_auto_queries``
    * ``negative_filters`` and ``positive_filters``. Basic implementation just re-uses queries from
      ``*auto_queries``
    * ``search_form_positive_data`` and ``search_form_negative_data``.
       Basic implementation just re-uses queries from ``*auto_queries``.

    """

    form_field_names = ["q"]
    """
    See class docstring.
    """

    filter_field_name = None
    """
    See class docstring.
    """

    def verify_resulant_qs(self, sqs):
        """
        Verifies SearchQuerySet in positive test case (it should contain an object)
        """
        self.assertEqual(1, sqs.count(), [auto_repr(r.object) for r in sqs.all()])

    def verify_resulant_qs_negative_case(self, sqs):
        """
        Verifies SearchQuerySet in positive test case (it should not contain an object)
        """
        self.assertEqual(0, sqs.count())

    def create_test_item(self):
        """
        Should create a Content item that can be searched.
        """
        raise NotImplementedError

    def positive_auto_queries(self) -> typing.Sequence[str]:
        """
        Returns a list of auto queries that should find item created by create_test_item.
        """
        raise NotImplementedError

    def negative_auto_queries(self) -> typing.Sequence[str]:
        """
        Returns a list of auto queries that should not find item created by create_test_item.
        """
        raise NotImplementedError

    def positive_filters(self) -> typing.Sequence[FilterExpression]:
        """
        Returns a list of filters that should find item created by create_test_item.
        """
        return [
            {self.filter_field_name: query}
            for query in self.positive_auto_queries()
        ]

    def negative_filters(self) -> typing.Sequence[FilterExpression]:
        """
        Returns a list of filters that should not find item created by create_test_item.
        """
        return [
            {self.filter_field_name: query}
            for query in self.negative_auto_queries()
        ]

    def search_form_positive_data(self) -> typing.Sequence[FormData]:
        """
        Returns a list of ContentItemSearchForm field values, ContentItemSearchForm configured
        with these values should find ContentItem.
        """
        result = []
        for field in self.form_field_names:
            for query in self.positive_auto_queries():
                result.append({field: query})
        return result

    def search_form_negative_data(self) -> typing.Sequence[FormData]:
        """
        Returns a list of ContentItemSearchForm field values, ContentItemSearchForm configured
        with these values should not find ContentItem.
        """
        result = []
        for field in self.form_field_names:
            for query in self.negative_auto_queries():
                result.append({field: query})
        return result

    def test_sanity(self):
        """
        Check create_test_item creates the content item.
        """
        self.assertEqual(SearchQuerySet().all().count(), 0)
        self.create_test_item()
        self.assertEqual(
            1,
            SearchQuerySet().all().count(),
            "Did you create Content item in Test? It didn't appear on the index"
        )

    @property
    def sqs(self) -> SearchQuerySet:
        """Shortcut for our filtered query set."""
        return ContentIndex.get_search_queryset()

    def test_positive_auto_queries(self):
        self.create_test_item()
        for query in self.positive_auto_queries():
            with self.subTest(query):
                self.verify_resulant_qs(self.sqs.auto_query(query))

    def test_negative_auto_queries(self):
        self.create_test_item()
        for query in self.negative_auto_queries():
            with self.subTest(query):
                self.verify_resulant_qs_negative_case(self.sqs.auto_query(query))

    def test_positive_filters(self):
        self.create_test_item()
        for query in self.positive_filters():
            with self.subTest(query):
                self.verify_resulant_qs(self.sqs.filter(**query))

    def test_negative_filters(self):
        self.create_test_item()
        for query in self.negative_filters():
            with self.subTest(query):
                self.verify_resulant_qs_negative_case(self.sqs.filter(**query))

    def test_positive_form(self):
        self.create_test_item()
        for item in self.search_form_positive_data():
            with self.subTest(item):
                form = forms.ContentItemSearchForm(data=item)
                self.assertTrue(form.is_valid(), form.errors)
                self.verify_resulant_qs(form.search())

    def test_negative_form(self):
        self.create_test_item()
        for item in self.search_form_negative_data():
            with self.subTest(item):
                form = forms.ContentItemSearchForm(data=item)
                self.assertTrue(form.is_valid(), form.errors)
                self.verify_resulant_qs_negative_case(form.search())


class TitleTests(BaseSearchTestMixin, RealtimeSearchTest, TestCase):

    """
    Tests for title field.
    """

    form_field_names = ["q"]

    filter_field_name = "title"

    def create_test_item(self):
        ContentItemFactory(title="Yellow Submarine")

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return [
            "Yellow", "Submarine", "Yellow Submarine"
        ]

    def negative_auto_queries(self)-> typing.Sequence[str]:
        return ["Red October"]


class DescriptionTests(BaseSearchTestMixin, RealtimeSearchTest, TestCase):
    """
    Tests for description field.
    """

    form_field_names = ["q"]

    filter_field_name = "description"

    def create_test_item(self):
        ContentItemFactory(description="""
As we live a life of ease (A life of ease)
Everyone of us (Everyone of us) has all we need (Has all we need)
Sky of blue (Sky of blue) and sea of green (Sea of green)
In our yellow (In our yellow) submarine (Submarine, ha, ha)
        """.strip())

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return [
            "Yellow", "Submarine", "sky of blue"
        ]

    def negative_auto_queries(self)-> typing.Sequence[str]:
        return ["Red October"]


class AuthorTests(BaseSearchTestMixin, RealtimeSearchTest, TestCase):
    """
    Tests for author field.
    """

    def create_test_item(self):
        ContentItemFactory(authors=["John Lennon", "Paul McCartney"])

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return [
            "John Lennon",
            "Paul McCartney",
            "Lennon",
            '"John Lennon"'
        ]

    def negative_auto_queries(self)-> typing.Sequence[str]:
        return ["Red October"]

    def positive_filters(self):
        return []

    def negative_filters(self):
        return []


class LicenseTests(BaseSearchTestMixin, RealtimeSearchTest, TestCase):
    """
    Tests for license field.
    """

    filter_field_name = "license"
    form_field_names = ["license"]

    def create_test_item(self):
        ContentItemFactory(license=const.License.cc_by)

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return []  # License is not included in contents

    def negative_auto_queries(self)-> typing.Sequence[str]:
        return []

    def positive_filters(self):
        return [
            {"license": const.License.cc_by},
            {"license__exact": const.License.cc_by},
        ]

    def negative_filters(self):
        return [
            {"license__exact": const.License.cc_by_nd},
            {"license__exact": const.License.cc_by_nc},
        ]

    def search_form_positive_data(self):
        return [
            {"license": const.License.cc_by}
        ]

    def search_form_negative_data(self):
        return [
            {"license": const.License.cc_by_nc}
        ]


class TagTests(BaseSearchTestMixin, RealtimeSearchTest, TestCase):
    """
    Tests for tags field.
    """

    filter_field_name = "tags"

    form_field_names = ["q", "tags"]

    def create_test_item(self):
        ContentItemFactory(tags=["foo", "bar", "baz"])

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return [
            "foo", "bar", "baz",
            "foo baz"
        ]  # License is not included in contents

    def negative_auto_queries(self)-> typing.Sequence[str]:
        return [
            "foobar", "notatag", "foo notatag"
        ]

    def search_form_positive_data(self) -> typing.Sequence[FormData]:
        result = [{"q": query} for query in self.positive_auto_queries()] + \
                 [{"tags": ",".join(query.split(" "))} for query in self.positive_auto_queries()]
        return result

    def search_form_negative_data(self):
        return [{"q": query} for query in self.negative_auto_queries()] + \
               [{"tags": ",".join(query.split(" "))} for query in self.negative_auto_queries()]


class TestHiddenContentItemsAreHidden(RealtimeSearchTest, TestCase):
    """
    Tests that hidden attribute is honored.

    Tests create three ContentItems that are identical, but:

    * one is visible,
    * one is hidden
    * one is unverified

    then we test that all searches find only the visible one.
    """

    def create_test_item(self):
        """Create two ContentItems one hidden and one visible."""
        self.hidden_ci = ContentItemFactory(tags=["foo", "bar", "baz"], hidden=True)
        self.unverified_ci = ContentItemFactory(tags=["foo", "bar", "baz"], verified=False)
        self.normal_ci = ContentItemFactory(tags=["foo", "bar", "baz"])

    @property
    def sqs(self) -> SearchQuerySet:
        """Shortcut for filtered queryset."""
        return ContentIndex.get_search_queryset()

    def test_sanity(self):
        """
        Check create_test_item creates the content item.
        """
        self.assertEqual(ContentItem.objects.count(), 0)
        self.create_test_item()
        self.assertEqual(ContentItem.objects.count(), 3)

    def test_single_item_visible_on_normal_query(self):
        """
        Test that only a single item is visible when we
        don't limit search at all.
        """
        self.create_test_item()
        self.assertEqual(self.sqs.all().count(), 1)

    def test_single_item_visible_on_a_query(self):
        """Test that only single item is visible."""
        self.create_test_item()
        self.assertEqual(self.sqs.filter(tags="foo").count(), 1)
        self.assertEqual(self.sqs.filter(tags="foo, bar").count(), 1)

    def test_single_item_visible_on_empty_form(self):
        """Test that only one item is visible in search form."""
        self.create_test_item()
        form = forms.ContentItemSearchForm(data={})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.search().count(), 1)

    def test_single_item_visible_on_filled_form(self):
        """Test that only one item is visible in filled search form."""
        self.create_test_item()
        form = forms.ContentItemSearchForm(data={"tags": "foo,baz"})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.search().count(), 1)


class TestReindex(TestSearchBase, TestCase):
    """
    Check that reindex commands properly re-indexes content items.
    """

    @classmethod
    def __update_content_item(cls, ci, last_updated_at):
        ContentItem.objects.filter(pk=ci.pk).update(
            last_updated_at=last_updated_at
        )
        ci.refresh_from_db()

    def setUp(self):
        super().setUp()
        self.now = timezone.now()
        self.content_item_1 = ContentItemFactory()
        self.__update_content_item(self.content_item_1, self.now - datetime.timedelta(days=1))
        self.content_item_2 = ContentItemFactory()
        self.__update_content_item(self.content_item_2, self.now - datetime.timedelta(days=2))
        self.content_item_3 = ContentItemFactory()
        self.__update_content_item(self.content_item_3, self.now - datetime.timedelta(days=3))

    def testSanity(self):
        """
        Check that items were not automatically indexed (realtime index is disabled here)
        """
        self.assertEqual(SearchQuerySet().all().count(), 0)

    def test_update_index_1(self):
        """
        Check that reindex reindexed content items based on age.
        """
        call_command("update_index", age=25)
        self.assertEqual(SearchQuerySet().all().count(), 1)

    def test_update_index_2(self):
        """
        Check that reindex reindexed content items based on age.
        """
        call_command("update_index", age=49)
        self.assertEqual(SearchQuerySet().all().count(), 2)

    def test_update_index_3(self):
        """
        Check that reindex reindexed content items based on age.
        """
        call_command("update_index", age=73)
        self.assertEqual(SearchQuerySet().all().count(), 3)

    def test_update_index_resource(self):
        """
        Test that update to Resource updates ContentItem
        :return:
        """
        # Sanity check
        self.assertLess(
            self.content_item_3.last_updated_at, self.now - datetime.timedelta(hours=1))
        Resource.objects.create(
            content_item=self.content_item_3,
            title="Not important"
        )
        self.content_item_3.refresh_from_db()
        self.assertGreater(
            self.content_item_3.last_updated_at, self.now - datetime.timedelta(hours=1))


class TestDomains(BaseSearchTestMixin, RealtimeSearchTest, TestCase):

    filter_field_name = "domain"

    form_field_names = ["q", "domain"]

    def create_test_item(self):
        """
        Should create a Content item that can be searched.
        """
        self.domain = DomainFactory(
            slug="red_coldfish",
            name="Yellow Canary"
        )
        self.negative_domain = DomainFactory()
        self.content_item = ContentItemFactory(
            domains=self.domain,
            tags=[]
        )

    def positive_filters(self) -> typing.Sequence[FilterExpression]:
        return [
            {"domains": "red_coldfish"},
        ]

    def negative_auto_queries(self) -> typing.Sequence[str]:
        return ["red", "coldfish", "red_coldfish"]

    def negative_filters(self) -> typing.Sequence[FilterExpression]:
        return [
            {"domain": "Yellow"},
            {"domain": "Canary"},
            {"domain": "Yellow Canary"},
        ]

    def positive_auto_queries(self) -> typing.Sequence[str]:
        return ["Yellow", "Canary"]

    def search_form_positive_data(self) -> typing.Sequence[FormData]:
        return [
            {"domain": self.domain.pk},
        ]

    def search_form_negative_data(self) -> typing.Sequence[FormData]:
        return [
            {"domain": self.negative_domain.pk},
        ]
