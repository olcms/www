# -*- coding: utf-8 -*-
"""Helper models just for testing purposes"""

from django.db import models

from django_fake_model import models as f


class Category(f.FakeModel):
    """Simple model to tests"""

    name = models.CharField(max_length=20)
    slug = models.SlugField(max_length=20)
    category_type = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()
