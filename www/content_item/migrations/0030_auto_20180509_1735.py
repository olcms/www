# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2018-05-09 15:35
from __future__ import unicode_literals

from django.db import migrations
from django.db.migrations.operations.special import RunPython
from django.utils.translation import ugettext_noop

from www.content_item.migration_utils import create_domain


def forwards_func(apps, schema_editor):
    create_domain(apps, type_slug="discipline", slug="citizen-science", name=ugettext_noop("Citizen Science"))


class Migration(migrations.Migration):

    dependencies = [
        ('content_item', '0029_auto_20171129_1414'),
    ]

    operations = [
        RunPython(forwards_func, reverse_code=RunPython.noop)
    ]
