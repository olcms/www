# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-03 18:14
from __future__ import unicode_literals

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content_item', '0004_auto_20170118_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='contentitem',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contentitem',
            name='last_updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
