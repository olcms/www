# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-13 14:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content_item', '0011_auto_20170503_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='contentitem',
            name='cover_image_resized',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Cover image for this resource, resized to correct aspect ratio of 6.75:1'),
        ),
        migrations.AddField(
            model_name='contentitem',
            name='miniature_image',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Miniature image for this resource'),
        ),
        migrations.AddField(
            model_name='contentitem',
            name='miniature_image_resized',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Miniature image for this resource, resized to correct aspect ratio of 1.185:1'),
        ),
    ]
