# -*- coding: utf-8 -*-
"""Apps module for ContentItem."""

from django.apps import AppConfig


class ContentItemConfig(AppConfig):
    """App for Content Item."""
    name = 'www.content_item'

    @staticmethod
    def verify_swift_works():
        """
        Here we check if Swift storage works, we do it here as we need to halt application 
        if swift is down, otherwise you'll encounter **weird bugs**. 
        
        Particular bug I was hunting was that links to uploaded files were not rendering 
        themselves, this was caused by default_storage not working properly and raising 
        ImproperlyConfigured error which was swallowed by {%include tag . 
        """

        from django.core.files.storage import default_storage
        default_storage._setup()  # pylint: disable=protected-access

    def ready(self):
        super().ready()
        self.verify_swift_works()
