# -*- coding: utf-8 -*-
"""
Constants for ContentItem app
"""

from django.conf import settings
from django.utils.translation import ugettext_lazy
from djchoices.choices import DjangoChoices, ChoiceItem


LANGUAGES = settings.LANGUAGES


class ChoiceItemWithIcon(ChoiceItem):
    """Choice item with additional property: icon."""

    def __init__(
            self, value=None, label=None, order=None, icon=None, url=None
    ):  # pylint: disable=too-many-arguments
        super().__init__(value, label, order)
        self.icon = icon
        self.url = url


class ChoiceItemWithUrl(ChoiceItem):
    """Choice item with additional property: url."""
    def __init__(self, value=None, label=None, order=None, url=None):
        super().__init__(value, label, order)
        self.url = url


class ResourceType(DjangoChoices):
    """
    Types of resource.

    These dependent on ContentItem type, checked by validator, not enforced in db.
    """

    document = ChoiceItemWithUrl('document', label=ugettext_lazy('Text'), url="resource:document-create")
    file = ChoiceItemWithUrl('file', label=ugettext_lazy('File'), url="resource:file-create")
    link = ChoiceItemWithUrl('link', label=ugettext_lazy('Link'), url="resource:link-create")
    video = ChoiceItemWithUrl('video', label=ugettext_lazy('Video'), url="resource:video-create")
    subtitle = ChoiceItemWithUrl('subtitle', label=ugettext_lazy('Subtitle'), url="resource:subtitle-create")
    simulation = ChoiceItemWithUrl('simulation', label=ugettext_lazy('Simulation'), url="simulation:create")
    quiz = ChoiceItemWithUrl('quiz', label=ugettext_lazy('Quiz'), url="simulation:createQuiz")


class License(DjangoChoices):
    """Available licenses for resource - All flavors of Creative Commons."""

    cc0 = ChoiceItemWithIcon(
        'CC0',
        label=ugettext_lazy('Free content globally without restrictions'),
        icon="img/content_item/cc-zero.svg",
        url="https://creativecommons.org/publicdomain/zero/1.0/"
    )
    cc_by = ChoiceItemWithIcon(
        'CC-BY-XX-XX',
        label=ugettext_lazy('Creative Commons, Attribution alone (BY)'),
        icon="img/content_item/by.svg",
        url="https://creativecommons.org/licenses/by/4.0/"
    )
    cc_by_nc = ChoiceItemWithIcon(
        'CC-BY-NC-XX',
        label=ugettext_lazy('Creative Commons, Attribution + Noncommercial (BY-NC)'),
        icon="img/content_item/by-nc.svg",
        url="https://creativecommons.org/licenses/by-nc/4.0/"
    )
    cc_by_nc_sa = ChoiceItemWithIcon(
        'CC-BY-NC-SA',
        label=ugettext_lazy(
            'Creative Commons, Attribution + Noncommercial + ShareAlike (BY-NC-SA)'
        ),
        icon="img/content_item/by-nc-sa.svg",
        url="https://creativecommons.org/licenses/by-nc-sa/4.0/"
    )
    cc_by_nc_nd = ChoiceItemWithIcon(
        'CC-BY-NC-ND',
        label=ugettext_lazy(
            'Creative Commons, Attribution + Noncommercial + NoDerivatives (BY-NC-ND)'
        ),
        icon="img/content_item/by-nc-nd.svg",
        url="https://creativecommons.org/licenses/by-nc-nd/4.0/"
    )
    cc_by_sa = ChoiceItemWithIcon(
        'CC-BY-SA-XX',
        label=ugettext_lazy('Creative Commons, Attribution + ShareAlike (BY-SA)'),
        icon="img/content_item/by-sa.svg",
        url="https://creativecommons.org/licenses/by-sa/4.0/"
    )
    cc_by_nd = ChoiceItemWithIcon(
        'CC-BY-ND-XX',
        label=ugettext_lazy('Creative Commons, Attribution + NoDerivatives (BY-ND)'),
        icon="img/content_item/by-nd.svg",
        url="https://creativecommons.org/licenses/by-nd/4.0/"

    )

    @classmethod
    def choices_with_empty(cls):
        """
        Returns choices with empty choice.
        """
        return (('', 'Every License'),) + cls.choices


class ImageProperties(object):
    """
    Image sizes for Content Items.
    """
    cover_image_size = (1080, 160)
    miniature_size = (154, 130)
