# coding=utf-8
"""
Various utilities for content_items and resources.
"""
import typing

from django.contrib.auth import models as auth_models

from django.http.request import HttpRequest
from django.utils.translation import ugettext_lazy

from www.content_item import const
from www.content_item.models import ContentItem
from www.resource.models import Resource, ResourceType, DocumentResourceDerivative


def is_content_item_editor(item: ContentItem, user: auth_models.AbstractUser) -> bool:
    """Checks that user can edit content item."""
    return user.is_authenticated and item.owners.filter(id=user.pk).exists()


def is_user_resource_editor(resource: Resource, user: auth_models.AbstractUser) -> bool:
    """Checks that user can edit a resource."""
    return user.is_authenticated and is_content_item_editor(resource.content_item, user)


class ResourceManager(object):
    """Object that wraps resource and adds various methods usable in templates."""
    def __init__(self, resource: Resource, request: HttpRequest):
        super().__init__()
        self.resource = resource
        self.request = request

    @property
    def user_has_edit_access(self):
        """
        :return: True if user can edit this resource
        """
        return is_user_resource_editor(self.resource, self.request.user)

    @property
    def should_display(self):
        """
        Returns true if resource should be displayed in content item details.

        Basically resource should be displayed if it is complete, or user can edit the resource.
        """

        if self.user_has_edit_access:
            # Editor can always see resource
            return True

        if self.resource.resource_type == ResourceType.link:
            return True
        if self.resource.resource_type in \
                {ResourceType.file, ResourceType.video, ResourceType.subtitle}:
            return self.resource.fileresource.is_complete
        if self.resource.resource_type == ResourceType.simulation:
            return self.resource.fileresource.simulationresource.is_complete
        if self.resource.resource_type == ResourceType.document:
            return self.resource.documentresource.is_complete
        raise NotImplementedError

    @property
    def template_name(self):
        """
        Template for displaying resource in resource list in content_item detail view.
        """
        return 'content_item/resources/{}.html'.format(self.resource.resource_type)

    @property
    def license_icon(self):
        """
        :return: Path to icon for the license
        """
        license_map = self.get_license_map()

        return license_map[self.resource.resource_license].icon

    @staticmethod
    def get_license_map():
        """
        :return: Map of license value to license object
        """
        license_map = {
            item.value: item
            for item in const.License._fields.values()  # pylint: disable=no-member
        }
        return license_map

    @property
    def license_url(self):
        """
        :return: Url under which you can read the license text.
        """
        license_map = self.get_license_map()

        return license_map[self.resource.resource_license].url

    @property
    def license_label(self):
        """
        :return: Label of a license
        """
        license_map = self.get_license_map()

        return license_map[self.resource.resource_license].label

    @property
    def derivatives(self) -> typing.Iterable['DerivativeUtils']:
        """
        Assumes that this resource is a document resource.

        :return: Iterable of derivatives
        """
        assert self.resource.documentresource is not None
        for derivative in self.resource.documentresource.derivatives.all():
            yield DerivativeUtils(derivative)  # pragma: no cover

    @property
    def default_derivative(self):
        """
        Return default derivative for document resource.

        Right now it returns html resource, and if there is no such
        resource it returns None.
        """
        assert self.resource.documentresource is not None
        for derivative in self.resource.documentresource.derivatives.all():
            if derivative.mime_type == "text/html":
                return DerivativeUtils(derivative)  # pragma: no cover
        return None


class DerivativeUtils(object):
    """
    Wrapper for DocumentResourceDerivative.
    """

    MIMETYPE_LABEL_MAP = {  # pragma: no cover
        "text/html": ugettext_lazy("Download as HTML"),  # pragma: no cover
        "application/pdf": ugettext_lazy("Download as PDF"),  # pragma: no cover
        "application/vnd.oasis.opendocument.text": ugettext_lazy("Download as Open Document"),  # pragma: no cover
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document":  # pragma: no cover
        ugettext_lazy("Download as Word Document"),  # pragma: no cover
    }  # pragma: no cover

    MIMETYPE_ICON = {  # pragma: no cover
        "text/html": "fa fa-file-code-o",  # pragma: no cover
        "application/pdf": "fa fa-file-pdf-o",  # pragma: no cover
        "application/vnd.oasis.opendocument.text": "fa fa-file-text-o",  # pragma: no cover
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document":  # pragma: no cover
        "fa fa-file-word-o",  # pragma: no cover
    }  # pragma: no cover

    DEFAULT_LABEL = ugettext_lazy("Download in '{}' format")

    DEFAULT_ICON = "fa fa-file-o"

    def __init__(self, derivative: DocumentResourceDerivative) -> None:
        super().__init__()
        self.derivative = derivative

    @property
    def is_ready(self):
        """Return true if derivative is ready for display."""
        return self.derivative.is_ready

    @property
    def download_url(self):
        """Url for downloading the derivative."""
        return self.derivative.download_url

    @property
    def link_text(self):
        """
        :return: Link text to download the derivative.
        """
        mimetype = self.derivative.mime_type
        return self.MIMETYPE_LABEL_MAP.get(
            mimetype, str(self.DEFAULT_LABEL.format(mimetype)))  # pylint: disable=no-member

    @property
    def link_icon(self):
        """
        :return: Link css classes for the download link.
        """
        mimetype = self.derivative.mime_type
        return self.MIMETYPE_ICON.get(mimetype, self.DEFAULT_ICON)
