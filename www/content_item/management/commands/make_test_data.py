# -*- coding: utf-8 -*-

import argparse

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from www.content_item.test import utils


class Command(BaseCommand):
    """Command that creates content items for local testing."""

    def add_arguments(self, parser: argparse.ArgumentParser):
        super().add_arguments(parser)
        parser.add_argument(
            '--count', help="How many content items to create", type=int, default=1000
        )
        parser.add_argument(
            '--batch', help="How big is the batch size", type=int, default=100
        )

    def handle(self, *args, **options):
        total_items = int(options['count'])
        batch_size = int(options['batch'])

        if get_user_model().objects.count() > 0:
            owner = get_user_model().objects.all()[0]
        else:
            owner = utils.UserFactory()

        for __ in range(total_items // batch_size):
            with transaction.atomic():
                [utils.ContentItemFactory(owners=[owner]) for __ in range(batch_size)]
            print(".")  # noqa

        with transaction.atomic():
            [utils.ContentItemFactory(owners=[owner]) for __ in range(total_items % batch_size)]
