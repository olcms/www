# -*- coding: utf-8 -*-
"""Views for ContentItem module"""

import copy
import logging
from urllib.parse import urlencode

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.contenttypes.models import ContentType
from django.core.mail import mail_admins
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseBadRequest
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from django.utils.translation import get_language
from django.views.generic import DetailView, UpdateView, CreateView
from django.views.generic.base import ContextMixin
from django.views.generic.edit import FormView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from www.landing_page.utils import DomainUtils
from www.page_block.models import PageBlock
from . import models, forms, utils, tasks


class BaseContentItemView(LoginRequiredMixin, ContextMixin):
    """Base item for editing/adding ContentItems."""

    model = models.ContentItem
    template_name = "content_item/content_item_create.html"

    def get_context_data(self, **kwargs):
        """
        Gets context data for the form.
        """
        kwargs['tag_suggestion_url'] = reverse("api:tag-list", kwargs={
            "content_type_id": ContentType.objects.get_for_model(
                models.ContentItem
            ).pk
        })
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        """Redirect when form is valid."""
        return reverse(
            "content_item:detail",
            kwargs={'pk': self.object.pk}  # pylint: disable=no-member
        )


class ContentItemCreate(BaseContentItemView, CreateView):
    """Creates content item."""

    form_class = forms.ContentItemCreateForm

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['group_id'] = self.kwargs['group_id']
        return form_kwargs

    def get_context_data(self, **kwargs):
        kwargs['group_id'] = self.kwargs['group_id']
        return super().get_context_data(**kwargs)

    def get_initial(self):
        """
        Initial data. By default we only load author name from current user.
        """
        initial = {}
        if self.request.user.get_full_name():
            initial['authors'] = self.request.user.get_full_name()
        return initial

    def is_user_auto_verified(self) -> bool:
        """
        Returns true if author of this item has items verified by default.
        """
        return self.request.user.has_perm("content_item.is_autoverified")

    def mark_item_as_verified(  # pylint: disable=no-self-use
            self, content_item: models.ContentItem
    ):
        """Marks content item as verified"""
        content_item.verified = True
        content_item.save(update_fields=['verified'])

    def mark_item_as_not_verified(self, content_item):
        """Marks content item as verified"""
        content_item.verified = False
        content_item.save(update_fields=['verified'])
        mail_admins("New content item to verify", """
        New content item to verify: 
        
        * Admin link: {admin_link}
        * Display link: {display_link}
        """.format(
            admin_link=self.request.build_absolute_uri(
                reverse('admin:content_item_contentitem_change', args=(content_item.pk,))),
            display_link=self.request.build_absolute_uri(
                reverse('content_item:detail', kwargs={"pk": content_item.pk})),
        ))

    def form_valid(self, form):
        """
        Saves instance. Adds current user to owners list, resizes images (cover and miniature).
        """
        content_item = form.save(commit=False)

        if self.kwargs['group_id']:
            content_item.group = models.ContentItemLanguageGroup.objects.get(pk=self.kwargs['group_id'])
        else:
            content_item.group = models.ContentItemLanguageGroup.objects.create()

        content_item.save()
        form.save_m2m()
        content_item.owners.add(self.request.user)

        if self.is_user_auto_verified():
            self.mark_item_as_verified(content_item)
        else:
            self.mark_item_as_not_verified(content_item)

        self.object = content_item  # pylint: disable=attribute-defined-outside-init
        tasks.resize_content_item_images.apply_async(args=[self.object.pk], countdown=10)

        return HttpResponseRedirect(self.get_success_url())


#                                                                             This is a mixin
class ContentItemOwnerViewMixin(UserPassesTestMixin, BaseContentItemView):  # pylint: disable=no-member
    """View visible only to owner of content item."""

    def test_func(self) -> bool:
        content_item = self.get_object()
        user = self.request.user
        return utils.is_content_item_editor(content_item, user)


class ContentItemDeleteView(
        ContentItemOwnerViewMixin, DeleteView
):

    """Deletes a content item."""

    model = models.ContentItem
    http_method_names = ['post', 'delete', 'get']
    template_name = "content_item/content_item_delete.html"

    def get_context_data(self, **kwargs):
        return {
            "action": self.request.path_info,
        }

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()  # pylint: disable=attribute-defined-outside-init
        self.object.hidden = True
        self.object.save()
        return HttpResponse(status=201)


class ContentItemUpdate(
        ContentItemOwnerViewMixin, UpdateView
):
    """
    ContentItem update view.
    """

    form_class = forms.ContentItemEditForm

    def get_context_data(self, **kwargs):
        """
        Update template context.

        ``is_editing`` hides type field.
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            # Hides type field.
            'is_editing': True,
            'toolbar_styling_discipline': self.object.main_discipline.slug,
        })
        return kwargs

    def form_valid(self, form):
        self.object = form.save()  # pylint: disable=attribute-defined-outside-init
        tasks.resize_content_item_images.apply_async(args=[self.object.pk], countdown=10)

        return HttpResponseRedirect(self.get_success_url())


class ContentItemDisplay(DetailView):
    """Displays content item."""

    model = models.ContentItem
    template_name = "content_item/content_item_display.html"

    def dispatch(self, request, *args, **kwargs):
        content_item = self.get_object()
        if content_item.hidden:
            if not utils.is_content_item_editor(content_item, request.user):
                raise Http404()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Update context data.
        """
        license_map = {
            item.value: item
            for item in models.License._fields.values()  # pylint: disable=no-member
        }
        license_obj = license_map[self.object.license]
        evaluation_link = PageBlock.objects.get_language_version(
            "evaluation", self.object.language
        )

        kwargs.update({
            "license": {
                "url": license_obj.url,
                "icon": license_obj.icon,
                "label": license_obj.label,
            },
            "workforce_block": "workforce." + self.object.main_discipline.slug,
            "evaluation_link": evaluation_link.url if evaluation_link else None,
            "can_edit": utils.is_content_item_editor(self.object, self.request.user),
            "resource_types": self.object.resource_types,
            "domain_types": models.DomainType.objects.all(),
            "toolbar_styling_discipline": self.object.main_discipline.slug,
            "domains_sans_rri_and_methodologies": self.object.domains.all().exclude(
                type__slug__in=["rri", "learning-methodologies"]),
            "rri": self.object.domains_with_descriptions(type_slug="rri"),
            "methodologies": self.object.domains_with_descriptions(type_slug="learning-methodologies"),
        })
        kwargs.update(get_context_for_resources(self.request, self.object))
        return super().get_context_data(**kwargs)


class ContentItemVerifyView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    """Allows user to mark Content item as verified or unverified."""

    permission_required = ["content_item.can_verify"]
    fields = ['verified']
    model = models.ContentItem
    template_name = "content_item/verify_form.html"

    def form_valid(self, form):  # pylint: disable=no-self-use
        form.save()  # pragma: no cover
        return HttpResponse(status=201)  # pragma: no cover


def get_content_resources(request, content_id):
    """
    View method.

    :param request:  http request
    :param content_id: Id of content item
    :return: returns html list of resources for given content
    """
    c = models.ContentItem.objects.get(id=content_id)
    return HttpResponse(render(request, 'content_item/resource_list.html', get_context_for_resources(request, c)))


def get_context_for_resources(request: HttpRequest, content_item: models.ContentItem):
    """
    :param request: HttpRequest
    :param content_item: Content item object
    :return: Returns map with context data for resource template
    """
    ctx = {
        'resources': [
            utils.ResourceManager(r, request)
            for r in content_item.resources.all()
        ]
    }
    default_resource = content_item.default_resource
    ctx['default_resource'] = utils.ResourceManager(default_resource, request) \
        if default_resource else None
    return ctx


class ContentItemSearchView(MultipleObjectMixin, FormView):

    """
    View that performs queries on ContentItem model.

    For information on how Content Items are indexed see search_indexes.ContentIndex.
    """

    form_class = forms.ContentItemSearchForm
    page_kwarg = 'page'
    form_name = 'form'
    search_field = 'q'
    paginate_by = settings.SEARCH_RESULT_PAGE_SIZE

    template_name = 'content_item/search.html'

    def get_context_data(self, **kwargs):
        kwargs.update({
            "disciplines": models.Domain.objects.filter(type__slug="discipline"),
            "methodologies": models.Domain.objects.filter(type__slug="learning-methodologies")
        })
        if self.kwargs['domain']:
            kwargs["toolbar_styling_discipline"] = self.kwargs['domain']
        return super().get_context_data(**kwargs)

    @classmethod
    def form_to_query_string(cls, form) -> dict:
        """
        Return field values that will be passed via querystring, rest of values will be
        passed via url kwargs.
        """

        return {
            "tags": ",".join(tag.name for tag in form.cleaned_data['tags']),
            "license": form.cleaned_data['license'],
            "language": form.cleaned_data['language']
        }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.method == 'GET':
            data = copy.copy(self.request.GET)
            data[self.search_field] = self.kwargs['term']
            data[self.page_kwarg] = self.kwargs[self.page_kwarg]
            domain = self.kwargs.get('domain', None)
            if domain:
                domain = get_object_or_404(
                    models.Domain,
                    slug=self.kwargs['domain']
                )
                data['domain'] = domain.pk
            methodology = self.kwargs.get('methodology', None)
            if methodology:
                methodology = get_object_or_404(
                    models.Domain,
                    slug=self.kwargs['methodology']
                )
                data['methodology'] = methodology.pk
            kwargs['data'] = data
        return kwargs

    def get(self, request, *args, **kwargs):
        form = self.get_form()
        if not form.is_valid():
            logging.error("Error in search form %s", form.errors)
            return HttpResponseBadRequest()

        domain_utils = DomainUtils(form.cleaned_data['domain'])
        available_languages = set(domain_utils.get_content_item_language_codes())
        selected_language = form.cleaned_data['language']
        if selected_language == 'all':
            form.cleaned_data['language'] = None
        elif selected_language not in available_languages:
            form.cleaned_data['language'] = domain_utils.get_language_for_user(get_language())
        self.object_list = form.search()  # pylint: disable=attribute-defined-outside-init
        context = self.get_context_data(**{
            self.form_name: form,
            'query': form.cleaned_data.get(self.search_field),
            'object_list': self.object_list,
            'available_languages': available_languages
        })
        return self.render_to_response(context)

    def form_valid(self, form):
        redirect_kwargs = {
            "term": form.cleaned_data['q'],
            "page": int(self.request.POST.get('page', 1)),
        }
        domain = form.cleaned_data.get('domain', None)
        if domain:
            redirect_kwargs['domain'] = domain.slug

        methodology = form.cleaned_data.get('methodology', None)
        if methodology:
            redirect_kwargs['methodology'] = methodology.slug

        redirect_to = reverse("content_item:search", kwargs=redirect_kwargs)

        return HttpResponseRedirect("{}?{}".format(
            redirect_to,
            urlencode(self.form_to_query_string(form))
        ))


class AdvancedContentItemSearchView(MultipleObjectMixin, FormView):

    """Advanced search view."""

    form_class = forms.AdvancedSearchForm

    page_kwarg = 'page'
    form_name = 'form'
    search_field = 'q'
    paginate_by = settings.SEARCH_RESULT_PAGE_SIZE
    object_list = ()

    template_name = "content_item/advanced_search.html"

    def form_valid(self, form):
        self.object_list = None
        return self.render_to_response(self.get_context_data(
            object_list=form.search()
        ))


class ContentItemEditDomainDescription(UpdateView):
    """
    View that allows editing domain descriptions
    """
    form_class = forms.ContentItemEditDomainDescriptionForm
    template_name = 'content_item/edit_domain_description.html'
    model = models.DomainDescription

    def form_valid(self, form):
        self.object = form.save()  # pylint: disable=attribute-defined-outside-init
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        """Redirect when form is valid."""
        return reverse(
            "content_item:detail",
            kwargs={'pk': self.object.content_item.pk}  # pylint: disable=no-member
        )


class ContentItemAddDomainDescription(CreateView):
    """
    View that allows editing domain descriptions
    """
    form_class = forms.ContentItemEditDomainDescriptionForm
    template_name = 'content_item/edit_domain_description.html'
    model = models.DomainDescription

    def form_valid(self, form):
        self.object = form.save(commit=False)  # pylint: disable=attribute-defined-outside-init
        self.object.content_item_id = self.kwargs['content_item']
        self.object.domain_id = self.kwargs['domain']
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        """Redirect when form is valid."""
        return reverse(
            "content_item:detail",
            kwargs={'pk': self.object.content_item.pk}  # pylint: disable=no-member
        )
