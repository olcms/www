# -*- coding: utf-8 -*-
"""Models for ContentItem app"""
import typing
from collections import OrderedDict

import markdown
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy
from djchoices import ChoiceItem, DjangoChoices

from www.content_item import const
from www.resource.models import License
from www.utils.bleach import CLEANER
from www.utils.validators import (
    image_name_validator,
    validate_language_code
)


class DomainType(models.Model):
    """
    DomainType model.

    Represents (unsurprisingly) type of domain, that is a type of classifier that can be attached
    to ContentItem. Example of DomainType would be age-group.

    List of default domains is in ``migrations/domains.yml.``
    """

    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=1000)
    order = models.IntegerField(null=True, default=None)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['order', 'slug']


class Domain(models.Model):

    """
    DomainType model.

    Represents (unsurprisingly) a domain, that is a classifier that can be attached
    to ContentItem. Example of Domain would be `9-12 years` (for domain type `age group`)
    List of default domains is in ``migrations/domains.yml.``
    """

    # flake whines about shadowing builtin here, however this name is reasonable
    type = models.ForeignKey(DomainType, related_name="domains")  # noqa: B001
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=1000)
    order = models.IntegerField(null=True, default=None)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['type__order', 'type__slug', 'order', 'slug']


class ContentItemLanguageGroup(models.Model):
    """
    Content Item group, content items with the same group have same content but in different languages
    """
    pass


class ContentItem(models.Model):
    """
    Content item model.

    For information on how Content Items are indexed see search_indexes.ContentIndex.
    """

    class ContentItemType(DjangoChoices):
        """
        Types of Content item.
        """

        basic = ChoiceItem('basic', label=ugettext_lazy('Basic'))
        movie = ChoiceItem('movie', label=ugettext_lazy('Movie'))

    title = models.CharField(
        max_length=100,
        verbose_name=ugettext_lazy("Title"),
        help_text=ugettext_lazy(
            "Provide concise descriptive title for your content. It should be short "
            "yet convey what your content is. "),
        blank=False,
        null=False
    )

    description = models.TextField(
        verbose_name=ugettext_lazy("Description"),
        null=True, blank=True,
        help_text=ugettext_lazy(
            "Optional description for your content. Markdown formatting allowed."
        )
    )

    authors = ArrayField(
        base_field=models.CharField(max_length=128),
        verbose_name=ugettext_lazy("Authors"),
        null=False,
        help_text=ugettext_lazy(
            "Provide list of authors. "
            "To add a new author write their name and press enter or comma. "
        )
    )

    owners = models.ManyToManyField(
        to=settings.AUTH_USER_MODEL,
        verbose_name=ugettext_lazy("Owners"),
        help_text=ugettext_lazy("List of users that can edit this resource")
    )

    # flake whines about shadowing builtin here, however this name is reasonable
    type = models.CharField(  # noqa: B001
        verbose_name=ugettext_lazy('Type'),
        help_text=ugettext_lazy(
            'Type of the this content item, it cannot be changed.'
        ),
        max_length=100,
        choices=ContentItemType.choices,
        validators=[ContentItemType.validator],
        default=ContentItemType.basic
    )

    tags = models.ManyToManyField(
        to="tags.Tag",
        db_index=True,
        blank=True,
        verbose_name=ugettext_lazy("Tags"),
        help_text=ugettext_lazy(
            "Tags help others find your contents. "
            "To add a new tag write its name and press enter or comma. ")

    )

    domains = models.ManyToManyField(
        to=Domain,
        verbose_name=ugettext_lazy("Domains"),
        blank=True,
        help_text=ugettext_lazy(
            "You can select more than one domain by Ctrl clicking on domains (On mac use Command '⌘' + Click)."
        )
    )

    main_discipline = models.ForeignKey(
        Domain,
        null=False,
        blank=False,
        related_name="+",
        limit_choices_to={
            "type__slug": "discipline"
        },
        help_text=ugettext_lazy("Main discipline, you can select more disciplines below.")

    )

    # flake whines about shadowing builtin here, however this name is reasonable
    license = models.CharField(  # noqa: B001
        verbose_name=ugettext_lazy('Resource license'),
        help_text=ugettext_lazy(
            'License on which resource is published. '
            'By selecting this license you certify that you have right to release '
            'this content under this license. '
            'Please note that while '
            'GUI will enable you to change the license, taking away rights '
            'from users retroactively is not possible in EC legal system '
            '(also this is not a legal advice, seek legal counsel if you need it).'
        ),
        max_length=128,
        choices=License.choices,
        validators=[License.validator],
        default=License.cc_by
    )

    cover_image = models.ImageField(
        verbose_name=ugettext_lazy('Cover image for this resource'),
        blank=True, null=True,
        validators=[image_name_validator],
        help_text=ugettext_lazy('Will be cropped to aspect ratio 6.75:1 (or 1080x160px)')
    )

    cover_image_resized = models.ImageField(
        verbose_name=ugettext_lazy('Cover image for this resource, resized to correct aspect ratio of 6.75:1'),
        blank=True, null=True
    )

    miniature_image = models.ImageField(
        verbose_name=ugettext_lazy('Miniature image for this resource'),
        help_text=ugettext_lazy(
            'Miniature image will be auto generated from Cover image if left blank or deleted. '
            'If you upload please use file with 1.185:1 aspect ratio (or 154x130px)'
        ),
        blank=True, null=True,
        validators=[image_name_validator]
    )

    miniature_image_resized = models.ImageField(
        verbose_name=ugettext_lazy('Miniature image for this resource, resized to correct aspect ratio of 1.185:1'),
        blank=True, null=True
    )

    min_group_size = models.PositiveIntegerField(
        verbose_name=ugettext_lazy('Minimal participant group size'),
        null=True, blank=True
    )
    max_group_size = models.PositiveIntegerField(
        verbose_name=ugettext_lazy('Maximal participant group size'),
        null=True, blank=True
    )
    activity_length = models.PositiveIntegerField(
        verbose_name=ugettext_lazy('Length of activity (minutes)'),
        null=True, blank=True
    )
    staff = models.PositiveIntegerField(
        verbose_name=ugettext_lazy('Number of staff'),
        null=True, blank=True
    )
    technical_requirements = models.TextField(
        verbose_name=ugettext_lazy("Special technical requirements"),
        null=True, blank=True
    )
    knowledge_gain = models.TextField(
        verbose_name=ugettext_lazy("Knowledge acquisition gain"),
        null=True, blank=True
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    last_updated_at = models.DateTimeField(
        auto_now=True, editable=False, null=False
    )

    hidden = models.BooleanField(
        default=False
    )
    """
    If this is set to true model is considered deleted, and will be removed from indexes and hence not-searchable. 
    """

    group = models.ForeignKey(ContentItemLanguageGroup)

    language = models.CharField(
        verbose_name=ugettext_lazy('Resource language'),
        max_length=2,
        validators=[validate_language_code],
        default='en',
        choices=const.LANGUAGES
    )

    featured = models.BooleanField(
        default=False,
        verbose_name=ugettext_lazy("Featured"),
        help_text=ugettext_lazy("Check this if you want to have this item to be featured on main page")
    )

    workforce_description = models.TextField(
        default='',
        blank=True,
        null=False,
        verbose_name=ugettext_lazy("Workforce description"),
        help_text=ugettext_lazy(
            "Here you can describe how this content will help students obtain "
            "high standing among workforce. If provided it will **replace** "
            "generic workforce box that contains discipline specific messages "
            "provided bu the consortium. You may use markdown here"
        )
    )

    workforce_title = models.CharField(
        default='',
        null=False,
        blank=True,
        max_length=1024,
        verbose_name=ugettext_lazy("Workforce description label"),
        help_text=ugettext_lazy(
            "If 'Workforce description' is filled this enables you to override "
            "it's title. "
        )
    )

    verified = models.BooleanField(
        default=False,
        verbose_name=ugettext_lazy("Is verified"),
        help_text=ugettext_lazy("Check if content is verified or partner provided.")
    )

    @property
    def workforce_description_html(self):
        """
        HTML formatted workforce description, it overrides generic
        description block.
        """
        return CLEANER.clean(markdown.markdown(self.workforce_description))

    @property
    def description_html(self):  # pragma: no cover
        """
        HTML formatted description.
        """
        if self.description:
            return CLEANER.clean(markdown.markdown(self.description))
        return ""

    @property
    def language_name(self):
        """
        Lookup language name in constants.
        """
        return dict(const.LANGUAGES)[self.language]

    def clean(self):
        if self.min_group_size and self.max_group_size and self.max_group_size < self.min_group_size:
            raise ValidationError({
                'max_group_size': ugettext_lazy('Maximum group size can no be lower than minimum size.')
            })

    def __str__(self):
        return self.title

    @property
    def resource_types(self) -> typing.Mapping[str, const.ResourceType]:
        """
        List of resource types valid for this content item.
        """
        if self.type == 'basic':
            return OrderedDict(
                (k, v) for k, v in const.ResourceType._fields.items()  # pylint: disable=no-member
                if k in {
                    const.ResourceType.document, const.ResourceType.link,
                    const.ResourceType.file, const.ResourceType.simulation,
                    const.ResourceType.quiz
                }
            )
        elif self.type == 'movie':
            types = OrderedDict(
                (k, v) for k, v in const.ResourceType._fields.items()  # pylint: disable=no-member
                if k in {const.ResourceType.video, const.ResourceType.file, const.ResourceType.subtitle}
            )
            res = self.resources.all()
            # remove video resource if it already is added to content item
            for r in res:
                if r.resource_type == const.ResourceType.video:
                    del types[const.ResourceType.video]
                    break
            return types
        raise NotImplementedError  # pragma: no cover

    @property
    def default_resource(self):
        """
        Gets default resource
        :return: default resource object
        """
        from www.resource.models import Resource
        try:
            return self.resources.get(default=True)
        except Resource.DoesNotExist:
            return None

    def domains_with_descriptions(self, type_slug=None):
        """
        Gets list of domains with descriptions (if they exist)
        :return:
        """
        if type_slug:
            domains = {domain: None for domain in self.domains.filter(type__slug=type_slug)}
            descriptions = {desc.domain: desc for desc in self.domain_descriptions.filter(domain__type__slug=type_slug)}
        else:
            domains = {domain: None for domain in self.domains.all()}
            descriptions = {desc.domain: desc for desc in self.domain_descriptions.all()}

        domains.update(descriptions)

        return domains

    def __repr__(self):
        return "<Content Item pk={s.pk} title={s.title}>".format(s=self)

    class Meta:
        unique_together = ('group', 'language')
        ordering = (
            'pk',
        )
        permissions = (
            ("is_autoverified", ugettext_lazy("Uploads by this user are automatically verified")),
            ("can_verify", ugettext_lazy("Can verify content items")),
        )


class ContentItemVerifier(ContentItem):
    """Proxy for Content Item used for content """

    class Meta:
        proxy = True


class DomainDescription(models.Model):
    """
       Domain description model

       Each RRI selected in content item has to have description,
       RRI is represented as a domain (optional, does not have to exist).
    """

    content_item = models.ForeignKey(ContentItem, related_name="domain_descriptions")
    domain = models.ForeignKey(Domain, related_name="descriptions")

    description = models.TextField(verbose_name=ugettext_lazy('Domain description'))

    @property
    def description_md(self):
        """ Return description parsed by markdown library """
        return markdown.markdown(self.description)

    class Meta:
        unique_together = ('content_item', 'domain')
        ordering = (
            'pk',
        )
