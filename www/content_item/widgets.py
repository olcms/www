# -*- coding: utf-8 -*-
"""Additional form widgets for content items"""  # pragma: no cover

from django.forms import widgets, SelectMultiple  # pragma: no cover
from django.utils.html import format_html  # pragma: no cover


class GroupedSelectMultiple(widgets.MultiWidget):  # pragma: no cover
    """
    MultiWidget with separated multi-select for each group.
    """

    def __init__(self, attrs=None):
        super(GroupedSelectMultiple, self).__init__([], attrs=attrs)

    def decompress(self, value):
        if value:
            return value
        return [None for _ in self.widgets]

    def format_output(self, rendered_widgets):  # pylint: disable=no-self-use,missing-docstring
        return u''.join(rendered_widgets)

    def _get_choices(self):
        # pylint: disable=no-member
        return self._choices

    def _set_choices(self, value):
        self.widgets = []
        for group in value:
            widget = TitledSelectMultiple(title=group[0], choices=group[1])
            self.widgets.append(widget)

    choices = property(_get_choices, _set_choices)


class TitledSelectMultiple(SelectMultiple):  # pragma: no cover
    """
    SelectMultiple widget with title.
    """

    def __init__(self, attrs=None, title='', choices=()):
        super(TitledSelectMultiple, self).__init__(attrs, choices)
        self.title = title

    def render(self, name, value, attrs=None):  # pylint: disable=arguments-differ,no-self-use
        """Render the widget"""
        return format_html('''<fieldset>
        <legend>{}</legend>
        {}
        </fieldset>''', self.title, super().render(name, value, attrs))
