$(function () {

    let bindToResources = function () {
        let resourceList = $("#resourceList");
        resourceList.find("a.upload-link").each(modalFormFactory);
        resourceList.find("a.edit-link").each(
            function(index: number, element: any)
            {
                modalFormFactory(index, element, gettext("Edit resource"), gettext("Submit"))
            }
        );
        resourceList.find("a.delete-link").each(
            function(index: number, element: any)
            {
                modalFormFactory(index, element, gettext("Delete resource?"), gettext("Confirm"))
            }
        );
        resourceList.find("a.upload-link").each(function (index: number, element: any) {
           modalFormFactory(index, element, gettext("Upload new version of file"), gettext("Submit"));
        });
    };

    let reloadResources = function() {
        $.ajax({
            type: "GET",
            url: window["content_item_resources_url"]
        }).done(function(response: any) {
            $('#_appendHere').empty();
            $('#_appendHere').html(response);
            bindToResources();
        }).fail(function() {
            location.reload();
        });
    };

    var modalFormFactory = function (index: number, element: any, formTitle: string, buttonText: string) {
        var modalManager = new ModalForm(
            element,
            "#generic-modal",
            $(element).attr('href'),
            gettext("Resource added successfully"),
            reloadResources,
            formTitle,
            buttonText
        )
    };
    $("#add-resource").find("a.dropdown-item").each(modalFormFactory);

    $(function () {
        $('[data-toggle="popover"]').popover();
        $('.popover-dismiss').popover({trigger: 'focus'});
    });

    bindToResources();

    new ModalForm(
        "#content-item-delete",
        "#generic-modal",
        $("#content-item-delete").attr('href'),
        gettext("Deleted this content item"),
        () => {
            window.location.href = "/";
        },
        gettext("Remove content item"),
        gettext("Delete")
    );

    if ($("#update-verified").length > 0){

        new ModalForm(
            "#update-verified",
            "#generic-modal",
            $("#update-verified").attr('href'),
            null,
            () => {window.location.reload()},
            gettext("Update verified state"),
            gettext("Save")
        );
    }


});
