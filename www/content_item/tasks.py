# -*- coding: utf-8 -*-
"""Tasks executed asynchronously by Celery"""
from PIL import Image
from celery import task

from www.utils.image_utils import image_resize_and_crop_to_aspect, save_pil_image

from www.content_item import const
from www.content_item.models import ContentItem


@task(bind=True)
def resize_content_item_images(self, content_item_pk):
    """
    Crop and resize cover image and miniature image to correct aspect ratios
    If there is no miniature image use cover image for it.
    :param content_item:
    :return:
    """
    try:
        content_item = ContentItem.objects.get(pk=content_item_pk)
        if content_item.cover_image:
            image = Image.open(content_item.cover_image)
            resized_image = image_resize_and_crop_to_aspect(image, *const.ImageProperties.cover_image_size)
            save_pil_image(content_item, content_item.cover_image_resized, resized_image, content_item.cover_image.name,
                           image.format)

            if not content_item.miniature_image:
                save_pil_image(content_item, content_item.miniature_image, image, content_item.cover_image.name,
                               image.format)

        if content_item.miniature_image:
            miniature_image = Image.open(content_item.miniature_image)
            resized_miniature_image = image_resize_and_crop_to_aspect(miniature_image,
                                                                      *const.ImageProperties.miniature_size)
            save_pil_image(content_item, content_item.miniature_image_resized, resized_miniature_image,
                           content_item.miniature_image.name, miniature_image.format)
    except Exception as e:
        raise self.retry(exc=e)
