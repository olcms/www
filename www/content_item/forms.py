# -*- coding: utf-8 -*-
"""Views for resource app."""
from collections import OrderedDict

from django import forms
from django.conf import settings
from django.contrib.postgres.forms.array import SimpleArrayField
from django.core.exceptions import ValidationError
from django.forms import ModelMultipleChoiceField
from django.forms.fields import CharField
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy
from haystack.backends import SQ
from haystack.forms import SearchForm
from haystack.inputs import AutoQuery
from haystack.query import SearchQuerySet

from www.content_item.fields import TagField
from www.content_item.search_indexes import ContentIndex
from www.tags.serializers import TagRelatedField
from . import models, const


class ContentItemCreateForm(forms.ModelForm):
    """
    Form for creating a content item.

    Mostly normal form, with some magic that turns
    tags (a list of strings) to related models.
    """

    tags = SimpleArrayField(CharField(max_length=100), required=False)

    def __init__(self, group_id: int=None, **kwargs):
        super().__init__(**kwargs)
        if self.instance is not None and self.instance.pk:
            self.initial['tags'] = ",".join((tag.name for tag in self.instance.tags.all()))
        self.fields['main_discipline'].queryset = models.Domain.objects.filter(type__slug="discipline")
        self.group_id = group_id
        self.domain_fields = []
        for domain_type in models.DomainType.objects.all():
            initial = []
            if self.instance and self.instance.pk:
                initial = self.instance.domains.filter(type=domain_type)
            field = ModelMultipleChoiceField(
                queryset=models.Domain.objects.filter(type=domain_type), label=domain_type.name,
                help_text=ugettext_lazy(
                    "You can leave this empty. You can select more than one by Ctrl "
                    "clicking on domains (On mac use Command '⌘' + Click)"),
                initial=initial,
                required=False
            )
            field_name = 'domain_{}'.format(domain_type.slug)
            self.fields[field_name] = field
            self.domain_fields.append(field.get_bound_field(self, field_name))

    def clean_tags(self):
        """Converts tags from list of strings to list of tags, ready to be saved."""
        tags = self.cleaned_data['tags']
        if tags is None:
            return []  # pragma: no cover
        tag_serializer = TagRelatedField(model=models.ContentItem)
        return [tag_serializer.to_internal_value(tag) for tag in tags]

    def clean(self):
        super().clean()
        lang = self.cleaned_data['language']

        if self.group_id:
            q_same_lang = models.ContentItem.objects.filter(group_id=self.group_id, language=lang)
            lang_names = dict(const.LANGUAGES)
            if q_same_lang.exists():
                raise ValidationError({
                    'language': ugettext_lazy(
                        mark_safe(
                            '{} version of this content already exists, You will find it <a href="{}">here</a>'.format(
                                lang_names[lang],
                                reverse('content_item:detail', kwargs={'pk': q_same_lang.first().pk}))))
                })

    def _save_m2m(self):
        super()._save_m2m()
        self.instance.tags.clear()
        self.instance.tags.add(*self.cleaned_data['tags'])

        self.instance.domains.clear()

        for k, v in self.cleaned_data.items():
            if k.startswith('domain_'):
                if v:
                    self.instance.domains.add(*v)

        self.instance.domains.add(self.instance.main_discipline)

    class Meta:
        """Meta Model."""
        model = models.ContentItem
        fields = [
            'title',
            'description',
            'authors',
            'type',
            'main_discipline',
            'tags',
            'cover_image',
            'miniature_image',
            'license',
            'language',
            'min_group_size',
            'max_group_size',
            'activity_length',
            'staff',
            'technical_requirements',
            'knowledge_gain',
            'featured',
            'workforce_description',
            'workforce_title',
        ]


class ContentItemEditForm(ContentItemCreateForm):
    """
    Form that edits ContentItem.

    During edition you can't change type.
    """
    class Meta:
        """Meta Model."""
        model = models.ContentItem
        fields = [
            'title',
            'description',
            'authors',
            'main_discipline',
            'tags',
            'cover_image',
            'miniature_image',
            'license',
            'language',
            'min_group_size',
            'max_group_size',
            'activity_length',
            'staff',
            'technical_requirements',
            'knowledge_gain',
            'domains',
            'featured',
            'workforce_description',
            'workforce_title',
        ]


LANGUAGES_WITH_ALL_LANGUAGES = (
    ('all', ugettext_lazy("All languages")),
) + settings.LANGUAGES


class BaseContentItemSearchForm(SearchForm):

    """Base class for search forms (both normal and advanced)."""

    license = forms.ChoiceField(
        choices=const.License.choices_with_empty(),
        required=False,
        label=ugettext_lazy("License"),
        initial='all'
    )

    language = forms.ChoiceField(
        label=ugettext_lazy("Language"),
        initial='all',
        required=False,
        choices=LANGUAGES_WITH_ALL_LANGUAGES
    )

    def __init__(self, **kwargs):
        kwargs.setdefault('searchqueryset', ContentIndex.get_search_queryset())
        super().__init__(**kwargs)

    def no_query_found(self):
        return self.searchqueryset.all()

    def search(self) -> SearchQuerySet:
        if not self.is_valid():
            return self.no_query_found()

        sqs = self.searchqueryset

        if self.cleaned_data['q']:
            query = self.cleaned_data['q']

            q = AutoQuery(query)

            # Due to how haystack ranking works, all "boosted" fields need to be present
            # in the search for the boost to work.
            # See: http://django-haystack.readthedocs.io/en/v2.4.1/boost.html#field-boost
            sqs = sqs.filter(
                SQ(text=q) | SQ(tags=q) | SQ(title=q) | SQ(description=q)
            )

        if self.cleaned_data['language']:
            if self.cleaned_data['language'] != 'all':
                sqs = sqs.filter(language__exact=self.cleaned_data['language'])

        if self.cleaned_data['license']:
            if self.cleaned_data['license'] != 'all':
                sqs = sqs.filter(license__exact=self.cleaned_data['license'])

        return sqs


class ContentItemSearchForm(BaseContentItemSearchForm):
    """
    Form for searching ContentItems.
    """

    tags = TagField(
        label=ugettext_lazy("Tags"),
        required=False
    )

    domain = forms.ModelChoiceField(
        required=False,
        queryset=models.Domain.objects.all()
    )

    methodology = forms.ModelChoiceField(
        required=False,
        queryset=models.Domain.objects.all()
    )

    def __init__(self, **kwargs):
        kwargs.setdefault('searchqueryset', ContentIndex.get_search_queryset())
        super().__init__(**kwargs)

    def search(self):

        sqs = super().search()

        if self.cleaned_data['methodology']:
            sqs = sqs.filter(
                domains__exact=self.cleaned_data['methodology'].slug
            )

        if self.cleaned_data['domain']:
            sqs = sqs.filter(domains__exact=self.cleaned_data['domain'].slug)

        if self.cleaned_data['tags']:
            for tag in self.cleaned_data['tags']:
                sqs = sqs.filter(tags__exact=tag.name)

        return sqs


class AdvancedSearchForm(BaseContentItemSearchForm):

    """Advanced search form."""

    @classmethod
    def __field_for_dt(cls, dt: models.DomainType):
        return forms.ModelMultipleChoiceField(
            label=dt.name,
            queryset=dt.domains.all(),
            widget=forms.CheckboxSelectMultiple(),
            required=False
        )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.extra_fields = OrderedDict()

        for dt in models.DomainType.objects.all():
            self.extra_fields[dt.slug] = self.__field_for_dt(dt)

        self.fields.update(self.extra_fields)

    def search(self):

        sqs = super().search()

        extra_field_names = set(self.extra_fields.keys())

        for key, value in self.cleaned_data.items():
            if key in extra_field_names:
                sqs = sqs.filter(
                    domains__in=[item.slug for item in value]
                )

        return sqs


class ContentItemEditDomainDescriptionForm(forms.ModelForm):
    """
    Form for adding and editing Domain descriptions.
    """
    class Meta:
        model = models.DomainDescription
        fields = ['description']
