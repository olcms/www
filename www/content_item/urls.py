"""Urls for resource app."""
# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'add(?:/(?P<group_id>\d+))?/?$',
        view=views.ContentItemCreate.as_view(),
        name='add-lang'
    ),
    url(
        regex=r'add/?$',
        view=views.ContentItemCreate.as_view(),
        name='add'
    ),
    url(
        regex=r'^edit/(?P<pk>\d+)/?$',
        view=views.ContentItemUpdate.as_view(),
        name='edit'
    ),
    url(
        regex=r'^verify/(?P<pk>\d+)/?$',
        view=views.ContentItemVerifyView.as_view(),
        name='verify'
    ),
    url(
        regex=r'detail/(?P<pk>\d+)/?$',
        view=views.ContentItemDisplay.as_view(),
        name='detail'
    ),
    url(
        regex=r'detail/resources/(?P<content_id>\d+)/?$',
        view=views.get_content_resources,
        name='contentresources'
    ),
    url(
        r'search/advanced/?$',
        view=views.AdvancedContentItemSearchView.as_view(),
        name='search-advanced'
    ),
    url(
        r'search(?:/domain/(?P<domain>[\w\-_\+\d]+))?(?:/page/(?P<page>\d+))?'
        r'(?:/methodology/(?P<methodology>[\w\-_\+\d]+))?(?:/term/(?P<term>.*))?/?',
        view=views.ContentItemSearchView.as_view(),
        name='search'
    ),
    url(
        regex=r'domain_description_edit/(?P<pk>\d+)/?$',
        view=views.ContentItemEditDomainDescription.as_view(),
        name='domain_description_edit'
    ),
    url(
        regex=r'domain_description_add/(?P<domain>\d+)/(?P<content_item>\d+)/?$',
        view=views.ContentItemAddDomainDescription.as_view(),
        name='domain_description_add'
    ),
    url(
        r'delete/(?P<pk>\d+)',
        view=views.ContentItemDeleteView.as_view(),
        name="delete"
    )

]
