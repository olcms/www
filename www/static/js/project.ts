/* Project specific Javascript goes here. */

declare const $: any, Bloodhound: any, Urls: any, SimpleMDE: any;

declare const gettext: (msg: string) => string;

/*
 Formatting hack to get around crispy-forms unfortunate hardcoding
 in helpers.FormHelper:

 if template_pack == 'bootstrap4':
 grid_colum_matcher = re.compile('\w*col-(xs|sm|md|lg|xl)-\d+\w*')
 using_grid_layout = (grid_colum_matcher.match(self.label_class) or
 grid_colum_matcher.match(self.field_class))
 if using_grid_layout:
 items['using_grid_layout'] = True

 Issues with the above approach:

 1. Fragile: Assumes Bootstrap 4's API doesn't change (it does)
 2. Unforgiving: Doesn't allow for any letiation in template design
 3. Really Unforgiving: No way to override this behavior
 4. Undocumented: No mention in the documentation, or it's too hard for me to find
 */

$('.form-group').removeClass('row');
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

class MiscUtils{

  static assertSingleElementMatched(selector: any, message: string= `Empty Selector ${selector}`){
    if(selector.length != 1){
      throw message;
    }
    return selector;
  }

  static assertSelectorMatches(selector: any, message: string= "Empty Selector"){
    if(selector.length == 0){
      throw message;
    }
    return selector;
  }

  static assertNotNull(object: any, message: string="Object is null"){
    if(object == null){
      throw message;
    }
  }

  static noop(){}

  static scrollTo(
    selectorElement: any,
    animationTime: number = 2000,
    callback: ()=>void = MiscUtils.noop
  ){
    $('html, body').animate({
      scrollTop: $(selectorElement).offset().top,
      callback: callback
    }, animationTime);
  }

}

interface TagInput{
  selector: string;
  required?: boolean;
  suggestionUrl?: string;
  missingTagMessage?: string;
}

/**
 * Simple wrapper for Bootstrap4 alerts component.
 * https://v4-alpha.getbootstrap.com/components/alerts/1
 */
class Messages{
  containerSelector: string;
  defaultScroll: boolean;

  private static validMessageTags = [
    "success", "warning", "danger", "info"
  ];

  /**
   * @param containerSelector Jquery selector for a div that will contain all messages.
   */
  constructor(containerSelector: string, defaultScroll: boolean=true){
    this.containerSelector = containerSelector;
    MiscUtils.assertSelectorMatches($(this.containerSelector), "No container for messages");
    this.defaultScroll = defaultScroll;
  }

  public clear(){
    $(this.containerSelector).empty();
  }

  private maybeScroll(container: any, scroll: boolean, callback: () => void){
    if(scroll){
      MiscUtils.scrollTo(container, 100, callback);
    }else{
      callback();
    }
  }

  private maybeClear(container: any, clear: boolean, callback: () => void){
    if(clear){
      let items = container.find("*");
      if(items.length != 0) {
        items.fadeOut(100, function () {
        container.empty();
        callback();});
      }
      else {
        callback()
      }
    }else{
      callback();
    }
  }

  public addMessage(text: string, tag: string = "info", clear: boolean=true, focus: boolean=null){
    if(Messages.validMessageTags.indexOf(tag) == -1){
      throw `Unknown message tag ${tag}`;
    }
    if (focus == null){
      focus = this.defaultScroll;
    }
    let container = $(this.containerSelector);
    let self = this;
    this.maybeScroll(container, focus, function () {
      self.maybeClear(container, clear, function () {
        container.append(
          $("<div/>").addClass("alert").addClass(`alert-${tag}`).html(text)
        );
        container.fadeIn();
      });
    });
  }
}

window['mainMessages'] = new Messages("#messages");

/**
 * This is jQuery helper for forms that contain tokenfield inputs.
 *
 * These objects serve two functions:
 *
 * 1. Handle bootstrap typahead suggestions.
 * 2. Help with rendering of required tag fields (see onSubmit method)
 */
class FormWithTags{
  submitSelector: string;
  tagInputs: Array<TagInput>;
  requiredInputs: Array<TagInput> = [];

  constructor(submitSelector: string, tagInputs: Array<TagInput>) {
    this.submitSelector = submitSelector;
    this.tagInputs = tagInputs;
    let self = this;
    this.tagInputs.map((value: TagInput) => self.initializeTagInput(value));
    $(submitSelector).on('click', () => self.onSubmit());

    for(let inp of this.tagInputs)
    {
      let tag_field = $(inp.selector);
      let tag_input = $(inp.selector + "-tokenfield");
      tag_input.on('focusout', () => self.submit_unfinished_tags(tag_field, tag_input));
    }
  }

  /**
   * Check if tag_input (hidden field under each registered tags field)
   * contains anything, if yes create new token and clear input field
   * */
  protected submit_unfinished_tags(tag_field: any, tag_input: any)
  {
    let input_text = tag_input.val();

    if(input_text != '')
    {
      tag_input.val('');
      tag_field.tokenfield('createToken', input_text);
    }
  }


  /**
   Authors field is required, howerver required attribute is hidden and what is
   visible is tokenfield proxy. When user clicks submit submit is stopped with
   HTML5 validation, error is suppressed as field is hidden.

   This adds error to the form via jQuery.
   */
  protected onSubmit(){
    let self = this;
    this.requiredInputs.map((value: TagInput) => self.handleRequiredField(value));
  }

  private handleRequiredField(field: TagInput){
    let authors = $(field.selector);
    let form_group = authors.closest(".form-group");
    let help_block = form_group.find('div.help-block');

    // If value is empty we display error message
    if(!authors.val()){
      form_group.addClass('has-danger');

      let feedback = help_block.prev();

      if(!feedback.hasClass('form-control-feedback')){
        feedback = $('<div/>').addClass('form-control-feedback').insertBefore(
          help_block
        );
      }
      feedback.show();
      if (field.missingTagMessage) {
        feedback.html(field.missingTagMessage);
      }
      feedback.focus();
    } else {
      // If value is not empty we hide it
      authors.closest(".form-group").removeClass('has-danger');
      let feedback = help_block.prev();
      feedback.hide()
    }
  }

  private makeTokenfield(input: TagInput) {
    let settings = {};
    let engine: any = null;

    if (input.suggestionUrl) {
      engine = new Bloodhound({
        remote: {
          "url": input.suggestionUrl,
          "prepare": function (query: any, settings: any) {
            settings.url += "?term=" + query;
            return settings
          },
          "filter": function (data :any) {
            return data.results.map(function (item: any) {
              return item.name;
            });
          }
        },
        datumTokenizer: function (d: any) {
          return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
      });
      settings['typeahead'] = [null, { source: engine.ttAdapter() }]
    }

    if(input.selector == null){
      throw "Selector for formfield is null.";
    }

    $(input.selector).tokenfield(settings);
  }

  private initializeTagInput(input: TagInput){
    let $field = $(input.selector);

    if(input.required == null){
      input.required = $field.prop('required');
    }

    if(input.required){
      this.requiredInputs.push(input);
    }

    this.makeTokenfield(input);
  }
}


abstract class BaseModalForm{

  /**
   * Two magic events attached to **modal** (i.e. element
   * pointed by this.modalSelector). Triggering ``CLOSE_EVENT``
   * will hide the modal, ``SUCCESS_EVENT`` will trigger
   * success event.
   */
  public static CLOSE_EVENT = "olcms:close-modal";
  public static SUCCESS_EVENT = "olcms:success-modal";

  private submitSelector: string;
  private modalSelector: string;
  private successCalback: ()=> void;

  private formTitle: string;
  private submitButtonText: string;

  constructor(
      submitSelector: string,
      modalSelector: string,
      successCalback: ()=> void,
      formTitle: string = "Upload a resource",
      submitButtonText: string = "Submit")
  {
    let self = this;
    this.submitSelector = submitSelector;
    this.modalSelector = modalSelector;
    this.successCalback = successCalback;
    this.submitButtonText = submitButtonText;
    this.formTitle = formTitle;
    MiscUtils.assertSingleElementMatched($(this.submitSelector)).click(function (e: any) {
        e.preventDefault();
        self.startFormDisplay();
      }
    );
  }

  protected waitingDialogContents(): string{
    return gettext("Please wait");
  }

  /**
   *
   * @param onNewHtml
   */
  protected abstract getFormHtml(onNewHtml: (html: string) => void): any;

  /**
   * Returns message displayed to user on form submitted.
   */
  protected abstract getSuccessMessage(): string;

  /**
   * Callback on data submitted,
   *
   * @param data whatever was returned by endpoint.
   */
  protected onFormSubmitted(data: any){
    window['mainMessages'].addMessage(
      this.getSuccessMessage(),
      "success"
    );
    this.successCalback();
  }

  protected getForm(): any{
    return MiscUtils.assertSingleElementMatched(this.getModal().find('form'));
  }

  protected getModal(): any{
    return MiscUtils.assertSelectorMatches($(this.modalSelector));
  }

  protected getModalBody(): any{
    return MiscUtils.assertSelectorMatches(this.getModal().find(".modal-body"));
  }

  protected getModalTitle(): any{
    return MiscUtils.assertSelectorMatches(this.getModal().find(".modal-title"));
  }

  protected getSubmitButton(): any{
    return MiscUtils.assertSelectorMatches(this.getModal().find(".modal-submit"));
  }

  /**
   * Returns true if status code signifies that form was processed,
   * and false if there were errors in the form and user needs to correct them.
   *
   * This is called only for 2XX statuses, other statuses are always considered an error.
   * @param status
   * @returns {boolean}
   */
  protected wasFormProcessedOk(status: number): boolean{
    switch (status){
      case 200:
        return false;
      case 201:
      case 202:
        return true;
      default:
        throw "Unknown status code {}"
    }
  }

  protected startFormDisplay(){
    let self = this;
    let modal = this.getModal();
    this.attachCustomEventListeners(modal);
    this.getSubmitButton().text(this.submitButtonText);
    this.getModalTitle().text(this.formTitle);
    this.getModalBody().html(this.waitingDialogContents());
    modal.modal("show");
    this.getFormHtml(function (html: string) {
      self.displayForm(html)
    });

  }

  protected attachCustomEventListeners(modal: any){
    modal.on(BaseModalForm.CLOSE_EVENT, (e: any) => {
      this.hideForm();
    });
    modal.on(BaseModalForm.SUCCESS_EVENT, (e: any, data: any) => {
      this.onFormSuccess(data);
    });
  }

  protected detachCustomEventListeners(){
    let modal = this.getModal();
    modal.off(BaseModalForm.CLOSE_EVENT).off(BaseModalForm.SUCCESS_EVENT);
  }

  protected displayForm(formHtml: string){
    let modal = this.getModal();
    // This needs to be done BEFORE loading form contents from html (it can contain js that manipulates buttons)
    this.getSubmitButton().prop('disabled', false);
    this.getModalBody().html(formHtml);
    modal.modal('show');
    console.log($._data(this.getSubmitButton().get(0), "events"));
    this.getSubmitButton().on('click', () => {
      this.getSubmitButton().prop('disabled', true);
      this.onFormSubmit();
    });
    this.getModal().on('hide.bs.modal', () => {
      this.unbindListenersOnHide();
    });
    modal.on()
  }

  private unbindListenersAfterFormSubmit(){
      this.getSubmitButton().off('click');
  }

  private unbindListenersOnHide(){
    this.unbindListenersAfterFormSubmit();
    this.getModal().off('hide.bs.modal');
  }

  protected getSubmitUrl(){
    return this.getForm().attr('action');
  }

  protected getAjaxOptions(data: any): Object{
    MiscUtils.assertNotNull(this.getSubmitUrl());
    return {
      "method": "POST",
      "data": data,
      "url": this.getSubmitUrl(),
      headers: [

      ]
    }
  }

  protected displayErrorMessage(){
    if ($("#form-messages").length == 0){
      this.getModalBody().prepend($("<div/>").attr('id', 'form-messages'));
    }
    let messages = new Messages("#form-messages", false);
    messages.addMessage(
      gettext("There was an error communicating with the server. Please try again, if problem persists please contact the support."),
      "danger"
    )
  }

  protected onFormSubmitError(){
    this.displayErrorMessage();
  }

  protected onFormSubmitInvalidForm(data: any, textStatus: any, jxhr: any){
    this.unbindListenersAfterFormSubmit();
    this.displayForm(data);
  }

  protected onFormSubmitValidForm(data: any, textStatus: any, jxhr: any){
    this.onFormSuccess(data);
  }

  protected onFormSuccess(data: any){
    this.onFormSubmitted(data);
    this.hideForm();
  }

  protected hideForm(){
    this.unbindListenersAfterFormSubmit();
    this.getModal().modal('hide');
    this.detachCustomEventListeners();
  }

  protected onFormSubmit(){
    let self = this;
    let form = this.getForm();
    let data = form.serialize();
    let deferred = $.ajax(this.getAjaxOptions(data));
    deferred.fail(function () {
      self.onFormSubmitError();
    });
    deferred.done(function (data: any, textStatus: any, jxhr: any) {
      if (self.wasFormProcessedOk(jxhr.status)){
        self.onFormSubmitValidForm(data, textStatus, jxhr);
      } else{
        self.onFormSubmitInvalidForm(data, textStatus, jxhr);
      }
    });
  }
}

class ModalForm extends BaseModalForm{

  private endpointUrl: string;
  private successMessage: string;

  constructor(
    submitSelector: string,
    modalSelector: string,
    endpointURL: string,
    successMessage: string,
    successCalback: ()=> void,
    formTitle: string,
    submitButtonText: string
  ) {
    super(submitSelector, modalSelector, successCalback, formTitle, submitButtonText);
    MiscUtils.assertNotNull(endpointURL);
    this.endpointUrl = endpointURL;
    this.successMessage = successMessage;
  }

  protected getFormHtml(onNewHtml: (html: string)=>void) {
    let self = this;
    MiscUtils.assertNotNull(this.endpointUrl);
    let deferred = $.ajax({
      "method": "GET",
      "url": this.endpointUrl
    });
    deferred.done(function (data: any) {
      onNewHtml(data);
    });
    deferred.fail(function () {
      self.displayErrorMessage();
    });
  }

  protected getSubmitUrl(): any {
    if (this.getForm().length == 0) {
      return this.endpointUrl;
    }
    return super.getSubmitUrl();
  }

  protected getSuccessMessage(): string {
    return this.successMessage;
  }
}


interface FileUploadResponse{
  url: string;
  file_name: string;
  path: string;
}

enum GetUploadUrlResult{
  OK,
  ACCESS_DENIED,
  ERROR
}

function get_upload_url(
  file_name: string,
  namespace:string,
  callback: (success: GetUploadUrlResult, response: FileUploadResponse) => any)
{
  let deferred = $.ajax({
    "url": Urls['api:start-file-upload'](),
    "method": "POST",
    "data": {
      "file_name": file_name,
      "namespace": namespace
    }
  });
  deferred.fail(function (jqXR: any) {
    if (jqXR.status == 403){
      callback(GetUploadUrlResult.ACCESS_DENIED, null);
    }
    callback(GetUploadUrlResult.ERROR, null);
  });
  deferred.done(function (data: FileUploadResponse) {
    callback(GetUploadUrlResult.OK, data) ;
  })
}
