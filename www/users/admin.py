# -*- coding: utf-8 -*-
"""Users app admin panel configuration."""

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import User


class MyUserChangeForm(UserChangeForm):
    """Custom user change form."""

    class Meta(UserChangeForm.Meta):
        """Meta class for custom form."""

        model = User


class MyUserCreationForm(UserCreationForm):
    """Custom user creation form."""

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        """Meta class for custom form."""

        model = User

    def clean_username(self):
        """Username validator."""
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    """Custom user panel."""

    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
        ('User Profile', {'fields': ('name',)}),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username', 'name', 'is_superuser')
    search_fields = ['username', 'name', 'email', 'emailaddress__email']
