"""Views for users app."""
# -*- coding: utf-8 -*-

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from .models import User


class UserDetailView(LoginRequiredMixin, DetailView):
    """View that displays user's details."""

    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRedirectView(LoginRequiredMixin, RedirectView):
    """Redirects user to his detail view."""

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        """Redirection URL."""
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):
    """"Allows user to edit his own profile."""

    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = User

    def get_success_url(self):
        """Sends the user back to their own page after a successful update."""
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self, queryset=None):
        """Only get the User record for the user making the request."""
        return get_object_or_404(User, username=self.request.user.username)


class UserListView(LoginRequiredMixin, ListView):
    """Displays a list of all users."""

    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'
