"""App config for users app."""
# -*- coding: utf-8 -*-

from django.apps import AppConfig


class UsersConfig(AppConfig):
    """Configuration for Users model."""

    name = 'www.users'
    verbose_name = "Users"

    def ready(self):
        """
        Invoked when model is ready.

        Override this to put in:
        Users system checks
        Users signal registration
        """
