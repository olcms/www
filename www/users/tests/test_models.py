from test_plus.test import TestCase


class TestUser(TestCase):

    def setUp(self):
        self.user = self.make_user()

    def test__str__(self):
        self.assertEqual(
            self.user.__str__(),
            'testuser'  # This is the default username for self.make_user()
        )

    def test_get_absolute_url(self):
        self.assertEqual(
            self.user.get_absolute_url(),
            '/users/testuser/'
        )


class CheckFileuploadPermIsAdded(TestCase):

    def check_fileupload(self):
        user = models.User.objects.create(username = 'foo')
        user.save()
        self.assertTrue(user.has_perm('fileupload.add_fileupload'))
