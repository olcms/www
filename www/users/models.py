# -*- coding: utf-8 -*-
""""Models for users app."""

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class User(AbstractUser):
    """User model."""

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)

    def __str__(self):
        """String representation."""
        return self.username

    def get_absolute_url(self):
        """Redirects to user detail view."""
        return reverse('users:detail', kwargs={'username': self.username})
