"""Contains adapters for users application."""
# -*- coding: utf-8 -*-

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from django.conf import settings
from django.contrib.auth.models import Permission


class AccountAdapter(DefaultAccountAdapter):
    """Adapter that allows registration disabling."""

    def is_open_for_signup(self, request):
        """Check if registration is enabled."""
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def save_user(self, request, user, form, commit=True):
        user = super().save_user(request, user, form, commit)
        add_upload_perm = Permission.objects.get(codename='add_fileupload')
        user.user_permissions.add(add_upload_perm)
        return user


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    """Adapter that allows registration disabling."""

    def is_open_for_signup(self, request, sociallogin):
        """Check if registration is enabled."""
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)
