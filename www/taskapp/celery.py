# -*- coding: utf-8 -*-
"""Celery config."""

# Bunch of imports protected by ifs.
# pylint: disable=import-error

import os

from celery import Celery

from django.apps import AppConfig, apps
from django.conf import settings


if not settings.configured:
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')  # pragma: no cover


app = Celery('www')


class CeleryConfig(AppConfig):
    """App config for Celery app."""

    name = 'www.taskapp'
    verbose_name = 'Celery Config'

    def ready(self):
        """
        Ready hook.

        Using a string here means the worker will not have to
        pickle the object when using Windows.
        """
        app.config_from_object('django.conf:settings')
        installed_apps = [app_config.name for app_config in apps.get_app_configs()]
        app.autodiscover_tasks(lambda: installed_apps, force=True)

        app.conf.task_routes = {
            'document_converter.*': {'queue': 'converter'}
        }

        if hasattr(settings, 'RAVEN_CONFIG'):
            # Celery signal registration
            from raven import Client as RavenClient
            from raven.contrib.celery import register_signal as raven_register_signal
            from raven.contrib.celery import register_logger_signal as raven_register_logger_signal

            raven_client = RavenClient(dsn=settings.RAVEN_CONFIG['DSN'])
            raven_register_logger_signal(raven_client)
            raven_register_signal(raven_client)

        if hasattr(settings, 'OPBEAT'):
            from opbeat.contrib.django.models import client as opbeat_client
            from opbeat.contrib.django.models import logger as opbeat_logger
            from opbeat.contrib.django.models import register_handlers as opbeat_register_handlers
            from opbeat.contrib.celery import register_signal as opbeat_register_signal

            try:
                opbeat_register_signal(opbeat_client)
            except Exception as e:  # pylint: disable=broad-except
                opbeat_logger.exception('Failed installing celery hook: %s' % e)

            if 'opbeat.contrib.django' in settings.INSTALLED_APPS:
                opbeat_register_handlers()


@app.task(bind=True)
def debug_task(self):
    """Example task."""
    # Don't ask why the syntax of comments. This needs to be ignored both by coverage and flake.
    # So I needed to add comment in comment.
    print('Request: {0!r}'.format(self.request))  # pragma: no cover # noqa


convert_md_to_html = app.Task()
convert_md_to_html.name = 'document_converter.tasks.convert_md_to_html'

convert_md_to_odt = app.Task()
convert_md_to_odt.name = 'document_converter.tasks.convert_md_to_odt'

convert_md_to_docx = app.Task()
convert_md_to_docx.name = 'document_converter.tasks.convert_md_to_docx'

convert_md_to_pdf = app.Task()
convert_md_to_pdf.name = 'document_converter.tasks.convert_md_to_pdf'
