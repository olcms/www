# -*- coding: utf-8 -*-
"""Common validators"""
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy

from www.content_item import const

image_name_validator = RegexValidator(
    "^[a-zA-Z0-9._-]*$",
    ugettext_lazy("File name can only consists of letters, numbers and '.', '_', '-'"),
    "invalid-file-name"
)


def validate_language_code(code):
    """Validate language code."""
    if code not in [x[0] for x in const.LANGUAGES]:
        raise ValidationError(ugettext_lazy('Please enter correct language code'))
