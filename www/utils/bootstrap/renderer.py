# -*- coding: utf-8 -*-

from bootstrap3.renderers import FieldRenderer
from bootstrap3.utils import render_template_file


class FieldRendererWithErrorFormatting(FieldRenderer):
    """
    Custom renderer so that help and error block will have separate styling.
    """

    def append_to_field(self, html):
        rendered_html = ''

        if self.field_errors:
            rendered_html += self.render_block('bootstrap/field_errors.html', self.field_errors)

        if self.field_help:
            rendered_html += self.render_block('bootstrap3/field_help_text_and_errors.html', [self.field_help])

        if rendered_html != '':
            html += '<span class="help-block">{help}</span>'.format(help=rendered_html)
        return html

    def render_block(self, template_path, messages):
        return render_template_file(
            template_path,
            context={
                'field': self.field,
                'help_text_and_errors': messages,
                'layout': self.layout,
                'show_help': self.show_help,
            }
        )
