# -*- coding: utf-8 -*-
"""Various selenium helpers"""
import pathlib
import shutil
import subprocess
from os import path
import unittest

from contextlib import contextmanager
from importlib import import_module

import typing

import logging

from django.conf import settings
from django.contrib.auth import SESSION_KEY, BACKEND_SESSION_KEY, HASH_SESSION_KEY
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.management import call_command
from django.http.request import HttpRequest
from django.middleware import csrf
from django.test import tag
from django.urls.base import reverse
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.support.ui import WebDriverWait
from seleniumrequests import Firefox
from swiftclient.service import mkdirs

from www.utils.test_debugging_utils import connect_debug_handler

from .test_utils import UserTestMixin

ROOT_DIR = pathlib.Path(__file__).parents[2]

SCREENSHOTS = ROOT_DIR / "screenshots"


connect_debug_handler()

LOGGER = logging.getLogger("selenium.logs")


class SeleniumUtilsMixin(unittest.TestCase):
    """Various selenium helpers"""

    # pylint: disable=no-member

    # Spell-checker chokes on the link ...
    # pylint: disable=wrong-spelling-in-docstring

    PAGE_LOAD_TIMEOUT_SEC = 25

    @contextmanager
    def wait_for_page_load(self, timeout=30):
        """
        From here: http://www.obeythetestinggoat.com/how-to-get-selenium-to-wait-for-page-load-after-a-click.html
        """
        old_page = self.driver.find_element_by_tag_name('html')
        yield
        WebDriverWait(self.driver, timeout).until(
            staleness_of(old_page)
        )

    def reverse_url(self, view, *, kwargs=None, **extra_args):
        """Creates full url form view name."""
        return self.live_server_url + reverse(view, kwargs=kwargs, **extra_args)

    def get_current_path(self) -> str:
        """Gets current path (without server url)"""
        url = self.driver.current_url
        self.assertTrue(url.startswith(self.live_server_url))
        return url[len(self.live_server_url):]

    def select_bootsrap_dropdown(self, select_id, item_value):
        """
        Usually to select a item using selenium you'd need to:

        .. code-block:: python

            select = Select(self.driver.find_element_by_id("id_type"))
            select.select_by_value('basic')

        which for some reason does not work with our selects,
        so I have decided to just do some JavaScript magic

        :param select_id: Identifier of HTML element.
        :param item_value: Value of selected option
        :return:
        """
        self.driver.execute_script("""
        document.getElementById('{}').value='{}';
        """.format(
            select_id, item_value
        ))

    def clean_tokenfield(self, element: WebElement):
        """
        Cleans an input field by clicking on close widget for every item.
        """
        closeable = self.get_parent(element).find_elements_by_class_name('close')
        for el in closeable:
            el.click()

    def fill_tokenfield(self, element: typing.Union[WebElement, str], tags: typing.Sequence[str]):
        """

        :param element: Either a webelement or an element id.
        :param tags: tokens to fill input with
        :return:
        """

        if isinstance(element, str):
            self.assertTrue(
                element.endswith("tokenfield"),
                "Tokenfield elements ids end with '-tokenfield' you seem to be missing it"
            )

            element = self.driver.find_element_by_id(element)

        element.send_keys(", ".join(tags) + ",")

    @staticmethod
    def get_parent(element: WebElement):
        """
        Returns parent for element
        """
        return element.find_element_by_xpath("./..")

    def save_screenshot(self, postfix: str):
        """
        Stores a screen-shot, stored file will contain test name.

        :param postfix: Will be a part of screen-shot file.
        """
        self.driver.save_screenshot(str((SCREENSHOTS / (".".join((self.id(), postfix, "png"))))))
        source = self.driver.page_source
        html_source = str(SCREENSHOTS / (".".join((self.id(), postfix, 'html'))))
        with open(html_source, 'w', encoding="utf-8") as f:
            f.write(source)

    def wait(self, timeout: float=None) -> WebDriverWait:
        """
        Shortcut for WebDriverWait
        :param timeout: timeout in seconds. Defaults to PAGE_LOAD_TIMEOUT_SEC
        """
        if timeout is None:
            timeout = self.PAGE_LOAD_TIMEOUT_SEC
        return WebDriverWait(self.driver, timeout)


FIREFOX_PROFILE_DIR = "/tmp/olcms-firefox-profile"
"""
Firefox profile will be stored in this folder
"""

__PROFILE_CREATED = False
"""
This is a global variable storing information whether firefox profile was created 
in this test run. 
"""


def initialize_profile():
    """
    Initializes Firefox profile once per test suite execution.
    """
    global __PROFILE_CREATED  # pylint: disable=global-statement
    if not __PROFILE_CREATED:
        if path.exists(FIREFOX_PROFILE_DIR):
            shutil.rmtree(FIREFOX_PROFILE_DIR)
        mkdirs(FIREFOX_PROFILE_DIR)
        subprocess.check_call(['firefox', '-CreateProfile', 'olcms-test', FIREFOX_PROFILE_DIR])
        __PROFILE_CREATED = True


def force_login(user, driver: RemoteWebDriver, base_url):
    """
    Logs in the end user without explicitly filling login form. Saves us a selenium post request.

    This is taken from https://github.com/feffe/django-selenium-login available under MIT license.

    This code was adapted to set CSRF cookie.
    """
    SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
    selenium_login_start_page = getattr(settings, 'SELENIUM_LOGIN_START_PAGE', '/page_404/')
    driver.get('{}{}'.format(base_url, selenium_login_start_page))

    session = SessionStore()
    session[SESSION_KEY] = user.id
    session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
    session[HASH_SESSION_KEY] = user.get_session_auth_hash()
    session.save()

    domain = base_url.split(':')[-2].split('/')[-1]
    driver.add_cookie({
        'name': settings.SESSION_COOKIE_NAME,
        'value': session.session_key,
        'path': '/',
        'domain': domain
    })
    request = HttpRequest()
    driver.add_cookie({
        'name': settings.CSRF_COOKIE_NAME,
        'value': csrf.get_token(request),
        'path': '/',
        'domain': domain
    })
    driver.refresh()


@tag('slow')
class BrowserTestCaseMixin(UserTestMixin, SeleniumUtilsMixin, unittest.TestCase):
    """
    Base test case for Selenium tests.

    It is a slow test, you can suppress it  with ``manage.py test --exclude-tag=slow.``.
    """

    WebDriver = "Chrome"

    def login(self):
        """
        Logs in selenium browser.
        """

        force_login(self.user, self.driver, self.live_server_url)

    @property
    def driver(self) -> RemoteWebDriver:
        """Read only property to load driver."""
        return self.__driver

    @classmethod
    def create_driver(cls) -> RemoteWebDriver:
        """
        Creates the Selenium driver.
        """

        if cls.WebDriver == "Chrome":
            options = webdriver.ChromeOptions()
            options.add_argument("--no-sandbox")
            options.add_argument("--enable-logging")
            options.add_argument("--v=1")
            desired_capabilities = DesiredCapabilities.CHROME
            desired_capabilities['loggingPrefs'] = {'browser': 'ALL'}
            driver = webdriver.Chrome(chrome_options=options, desired_capabilities=desired_capabilities)
            driver.set_window_size(2000, 2000)
            # driver.maximize_window()
            return driver

        # Use the same profile in all tests
        initialize_profile()
        profile = webdriver.FirefoxProfile("/tmp/olcms-firefox-profile")
        firefox = Firefox(profile)
        firefox.maximize_window()
        return firefox

    def setUp(self):
        """
        Start driver.
        """
        # print("SET UP")
        super().setUp()
        if settings.ENABLE_XVBF:
            self.__display = Display(
                size=(2000, 2000)
            )
            self.__display.start()
        self.__driver = self.create_driver()
        self.__driver.set_page_load_timeout(self.PAGE_LOAD_TIMEOUT_SEC)
        self.__driver.implicitly_wait(self.PAGE_LOAD_TIMEOUT_SEC)

    def tearDown(self):
        """
        Close driver.
        """
        # print("TEAR DOWN")
        super().tearDown()
        for entry in self.__driver.get_log('browser'):
            LOGGER.info(entry)
        self.save_screenshot("final")
        self.__driver.quit()
        if settings.ENABLE_XVBF:
            self.__display.stop()


class BaseLiveServerTestCase(StaticLiveServerTestCase):
    """
    Live server test case that uses serialized rollback.
    """

    serialized_rollback = True


class LiveServerBrowserTestCase(BaseLiveServerTestCase, BrowserTestCaseMixin):
    """
    Inherit from this class.
    """

    CLEAR_INDEX = True

    def setUp(self):
        super().setUp()
        if self.CLEAR_INDEX:
            call_command("clear_index", interactive=False, stdout=open('/dev/null', 'w'))
