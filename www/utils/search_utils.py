# -*- coding: utf-8 -*-

"""
Various utilities related to testing haystack.
"""

import typing

import django.apps
from django.core.management import call_command
from django.test import SimpleTestCase

import haystack
import haystack.signals


class TestSearchBase(SimpleTestCase):

    """
    Instruments Haystack for testing.

    1. Overrides folder that will be used by Whoosh to store indexed data.
    2. Clears this temporary folder after tests.
    """

    def setUp(self):
        super().setUp()
        call_command("clear_index", interactive=False, stdout=open('/dev/null', 'w'))

    def tearDown(self):
        super().tearDown()
        call_command("clear_index", interactive=False, stdout=open('/dev/null', 'w'))


class RealtimeSearchTest(TestSearchBase):
    """
    Patches Haystack to use real-time signal processor.

    After the test restores old signal processor.
    """
    def setUp(self):
        super().setUp()
        haystack_config = django.apps.apps.get_app_config("haystack")
        self.__haystack_processor = haystack_config.signal_processor
        self.__haystack_processor.teardown()
        self.__processor = haystack.signals.RealtimeSignalProcessor(
            haystack.connections, haystack.connection_router
        )

    def tearDown(self):
        super().tearDown()
        self.__processor.teardown()
        haystack_config = django.apps.apps.get_app_config("haystack")
        haystack_processor_type = type(self.__haystack_processor)
        haystack_config.signal_processor = haystack_processor_type(
            haystack.connections, haystack.connection_router
        )


def verify_api_result_length_and_get_result_list(
        self: SimpleTestCase, count: int, result=typing.Any
) -> typing.Sequence[dict]:
    """
    Unpacks result list from DRF response, checking some precondintions.
    :param self: a TestCase instance.
    :param count: Expected number of items
    :param result: API json data.
    """

    self.assertIsInstance(result, dict)
    self.assertEqual(count, result['count'])
    results = result['results']
    self.assertIsInstance(results, list)
    self.assertEqual(count, len(results))
    return results
