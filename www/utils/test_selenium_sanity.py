# -*- coding: utf-8 -*-
"""Tests that we have working selenium integration. Smoke tests basically."""

from .selenium_utils import LiveServerBrowserTestCase


class TestSanityChrome(LiveServerBrowserTestCase):
    """
    Sanity test for Chrome.
    """

    WebDriver = "Chrome"
    PAGE_LOAD_TIMEOUT_SEC = 60

    def test_sanity(self):
        """
        Just check if we can go to a page.
        """
        self.driver.get("https://docs.python.org/3/")
        self.assertIn("Documentation", self.driver.title)
