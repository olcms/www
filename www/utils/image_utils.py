# coding=utf-8
"""Image utilities."""

from io import BytesIO

from PIL import Image

from django.db import models

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage


def save_pil_image(
        instance: models.Model,
        field: models.ImageField,
        image: Image,
        name: str,
        image_format: str
):
    """
    Save PIL image into Django ImageField.
    :param instance: Content item
    :param field: Image field that needs to be filled
    :param image: Image to be saved
    :param name: Name of image file
    :param image_format: Image format
    :return:
    """
    f = BytesIO()
    try:
        image.save(f, format=image_format)
        field.save(default_storage.get_available_name(name), ContentFile(f.getvalue()))
        instance.save()
    finally:
        f.close()


def image_resize_and_crop_to_aspect(image: Image, w, h):
    """
    Resize PIL image to desired width and height, crop edges to aspect ratio of w/h
    :param image: image to be resized
    :param w: desired width
    :param h: desired height
    :return: resized and cropped image
    """
    src_w = image.size[0]
    src_h = image.size[1]

    src_aspect = src_w / float(src_h)
    d_aspect = w / float(h)

    if src_aspect > d_aspect:
        # crop the left and right edges:
        new_width = int(d_aspect * src_h)
        offset = (src_w - new_width) / 2
        resize = (offset, 0, src_w - offset, src_h)
    else:
        # crop the top and bottom:
        new_height = int(src_w / d_aspect)
        offset = (src_h - new_height) / 2
        resize = (0, offset, src_w, src_h - offset)

    return image.crop(resize).resize((w, h), Image.ANTIALIAS)
