# coding=utf-8
"""
Bleach instance we use.
"""

from bleach import sanitizer

ALLOWED_TAGS = ["p"] + ["h{}".format(idx) for idx in range(1, 10)]
ALLOWED_TAGS.extend(sanitizer.ALLOWED_TAGS)

CLEANER = sanitizer.Cleaner(tags=ALLOWED_TAGS)
