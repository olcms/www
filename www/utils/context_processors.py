# coding=utf-8
"""
Misc context processors.
"""


from django.conf import settings


def use_local_assets(request):  # pylint: disable=unused-argument
    """
    Extracts USE_LOCAL_ASSETS template variable
    """
    return {
        "USE_LOCAL_ASSETS": settings.USE_LOCAL_ASSETS
    }


def add_discourse_url(request):  # pylint: disable=unused-argument
    """
    Extracts DISCOURSE_ROOT_URL template variable
    """
    return {
        "DISCOURSE_ROOT_URL": settings.DISCOURSE_ROOT_URL,
        "ENABLE_DISCOURSE_FORUM_TRIGGERS": settings.ENABLE_DISCOURSE_FORUM_TRIGGERS
    }


def add_google_analytics_code(request):  # pylint: disable=unused-argument
    """Add our site tracking code."""
    return {
        "GOOGLE_ANALYTICS_TRACKING_CODE": settings.GOOGLE_ANALYTICS_TRACKING_CODE
    }


def add_disciplines(request):  # pylint: disable=unused-argument
    """Adds all disciplines to the context."""
    from www.content_item import models
    return {
        "ALL_DISCIPLINES": models.Domain.objects.filter(type__slug="discipline").all()
    }
