# -*- coding: utf-8 -*-
"""Various test utilities.."""
import pathlib
import unittest

import typing
from allauth.account.models import EmailAddress
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.db import transaction
from django.db.models.base import Model
from django.db.models.fields import AutoField
from django.db.models.fields.files import ImageField
from django.db.models.fields.related import OneToOneField, ManyToManyField, ForeignKey
from django.db.models.options import Options
from django.test.testcases import TestCase

ROOT_DIR = pathlib.Path(__file__).parents[2]

SCREENSHOTS = ROOT_DIR / "screenshots"


if not SCREENSHOTS.exists():
    SCREENSHOTS.mkdir()


def auto_repr(obj):
    """Generates simple __repr__ for an object"""
    try:
        items = ("%s = %r" % (k, v) for k, v in sorted(obj.__dict__.items()))
        return "<%s: {%s}." % (obj.__class__.__name__, ', '.join(items))
    except AttributeError:
        return repr(obj)


FieldDict = typing.Mapping[str, typing.Any]


#                          It is an useful function, and 6 arguments is not that much.
def model_rich_compare(  # pylint: disable=too-many-arguments
        case: unittest.TestCase,
        model1,
        model2,
        exclude_pk: bool=False,
        include_not_editable: bool=False,
        ignored_attrs: typing.Sequence[str]=None
):
    """Compares two models attribute by attribute."""

    if ignored_attrs is None:
        ignored_attrs = {}

    def _filter_dict(
            model_meta: Options,
            model: Model
    ):
        field_map = {
            field.name: field
            for field in model_meta.fields
        }
        ignore_not_editable = not include_not_editable

        result = {}

        for name, field in field_map.items():
            if name in ignored_attrs:
                continue
            if ignore_not_editable and not field.editable:
                continue
            if exclude_pk and isinstance(field, AutoField):
                continue
            if isinstance(field, (ImageField, ManyToManyField)):
                # Skip Image field unconditionally as ImageField.None is not equal to itself
                # Skip ManyToMany as comparing it would be too much of a hassle.
                continue
            if isinstance(field, (OneToOneField, ForeignKey)):
                if getattr(field.remote_field, 'parent_link', True):
                    # Ignore parent link fields, we will compare all parent sizes
                    continue
                related = getattr(model, name)
                # For simple ForeignKeys just compare pks
                result[name] = related.pk if related is not None else None
                continue

            result[name] = getattr(model, name)

        return result

    case.assertEqual(type(model1), type(model2))
    case.assertEqual(
        _filter_dict(model1._meta, model1),
        _filter_dict(model2._meta, model2)
    )


class UserTestMixin(unittest.TestCase):
    """Test mixin that creates an user."""

    USER_USERNAME = "test"
    USER_PASSWORD = "foobarbaz123"
    USER_EMAIL = "test@example.org"

    USER_PERMS = []
    """
    A list of user perms to assign to user. Each permission is in format: 'filestorage.add_fileupload'
    """

    def setUp(self):
        super().setUp()
        with transaction.atomic():
            User = get_user_model()
            self.user = User.objects.create(
                username=self.USER_USERNAME,
                email="test@example.org",
                is_active=True
            )

            self.user.set_password(self.USER_PASSWORD)
            self.user.save()
            for perm in self.USER_PERMS:
                app, codename = perm.split(".")
                self.user.user_permissions.add(
                    Permission.objects.get(content_type__app_label=app, codename=codename)
                )

            EmailAddress.objects.create(
                user=self.user,
                email=self.USER_EMAIL,
                verified=True,
                primary=True
            )


class LoginTestMixin(UserTestMixin, TestCase):
    """Test mixin that logs in user to django client."""

    def login(self, username: str=None, password: str=None):
        """Logs in user using django client."""
        extra_nonexisting_user_msg = ""
        if username is None:
            username = self.USER_USERNAME
            extra_nonexisting_user_msg = " Have you overridden setUp method without invoking super().setUp()?"
        user_exists = get_user_model().objects.filter(username=username).exists()
        self.assertTrue(
            user_exists,
            "There is no user with username '{}'.{}".format(username, extra_nonexisting_user_msg)
        )
        if password is None:
            password = self.USER_PASSWORD
        self.assertTrue(
            self.client.login(username=username, password=password),
            "Couldn't login user, this is a bug."
        )
