# coding=utf-8
"""
Signal handlers useful in debugging tests.
"""
import code
import traceback
import signal


def __debug(sig, frame):  # pylint: disable=unused-argument
    """Interrupt running process, and provide a python prompt for
    interactive debugging."""
    d = {'_frame': frame}         # Allow access to frame object.
    d.update(frame.f_globals)  # Unless shadowed by global
    d.update(frame.f_locals)

    i = code.InteractiveConsole(d)
    message = "Signal received : entering python shell.\nTraceback:\n"
    message += ''.join(traceback.format_stack(frame))
    i.interact(message)


def __print_stack(sig, frame):  # pylint: disable=unused-argument
    with open("stack-trace.log", 'w') as f:
        traceback.print_stack(f=frame, file=f)


def connect_debug_handler():
    """
    Connects debug signal handlers.

    For USR1 we print stack to stack-trace.log
    For USR2 we start console in current thread
    """
    signal.signal(signal.SIGUSR1, __print_stack)  # Register handler
    signal.signal(signal.SIGUSR2, __debug)  # Register handler
