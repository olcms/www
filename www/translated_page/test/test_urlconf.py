# -*- coding: utf-8 -*-

"""
Urlconf for testing. Might be removed when we abandon Page module and
will have all cms served from this module.
"""

import debug_toolbar
from discourse_django_sso.views import SSOProviderView
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog

from www.landing_page.views import LandingPage
from www.translated_page.views import PageCatchallView

urlpatterns = [
    url('(?P<slug>.*)/', PageCatchallView.as_view()),
    url(r'^$', RedirectView.as_view(pattern_name='landing-discipline'), name='home'),
    url(r'^discipline/?$', LandingPage.as_view()),
    url(r'^discipline(?:/(?P<discipline>[\w+.\-_]+))?/?$', LandingPage.as_view(), name='landing-discipline'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='javascript-catalog'),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    # User management
    url(r'^users/', include('www.users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^resource/', include('www.resource.urls', namespace='resource')),
    url(r'^content_item/', include('www.content_item.urls', namespace='content_item')),
    url(r'^course/', include('www.course.urls', namespace='course')),
    url(
        r'^discourse/sso/?',
        SSOProviderView.as_view(
            sso_redirect=settings.DISCOURSE_SSO_URL,
            sso_secret=settings.DISCOURSE_SSO_SECRET
        ),
        name='sso'
    )
]


urlpatterns = [
    url(r'^__debug__/', include(debug_toolbar.urls)),
] + urlpatterns
