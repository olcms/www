# -*- coding: utf-8 -*-
"""Test factories."""
from factory import DjangoModelFactory, SubFactory

from .. import models


class PageFactory(DjangoModelFactory):
    """Factory for translatable page"""

    slug = "page_slug"

    class Meta:
        """Meta (unsurprisingly)"""
        model = models.TranslateablePage


class PageTranslation(DjangoModelFactory):
    """Factory for translatable page"""

    title = "Page title"
    parent = SubFactory(PageFactory)
    language_code = "en"
    default = False

    class Meta:
        """Meta (unsurprisingly)"""
        model = models.PageTranslation
