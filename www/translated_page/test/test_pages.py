# -*- coding: utf-8 -*-
"""Test module."""

from django.test import TestCase, override_settings

from .factories import PageFactory, PageTranslation


# pylint: disable=no-member


@override_settings(ROOT_URLCONF="www.translated_page.test.test_urlconf")
class TestTranslateblePages(TestCase):
    """Test case."""

    def test_does_not_common_middleware(self):
        """
        This is a regression test, catch-all view may break CommonMiddleware
        that redirects from "
        :return:
        """
        response = self.client.get("/does-not-exist", follow=False)
        self.assertEqual(response.status_code, 301)

    def test_page_not_exists(self):
        """Test not existent slug returns 404."""
        response = self.client.get("/does-not-exist", follow=True)
        self.assertEqual(response.status_code, 404)

    def test_page_without_translations(self):
        """Test page without translations returns 404."""
        PageFactory(slug="test")
        response = self.client.get("/test", follow=True)
        self.assertEqual(response.status_code, 404)

    def test_page_with_translation(self):
        """Test page with single translation returns it."""
        page_factory = PageFactory(slug="test")
        PageTranslation(
            parent=page_factory,
            language_code="pl",
            title="AAAABBBB"
        )
        response = self.client.get("/test", follow=True)
        self.assertIn("AAAABBBB", response.rendered_content)

    def test_page_with_two_translations_default(self):
        """
        Test page with single two translations returns default when user asks
        not translated language.
        """
        page_factory = PageFactory(slug="test")
        PageTranslation(
            parent=page_factory,
            language_code="pl",
            title="AAAABBBB",
            default=True
        )
        PageTranslation(
            parent=page_factory,
            language_code="en",
            title="CCCCDDDD",
            default=False
        )
        response = self.client.get("/test", follow=True, HTTP_ACCEPT_LANGUAGE='el')
        self.assertIn("AAAABBBB", response.rendered_content)

    def test_page_with_two_translations_no_default(self):
        """Test some translation is returned when there are no default."""
        page_factory = PageFactory(slug="test")
        PageTranslation(
            parent=page_factory,
            language_code="pl",
            title="AAAABBBB",
            default=False
        )
        PageTranslation(
            parent=page_factory,
            language_code="en",
            title="CCCCDDDD",
            default=False
        )
        response = self.client.get("/test", follow=True, HTTP_ACCEPT_LANGUAGE='el')
        self.assertEqual(response.status_code, 200)

    def test_page_with_two_translations_user_selected(self):
        """Test that user can select language different than default."""
        page_factory = PageFactory(slug="test")
        PageTranslation(
            parent=page_factory,
            language_code="pl",
            title="AAAABBBB",
            default=True
        )
        PageTranslation(
            parent=page_factory,
            language_code="el",
            title="CCCCDDDD",
            default=False
        )
        response = self.client.get("/test", follow=True, HTTP_ACCEPT_LANGUAGE='el')
        self.assertIn("CCCCDDDD", response.rendered_content)

    def test_delete_translation(self):
        """Tests that deletion doesn't explode."""
        page_factory = PageFactory(slug="test")
        to_be_deleted = PageTranslation(
            parent=page_factory,
            language_code="pl",
            title="AAAABBBB",
            default=True
        )
        to_be_deleted.delete()
        PageTranslation(
            parent=page_factory,
            language_code="el",
            title="CCCCDDDD",
            default=False
        )
        response = self.client.get("/test", follow=True, HTTP_ACCEPT_LANGUAGE='el')
        self.assertIn("CCCCDDDD", response.rendered_content)

    def test_clean_slug(self):
        """Test clean method"""
        for slug in ("test", "/test", "test/", "/test/"):
            factory = PageFactory(slug=slug)
            factory.clean()
            self.assertEqual(factory.slug, "test")

    def test_page_str(self):
        """Test __str__ method"""
        self.assertEqual(str(PageFactory(slug="page-slug")), "page-slug")

    def test_translation_str(self):
        """Test __str__ method"""
        self.assertEqual(
            str(PageTranslation(parent__slug="page-slug", language_code="en", title="title")),
            "page-slug en title"
        )
