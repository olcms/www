# -*- coding: utf-8 -*-
"""Page translation app"""

from django.apps import AppConfig


class TranslatedPageConfig(AppConfig):
    """Page translation application."""
    name = 'www.translated_page'
