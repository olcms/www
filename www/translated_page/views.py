# -*- coding: utf-8 -*-
"""Views."""
from collections import OrderedDict
import typing

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.http import Http404
# Create your views here.
from django.shortcuts import redirect
from django.utils.translation import get_language_from_request
from django.views.generic import DetailView

from www.translated_page.models import TranslateablePage, PageTranslation


class PageCatchallView(DetailView):
    """View that renders a page given it's slug. If no such page returns 404."""

    queryset = TranslateablePage.objects
    default_language = "en"
    kwargs_slug = "slug"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None
        self.page = None

    def get_slug(self):
        """Return slug from kwargs."""
        return self.kwargs[self.kwargs_slug].strip("/")

    def get_template_names(self):
        return [self.object.template_key]

    def get_correct_translation(self, translations: typing.Sequence[PageTranslation]):
        """
        Select correct translations.

        :param translations: All translations for a given page.
        """
        assert len(translations) > 0

        # Query dataset single time
        translation_dict = OrderedDict(
            (trans.language_code, trans) for trans in translations
        )
        default_translation = [trans for trans in translation_dict.values() if trans.default]

        user_language = get_language_from_request(self.request)

        # If we have user language return this
        if translation_dict.get(user_language):
            return translation_dict[user_language]

        # If we have a translation marked as default one return it
        if default_translation:
            return default_translation[0]

        # Then return default language (English)
        if translation_dict.get(self.default_language):
            return translation_dict[self.default_language]

        # Return first language we have in dataset.
        return next(iter(translation_dict.values()))

    def get_object(self, queryset=None):
        if self.object is not None:
            return self.object  # pragma: no cover
        if queryset is None:
            queryset = self.get_queryset()
        slug = self.get_slug()
        try:
            self.page = queryset.get(slug=slug)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            raise Http404(slug)
        translations = self.page.translations.all()
        if not translations:
            raise Http404(slug)
        self.object = self.get_correct_translation(translations)
        return self.object

    def get(self, request, *args, **kwargs):
        try:
            return super().get(request, *args, **kwargs)
        except Http404:
            if not request.path.endswith("/"):
                return redirect(request.path + "/")
            raise
