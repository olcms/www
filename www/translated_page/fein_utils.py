# -*- coding: utf-8 -*-
"""
This contains utilities copied from Fein cms with changes I needed.
"""
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


def Translation(model, base_class=models.Model):
    """
    Return a class which can be used as inheritance base for translation models
    """

    class Inner(base_class):
        """Parent class for generated model."""
        parent = models.ForeignKey(
            model, related_name='translations', on_delete=models.CASCADE)
        language_code = models.CharField(
            _('language'), max_length=10,
            choices=settings.LANGUAGES, default=settings.LANGUAGES[0][0],
            editable=len(settings.LANGUAGES) > 1)

        class Meta:
            unique_together = ('parent', 'language_code')
            # (beware the above will not be inherited automatically if you
            #  provide a Meta class within your translation subclass)
            abstract = True

        def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
            super(Inner, self).save(*args, **kwargs)
            self.parent.purge_translation_cache()

        save.alters_data = True

        def delete(self, *args, **kwargs):  # pylint: disable=arguments-differ
            super(Inner, self).delete(*args, **kwargs)
            self.parent.purge_translation_cache()

        delete.alters_data = True

    return Inner
