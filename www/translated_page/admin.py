# -*- coding: utf-8 -*-
"""Admin module."""
from django.contrib import admin
from feincms.admin.item_editor import ItemEditor

from . import models


@admin.register(models.TranslateablePage)
class PageAdmin(admin.ModelAdmin):
    """Page admin."""


@admin.register(models.PageTranslation)
class PageTranslationAdmin(ItemEditor):
    """Page translation admin"""

    list_display = ['title', 'language_code', 'parent', 'default']

    list_filter = [
        'language_code', 'parent'
    ]
