# -*- coding: utf-8 -*-
"""Models for CMS app."""

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy
from feincms.content.richtext.models import RichTextContent
from feincms.models import create_base_model
from feincms.module.medialibrary.contents import MediaFileContent
from feincms.translations import TranslatedObjectMixin

from .fein_utils import Translation


class TranslateablePage(models.Model, TranslatedObjectMixin):
    """
    CMS page that can be translated. Translations are read from HTTP headers.
    """

    slug = models.CharField(max_length=128, unique=True, blank=False)

    def __str__(self):
        return self.slug

    def clean(self):
        """Clean this instance. Strip slashes from slug."""
        if self.slug.startswith("/"):
            self.slug = self.slug[1:]
        if self.slug.endswith("/"):
            self.slug = self.slug[0:-1]

    class Meta:
        """Meta (unsurprisingly)"""
        ordering = ['slug']


class PageTranslation(
        Translation(TranslateablePage, create_base_model(models.Model))
):

    """
    Translation of a page.
    """

    title = models.CharField(max_length=256)
    default = models.BooleanField()

    def __str__(self):
        return "{} {} {}".format(self.parent.slug, self.language_code, self.title)

    class Meta:
        """Meta (unsurprisingly)"""
        ordering = ['parent__slug', "title"]
        unique_together = (('parent', 'language_code'),)


@receiver(pre_save, sender=PageTranslation)
def on_page_translation_save(instance: PageTranslation, **kwargs):  # pylint: disable=unused-argument
    """Update default."""
    if instance.default:
        PageTranslation.objects\
            .filter(parent=instance.parent)\
            .exclude(pk=instance.pk)\
            .update(default=False)


# pylint: disable=no-member

PageTranslation.register_templates({
    'title': ugettext_lazy('Standard template'),
    'path': 'feincms-translated.html',
    'regions': (
        ('main', ugettext_lazy('Main content area')),
    ),
})

PageTranslation.create_content_type(RichTextContent)

PageTranslation.create_content_type(
    MediaFileContent,
    TYPE_CHOICES=(
        ('default', ugettext_lazy('default')),
        ('lightbox', ugettext_lazy('lightbox')),
    )
)
