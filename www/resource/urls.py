"""Urls for resource app."""
# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^(?P<pk>\d+)/$',
        view=views.ResourceDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^link/create/(?P<content_item_id>\d+)/?$',
        view=views.LinkResourceCreateView.as_view(),
        name='link-create'
    ),
    url(
        regex=r'^file/create/(?P<content_item_id>\d+)/?$',
        view=views.FileResourceCreateView.as_view(),
        name='file-create'
    ),
    url(
        regex=r'^document/create/(?P<content_item_id>\d+)/?$',
        view=views.DocumentResourceCreateView.as_view(),
        name='document-create'
    ),
    url(
        regex=r'^document/edit/(?P<pk>\d+)/?$',
        view=views.DocumentResourceUpdateView.as_view(),
        name='document-edit-metadata'
    ),
    url(
        regex=r'^file/upload/(?P<pk>\d+)/?$',
        view=views.FileResourceUploadView.as_view(),
        name='file-upload'
    ),
    url(
        regex=r'^link/edit/(?P<pk>\d+)/?$',
        view=views.LinkResourceUpdateView.as_view(),
        name='link-edit'
    ),

    url(
        regex=r'^file/edit/(?P<pk>\d+)/?$',
        view=views.FileResourceUpdateView.as_view(),
        name='file-edit'
    ),
    url(
        regex=r'^link/delete/(?P<pk>\d+)/?$',
        view=views.ResourceDeleteView.as_view(),
        name='delete'
    ),
    url(
        regex=r'^document/upload/(?P<pk>\d+)/?$',
        view=views.DocumentResourceEditMarkdownView.as_view(),
        name='document-upload'
    ),
    url(
        regex=r'^video/create/(?P<content_item_id>\d+)/?$',
        view=views.VideoResourceCreateView.as_view(),
        name='video-create'
    ),
    url(
        regex=r'^video/upload/(?P<pk>\d+)/?$',
        view=views.VideoResourceUpdateUrl.as_view(),
        name='video-upload'
    ),
    url(
        regex=r'^video/edit/(?P<pk>\d+)/?$',
        view=views.VideoResourceUpdateView.as_view(),
        name='video-edit'
    ),
    url(
        regex=r'^subtitle/create/(?P<content_item_id>\d+)/?$',
        view=views.SubtitleResourceCreateView.as_view(),
        name='subtitle-create'
    ),
    url(
        regex=r'^subtitle/upload/(?P<pk>\d+)/?$',
        view=views.SubtitleResourceUpdateUrl.as_view(),
        name='subtitle-upload'
    ),
    url(
        regex=r'^subtitle/edit/(?P<pk>\d+)/?$',
        view=views.SubtitleResourceUpdateView.as_view(),
        name='subtitle-edit'
    ),
]
