# -*- coding: utf-8 -*-
"""App with resource and ContentItems."""

from django.apps import AppConfig


class ResourceConfig(AppConfig):
    """resource app config."""

    name = 'www.resource'
