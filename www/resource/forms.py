# -*- coding: utf-8 -*-
""" Forms for resources """

from django import forms
from django.forms import ModelForm, TextInput, Textarea

from . import models


class ResourceForm(ModelForm):
    """
    Form that creates link resource.

    During edition you can't change type.
    """
    class Meta:
        """Meta Model."""
        model = models.Resource
        fields = [
            'title', 'description', 'resource_license', 'role', 'default',
        ]


class LinkResourceForm(ModelForm):
    """Form that creates link resource."""

    class Meta:
        """Meta Model."""
        model = models.LinkResource
        fields = [
            'title',
            'url',
            'resource_license',
            'role',
            'description',
            'default',
        ]
        widgets = {
            'url': TextInput,
            'description': Textarea(attrs={'rows': 5}),
        }


class FileResourceForm(ModelForm):
    """Form that creates file resource."""

    class Meta:
        """Meta Model."""
        model = models.FileResource
        fields = [
            'title',
            'resource_license',
            'role',
            'description',
            'default',
        ]
        widgets = {
            'description': Textarea(attrs={'rows': 5}),
        }


class FileResourceUploadForm(ModelForm):
    """Form for sending path to resource."""

    class Meta:
        """Metaclass"""
        model = models.FileResource
        fields = ['path']
        widgets = {"path": forms.HiddenInput}


class DocumentResourceEditMetadata(ModelForm):
    """Form that creates document resource."""

    class Meta:
        """Metaclass"""
        model = models.DocumentResource

        fields = [
            'title',
            'resource_license',
            'role',
            'description',
            'default',
        ]
        widgets = {
            'description': Textarea(attrs={'rows': 5}),
        }


class DocumentResourceUpdateDocument(ModelForm):
    """Form that updates markdown for document resource."""

    class Meta:
        """Metaclass"""
        model = models.DocumentResource

        fields = [
            'markdown'
        ]


class DeletionForm(ModelForm):
    """
    Form that asks deletion confirmation
    """
    class Meta:
        """Meta Model."""
        model = models.Resource
        fields = []


class VideoResourceForm(ModelForm):
    """Form that creates link resource."""

    class Meta:
        """Meta Model."""
        model = models.VideoResource
        fields = [
            'title',
            'resource_license',
            'role',
            'description',
            'default',
        ]
        widgets = {
            'description': Textarea(attrs={'rows': 5}),
        }


class VideoResourceUpdateUrl(ModelForm):
    """Form for sending path to resource."""

    class Meta:
        """Metaclass"""
        model = models.VideoResource
        fields = [
            'path'
        ]
        widgets = {
            "path": forms.HiddenInput
        }


class SubtitleResourceForm(ModelForm):
    """Form that creates link resource."""

    class Meta:
        """Meta Model."""
        model = models.SubtitleResource
        fields = [
            'title',
            'language',
            'resource_license',
        ]


class SubtitleResourceUpdateUrl(ModelForm):
    """Form for sending path to resource."""

    class Meta:
        """Metaclass"""
        model = models.SubtitleResource
        fields = [
            'path'
        ]
        widgets = {
            "path": forms.HiddenInput
        }
