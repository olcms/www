
Please provide document contents in Markdown syntax, see examples below.

# Intro

To make headers place # at the beginning of the line. 
"#" Will not be visible in the final document. In the above line ``# Intro``
will be a section header.

You can add header by clicking ``H`` symbol on the toolbar.  

Go ahead, play around with the editor! Be sure to check out **bold**
and *italic* styling, or even [links](https://google.com). You can type the Markdown syntax,
use the toolbar, or use shortcuts like `cmd-b` or `ctrl-b`.

## Lists
Unordered lists can be started using the toolbar or by typing `* `, `- `, or `+ `.
Ordered lists can be started by typing `1. `.

#### Unordered

Example of list (please note that in rendered document 
``*`` will be replaced by appropriate symbol) 

* Lists are a piece of cake
* They even auto continue as you type
* A double enter will end them
* Tabs and shift-tabs work too

#### Ordered
1. Numbered lists...
2. ...work too!

## What about images?

![Yes](https://i.imgur.com/sZlktY7.png)
