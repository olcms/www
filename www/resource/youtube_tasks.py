# -*- coding: utf-8 -*-
"""
Tasks concerned with youtube upload, since testing it is non trivial
coverage checking is disabled for this file.
"""

import argparse
import os
import shutil
import hashlib

# This import works.
from apiclient.errors import HttpError  # pylint: disable= import-error
from celery import shared_task, Task
from celery.utils.log import get_task_logger

from django.core.mail import mail_admins
from django.conf import settings

import requests

from . import models, upload_video


logger = get_task_logger(__name__)


# This catches exceptions and logs it for admin to take action
# pylint: disable=broad-except

# Makes sense here
# pylint: disable=no-self-use


# This is an abstract class
class YoutubeUploadTask(Task):  # pylint: disable=abstract-method
    """
    Task that uploads video to youtube.
    """
    abstract = True
    max_retries = 5
    youtube = None
    playlist_id = None

    def __init__(self):
        super().__init__()
        self.youtube = upload_video.get_authenticated_service(
            self._prepare_yt_service_arguments(),
            '/videotmp/conf/client_secret.json',
            '/videotmp/conf/upload_oauth2.json'
        )
        self.playlist_id = settings.YOUTUBE['playlist_id']

    def upload_to_youtube(self, object_id: int):
        """Perform the actual upload."""
        video_res = models.VideoResource.objects.get(pk=object_id)
        # download file to local storage
        logger.debug('Downloading video file: %s', video_res.download_url)

        local_filename = self._get_local_file_path(video_res)
        r = requests.get(video_res.download_url, stream=True)
        with open(local_filename, 'wb') as f:
            # This streams the file to disk without using excessive memory, and the code is simple.
            # based on http://stackoverflow.com/q/16694907/7918
            shutil.copyfileobj(r.raw, f)
            logger.debug('Download file: %s size: %s', f.name, os.stat(local_filename).st_size)

        try:
            upload_video.initialize_upload(
                self.youtube,
                self._prepare_video_data(video_res, local_filename),
                self._video_upload_callback, video_res.pk
            )
            video_res = models.VideoResource.objects.get(pk=object_id)
            if video_res.youtube_id is None:
                raise ValueError('Failed to upload video to youtube, throw exception to retry')
        except HttpError as e:
            logger.exception("An HTTP error %d occurred:\n%s", e.resp.status, e.content)
            raise e

    @classmethod
    def _get_local_file_path(cls, video_res: models.VideoResource):
        """
        Generates file path for given video resource.
        :return: File path for the resource
        """
        file_name = video_res.download_url.split('/')[-1]
        ext = file_name.split(".")[-1]
        new_file = hashlib.sha256(file_name.encode("UTF-8")).hexdigest()
        return '/videotmp/' + ".".join((new_file, ext))

    @classmethod
    def _prepare_yt_service_arguments(cls):
        namespace = argparse.Namespace()
        # Hostname when running a local web server.
        setattr(namespace, 'auth_host_name', 'localhost')
        # Do not run a local web server.
        setattr(namespace, 'noauth_local_webserver', True)
        # Port web server should listen on.
        setattr(namespace, 'auth_host_port', [8080, 8090])
        # Set the logging level of detail. choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
        setattr(namespace, 'logging_level', 'ERROR')

        return namespace

    @classmethod
    def _prepare_video_data(cls, file_resource: models.FileResource, file_path):
        content_item = file_resource.content_item
        namespace = argparse.Namespace()
        setattr(namespace, 'file', file_path)
        setattr(namespace, 'title', file_resource.title)
        setattr(namespace, 'description', file_resource.description)
        setattr(namespace, 'category', None)
        setattr(namespace, 'keywords', ",".join([tag.name for tag in content_item.tags.all()]))
        setattr(namespace, 'privacyStatus', 'public')
        setattr(namespace, 'defaultLanguage', content_item.language)     # language of title and description
        setattr(namespace, 'defaultAudioLanguage', content_item.language)

        return namespace

    def _video_upload_callback(self, response: dict, object_id: int):
        video_res = models.VideoResource.objects.get(pk=object_id)
        try:
            if 'id' in response:
                logger.debug("Video id '%s' was successfully uploaded.", response['id'])
                video_res.youtube_id = response['id']
                video_res.save()
                self._add_to_playlist(video_res.youtube_id)
            else:
                logger.error("The upload failed for video: %s", video_res.pk)
                logger.error("The upload failed with an unexpected response: %s", response)
        except Exception:  # pylint: disable=broad-except
            # catch exceptions here so that they will not re-trigger another attempt of upload. Since
            # that already happened and what is failing is only callback
            logger.exception("Video upload failed due to unexpected error.")
        finally:
            file_path = self._get_local_file_path(video_res)
            if os.path.isfile(file_path):
                os.remove(file_path)

    def _add_to_playlist(self, video_id):
        """
        Add given video to the play-list. Playlist id is taken from environment variables.
        :param video_id: Video to be added
        :return:
        """
        if self.playlist_id is None:
            logger.error('Can not add video %s to the playlist. Missing ID of playlist', video_id)
            return
        body = {
            "snippet": {
                "playlistId": self.playlist_id,
                "resourceId": {
                    "videoId": video_id,
                    "kind": 'youtube#video'
                }
            }
        }
        self.youtube.playlistItems().insert(body=body, **{'part': 'snippet'}).execute()

    def delete_video(self, video_id):
        """Deletes the video."""
        self.youtube.videos().delete(**{'id': video_id}).execute()

    def on_failure(self, exc, task_id, args, kwargs, einfo):  # pylint: disable=too-many-arguments
        """Callback called on task failure."""
        mail_admins(
            "upload to youtube failed",
            "Operation for video for resource {} has failed. Operation is "
            "{}".format(args[0], type(self).__name__),
            fail_silently=True
        )
        super().on_failure(exc, task_id, args, kwargs, einfo)

    def update_video(self, object_id):
        """
        Update metadata of video based on the information from video resource object.
        :param object_id: PK of object to be updated
        :return:
        """
        video_res = models.VideoResource.objects.get(pk=object_id)
        if video_res.youtube_id is None:
            raise ValueError('Failed to update video - missing youtube id. Video was not uploaded in the first place')

        # Call the API videos.list method to retrieve the video resource.
        videos_list_response = self.youtube.videos().list(
            id=video_res.youtube_id,
            part='snippet'
        ).execute()

        # If the response does not contain an array of "items" then the video was not found.
        if not videos_list_response["items"]:
            raise ValueError("Video '%s' was not found for resource %s." % video_res.youtube_id, video_res.pk)

        # Since the request specified a video ID, the response only contains one
        # video resource. This code extracts the snippet from that resource.
        videos_list_snippet = videos_list_response["items"][0]["snippet"]
        content_item = video_res.content_item

        # API described here: https://developers.google.com/youtube/v3/docs/videos#resource
        videos_list_snippet["tags"] = [tag.name for tag in content_item.tags.all()]
        videos_list_snippet["title"] = video_res.title
        videos_list_snippet["description"] = video_res.description
        videos_list_snippet["defaultLanguage"] = content_item.language     # language of title and description
        videos_list_snippet["defaultAudioLanguage"] = content_item.language

        # Update the video resource by calling the videos.update() method.
        videos_update_response = self.youtube.videos().update(
            part='snippet',
            body={
                "snippet": videos_list_snippet,
                "id": video_res.youtube_id
            }
        ).execute()
        logger.debug("Video was updated: %s", videos_update_response["id"])

    def update_subtitle(self, object_id):
        """Uploads a subtitle to YouTube. object_id is pk of SubtitleResource."""
        sub_res = models.SubtitleResource.objects.get(pk=object_id)
        if sub_res.subtitle_id is None:
            raise ValueError('Failed to update subtitle - missing id. Subtitle was not uploaded in the first place')

        # YouTube API does not allow to update title and language of subtitle,
        # so we delete and upload once more with new data
        try:
            self.delete_subtitle(sub_res.subtitle_id)
        except Exception:
            logger.exception("Delete of subtitle %s failed", sub_res.subtitle_id)
        try:
            self.upload_subtitle(object_id)
        except Exception:
            logger.exception("Upload of new subtitle version failed. pk = %s", object_id)

    def upload_subtitle(self, object_id: int):
        """Uploads a subtitle to YouTube. object_id is pk of SubtitleResource."""
        sub_res = models.SubtitleResource.objects.get(pk=object_id)
        video_res = self._get_video_for_subtitle(sub_res)
        if not video_res:
            logger.warning('There is no video for subtitle: %s', object_id)
            return
        # download file to local storage
        logger.debug('Downloading subtitle file: %s', sub_res.download_url)

        local_filename = self._get_local_file_path(sub_res)
        r = requests.get(sub_res.download_url, stream=True)
        with open(local_filename, 'wb') as f:
            # This streams the file to disk without using excessive memory, and the code is simple.
            # based on http://stackoverflow.com/q/16694907/7918
            shutil.copyfileobj(r.raw, f)
            logger.debug('Download file: %s size: %s', f.name, os.stat(local_filename).st_size)

        try:
            yt_sub_id = upload_video.upload_caption(self.youtube, video_res.videoresource.youtube_id, sub_res.language,
                                                    sub_res.title, local_filename)
            sub_res.subtitle_id = yt_sub_id
            sub_res.save()
        except HttpError as e:
            logger.exception("An HTTP error %d occurred:\n%s", e.resp.status, e.content)
            raise e

    def _get_video_for_subtitle(self, sub_res):
        return models.VideoResource.objects.get(content_item=sub_res.content_item)

    def delete_subtitle(self, subtitle_id):
        """Deletes a subtitle. subtitle id is primary_key of subtitle."""
        self.youtube.captions().delete(id=subtitle_id).execute()


if settings.ENABLE_YOUTUBE_VIDEOS:

    @shared_task(
        bind=True,
        name='resource.video.yt-upload',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_youtube_upload(self, resource_id):
        """Actual task to upload to youtube."""
        try:
            self.upload_to_youtube(resource_id)
        except Exception as exc:
            logger.exception('There was exception while uploading video %s to youtube', resource_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)

    @shared_task(
        bind=True,
        name='resource.video.yt-edit',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_youtube_edit(self, resource_id):
        """Actual task to edit metadata information about video in youtube."""
        try:
            self.update_video(resource_id)
        except Exception as exc:
            logger.exception('There was exception while updating video metadata for resource: %s', resource_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)

    @shared_task(
        bind=True,
        name='resource.video.yt-delete',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_youtube_delete(self, resource_id):
        """Actual task to delete video from youtube."""
        try:
            self.delete_video(resource_id)
        except Exception as exc:
            logger.exception('There was exception while deleting video from YT, videoId: %s', resource_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)

    @shared_task(
        bind=True,
        name='resource.subtitle.yt-upload',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_subtitle_upload(self, resource_id):
        """Actual task to upload to youtube."""
        try:
            self.upload_subtitle(resource_id)
        except Exception as exc:
            logger.exception('There was exception while uploading subtitle %s to youtube', resource_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)

    @shared_task(
        bind=True,
        name='resource.subtitle.yt-edit',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_subtitle_edit(self, resource_id):
        """Actual task to edit metadata information about video in youtube."""
        try:
            self.update_subtitle(resource_id)
        except Exception as exc:
            logger.exception('There was exception while updating subtitle metadata for resource: %s', resource_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)

    @shared_task(
        bind=True,
        name='resource.subtitle.yt-delete',
        base=YoutubeUploadTask,
        throws=(Exception,),
        ignore_result=True
    )
    def start_subtitle_delete(self, subtitle_id):
        """Actual task to delete video from youtube."""
        try:
            self.delete_subtitle(subtitle_id)
        except Exception as exc:
            logger.exception('There was exception while deleting video from YT, videoId: %s', subtitle_id)
            # retry after 10 min, max number of retries is defined in YoutubeUploadTask
            raise self.retry(exc=exc, countdown=60 * 10)
