# coding=utf-8

from django.core.management.base import BaseCommand

from www.resource import youtube_tasks, models


class Command(BaseCommand):
    """Upload failed video to YT."""

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '--pk', help="Pk of resource", type=int, required=True
        )

    def execute(self, *args, **options):
        pk = options['pk']
        video = models.VideoResource.objects.get(pk=pk)
        if video.youtube_id:
            raise ValueError("Sorry this video appears to be already uploaded. And we don't support re-upload.")

        youtube_tasks.start_youtube_upload.delay(pk)
