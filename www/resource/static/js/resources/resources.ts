/**
 * This is a one-off class that mostly handles UI of the file upload form. All file upload logic is stored in
 * SwiftUploadManager and FileUploadManager.
 *
 * For upload details see: docs/modules/fileupload.rst.
 */
class FileResourceManager{

  private static GB: number = 1024 * 1024 * 1024;
  private static MAX_FILE_SIZE = 3 * FileResourceManager.GB;

  private static MODAL_SELECTOR = "#generic-modal";

  messages: Messages;
  swiftUploadManager: SwiftUploadManager;

  shouldRestartUpload: boolean = false;

  constructor(resourceUpdateUrl: string, swiftNamespace: string){
    this.messages = new Messages("#upload-messages", false);
    this.swiftUploadManager = new SwiftUploadManager(resourceUpdateUrl, swiftNamespace);

    // Disable form submit button
    MiscUtils.assertSingleElementMatched($('#generic-modal').find('.btn-primary')).prop('disabled', true);

    this.swiftUploadManager.addUploadStateCallback((isOk: boolean, error: string) => {
      if(!isOk){
        this.shouldRestartUpload = true;
        this.messages.addMessage(error, "danger");
        this.toggle_button_state(false);
        this.$upload_file_input().prop('disabled', false);
      } else {
        $(FileResourceManager.MODAL_SELECTOR).trigger(BaseModalForm.SUCCESS_EVENT, [{}]);
      }
    });

    this.swiftUploadManager.addProgressListener((bytes: number, percentage: number) => {
      let progressBar = this.$upload_progressbar();

      if (percentage >= 99){
        progressBar.css('width', `100%`);
        this.messages.addMessage(gettext("Finishing upload. Please wait."), "success");
      } else {
        progressBar.css('width', `${percentage}%`);
      }
    });

    this.$upload_file_input().on('change', () => {
      this.onFileSelected();
    });

    this.$upload_button().on('click', (e: any) => {
      e.preventDefault();
      this.onUploadClicked();
      return false;
    });
  }

  $upload_button(): any{
    return MiscUtils.assertSingleElementMatched($("#upload-button"));
  }

  $upload_progressbar(): any{
    return MiscUtils.assertSingleElementMatched($("#upload-progress").find('.progress-bar'));
  }

  $upload_file_input(): any{
    return MiscUtils.assertSingleElementMatched($("#id_file"));
  }

  set_icon(classes: string){
    MiscUtils.assertSingleElementMatched(this.$upload_button().find('i')).removeClass().addClass(
      classes
    )
  }

  toggle_button_state(disabled: boolean){
    $("#upload-button").prop('disabled', disabled);
    if(!disabled){
        this.set_icon("fa fa-cloud-upload");
    }
  }

  get_uploaded_file(){
    let files: File[] = this.$upload_file_input().get(0).files;
    if (files.length != 1){
      throw "Invalid file count";
    }
    return files[0];
  }

  private onFileSelected(){
    if(this.$upload_file_input().val()){

      let file = this.get_uploaded_file();

      if (file.size > FileResourceManager.MAX_FILE_SIZE){
        this.messages.addMessage(
          "File is too big, if you need to upload file larger than 3GB please " +
          "contact OLCMS administrators.",
          "danger"
        );
        return;
      }

      this.toggle_button_state(false);
      this.swiftUploadManager.setFile(file);
      // this.swiftUploadManager.start();
    }
  }

  private onUploadClicked(){
    this.set_icon("fa fa-spin fa-spinner");
    this.$upload_file_input().prop('disabled', true);
    if (this.shouldRestartUpload){
      this.swiftUploadManager.retry();
    } else {
      this.swiftUploadManager.start();
    }
  }

}

/**
 * States for SwiftUploadManager. For logical details see docs/modules/fileupload.rst.
 */
enum SwiftUploadManagerState{
  /**
   * Initial state. Since file is not selected nothing can be done here.
   */
  FILE_NOT_SELECTED,
  /**
   * File is selected
   */
  FILE_SELECTED,
  /**
   * We are loading temporary swift url to upload to swift.
   */
  GETTING_UPLOAD_URL,
  /**
   * Uploading file.
   */
  UPLOADING,
  /**
   * Posting back swift upload url.
   */
  POSTING_BACK_PATH,
  /**
   * Done
   */
  DONE
}


type UploadStateCallback = (isOk: boolean, errorMsg: string) => any;


/**
 * Manages the upload.
 *
 * This is a state machine with states defined SwiftUploadManagerState.
 *
 * Following methods mutate state:
 *
 * * setFile
 * * start --- starts the upload
 * * retry --- retries last step
 *
 * You can install two callbacks:
 *
 * * addUploadStateCallback --- callbacks that is called when upload is finished (either successfully or not!)
 * * addProgressListener --- notifies user of upload progress, however only progress for actual file upload is measured
 */
class SwiftUploadManager{

  resourceUpdateUrl: string;
  swiftNamespace: string;

  currentFile: File;
  swiftUploadResponse: FileUploadResponse;

  state: SwiftUploadManagerState = SwiftUploadManagerState.FILE_NOT_SELECTED;
  uploadStateCallback: UploadStateCallback[] = [];
  uploadProgressListener: ProgressListener[] = [];

  constructor(resourceUpdateUrl: string, swiftNamespace: string){
    this.resourceUpdateUrl = resourceUpdateUrl;
    this.swiftNamespace = swiftNamespace;
  }

  public addUploadStateCallback(l: UploadStateCallback){this.uploadStateCallback.push(l)}
  public addProgressListener(l: ProgressListener){this.uploadProgressListener.push(l)}


  public setFile(file: File){
    this.currentFile = file;
    this.state = SwiftUploadManagerState.FILE_SELECTED;
  }

  public start(){
    this.moveToNextState();
  }

  public retry() {
    switch (this.state) {
      case SwiftUploadManagerState.FILE_NOT_SELECTED:
      case SwiftUploadManagerState.FILE_SELECTED:
      case SwiftUploadManagerState.DONE:
        throw "Can't retry this state";
      case SwiftUploadManagerState.GETTING_UPLOAD_URL:
        this.getSwiftUploadUrl();
        break;
      case SwiftUploadManagerState.UPLOADING:
        this.doUpload();
        break;
      case SwiftUploadManagerState.POSTING_BACK_PATH:
        this.postBackPath();
        break;
    }
  }

  private onUploadFailed(message: string){
    this.fireUploadStateCallbacks(false, message);
  }

  private getSwiftUploadUrl(){
    let uploadCallback = (result: GetUploadUrlResult, response: FileUploadResponse) => {
      switch (result){
        case GetUploadUrlResult.ACCESS_DENIED:
          this.onUploadFailed(gettext(
            "You don't appear to have rights to upload files to our system. Please contact administration"
          ));
          break;
        case GetUploadUrlResult.ERROR:
          this.onUploadFailed(gettext(
            "Server encountered an error when generating upload link."
          ));
          break;
        case GetUploadUrlResult.OK:
          this.swiftUploadResponse = response;
          this.moveToNextState();
      }
    };

    get_upload_url(this.currentFile.name, this.swiftNamespace, uploadCallback)
  }

  private doUpload(){
    let fileUploadManager = new FileUploadManager(
      this.currentFile, this.swiftUploadResponse['url']
    );

    fileUploadManager.addProgressListener(function (bytes, percent) {
      console.log(bytes, percent);
    });

    for (let l of this.uploadProgressListener){
      fileUploadManager.addProgressListener(l);
    }

    fileUploadManager.addOnDoneListener((status_code: number) => {
      if(status_code < 0){
        this.onUploadFailed(gettext("Upload failed."))
      } else {
        this.moveToNextState();
      }
    });

    fileUploadManager.startUpload();
  }

  private postBackPath(){
    let deferred = $.ajax({
      "method": "POST",
      "url": this.resourceUpdateUrl,
      "data": {
        "path": this.swiftUploadResponse.path
      }
    });
    deferred.done(() => {
      this.state = SwiftUploadManagerState.DONE;
      this.fireUploadStateCallbacks(true, null);
    });
    deferred.fail(() => {
      this.onUploadFailed(gettext("Error while posting back path to OLCMS;"))
    });
  }


  private moveToNextState(){
    switch (this.state){
      case SwiftUploadManagerState.FILE_NOT_SELECTED:
        throw "Please select file first";
      case SwiftUploadManagerState.FILE_SELECTED:
        this.state = SwiftUploadManagerState.GETTING_UPLOAD_URL;
        this.getSwiftUploadUrl();
        break;
      case SwiftUploadManagerState.GETTING_UPLOAD_URL:
        this.state = SwiftUploadManagerState.UPLOADING;
        this.doUpload();
        break;
      case SwiftUploadManagerState.UPLOADING:
        this.state = SwiftUploadManagerState.POSTING_BACK_PATH;
        this.postBackPath();
        break;
      case SwiftUploadManagerState.DONE:
        // Ignore
        break;
    }
  }

  private fireUploadStateCallbacks(isOk: boolean, errorMsg: string){
    for (let cb of this.uploadStateCallback){
      cb(isOk, errorMsg);
    }
  }

}


/**
 * Negative status code always means upload failed. Positive status code is a HTTP status code.
 */
type OnDoneListener = (statusCode: number) => any;
type ProgressListener = (bytesRead: number, percentageProgress: number) => any;


/**
 * Very simple class that just does POST on a specified url, and emits progress events.
 *
 *
 */
class FileUploadManager{

  private file: File;
  private url: string;

  private onDoneListeners: OnDoneListener[] = [];
  private progressListeners: ProgressListener[] = [];

  public constructor(file: File, url: string){
    this.file = file;
    this.url = url;
  }

  private fireOnDone(statusCode: number){
    for (let l of this.onDoneListeners){
      l(statusCode);
    }
  }

  public startUpload(){
    let xhr = new XMLHttpRequest();
    xhr.open("PUT", this.url, true);
    xhr.upload.addEventListener('progress', (e) => {
      let done = e.loaded, total = e.total;
      let percentage = Math.floor( (done/total) * 100 );
      for (let l of this.progressListeners){
        l(done, percentage);
      }
    });
    xhr.ontimeout = (e) => {this.fireOnDone(-1)};
    xhr.onabort = (e) => {this.fireOnDone(-3)};
    xhr.onerror = (e) => {this.fireOnDone(-3)};
    xhr.onreadystatechange = (e) => {
      if(xhr.readyState == 4){
        console.log(xhr.status);
        if(xhr.status >= 200 && xhr.status <= 300)
        {
          this.fireOnDone(xhr.status);
        }
      }
    };
    xhr.send(this.file);
  }

  public addOnDoneListener(l: OnDoneListener){this.onDoneListeners.push(l)}
  public addProgressListener(l: ProgressListener){this.progressListeners.push(l)}
}
