class SimpleMDEManager {

    private uniqeId: string;
    private initial: string;
    private editor: any;
    private textAreaSelector: string;

    /**
     *
     * @param {string} uniqueId --- Unique id that will be used to store markdown document in localStorage
     * @param {string} initial --- initial value for field
     * @param {string} textAreaSelector --- selector for text area that will contain markdown
     */
    constructor(uniqueId: string, initial: string, textAreaSelector: string = "#id_markdown"){
        this.uniqeId = uniqueId;
        this.initial = initial;
        this.textAreaSelector = textAreaSelector;
        this.init();
    }

    /**
     * @param e event returned from SimpleMDE, this is **NOT** typical JS event.
     */
    private save(e: any): void{
        let $target = $(e.gui.toolbar).find('a').first();
        $target.removeClass().addClass("fa fa-spin fa-spinner");
        $.ajax({
            method: "post",
            data: {
                "markdown": this.editor.value()
            }
        }).done(function () {
            $target.removeClass().addClass("fa fa-check-circle text-success");
            setTimeout(function () {
                $target.removeClass().addClass("fa fa-cloud-upload").css('color', '');
            }, 1000)
        }).fail(function () {
            $target.removeClass().addClass("fa fa-times-circle text-danger")
            setTimeout(function () {
                $target.removeClass().addClass("fa fa-cloud-upload")
            }, 1000)
        })
    }

    private init(): void{
        let toolbar = [
            // In the save() above we assume that save action is the first.
            {
                name: "save",
                action: (e: any) => {this.save(e)},
                className: "fa fa-cloud-upload",
                title: "Save",
            },
            "|", "bold", "italic", "heading", "|",
            "quote", "unordered-list", "ordered-list", "|",
            "link", "image", "horizontal-rule", "table", "|",
            "preview", "side-by-side", "fullscreen", "guide"
        ];

        this.editor = new SimpleMDE({
            "autofocus": true,
            "autosave": {
                "enabled": true,
                "uniqueId": this.uniqeId
            },
            "element":  $().get(0),
            "forceSync": true,
            "spellChecker": false,
            "initialValue": this.initial,
            toolbar: toolbar
        });

    }

}
