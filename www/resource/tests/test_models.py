# -*- coding: utf-8 -*-
"""Models for resource app."""

from django.core.exceptions import ValidationError
from test_plus.test import TestCase

from www.resource.models import ResourceRole, validate_link_url


class TestResourceRoleModel(TestCase):

    def test_resource_str(self):
        resource = ResourceRole(slug="slug", name="A Role")
        self.assertEqual(
            str(resource),
            "A Role"
        )


class TestDomainValidator(TestCase):

    def test_valid_domain(self):
        validate_link_url('http://www.google.pl')

    def test_restricted_domain(self):
        try:
            validate_link_url('https://redtube.com')
            self.fail('Restricted domains does not work')
        except ValidationError:
            pass
