# -*- coding: utf-8 -*-
import tempfile
from os import urandom

import factory
from django.core.files.storage import default_storage

from www.content_item.const import License, ResourceType
from www.content_item.test.utils import ContentItemFactory
from www.resource import models


class ResourceRoleFactory(factory.DjangoModelFactory):

    slug = "supplementary-material"
    name = "Supplementary material"

    class Meta:
        model = models.ResourceRole


class AbstractResourceFactory(factory.DjangoModelFactory):

    resource_license = License.cc_by
    title = "Printout for students"
    description = "A description"
    content_item = factory.SubFactory(ContentItemFactory)
    role = factory.LazyAttribute(lambda _: models.ResourceRole.objects.all()[0])


class LinkResourceFactory(AbstractResourceFactory):

    url = "http://www.google.pl"
    resource_type = ResourceType.link

    class Meta:
        model = models.LinkResource


class FileResourceFactory(AbstractResourceFactory):

    resource_type = ResourceType.file
    path = 'dummy/path/to/stop/failing/on/nonexistent/files'

    @staticmethod
    def create_example_file(file_resource: models.FileResource, file_size_mb: float=1.0, upload: bool=False):
        # Technically we might want to delete these files sometime, however this is not a pressing thing as:
        # 1. Each of these files contains 1024 random bytes, and then files_size_mb megabytes of zeros,
        #    so on any decent system we'd need quite a lot of tests to mess something.
        # 2. Uploads are stored on swift, which compresses data.
        file_resource.local_file_path = FileResourceFactory.create_file_with_random_data(
            int(file_size_mb * 1024 * 1024)
        )
        if upload:
            FileResourceFactory.upload_to_swift(file_resource.path)

    @staticmethod
    def create_file_with_random_data(file_size: int) -> str:
        file = tempfile.NamedTemporaryFile(delete=False)
        file.write(urandom(1024))
        file.seek(file_size)
        file.write(b'\0')
        file.close()
        return file.name

    @staticmethod
    def upload_to_swift(file_name) -> str:
        with open(file_name) as f:
            default_storage.save(file_name, f)
        return file_name

    class Meta:
        model = models.FileResource


class DocumentResourceFactory(AbstractResourceFactory):

    resource_type = ResourceType.document
    markdown = """
Markdown haiku:
    
# A Header 
Some text 
*emphasis*
    """.strip()

    class Meta:
        model = models.DocumentResource


class SubtitleResourceFactory(AbstractResourceFactory):

    resource_type = ResourceType.subtitle
    path = 'dummy/path/to/stop/failing/on/nonexistent/files'
    language = 'pl'
    subtitle_id = 'doesNotMatter'

    class Meta:
        model = models.SubtitleResource


class VideoResourceFactory(AbstractResourceFactory):

    resource_type = ResourceType.video
    path = 'dummy/path/to/stop/failing/on/nonexistent/files'
    youtube_id = 'doesNotMatter'

    class Meta:
        model = models.VideoResource
