# -*- coding: utf-8 -*-
import json

import unittest
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.test.testcases import TestCase
from django.test import Client
from django.urls.base import reverse

from www.content_item.const import License
from www.content_item.test.utils import ContentItemFactory
from www.resource.tests.utils import LinkResourceFactory, VideoResourceFactory, SubtitleResourceFactory
from www.resource.views import can_edit_resource
from www.utils.test_utils import LoginTestMixin, model_rich_compare

from www.resource import models


class PermissionChecks(LoginTestMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.resource = LinkResourceFactory(
            content_item__owners=self.user
        )

    def test_anonymous_user(self):
        self.assertFalse(can_edit_resource(AnonymousUser(), self.resource))

    def test_another_user(self):
        not_an_owner = get_user_model().objects.create(username="other-user")
        self.assertFalse(can_edit_resource(not_an_owner, self.resource))

    def test_owner(self):
        self.assertTrue(can_edit_resource(self.user, self.resource))

    def test_delete_resource_logged_in(self):
        del_url = reverse("resource:delete", args=[self.resource.pk])
        self.login()
        self.client.post(del_url)

        self.assertEqual(models.Resource.objects.filter(pk=self.resource.pk).count(), 0)

    def test_delete_resource_not_logged_in(self):
        del_url = reverse("resource:delete", args=[self.resource.pk])
        self.client.post(del_url)

        self.assertEqual(models.Resource.objects.filter(pk=self.resource.pk).count(), 1)

    def test_delete_resource_another_user(self):
        del_url = reverse("resource:delete", args=[self.resource.pk])

        self.USER_USERNAME = 'notme'
        self.USER_EMAIL = "aaa@bbb.ccc"
        super(LoginTestMixin, self).setUp()
        self.login('notme')
        self.client.post(del_url)
        self.assertEqual(models.Resource.objects.filter(pk=self.resource.pk).count(), 1)


class VideoResourceCreateViewChecks(LoginTestMixin, TestCase):

    def test_add_video_resource(self):
        self.login()
        item = ContentItemFactory(owners=self.user)
        create_vid_url = reverse("resource:video-create", args=[item.pk])
        response = self.client.post(create_vid_url, data={'resource_license': 'CC0',
                                                          'title': 'test title',
                                                          'role': '1'})
        # user is redirected to upload video file
        self.assertEqual(response.status_code, 302)
        self.assertIn('video/upload', response.url)

    def test_only_one_video_in_content_item(self):
        self.login()
        vid_res = VideoResourceFactory(content_item__owners=self.user)

        create_vid_url = reverse("resource:video-create", args=[vid_res.content_item.pk])
        response = self.client.post(create_vid_url, data={'resource_license': 'CC0',
                                                          'title': 'test title',
                                                          'role': '1'})
        self.assertEqual(len(response.context_data['form'].errors), 1)
        self.assertIn('Video resource already exists for this content item', response.context_data['form'].errors.as_text())


class SubtitleResourceCreateViewChecks(LoginTestMixin, TestCase):

    def test_add_subtitle(self):
        self.login()
        vid_res = VideoResourceFactory(content_item__owners=self.user)

        create_vid_url = reverse("resource:subtitle-create", args=[vid_res.content_item.pk])
        response = self.client.post(create_vid_url, data={'resource_license': 'CC0',
                                                          'title': 'test title',
                                                          'role': '1',
                                                          'language': 'pl'})
        self.assertEqual(response.status_code, 302)
        self.assertIn('subtitle/upload', response.url)

    def test_can_not_add_subtitle_if_video_missing(self):
        self.login()
        item = ContentItemFactory(owners=self.user)
        create_vid_url = reverse("resource:subtitle-create", args=[item.pk])
        response = self.client.post(create_vid_url, data={'resource_license': 'CC0',
                                                          'title': 'test tile',
                                                          'role': '1',
                                                          'language': 'pl'})
        self.assertEqual(len(response.context_data['form'].errors), 1)
        self.assertIn('Can not add subtitle if video resource is missing', response.context_data['form'].errors.as_text())

    def test_can_not_add_subtitle_with_existing_title_and_lang(self):
        self.login()
        vid_res = VideoResourceFactory(content_item__owners=self.user)
        sub_res = SubtitleResourceFactory(content_item=vid_res.content_item)

        create_vid_url = reverse("resource:subtitle-create", args=[sub_res.content_item.pk])
        response = self.client.post(create_vid_url, data={'resource_license': 'CC0',
                                                          'title': sub_res.title,
                                                          'role': '1',
                                                          'language': sub_res.language})
        self.assertEqual(len(response.context_data['form'].errors), 1)
        self.assertIn('There is already subtitle with same title and language', response.context_data['form'].errors.as_text())
