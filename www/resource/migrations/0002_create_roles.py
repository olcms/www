# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-12-04 14:44
from __future__ import unicode_literals

import pathlib

import yaml
from django.db import migrations
from django.db.migrations.operations.special import RunPython

domains = pathlib.Path(__file__).parent / "roles.yml"


def forwards_func(apps, schema_editor):
    ResourceRole = apps.get_model("resource", "ResourceRole")

    with domains.open('r') as f:
        data = yaml.load(f)

    for codename, role in data['roles'].items():
        role_model, created = ResourceRole.objects.get_or_create(
            slug=codename
        )
        role_model.name = role['name']
        role_model.save()


class Migration(migrations.Migration):

    dependencies = [
        ('resource', '0001_initial'),
    ]

    operations = [
        RunPython(forwards_func, reverse_code=RunPython.noop)
    ]
