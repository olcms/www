# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-24 18:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resource', '0013_auto_20170507_1547'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='language',
        ),
    ]
