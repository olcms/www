# -*- coding: utf-8 -*-
"""Models for resource app."""

import io
from pathlib import Path

import slugify
from django.core.exceptions import ValidationError
from django.core.files.storage import default_storage
from django.core.validators import URLValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy

from www.content_item.const import ResourceType, License, LANGUAGES
from www.content_item.models import validate_language_code

from .convert_tasks import convert_document_resource


def read_restricted_domains():
    """ Read file with restricted domains to list  """
    restricted_domain_file = Path(__file__).parent / 'data' / 'restricted_domains.list'
    with restricted_domain_file.open() as domain_file:  # pylint: disable=no-member
        domains = domain_file.readlines()
    return [d.strip() for d in domains]


restricted_domains = read_restricted_domains()


def validate_link_url(url):
    """Validate link resource."""
    url_valid = URLValidator()
    url_valid(url)
    for d in restricted_domains:
        if url.startswith('http://' + d) or url.startswith('https://' + d):
            raise ValidationError('Restricted domain')


class ResourceRole(models.Model):
    """Resource role model."""

    slug = models.SlugField()
    name = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


class Resource(models.Model):
    """Resource model."""

    content_item = models.ForeignKey(
        "content_item.ContentItem",
        null=False,
        related_name="resources"
    )

    resource_type = models.CharField(
        verbose_name=ugettext_lazy('Resource type'),
        help_text=ugettext_lazy('Type of the resource'),
        max_length=100,
        choices=ResourceType.choices,
        validators=[ResourceType.validator],
        default=ResourceType.link
    )

    resource_license = models.CharField(
        verbose_name=ugettext_lazy('Resource license'),
        help_text=ugettext_lazy('License on which resource is published'),
        max_length=12,
        choices=License.choices,
        validators=[License.validator],
        default=License.cc_by
    )

    title = models.CharField(
        verbose_name=ugettext_lazy('Resource title'),
        help_text=ugettext_lazy('Short title of the resource'),
        null=False, blank=False,
        max_length=256
    )

    description = models.TextField(
        verbose_name=ugettext_lazy('Resource description'),
        null=True, blank=True
    )

    role = models.ForeignKey(ResourceRole, null=True)

    default = models.BooleanField(
        verbose_name=ugettext_lazy('Is default?'),
        null=False, blank=False, default=False
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        from www.content_item.models import ContentItem
        ContentItem.objects.filter(pk=self.content_item_id).update(
            last_updated_at=timezone.now()
        )

        self.__ensure_default_constraints()

        super().save(force_insert, force_update, using, update_fields)

    def __ensure_default_constraints(self):
        if self.default:
            self.content_item.resources.exclude(  # pylint: disable=no-member
                pk=self.pk).filter(default=True).update(default=False)
        elif self.content_item.resources.exclude(pk=self.pk).count() == 0:  # pylint: disable=no-member
            self.default = True

    class Meta:
        # Right now just make ordering consistent
        ordering = ['-default', 'pk']


class LinkResource(Resource):
    """ Resource of link type - url to anything in the web """

    url = models.TextField(
        verbose_name=ugettext_lazy('Link url'),
        null=False, blank=False,
        validators=[validate_link_url],
    )


class FileResource(Resource):
    """File resource."""

    path = models.TextField(
        verbose_name=ugettext_lazy('File path'),
        null=True, blank=True
    )

    @property
    def download_url(self):
        """Http url for file download."""
        if self.path is None:
            return None
        return default_storage.base_url + self.path

    @property
    def is_complete(self):
        """Check if file resource is complete (has non null attached file path)"""
        return self.path is not None


def read_example_document() -> str:
    """Returns example document."""
    example_resource = Path(__file__).parent / 'data' / 'example_document_resource.md'
    return example_resource.read_text()  # pylint: disable=no-member


class DocumentResource(Resource):
    """Resource containing a document."""

    HTML_MIMETYPE = "text/html"  # pragma: no cover
    ODT_MIMETYPE = "application/vnd.oasis.opendocument.text"  # pragma: no cover
    DOCX_MIMETYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"  # pragma: no cover
    PDF_MIMETYPE = "application/pdf"  # pragma: no cover

    markdown = models.TextField(
        verbose_name=ugettext_lazy("Markdown"),
        null=True, blank=True,
        default=None,
    )

    @property
    def defaulted_markdown(self) -> str:
        """
        Returns document markdown. If markdown is None return
        default markdown without changing the model.
        """
        if self.markdown is None:
            return read_example_document()
        return self.markdown

    @property
    def is_complete(self):
        """Check if resource is complete (has markdown)"""
        return self.markdown is not None

    def _store_resource(self, file_name: str, contents: bytes) -> str:  # pragma: no cover
        name = "{s.content_item.pk}/{s.pk}/{name}".format(  # pylint: disable=missing-format-attribute
            s=self, name=file_name
        )
        return default_storage.save(name, io.BytesIO(contents))

    def store_resource(self, file_name: str, mimetype: str, contents: bytes):  # pragma: no cover
        """
        Stores resource derivative.

        :param contents: Computed derivative contents.
        :param mimetype: Mimetype for the derivative.
        :param file_name: File name to store.
        """
        derivative, created = DocumentResourceDerivative.objects.get_or_create(
            resource=self,
            mime_type=mimetype
        )
        if not created and derivative.path is not None:
            default_storage.delete(derivative.path)
        derivative.path = self._store_resource(file_name, contents)
        derivative.is_ready = True
        derivative.save()


@receiver(post_save, sender=DocumentResource)
def render_derivatives(instance: DocumentResource, **kwargs):  # pylint: disable=unused-argument
    """Renders markdown to different formats on save."""

    if instance.markdown is None:
        return

    base_filename = slugify.slugify_filename(instance.title)
    convert_document_resource.delay(instance.id, base_filename)


class DocumentResourceDerivative(models.Model):
    """
    Stores resource derivative --- a transformed version of document resource.
    """

    resource = models.ForeignKey(
        DocumentResource, null=False, blank=False,
        related_name="derivatives"
    )
    mime_type = models.CharField(
        max_length=128, null=False
    )
    path = models.CharField(default=None, null=True, max_length=1024)
    is_ready = models.BooleanField(default=False)
    celery_task_id = models.CharField(max_length=128, null=True, blank=True)

    @property
    def download_url(self):  # pragma: no cover
        """Http url for file download."""
        if self.path is None:
            return None
        return default_storage.base_url + self.path

    class Meta:
        unique_together = (
            ("resource", "mime_type"),
        )


class VideoResource(FileResource):
    """ Video resource - it is uploaded to swift and to youtube """

    youtube_id = models.TextField(
        verbose_name=ugettext_lazy('Youtube video id'),
        null=True, blank=True
    )


class SubtitleResource(FileResource):
    """ Subtitle resource - this are subtitles for video resource """

    language = models.CharField(
        verbose_name=ugettext_lazy('Resource language'),
        max_length=2,
        validators=[validate_language_code],
        choices=LANGUAGES
    )

    subtitle_id = models.TextField(
        verbose_name=ugettext_lazy('Subtitle id'),
        help_text=ugettext_lazy(
            'Id of subtitle generated by YouTube when adding to video'
        ),
        null=True, blank=True
    )
