# -*- coding: utf-8 -*-
"""Views for resource app."""
import abc
import logging

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.mail import mail_admins
from django.forms.models import ModelForm
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls.base import reverse
from django.views.generic import DetailView, CreateView
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, DeleteView
from django.utils.translation import ugettext_lazy, ugettext

from www.content_item.const import ResourceType
from www.content_item.models import ContentItem
from www.content_item.utils import is_content_item_editor
from . import models, forms, youtube_tasks

# pylint: disable=no-self-use


# Class Based Views define some attributes outside init.
# pylint: disable=attribute-defined-outside-init

log = logging.getLogger(__name__)


def can_edit_resource(user, resource: models.Resource) -> bool:
    """
    Checks that user can edit resource.

    He can do it when he is an owner of content_item to which this resource is attached.

    """
    if user.is_anonymous:
        return False
    return resource.content_item.owners.filter(pk=user.pk).exists()


class ResourceDetailView(LoginRequiredMixin, DetailView):
    """View that displays resource details."""

    model = models.Resource
    template_name = 'resource/resource_detail.html'


class BaseResourceView(
        UserPassesTestMixin,
        LoginRequiredMixin,
        TemplateView,
        metaclass=abc.ABCMeta
):
    """Base view for all resource views."""
    resource_type = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def get_context_data(self, **kwargs):
        kwargs.update({
            "action": self.request.path_info,
            "content_item": self.content_item
        })
        return super().get_context_data(**kwargs)

    @abc.abstractmethod
    def get_content_item(self) -> 'ContentItem':
        """Gets content item from the db."""
        raise NotImplementedError

    def dispatch(self, request, *args, **kwargs):
        # Just send 404 on GET not only when we save the resource
        self.content_item = self.get_content_item()
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return is_content_item_editor(self.content_item, self.request.user)

    def get_success_http_response(self):  # pylint: disable=no-self-use
        """
        On success we return 201 so JS knows Resource was saved.
        """
        return HttpResponse(status=201)

    def form_valid(self, form: ModelForm):
        """Update resource with content item."""
        self.object = form.save(commit=False)
        if self.object.pk is None:
            self.object.resource_type = self.resource_type
        self.object.content_item = self.content_item
        self.object.save()
        form.save_m2m()
        return self.get_success_http_response()


class BaseResourceCreateView(BaseResourceView, CreateView, TemplateView):
    """Base view for creating a resource."""
    def get_content_item(self) -> 'ContentItem':
        """Gets content item from the db."""
        return get_object_or_404(ContentItem, pk=self.kwargs['content_item_id'])


class BaseResourceDeleteView(BaseResourceView, DeleteView):
    """Base view for deleting a resource."""
    def get_content_item(self) -> 'ContentItem':
        return self.get_object().content_item

    def delete(self, request, *args, **kwargs):  # pragma: no cover
        res = self.get_object()
        self._if_video_remove_from_yt(res)
        self._if_subtitle_remove_from_yt(res)
        res.delete()
        return self.get_success_http_response()

    def _if_video_remove_from_yt(self, res):  # pragma: no cover
        try:
            if res.resource_type == ResourceType.video and res.fileresource.videoresource.youtube_id:
                youtube_tasks.start_youtube_delete.delay(res.fileresource.videoresource.youtube_id)
                # FIX remove video and all subtitles for video from cloud storage
                # FIX remove all subtitle resource from content item for this video
        except Exception:  # pylint: disable=broad-except
            log.exception('Exception while deleting youtube video, id %s', res.fileresource.videoresource.youtube_id)
            mail_admins(
                'Exception while deleting youtube video',
                message='Exception while deleting youtube video, id %s' % res.fileresource.videoresource.youtube_id
            )

    def _if_subtitle_remove_from_yt(self, res):  # pragma: no cover
        try:
            if res.resource_type == ResourceType.subtitle and res.fileresource.subtitleresource.subtitle_id:
                youtube_tasks.start_subtitle_delete.delay(res.fileresource.subtitleresource.subtitle_id)
                # FIX remove subtitle from cloud storage
        except Exception:  # pylint: disable=broad-except
            log.exception('Exception while deleting subtitle, id %s', res.fileresource.subtitleresource.subtitle_id)


class BaseResourceUpdateView(BaseResourceView, UpdateView, TemplateView):
    """Base view for updating a resource."""
    def get_content_item(self) -> 'ContentItem':
        return self.get_object().content_item

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)


class LinkResourceCreateView(BaseResourceCreateView):
    """Allows creation of resource."""

    model = models.LinkResource
    form_class = forms.LinkResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.link


class FileResourceCreateView(BaseResourceCreateView):
    """
    File upload works as follows:

    1. First user is fills resource metadata in **this** view.
    2. On success this view returns redirect (that is handled transparently by the jxhr)
       to FileResourceUpdateUrl.
    3. There user uploads file to SWIFt, and then file path is posted back.
    """

    model = models.FileResource
    form_class = forms.FileResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.file

    def get_success_http_response(self):
        return HttpResponseRedirect(
            reverse("resource:file-upload", kwargs={"pk": self.object.pk})
        )


class FileResourceUploadView(BaseResourceUpdateView):
    """View to update file resource path."""

    model = models.FileResource
    form_class = forms.FileResourceUploadForm
    template_name = "resource/file_resource_upload_form.html"
    resource_type = ResourceType.file


class LinkResourceUpdateView(BaseResourceUpdateView):
    """ View to update link resource """
    form_class = forms.LinkResourceForm
    model = models.LinkResource
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.link


class FileResourceUpdateView(BaseResourceUpdateView):
    """ View to update file resource """
    model = models.FileResource
    form_class = forms.FileResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.file


class DocumentResourceCreateView(BaseResourceCreateView):

    """
    Works almost as FileResourceCreateView --- creates a file resource and then redirects to
    page where you might upload contents.
    """

    model = models.DocumentResource
    form_class = forms.DocumentResourceEditMetadata
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.document

    def get_success_http_response(self):
        return HttpResponseRedirect(
            reverse("resource:document-upload", kwargs={"pk": self.object.pk})
        )


class DocumentResourceUpdateView(BaseResourceUpdateView):

    """
    Works almost as FileResourceCreateView --- creates a file resource and then redirects to
    page where you might upload contents.
    """

    model = models.DocumentResource
    form_class = forms.DocumentResourceEditMetadata
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.document

    def get_success_http_response(self):
        return HttpResponse(status=201)


class DocumentResourceEditMarkdownView(BaseResourceUpdateView):
    """
    This view renders markdown editor. It renders either a full page (when requested by browser) or just
    a form (when requested by Ajax).
    """

    model = models.DocumentResource
    form_class = forms.DocumentResourceUpdateDocument
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.document

    @property
    def requested_via_ajax(self) -> bool:
        """
        :return: True if this view was requested by Ajax.
        """
        return 'HTTP_X_REQUESTED_WITH' in self.request.META

    def get_template_names(self):
        if self.requested_via_ajax:
            return "resource/html_resource_file_form.html"
        return "resource/html_resource_file_form_full.html"

    def get_success_http_response(self):
        if self.requested_via_ajax:
            return super().get_success_http_response()
        return HttpResponseRedirect(reverse("content_item:detail", kwargs={"pk": self.object.content_item_id}))


class ResourceDeleteView(BaseResourceDeleteView):
    """Allows deletion of resource."""

    model = models.Resource
    form_class = forms.DeletionForm
    template_name = "resource/resource_form.html"
    resource_type = 'link'


class VideoResourceCreateView(BaseResourceCreateView):
    """
    File upload works as follows:

    1. First user is fills resource metadata in **this** view.
    2. On success this view returns redirect (that is handled transparently by the jxhr)
       to VideoResourceUpdateUrl.
    3. There user uploads file to SWIFt, and then file path is posted back.
    4. Than in the background task is scheduled to download file to local file system and upload it to youtube
    """

    model = models.VideoResource
    form_class = forms.VideoResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.video

    def get_success_http_response(self):    # pragma: no cover
        return HttpResponseRedirect(
            reverse("resource:video-upload", kwargs={"pk": self.object.pk})
        )

    def form_valid(self, form: ModelForm):
        res = self.content_item.resources.all()
        # do not allow to add more than one video resource
        for r in res:
            if r.resource_type == ResourceType.video:
                log.warning('Video resource already exists for content item %s', self.content_item.pk)
                form.errors['title'] = [ugettext('Video resource already exists for this content item')]
                return super().form_invalid(form)
        return super().form_valid(form)


class VideoResourceUpdateUrl(BaseResourceUpdateView):
    """View to update file resource path."""

    model = models.VideoResource
    form_class = forms.VideoResourceUpdateUrl
    template_name = "resource/file_resource_upload_form.html"
    resource_type = ResourceType.video

    def form_valid(self, form: ModelForm):      # pragma: no cover
        resp = super().form_valid(form)
        youtube_tasks.start_youtube_upload.delay(self.object.pk)
        return resp


class VideoResourceUpdateView(BaseResourceUpdateView):
    """ View to update file resource """
    model = models.VideoResource
    form_class = forms.VideoResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.video

    def form_valid(self, form: ModelForm):      # pragma: no cover
        resp = super().form_valid(form)
        youtube_tasks.start_youtube_edit.delay(self.object.pk)
        return resp


class SubtitleResourceCreateView(BaseResourceCreateView):
    """
    File upload works as follows:

    1. First user is fills resource metadata in **this** view.
    2. On success this view returns redirect (that is handled transparently by the jxhr)
       to VideoResourceUpdateUrl.
    3. There user uploads file to SWIFt, and then file path is posted back.
    4. Than in the background task is scheduled to download file to local file system and upload it to youtube
    """

    model = models.SubtitleResource
    form_class = forms.SubtitleResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.subtitle

    def get_success_http_response(self):    # pragma: no cover
        return HttpResponseRedirect(
            reverse("resource:subtitle-upload", kwargs={"pk": self.object.pk})
        )

    def form_valid(self, form: ModelForm):
        c_item = self.content_item
        if not self.is_video_present(c_item):
            form.errors['title'] = [ugettext_lazy('Can not add subtitle if video resource is missing')]
            return self.form_invalid(form)
        if not self.is_subtitle_unique(form, c_item):
            form.errors['title'] = [ugettext_lazy('There is already subtitle with same title and language')]
            return self.form_invalid(form)
        return super().form_valid(form)

    def is_subtitle_unique(self, form, c_item: ContentItem) -> bool:
        """
        Checks if subtitle with the same language and title is in the database.
        """
        obj = models.SubtitleResource.objects.filter(
            content_item=c_item,
            language=form.cleaned_data['language'],
            title=form.cleaned_data['title'])
        return obj.count() == 0

    def is_video_present(self, c_item: ContentItem) -> bool:
        """Returns True if c_item has video attached. """
        obj = models.VideoResource.objects.filter(content_item=c_item)
        return obj.count() > 0


class SubtitleResourceUpdateUrl(BaseResourceUpdateView):
    """View to update file resource path."""

    model = models.SubtitleResource
    form_class = forms.SubtitleResourceUpdateUrl
    template_name = "resource/file_resource_upload_form.html"
    resource_type = ResourceType.subtitle

    def form_valid(self, form: ModelForm):      # pragma: no cover
        resp = super().form_valid(form)
        youtube_tasks.start_subtitle_upload.delay(self.object.pk)
        return resp


class SubtitleResourceUpdateView(BaseResourceUpdateView):
    """ View to update file resource """
    model = models.SubtitleResource
    form_class = forms.SubtitleResourceForm
    template_name = "resource/resource_form.html"
    resource_type = ResourceType.subtitle

    def form_valid(self, form: ModelForm):      # pragma: no cover
        prev_sub_res = models.SubtitleResource.objects.get(pk=self.object.pk)
        resp = super().form_valid(form)

        # exec YouTube task only if YouTube data changed
        if prev_sub_res.title != self.object.title or prev_sub_res.language != self.object.language:
            youtube_tasks.start_subtitle_edit.delay(self.object.pk)
        return resp
