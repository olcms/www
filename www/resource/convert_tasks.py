# -*- coding: utf-8 -*-
"""
Tasks concerned with youtube upload, since testing it is non trivial
coverage checking is disabled for this file.
"""

import base64

from celery import shared_task
from celery.utils.log import get_task_logger

from ..taskapp.celery import (
    convert_md_to_html,
    convert_md_to_odt,
    convert_md_to_docx,
    convert_md_to_pdf
)

from . import models

logger = get_task_logger(__name__)


@shared_task(ignore_result=True)
def convert_document_resource(resource_id, base_filename):
    """Triggers conversions to different formats"""
    resource = models.DocumentResource.objects.get(pk=resource_id)

    chain = convert_md_to_html.s(resource.markdown) | \
            store_derivative.s(models.DocumentResource.HTML_MIMETYPE,  # noqa
                               base_filename + '.html', resource_id)
    chain()

    chain = convert_md_to_odt.s(resource.markdown) | \
            store_derivative.s(models.DocumentResource.ODT_MIMETYPE,  # noqa
                               base_filename + '.odt', resource_id)
    chain()

    chain = convert_md_to_docx.s(resource.markdown) | \
            store_derivative.s(models.DocumentResource.DOCX_MIMETYPE,  # noqa
                               base_filename + '.docx', resource_id)
    chain()

    chain = convert_md_to_pdf.s(resource.markdown) | \
            store_derivative.s(models.DocumentResource.PDF_MIMETYPE,  # noqa
                               base_filename + '.pdf', resource_id)
    chain()


@shared_task(ignore_results=True)
def store_derivative(data, mime_type, filename, resource_id):
    """Saves converted derivatives to db"""
    resource = models.DocumentResource.objects.get(pk=resource_id)
    content = data['result']
    if data['format'] == 'text':
        content = content.encode('utf-8')
    elif data['format'] == 'base64':
        content = base64.b64decode(content)
    assert content is not None
    resource.store_resource(filename, mime_type, content)
