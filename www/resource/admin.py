# -*- coding: utf-8 -*-
"""Django admin config for resource app."""

from django.contrib import admin

from . import models

admin.site.register(models.ResourceRole)
admin.site.register(models.Resource)
admin.site.register(models.LinkResource)
admin.site.register(models.FileResource)
admin.site.register(models.VideoResource)
admin.site.register(models.SubtitleResource)
