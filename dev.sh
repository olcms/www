#!/usr/bin/env bash

set -eo pipefail

export UID
export GID=$(id --group)

DC="docker-compose"
RUN="${DC} run --rm"
RD="${RUN} django"
TD="${DC} -f docker-compose-test.yml run --rm django"

function push_images {

  set -e

  git status;

  if ! git diff-index --quiet HEAD --
  then
    echo "Please commit all changes."
    exit 1
  fi

  export GIT_TAG=$1

  docker-compose -f compose-build.yml build --pull
  docker-compose -f compose-build.yml push


  export GIT_TAG=$(git rev-parse --short HEAD)

  docker-compose -f compose-build.yml build
  docker-compose -f compose-build.yml push

}

function push_images_ci {

    docker_tag=$1
    expected_branch=$2
    current_branch=$(git rev-parse --abbrev-ref HEAD)
    if test ${current_branch} = ${expected_branch}; then
        push_images ${docker_tag} ;
    fi

}

case "$1" in
    "build") ${DC} build --pull ${@:2};;
    "init") ${RD} /scripts/migrations.sh page medialibrary;;
    "up") ${DC} up -d ${@:2};;
    "restart") ${DC} restart ${@:2};;
    "down") ${DC} down ${@:2};;
    "stop") ${DC} stop ${@:2};;
    "rm") ${DC} stop ${@:2}; ${DC} rm ${@:2};;
    "logs") ${DC} logs ${@:2};;
    "ps") ${DC} ps;;
    "reload") ${DC} down && ${DC} build && ${DC} up -d;;
    "reload-full") ${DC} down -v && ${DC} pull && ${DC} build && ${DC} up -d;;

    "manage") ${RD} python manage.py ${@:2};;
    "reindex") ${RD} python manage.py rebuild_index;;
    "shell") ${RD} python manage.py shell;;

    "lint") ${TD} /scripts/lint.sh;;
    "test") ${TD} utility/run-tests-in-container.sh ${@:2};;
    "test-fast") ${TD} utility/run-tests-in-container.sh --exclude-tag=slow ${@:2};;

    "push-preprod") push_images preprod;;
    "push-prod") push_images latest;;
    "push-preprod-ci") push_images_ci preprod preprod;;
    "push-prod-ci") push_images_ci latest master;;
    "bash") ${RD} bash;;

    *) echo "Unknown command. Pleas type one of following: build, up, stop, logs, ps, manage, shell, test, test-fast. push-preprod, push-prod, bash, reindex, reload-full";;
esac
