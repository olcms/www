Simulation resource
===================

Hosting requirements
--------------------

Right now simulation resource depends on having `Open Stack Swift <https://wiki.openstack.org/wiki/Swift>`__ available for you.

`OVH <https://www.ovh.ie/>`__ is one of the providers of Swift Storage.

Simulation resource requires `StaticWeb <https://docs.openstack.org/swift/latest/middleware.html#staticweb>`__ middleware enabled (which
is usually enabled).

How it works
------------

* User uploads a zip file with a complete web-page;
* File is validated;
* This file is then unpacked and every member of the archive is uploaded
  to dedicated swift container, where it is served as static web page;
* Simulation resource extends File resource;

How to switch to different storage provider
-------------------------------------------


