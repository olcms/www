CMS End user documentation
==========================

This is documentation for consortium partners

Obtaining permissions
---------------------

If you cant log-in to the Admin interface at
``https://olcms.stem4youth.pl/admin/`` ask the OLCMS administrator for
permissions.

Adding new CMS page
-------------------

To add new CMS page:

1. Go to admin interface: ``https://olcms.stem4youth.pl/admin/``;
2. Select: ``Translatable Pages``
3. Click ``Add new Translatable page``
4. Translatable page has only a single property ``slug``, which will form the
   ``URL`` of the page in our system.

  So when you set slug to ``framework`` your page will be visible as
  ``https://olcms.stem4youth.pl/framework``.
5. Now you'll want to add some content to the translatable page, to do this
   go to ``Page Translations`` and click ``Add new Page Translation``.
6. Then fill following fields:

   * ``parent`` --- this is reference to Translatable page;
   * ``title`` --- page title
   * ``language`` --- page language

  Then click combo-box that says: ``Insert New Content`` and select ``Rich Text``.

  When you are done click save.

.. figure:: img/admin-page-translation-options.png
    :width: 80%

    Admin options for page translations

.. figure:: img/add-page.png
   :width: 80%

   Adding a new page.

.. figure:: img/admin-page-translation-options.png
   :width: 80%

   Adding a new page translation

Add new link to menu
--------------------

To add new link in the menu you need:

1. Go to admin interface: ``https://olcms.stem4youth.pl/admin/``;
2. Select ``Menu Link`` and then ``Add Menu Link``.
3. Set url to which this link will be pointing, eg. you want to put link to
   ``framework`` page you were creating, then set url to ``/framework/``.

   If you want to link to external resource (which might **not** be a good idea)
   put fully qualified url here, eg: ``http://google.com/?q=why+i+am+doing+this``),

4. Add translation of link label by selecting language and link text.


.. figure:: img/menu-links.png
   :width: 80%

   Menu link location

.. figure:: img/menu-link-example.png
   :width: 80%

   Fully filled menu item
