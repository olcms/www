How to configure forum snippets
===============================

Configure docker cluster
------------------------

``ENABLE_DISCOURSE_FORUM_TRIGGERS``
    Set this to ``true`` to enable integration.


Configure discourse
-------------------

.. note::

    Values are for developer enviornment.

Configuring this automatically is a pain, so you'll need to click through some settings.

1. Login as admin
2. Create user that will own all posts (on developer machine user ``admin``)
3. Create a category all posts will be in.
4. Go to ``Admin Settings`` -> Customize -> Embedding. Fill the following:

   Allowed hosts: olcms.dev.local:8000
   Path whitelist: /content_item/detail/.*

   Click Save.

5. On the same page fill in the following:

   Username for topic creation: username from step 2.

   CSS selector for elements that are allowed in embeds: ``.discourse-embed``

   Click: ``Save Embedding Settings``.




Developer configuration
-----------------------

Configure your machine
**********************

Make sure that your ``/etc/hosts`` file contains the following::

    127.0.2.1       olcms.dev.local
    127.0.3.1       discourse.olcms.dev.local


