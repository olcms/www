HOWTO access discourse logs
===========================

Discourse insists on storing logs inside container.
So: ``docker-compose exec discourse bash`` (user ``exec`` not ``run``!)
And then ``tail -f /shared/logs/rails*``.
