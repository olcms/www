How to obtain admin user in discourse
=====================================

Create **django** user with following properties

``username``
    Equal to: ``COMPOSE_DISCOURSE_ADMIN_EMAIL``

``email``
    Equal to: ``COMPOSE_DISCOURSE_ADMIN_USERNAME``

And then use login via SSO as this user.

