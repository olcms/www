.. _glossary:

Glossary
========

* ``Content Item`` --- is a smallest piece of information that is usable by
  teacher on it's own. Some partners call it ``Lesson``. Content items contain
  mostly metadata, actual pieces of content are stored as ``Resource`` items.
* ``Resource`` --- single material (e.g. document) usable by teacher.
* ``Domain`` --- A categorisation system for Content Items with predefined
  categories.
* ``Tag`` --- User provided categories (tags) for Content Items.

