.. www documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OLCMS documentation!
===============================

Intended audience
-----------------

This guide is mainly for technical people that want to re-use our solution.


Contents:
---------

.. toctree::
   :glob:
   :maxdepth: 2

   glossary
   contributing
   deploy
   modules/*
   end_user_doc/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
