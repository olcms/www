.. _css-stylesheets:

CSS stylesheets
===============

We use bootstrap 4, so most of the styling is done using this library,
all our "project specific" styles are stored in ``project.scss`` file
(which is in slightly better CSS variant).

Per-discipline styling
----------------------

We use per-discipline styling, where each discipline gets it's own color.

Disciplines and its colors are defined in ``$stem_colors`` variable:

.. code-block:: scss

    $stem_colors: (
      "medicine": #ea5284,
      "chemistry": #97BF0D,
      "engineering": #009DD1,
      "mathematics": darken(#FFDD00, 10%),
      "astronomy": #A5027D,
      "physics": #E7511E,
      "citizen-science": #9BBDBD
    );

Which maps discipline slug to discipline color. To add new discipline just add
it to above variable in ``project.scss``.

