.. _obtain-youtube-credentials:

Obtaining youtube credentials
=============================

Introduction
------------

To integrate with youtube you need to obtain Oauth2 credentials.

We use Youtube API in a not typical way (that is we upload videos from OLCMS
on behalf of OLCMS Consortium), this upload method is largely undocumented.

So we ended up with with roundabout solution.

What credentials do you need
----------------------------

First obtain a Google Account for your service.

OLCMS uses Youtube Data API v3.

You'll need to obtain two types of youtube credentials:

* Youtube Oauth2 Key for logging in to Youtube data API v3.
* Oauth2 Refresh token

Obtaining youtube oauth2 key
----------------------------

Follow this `google guide <https://developers.google.com/youtube/registering_an_application>`__,
which boils down to:

* Obtain Google Account.
* `Go to Google Developer Console <https://console.developers.google.com/apis/credentials>`__
  to Credentials Page.
* Create "OAuth client ID credentials"
* Download ``client_secret.json`` file.


Obtain refresh token
--------------------

Current method
**************

We currently use ``video_upload.py`` script from youtube-api samples repository,
to upload videos to youtube.

This script stores refresh token in a json file generated on first use, so you'll
need to:

1. Download `our fork of api-samples-repository <https://github.com/high-school-remote-lab/youtube-api-samples>`__.
2. Copy client secrets to ``python`` directory
3. Run ``video_upload.py`` with dummy file.
4. Perform authentication.
5. Keep ``upload-oauth2.json`` file.


Desired method
**************

.. note::

    This method is not implemented but I decided to outline it here as
    I want to move from deprecated google API to something more future-proof,
    since Google does not document our use case need to document upgrade plan
    here.

Here I will outline how this **should be done**, if you have some time
feel free to implement it.

Changes to OLCMS code
^^^^^^^^^^^^^^^^^^^^^

Instead of using old ``upload_video`` sample either use recent one or just
rewrite it in pure python using sample as a sample.

New code doesn't use ``Storage`` type to store refresh token, so you'll need
to create credentials by hand.

To obtain authenticated service you might use following snippet (that was
tested to work):

.. code-block:: python

    # Authorize the request and store authorization credentials.
    def get_authenticated_service():
      # flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
      # credentials = flow.run_console()
      credentials = Credentials.from_authorized_user_info(
        scopes=SCOPES,
        info={
        'refresh_token': u'REFRESH_TOKEN',
        'client_id': u'CLIENT_ID',
        'client_secret': u'CLIENT_SECRET',
        }
      )
      return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

To obtain ``client_id`` and ``client_secret`` open  ``client_secret.json``
and read ``client_id`` and ``client_secret`` properties there.

Obtaining refresh token
^^^^^^^^^^^^^^^^^^^^^^^

`You can follow this tutorial <http://www.whitewareweb.com/get-youtube-refresh-token-oauth-2-0-playground/>`__,
but here is the gist of it:

1. Go to `google oauth playground <https://developers.google.com/oauthplayground/>`__.
2. Select all youtube scopes.
3. Login.
4. Click "Exchange Authorization Code for Tokens"
5. You should get something that looks like::


    HTTP/1.1 200 OK
    Content-length: 287
    (...)
    {
      "access_token": "token",
      "token_type": "Bearer",
      "expires_in": 3600,
      "refresh_token": "REFRESH TOKEN abdceffff"
    }

6. Copy value of `refresh_token`, in your case this will be ``REFRESH TOKEN abdceffff``.

Create a playlist and obtain playlist ID
----------------------------------------

1. Login to youtube, create new playlist.
2. Open said playlist.
3. You should get url that looks like: ``https://www.youtube.com/playlist?list=PLrwbUZAqoi2gYedn9wbjqxXsRCMI_HOZb``
4. So your playlistId is value of ``list`` parameter,
   in our example it's ``PLrwbUZAqoi2gYedn9wbjqxXsRCMI_HOZb``.


Store permissions in environment variables
------------------------------------------

To store development permissions save them to ``.local.dev.env``, in case
of deployment see: :ref:`deployment docs <youtube-settings>`.

Store contents of ``client_secret.json`` as ``YOUTUBE_SECRET_JSON`` variable and
contents of ``upload-oauth2.json`` as ``YOUTUBE_OAUTH2_JSON``. Playlist id
should be stored as ``YT_PLAYLIST_ID``

