.. _permissions:

Permissions
===========

We extensively use django permissions, we use following permissions:

* ``filestorage.add_fileupload`` --- allows user to attach files to content items;
  given to all users by default;
* ``content_item.is_autoverified`` --- when user has this permission his uploads
  are automatically verified, see :ref:`Verification documentation <content-item-verification>`.
* ``content_item.can_verify`` --- when user has this permission they can mark
  other content items as verified/unverified, see: :ref:`Verification documentation <content-item-verification>`.

Also we use Django admin panel for some administrative work, you might want give
some of your high-powered users following permissions:

* ``content_item.change_contentitemverifier`` --- allows admin users to
  edit verification status from admin page.
* ``page_block.*_pageblock``, ``page_block.*_pageblocktranslation`` --- allows
  admin users to edit snippets shared between content items. See :ref:`CMS documentation <cms-functionality>`.
* ``cms.*_translateablepage``, ``cms.*_pagetranslation``, ``cms.*mediafile*``,
  ``cms.*mediafile*``, ``cms.*richtext*`` --- allows users to
  add CMS pages in different languages. See :ref:`CMS documentation <cms-functionality>`
* ``landing_page.*menulink*``, ``landing_page.*_menulinktranslation`` --- allows you to
  add links to menu on the side of main page and in the ``/about/`` menu.



