.. _content-item-verification:

ContentItem Verification
========================

How does content verification work
----------------------------------

Whenever user creates a ``Content Item`` it starts in ``unverified`` state.
Admins might switch ``Content Item`` between verified and unverified states.
``Content Items`` added by users given special permission are automatically
verified (:ref:`See permission documentation <permissions>`).

What changes with unverified content
------------------------------------

Unverified content:

* has red "unverified" label;
* is not searchable;

How to verify content
---------------------

If you have proper permissions you will see big "Update" button near "unverified"
marker.

.. figure:: verification/unverified.png
   :width: 80%

   Update button location

Also if you have ``admin`` access you can edit verified state on the
"Content Item Verifier" list, where you can see verification state for
all content items and then update it.
