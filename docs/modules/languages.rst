
.. _language-switching:

Language switching
==================

.. _django-translations:

Making this application translatable
------------------------------------

Wherever you add a new user visible string mark it for translation,

* `In Python code user ugettext function --- as described in the documentation
  <https://docs.djangoproject.com/en/1.11/topics/i18n/translation/#internationalization-in-python-code>`__
* `In HTML templates user {% trans %} tag --- as described in the documenation
  <https://docs.djangoproject.com/en/1.11/topics/i18n/translation/#internationalization-in-template-code>`__

:ref:`Instructions for generating translation files linked here <generate-translation-files>`.


How to translate this application
---------------------------------

1. Generate new translation files;
2. Notice which ``*po`` files changed;
3. Send them to translators;

   Any translation program that can handle ``gettext`` files, eg: https://poedit.net/.
4. Update any ``*po`` file.

How to add new languages
------------------------

To add new language add it to the ``LANGUAGE`` setting in ``config/settings/common.py``
file.

And then generate new translation file. Start shell in the container ``./dev.sh bash`` and then do following:

* ``cd /app``
* ``./manage.py makemessages --locale <locale code>`` --- this will extract strings from all applications.
* ``cd www/templates``
* ``./manage.py makemessages --locale <locale code>`` --- this will extract strings from templa

And then translate new files.

How does language detection work
--------------------------------

Language is detected using standard HTTP headers.
