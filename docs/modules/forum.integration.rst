.. _forum-integration:

Forum integration
=================

Our deployment practices
------------------------

We use 'unsupported' (by discourse team) method of deploying Discourse ---
we want:

* To treat discourse as "more or less" stateless service
* Be able to deploy discourse automatically
* To automatically configure discourse using 7 factor app.

Feel free to use our deployment scripts, feel free also to use official
docker image for discourse (and of course, feel free to not use discourse at all).
In any case integrating stock discourse service should be relatively straightforward.


Integration points
------------------

OLCMS is integrated with Discourse in two points:

- OLCMS serves as login provider for Discourse;

  Discourse SSO is documented here: https://meta.discourse.org/t/official-single-sign-on-for-discourse-sso/13045

  Django provider (developed in this team) is here: https://bitbucket.org/olcms/django-discourse-sso/src/master/

- OLCMS loads comments for every content item;

  Integration is purely in JavaScript: https://meta.discourse.org/t/embedding-discourse-comments-via-javascript/31963
