.. _www-base-repository:

``www-base`` repository
=======================

To speed up build times we have created ``www-base`` repository that contains
Docker image with *most* of our python dependencies already installed, so
when running tests or building image on CI most of lengthy install process
is done.

This is purely a performance enhancement for the CI.
