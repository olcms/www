How does indexing work
======================

We use haystack, this is an API that allows one to index Django models using various
indexing engines.

Indexing is controlled by ``SearchIndex`` instances, we use single index named: ``ContentIndex``.

Indexing in dev environment
--------------------------

In dev environment we use Whoosh index, it is completely inside docker container. Whoosh is a
python only library.

To index your data you'll need to call: ``./manage.py rebuild_index``.

Indexing in prod environment
---------------------------

First you'll need to set up elastic search index service. Indexing should be done outside of request
context, so you'll need to call ``./manage.py update_index --age HOURS``, it will update index
for ContentItems updated less than ``HOURS`` ago.

How to add new fields
=====================

How to add field searchable by full text search
-----------------------------------------------

Haystack uses document indexing, so you'll need to add this field to ``contentitem_text.txt``
template. This template will be rendered and indexed.

How to add field searchable as an extra field
---------------------------------------------

If you want to add filtering capabilities, like filter by license, you'll need to add additional
field to ``ContentIndex``.

How to add ranked field
-----------------------

Ranks are a way of saying: "this field is more important". To add a ranked field you'll need
to just add a "rank" attribute to field.

If this field is also a part of "full text search" (or is indexed inside document) you'll need
to add some magic inside search form, like here: http://django-haystack.readthedocs.io/en/v2.4.1/boost.html#field-boost


