Simulation resources
====================

Simulation resources allow you to upload interactive HTML5 simulations.

It works in following way:

* You can upload a zip archive containing a HTML file called "index.html"
* We will store this file (so end-users can download it, run locally and
  remix)
* Then we will upload contents of the archive to a server, from where
  these files will be served, so users can use them interactively.

Pluggability
------------

In the end we want OLCMS to work for different providers than Swift
(which is a long way to go).

To implement upload of simulation to other endpoint (e.g. S3, or your own
solution) you'll need to implement: ``www.simulation.services.api.SimulationService``.
And then update ``SIMULATION_SERVICE`` setting.
