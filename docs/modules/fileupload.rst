.. _swift-upload-explained:

Uploading files how does it work
================================

Uploading a file to a swift server is a little bit more complex than normally.

From javascript you need to do the following:

* Obtain the temporary url to which end-user will be able upload files.
* Upload the file to before mentioned url
* Notify OLCMS about upload.

Rationale
---------

Generally speaking normal django storage backends should work,
so you can use FileFields and so on.

However we'd like end-users to upload files directly to SWIFT,
which in case of big files could put some strain on the server.

To do this we use Swift cluster using two middlewares:

1. ``temp url`` middleware. This middleware allows us to generate
   temporary urls, that allow user to perform a single HTTP
   verb (GET or PUT). After timeout these urls become invalid.
2. ``CORS`` middleware, enables Swift to server proper ``CORS``
   headers.

So upload flow is as follows:

1. End user does ``POST`` on ``/api/fileupload`` endpoint and gets
   a temporary url in return.
2. User uploads a file to swift.
3. User sends file path to OLCMS to associate uploaded file
   with Resource or something else.

For auditory purposes when user obtains a temp url for upload
we store user id and uploaded file name in a table.

Python api
----------

Public python API for file upload is in: ``www.fileupload.api``,
generally method to use is: ``register_and_prepare_upload``.

HTTP API
--------

There is a single endpoint ``/api/fileupload`` to use it
user needs to have ``filestorage.add_fileupload`` permission.


How to handle file updates
--------------------------

Just upload a new file, it will get new path, and update resource field with new file.
