.. _tagging-system:

Tagging system
==============

We have elaborate tagging system for content items.

There are two kinds of ``tags`` in OLCMS:

* ``domains`` --- this is categorization system that contains predefined
  cathegories;
* ``tags`` --- user defined tagging system;

Domains
-------

Domains are our predefined tagging system, that is domains are provided
by the system, and user can't add new domain.

Domains are stored in the database, so feel free to add new domains from
the ``admin`` interface (feel free also to delete unused ones).

When deleting/adding disciplines some care needs to be taken as they are
referenced in :ref:`the css system <css-stylesheets>`.

Each domain belong to a domain type, example of domain type is: ``Dicipline``
or ``Education Level``.

Domain Type
***********

Domain type has following properties:

* ``name`` --- Human readable domain name
* ``slug`` --- unique short name of domain type. Used to reference to domain type
               inside OLCMS code.

Domain
******

Domain has following properties

* ``name`` --- Human readable domain name
* ``slug`` --- unique short name of domain. Used to reference to domain type
               inside OLCMS code.
* ``type`` --- domain type.

Translating domains
*******************

Only domain names need to be translated, since they are well integrated into the
system it was decided that translations should be stored not in the database,
but in django ``*po`` files along with the rest of internal strings (
:ref:`see django translations <django-translations>`).

How to add new domains
**********************

Best way to add new domains (and or delete domains) is to:

* Add new django migration;
* Generate domains there, see ``0002_create_domains.py`` migration for
  a good way to do this.

Adding new disciplines
**********************

Add them just like other domains, however please note that right now we have
``per-discipline`` styling, so whenever you add new discipline you'll need
to update ``projects.scss`` file. See :ref:`css documentation for details <css-stylesheets>`.

Tags
----

End users can add any tags they want to content items.
