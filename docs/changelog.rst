2018-11-12
==========

* Added advanced search;
* Used python 3.6 (longer support);
* Bumped vulnerable requests module;
* Introduced better ordering for domains;
* Fixed bug where two domains had the same slugs;

2018-10-17
==========

* Added language switcher;
* Removed unnecessary block in landing page
* Marking of resoures as default resources

2018-09-19
==========

* Released courses;
* Released quizzes and simulations;

2018-09-05
==========

* Bumped django to newest LTS version;

2018-08-22
==========

* Updated translations;
* Re-introduced landing pages;
* Added workforce and RRI information to landing pages;
* Introduced redirect from `example` to `example/` to CMS view.

2018-07-31
==========

* Updated translations
* Updated documentation
* Updated admin page for CMS features
* Downgraded TinyMCS js editor to fix missing <li> items.
* Added fileupload permission to all users;
* Landing page tweaks.


2018-07-22
==========

* Updated translations.

2018-07-03
==========

* Added content verification mechanism. Unverified content does not appear in
  search;
* Fixed image placement in CMS;

2018-06-22
==========

* Updated main page layout;
* Bumped django to newest LTS;
* Fixed sign-up link in login page;
* Used psycopg2-binary library;

2018-06-18
==========

* Updated admin page for content items
* Added markdown editor in page page block admin
* Added ability to override workforce box text in Content Item admin
  with markdown editor.

2018-05-24
==========

* Removed competition


2018-05-09
==========

* Updated to django LTS
* Updated error in html of document resources
* Updated error in UTF encoding of document resources.

2018-03
=======

* Added competition module


2018-02-24
================

* Added descriptions for Learning Methodologies;
* Added markdown to descriptions of LM and RRI;
* Added submissions module;
* Various UI updates;
* Fixed spurious issue on CI;

2018-01-26
================

* Added SSO for silf;
* Added descriptions for RRI;
* Various UI updates;
* Fixed spurious issue on CI;

2018-01-17
==========

* Added competition link

2017-11-27
==========

* Popovers for domains
* Added Learning methodologies aspects
* Moved RRI and metodologies to another place on page
* Added default resource
* Better select for adding Content Item to courses.
* Some bugfixing for issue on CI
* Fixed error in uploading videos to YT
* Better CMS for about page etc.


2017-11-09
==========

* Added course initial implementation
* Fixed About page switching languages for users.
* Link to evaluation page


2017-10-XX
==========

* More translations
* Slight layout fixes
* Fixed bug with search by none domain

2017-10-15
==========

* A lot of translations

2017-09-20
==========

* Added box where employment data will be shown
* Added landing page. Landing page is hidden yet,
  when we will add all descriptions it will be shown.

2017-09-16
==========

* Added footer with information about project
* Added terms and Conditions and about page
* Started Google analytics


2017-09-10
==========

* Added ability to add more than one discipline to Content Item
* Small fix for youtube uploads
* Minor fixes to text on Content Item Sites.
* Updated translations.
* Added links between the disciplines and tags.

2017-09-02
==========

* Added ability to upload YT subtitle
* Used bootstrap 4-beta
* Search by discipline.


2017-08-26
==========

.. note::

    There were some releases in between.

* Added RRI domain
* Added logs to developer enviornment
* Handling longer resource names
* In developer enviornment you couldn't add two
  resources without page reload (fixed that)


2017-08-26 --- 2017-05-01
=========================

* Added YouTube upload;
* Several bugfixes
* Updated favicon
* Inroduced CMS functionality using FeinCMS package
* Introduced forum integration
* Introduced HTTPS
* Introduced multi-language support to content items
* Fixed bug introduced by invalid file names of uploaded files
* Added translations for OLCMS GUI
* Introduced Full Text search engine
* Introduced document resources;
* Introduced cover images and icons to Content Item;
* Introduced automatic resizing of cover images and content items;
* Nicer graphic layout --- supplied by graphic designer;

2017-04-19
==========

* Allowed content-item deletion
* Other minor fixes
* Added cover image

Beta 1
======

1. FIX SILF-1317 better error handling on ajax calls.
2. SILF-1322 Added toolbar
3. SILF-1323 Tags and authors will be stored even if user didn't press enter after the last tag.
4. SILF-1316 Fixed content item display if there are empty values.
5. SILF-1327 Disallowed negative counts in content item iteger fields
